⎔ pragma once

⎔ include_directives ⟨ellipsis-unique.dirs⟩ __prefix__(bind UNIQUE_NAME DEFER)
⎔ include_directives ⟨ellipsis-loc.h⟩ __prefix__(bind LOC_NAME DEFER_END)
⎔ include_directives ⟨ellipsis-loc.h⟩ __prefix__(bind LOC_NAME DEFER_LOC)
⎔ include_directives ⟨ellipsis-blockstate.h⟩
⎔ include_directives ⟨ellipsis-trigger.h⟩ __prefix__(bind NAME DEFER_TRIGGER)
⎔ include_directives ⟨ellipsis-forcebrace.h⟩

⎔ ifndef DOXYGEN_SPECIAL

⎔ xdefine DEFER_MODIFIABLE DEFER_LOC_NEW
⎔ xdefine DEFER_ONE DEFER_LOC_NEW

/* Each {} level has a flag that is 0 if no defer has yet been seen in
   the current {} and that is 1, once such a defer has been seen. Each
   flag has function test-and-set (at each defer) and clear (at the
   terminating })*/
⎔ define DEFER_CLEAR __BLOCKSTATE_SET0(DEFER_FLAG_, DEFER_END_LEVEL)

⎔ define DEFER_TAS() DEFER_TAS_(__BLOCKSTATE_TST(DEFER_FLAG_, DEFER_END_LEVEL))
⎔ define DEFER_TAS_(V) __MULTI__(DEFER_TAS, V)()V
⎔ define DEFER_TAS_0() __BLOCKSTATE_SET1(DEFER_FLAG_, DEFER_END_LEVEL)
⎔ define DEFER_TAS_1()


/* At the very end of a {} block, after having handled all defer of
   the block we have to test if have to unwind further or not. For all
   higher levels we have to test if we are returning, only on level 0
   we now that we are at the end and we should just fall off. */
⎔ define DEFER_RET DEFER_RET_(DEFER_LEVEL)
⎔ define DEFER_RET_(L) DEFER_RET__(DEFER_RET_ ⨝ L, /*^*/⸤if (defer_return_flag) goto⸥ DEFER(0, 1)⸤;⸥)
⎔ define DEFER_RET__(...) DEFER_RET___(__VA_ARGS__)
⎔ define DEFER_RET___(X, Y, ...) Y
⎔ define DEFER_RET_1 ,

⎔ define DEFER_RETURN(LABEL) __MULTI__(DEFER_RETURN, DEFER_MODE)(LABEL)
⎔ define DEFER_RETURN_0(LABEL)
⎔ define DEFER_RETURN_1(LABEL) ⸤goto⸥  LABEL
⎔ define DEFER_RETURN_2(LABEL) ⸤return⸥ DEFER_MODIFIABLE

⎔ define DEFER_OPENBR() DEFER_OPENBR_(__MULTI__(DEFER_OPENBR, __BLOCKSTATE_TST(DEFER_NUMBER))())__BLOCKSTATE_INC(DEFER_NUMBER)
⎔ define DEFER_OPENBR_(...) DEFER_OPENBR__(__VA_ARGS__,,)
⎔ define DEFER_OPENBR__(X, Y, ...) Y
⎔ define DEFER_OPENBR_0() , ⸤{⸥ /*^ ⸤start inner defer anchor at level⸥ __INSTANT__(DEFER_LEVEL) ^*/

/* Now instrument the closing brace. Basically do nothing, if there
   has not been a defer. Otherwise, launch the chain. */
⎔ bind DEFER_GO __MULTI__(DEFER_GO, DEFER_TAS())()DEFER_FINALIZE(DEFER_LEVEL)
⎔ define DEFER_GO_0() DEFER_CLEAR

<|
⎔ define DEFER_GO_1()
  /*^*/defer_skip⸤;⸥
  /*^*/⸤}⸥ /*^ ⸤end inner defer anchor, level⸥ __INSTANT__(DEFER_LEVEL) ^*/
  /*^*//*>*/⸤[[__maybe_unused__]]⸥ DEFER_END()⸤:;⸥
  DEFER_SWITCH_END()DEFER_LOOP_END()DEFER_CLEAR DEFER_DOWN __BLOCKSTATE_SET0(DEFER_NUMBER)
|>

/* Prepend our code to whatever might already be plugged into } */
⎔ gather DEFER_GO }
⎔ gather } DEFER_GO

⎔ define DEFER_FINALIZE_RETURN() DEFER_FINALIZE_RETURN_(__MULTI__(DEFER_FINALIZE_RETURN, DEFER_MODE)())
⎔ define DEFER_FINALIZE_RETURN_(...) DEFER_FINALIZE_RETURN__(__VA_ARGS__)
⎔ define DEFER_FINALIZE_RETURN__(...) __VA_ARGS__
⎔ define DEFER_FINALIZE_RETURN_0()
⎔ define DEFER_FINALIZE_RETURN_1()
⎔ define DEFER_FINALIZE_RETURN_2() /*^*/⸤return⸥ DEFER_MODIFIABLE⸤;⸥

/* clear the mode counter only on the top most level */
⎔ define DEFER_FINALIZE(L) DEFER_FINALIZE_(__MULTI__(DEFER_FINALIZE, L)(),,)
⎔ define DEFER_FINALIZE_(...) DEFER_FINALIZE__(__VA_ARGS__)
⎔ define DEFER_FINALIZE__(X, Y, ...) Y
⎔ define DEFER_FINALIZE_0() , DEFER_FINALIZE_RETURN()__SET0__(DEFER_MODE)

⎔ define DEFER_DO() __MULTI__(DEFER_DO, DEFER_TAS())
⎔ define DEFER_DO_0 DEFER_UP __SET1__(DEFER_IS_FIRST)DEFER_END_NEW
⎔ define DEFER_DO_1 DEFER()

⎔ define DEFER_DONE() DEFER_DONE_(__INSTANT__(DEFER_IS_FIRST))
⎔ define DEFER_DONE_(L) __MULTI__(DEFER_DONE, L)()
⎔ define DEFER_DONE_1() __SET0__(DEFER_IS_FIRST)DEFER_END()
⎔ define DEFER_DONE_0() DEFER(1)

/* If there is no defer return context, yet, the top most {} level
   with a defer is forced to be a defer return context with a void
   return type. */
⎔ define DEFER_DEF() __MULTI__(DEFER_DEF, DEFER_MODE)()DEFER_LOOP_START()DEFER_SWITCH_START()
⎔ define DEFER_DEF_0() DEFER_DEF_FORCE(DEFER_LEVEL)
⎔ define DEFER_DEF_1()
⎔ define DEFER_DEF_2()

⎔ define DEFER_DEF_FORCE(L) DEFER_DEF_FORCE_(DEFER_DEF_FORCE_ ⨝ L)


<|
⎔ define DEFER_DEF_FORCE_0
 ,
 /*>*/⸤[[__maybe_unused__]] register unsigned⸥ DEFER_ONE ⸤= 1U;⸥
 /*^*/⸤[[__maybe_unused__]] register bool defer_return_flag = false;⸥__SET1__(DEFER_MODE) /*! ⸤return context forced⸥ !*//*^*/
|>

⎔ define DEFER_DEF_FORCE_(...) DEFER_DEF_FORCE__(__VA_ARGS__)
⎔ define DEFER_DEF_FORCE__(X, ...) __VA_ARGS__

⎔ define defer_return(M) __MULTI__(defer_return, M)()

⎔ define defer_return_0() ⸤return⸥

<|
⎔ define defer_return_1()
/*^*/⸤do {⸥
/*>*/   /*! ⸤return mode 1⸥ !*/
/*^*/   ⸤defer_return_flag = true;⸥
/*^*/   defer_skip⸤;⸥
/*^*/⸤} while(false)⸥
|>

⎔ define defer_return_2() DEFER_RETURN_TO()

⎔ define DEFER_TOP DEFER_TOP_(DEFER_DO(), DEFER_NEW)
⎔ define DEFER_TOP_(...) DEFER_TOP__(__VA_ARGS__)

<|
⎔ define DEFER_TOP__(TARGET, LOCATION) ⸤if (false) {⸥
/*^*/LOCATION:
 /*^*/  ⸤goto⸥ DEFER_END_NEW⸤;⸥
/*^*/⸤} else⸥ DEFER_OPENBR()
/*^*/⸤(void)0⸥ /*^ ⸤DEFER_TYPE needs a semicolon⸥ ^*/__SET0__(DEFER_IS_FIRST)
|>

/*
  There is a per level state variable that knows if a compound
  statement is inside a loop, and if so if it is the outermost (values
  1 and 2) or an inner compound statement (value 3). That difference
  is necessary to distinguish different terminating strategies for the
  compound statement. If it is the outermost, execution may just fall
  through, or perform a break or a continue for the loop. If it is an
  inner one, it must continue unwinding to outer levels if a `break`
  or `continue` has been requested.

  To know if a `break` or `continue` has been requested there are two
  local flags that hold the information. */
⎔ define DEFER_CONSTRUCTING_LOOP 0
⎔ define DEFER_LOOP_MARK_0 0
⎔ define DEFER_LOOP_TST() __BLOCKSTATE_TST(DEFER_LOOP_MARK_, DEFER_LOC_LEVEL)
⎔ define DEFER_LOOP_INH() __BLOCKSTATE_TST(DEFER_LOOP_MARK_, __EXPAND_DEC__(DEFER_LOC_LEVEL-1))
⎔ define DEFER_LOOP_SET0() __BLOCKSTATE_SET0(DEFER_LOOP_MARK_, DEFER_LOC_LEVEL)
⎔ define DEFER_LOOP_SET1() __BLOCKSTATE_SET1(DEFER_LOOP_MARK_, DEFER_LOC_LEVEL)
⎔ define DEFER_LOOP_SET2() __BLOCKSTATE_SET2(DEFER_LOOP_MARK_, DEFER_LOC_LEVEL)
⎔ define DEFER_LOOP_SET3() __BLOCKSTATE_SET3(DEFER_LOOP_MARK_, DEFER_LOC_LEVEL)

⎔ define DEFER_CONSTRUCT_LOOP          __SET1__(DEFER_CONSTRUCTING_LOOP)
⎔ define DEFER_CONSTRUCT_LOOP_CANCEL() __SET0__(DEFER_CONSTRUCTING_LOOP)

⎔ define DEFER_CONSTRUCT_LOOP_SHOW() __MULTI__(DEFER_CONSTRUCT_LOOP_SHOW, DEFER_CONSTRUCTING_LOOP)()DEFER_CONSTRUCT_LOOP_CANCEL()
⎔ define DEFER_CONSTRUCT_LOOP_SHOW_1() DEFER_SWITCH_SET0()DEFER_LOOP_SET1()
⎔ define DEFER_CONSTRUCT_LOOP_SHOW_0() DEFER_LOOP_UPPER_(DEFER_LOOP_INH())

⎔ define DEFER_LOOP_UPPER_(M) __MULTI__(DEFER_LOOP_UPPER, M)()
⎔ define DEFER_LOOP_UPPER_0() DEFER_LOOP_SET0()
⎔ define DEFER_LOOP_UPPER_1() DEFER_LOOP_SET1()
⎔ define DEFER_LOOP_UPPER_2() DEFER_LOOP_SET3()
⎔ define DEFER_LOOP_UPPER_3() DEFER_LOOP_SET3()

⎔ define DEFER_LOOP_START() DEFER_LOOP_START_(DEFER_LOOP_TST(), DEFER_SWITCH_TST())
⎔ define DEFER_LOOP_START_(M, N) __MULTI__(DEFER_LOOP_START, M, N)()

⎔ define DEFER_LOOP_START_0_0()
⎔ define DEFER_LOOP_START_0_1()
⎔ define DEFER_LOOP_START_0_2()
⎔ define DEFER_LOOP_START_0_3()

<|
⎔ define DEFER_LOOP_START_1_1()
 DEFER_LOOP_SET2()
 ⸤register bool defer_continue_flag = false;⸥/*^*/
|>

<|
⎔ define DEFER_LOOP_START_1_0()
 DEFER_LOOP_START_1_1()
⸤register bool defer_break_flag = false;⸥/*^*/__BLOCKSTATE_SET1(DEFER_BREAK_STATE)
|>

⎔ define DEFER_LOOP_START_1_2() DEFER_LOOP_START_1_0()
⎔ define DEFER_LOOP_START_1_3() DEFER_LOOP_START_1_0()

⎔ define DEFER_LOOP_START_2_0()
⎔ define DEFER_LOOP_START_2_1()
⎔ define DEFER_LOOP_START_2_2()
⎔ define DEFER_LOOP_START_2_3()

⎔ define DEFER_LOOP_START_3_0()
⎔ define DEFER_LOOP_START_3_1()
⎔ define DEFER_LOOP_START_3_2()
⎔ define DEFER_LOOP_START_3_3()

⎔ define DEFER_LOOP_END() __MULTI__(DEFER_LOOP_END, DEFER_LOOP_TST(), DEFER_MODE)()

⎔ define DEFER_LOOP_END_0_0()
⎔ define DEFER_LOOP_END_0_1()
⎔ define DEFER_LOOP_END_0_2()

⎔ define DEFER_LOOP_END_1_0() DEFER_LOOP_SET0()
⎔ define DEFER_LOOP_END_1_1() DEFER_LOOP_SET0()
⎔ define DEFER_LOOP_END_1_2() DEFER_LOOP_SET0()
⎔ define DEFER_LOOP_END_2_0() DEFER_LOOP_SET0()
⎔ define DEFER_LOOP_END_2_1() DEFER_LOOP_SET0()
⎔ define DEFER_LOOP_END_2_2() DEFER_LOOP_SET0()

⎔ define DEFER_LOOP_END_3_0() DEFER_LOOP_SET0()
⎔ define DEFER_LOOP_END_3_1() DEFER_LOOP_SET0()
⎔ define DEFER_LOOP_END_3_2() DEFER_LOOP_SET0()

⎔ define DEFER_SWITCH_BREAK() __MULTI__(DEFER_SWITCH_BREAK, DEFER_SWITCH_TST(), DEFER_MODE, __BLOCKSTATE_TST(DEFER_BREAK_STATE))()

⎔ define DEFER_SWITCH_BREAK_0_0_0()
⎔ define DEFER_SWITCH_BREAK_0_1_0()
⎔ define DEFER_SWITCH_BREAK_0_2_0()

⎔ define DEFER_SWITCH_BREAK_1_0_0()
⎔ define DEFER_SWITCH_BREAK_1_1_0()
⎔ define DEFER_SWITCH_BREAK_1_2_0()

⎔ define DEFER_SWITCH_BREAK_2_0_0()
⎔ define DEFER_SWITCH_BREAK_2_1_0()
⎔ define DEFER_SWITCH_BREAK_2_2_0()

⎔ define DEFER_SWITCH_BREAK_3_0_0()
⎔ define DEFER_SWITCH_BREAK_3_1_0() /*^*/⸤if (defer_break_flag) goto⸥ DEFER(0, 1)⸤;⸥__SET1__(DEFER_SWITCH_BREAK_SET)
⎔ define DEFER_SWITCH_BREAK_3_2_0() DEFER_SWITCH_BREAK_3_1_0()

⎔ define DEFER_SWITCH_BREAK_0_0_1()
⎔ define DEFER_SWITCH_BREAK_0_1_1()
⎔ define DEFER_SWITCH_BREAK_0_2_1()

⎔ define DEFER_SWITCH_BREAK_1_0_1()
⎔ define DEFER_SWITCH_BREAK_1_1_1()
⎔ define DEFER_SWITCH_BREAK_1_2_1()

⎔ define DEFER_SWITCH_BREAK_2_0_1()
⎔ define DEFER_SWITCH_BREAK_2_1_1() /*^*/⸤if (defer_break_flag) break;⸥__SET1__(DEFER_SWITCH_BREAK_SET)
⎔ define DEFER_SWITCH_BREAK_2_2_1()  DEFER_SWITCH_BREAK_2_1_1()

⎔ define DEFER_SWITCH_BREAK_3_0_1()
⎔ define DEFER_SWITCH_BREAK_3_1_1()
⎔ define DEFER_SWITCH_BREAK_3_2_1()

⎔ define DEFER_LOOP_BREAK() __MULTI__(⸤DEFER_LOOP_BREAK⸥, __INSTANT__(DEFER_SWITCH_BREAK_SET), DEFER_LOOP_TST(), DEFER_MODE)()__SET0__(DEFER_SWITCH_BREAK_SET)

⎔ define DEFER_LOOP_BREAK_0_0_0()
⎔ define DEFER_LOOP_BREAK_0_0_1()
⎔ define DEFER_LOOP_BREAK_0_0_2()

⎔ define DEFER_LOOP_BREAK_0_1_0()
⎔ define DEFER_LOOP_BREAK_0_1_1()
⎔ define DEFER_LOOP_BREAK_0_1_2()

⎔ define DEFER_LOOP_BREAK_0_2_0()
⎔ define DEFER_LOOP_BREAK_0_2_1() /*^*/⸤if (defer_break_flag) break;⸥
⎔ define DEFER_LOOP_BREAK_0_2_2() DEFER_LOOP_BREAK_0_2_1()

⎔ define DEFER_LOOP_BREAK_0_3_0()
⎔ define DEFER_LOOP_BREAK_0_3_1() /*^*/⸤if (defer_break_flag) goto⸥ DEFER(0, 1)⸤;⸥
⎔ define DEFER_LOOP_BREAK_0_3_2() DEFER_LOOP_BREAK_0_3_1()

⎔ define DEFER_LOOP_BREAK_1_0_0()
⎔ define DEFER_LOOP_BREAK_1_0_1()
⎔ define DEFER_LOOP_BREAK_1_0_2()

⎔ define DEFER_LOOP_BREAK_1_1_0()
⎔ define DEFER_LOOP_BREAK_1_1_1()
⎔ define DEFER_LOOP_BREAK_1_1_2()

⎔ define DEFER_LOOP_BREAK_1_2_0()
⎔ define DEFER_LOOP_BREAK_1_2_1()
⎔ define DEFER_LOOP_BREAK_1_2_2()

⎔ define DEFER_LOOP_BREAK_1_3_0()
⎔ define DEFER_LOOP_BREAK_1_3_1()
⎔ define DEFER_LOOP_BREAK_1_3_2()

⎔ define DEFER_LOOP_CONTINUE() __MULTI__(⸤DEFER_LOOP_CONTINUE⸥, DEFER_LOOP_TST(), DEFER_MODE)()

⎔ define DEFER_LOOP_CONTINUE_0_0()
⎔ define DEFER_LOOP_CONTINUE_0_1()
⎔ define DEFER_LOOP_CONTINUE_0_2()

⎔ define DEFER_LOOP_CONTINUE_1_0()
⎔ define DEFER_LOOP_CONTINUE_1_1()
⎔ define DEFER_LOOP_CONTINUE_1_2()

⎔ define DEFER_LOOP_CONTINUE_2_0()
⎔ define DEFER_LOOP_CONTINUE_2_1() /*^*/⸤if (defer_continue_flag) continue;⸥
⎔ define DEFER_LOOP_CONTINUE_2_2() DEFER_LOOP_CONTINUE_2_1()

⎔ define DEFER_LOOP_CONTINUE_3_0()
⎔ define DEFER_LOOP_CONTINUE_3_1() /*^*/⸤if (defer_continue_flag) goto⸥ DEFER(0, 1)⸤;⸥
⎔ define DEFER_LOOP_CONTINUE_3_2() DEFER_LOOP_CONTINUE_3_1()

⎔ define DEFER_LOOP_LEVEL 0
⎔ define DEFER_LOOP_LEVEL_UP DEFER_LOOP_LEVEL_UP_(DEFER_LOOP_LEVEL)
⎔ define DEFER_LOOP_LEVEL_UP_(L) DEFER_LOOP_LEVEL_UP__(DEFER_LOOP_LEVEL_UP_ ⨝ L,,)
⎔ define DEFER_LOOP_LEVEL_UP__(…) DEFER_LOOP_LEVEL_UP___(__VA_ARGS__)
⎔ define DEFER_LOOP_LEVEL_UP___(X, Y, …) Y

⎔ define DEFER_CONSTRUCTING_SWITCH 0
⎔ define DEFER_SWITCH_MARK_0 0
⎔ define DEFER_SWITCH_TST() __BLOCKSTATE_TST(DEFER_SWITCH_MARK_, DEFER_LOC_LEVEL)
⎔ define DEFER_SWITCH_INH() __BLOCKSTATE_TST(DEFER_SWITCH_MARK_, __EXPAND_DEC__(DEFER_LOC_LEVEL-1))
⎔ define DEFER_SWITCH_SET0() __BLOCKSTATE_SET0(DEFER_SWITCH_MARK_, DEFER_LOC_LEVEL)
⎔ define DEFER_SWITCH_SET1() __BLOCKSTATE_SET1(DEFER_SWITCH_MARK_, DEFER_LOC_LEVEL)
⎔ define DEFER_SWITCH_SET2() __BLOCKSTATE_SET2(DEFER_SWITCH_MARK_, DEFER_LOC_LEVEL)
⎔ define DEFER_SWITCH_SET3() __BLOCKSTATE_SET3(DEFER_SWITCH_MARK_, DEFER_LOC_LEVEL)

⎔ define DEFER_CONSTRUCT_SWITCH          __SET1__(DEFER_CONSTRUCTING_SWITCH)
⎔ define DEFER_CONSTRUCT_SWITCH_CANCEL() __SET0__(DEFER_CONSTRUCTING_SWITCH)

⎔ define DEFER_CONSTRUCT_SWITCH_SHOW() __MULTI__(DEFER_CONSTRUCT_SWITCH_SHOW, DEFER_CONSTRUCTING_SWITCH)()DEFER_CONSTRUCT_SWITCH_CANCEL()
⎔ define DEFER_CONSTRUCT_SWITCH_SHOW_1() DEFER_SWITCH_SET1()
⎔ define DEFER_CONSTRUCT_SWITCH_SHOW_0() DEFER_SWITCH_UPPER(DEFER_SWITCH_INH())

⎔ define DEFER_SWITCH_UPPER(M) __MULTI__(DEFER_SWITCH_UPPER, M)()
⎔ define DEFER_SWITCH_UPPER_0() DEFER_SWITCH_SET0()
⎔ define DEFER_SWITCH_UPPER_1() DEFER_SWITCH_SET1()
⎔ define DEFER_SWITCH_UPPER_2() DEFER_SWITCH_SET3()
⎔ define DEFER_SWITCH_UPPER_3() DEFER_SWITCH_SET3()

⎔ define DEFER_SWITCH_START() __MULTI__(DEFER_SWITCH_START, DEFER_SWITCH_TST())()
⎔ define DEFER_SWITCH_START_0()
⎔ define DEFER_SWITCH_START_1() DEFER_SWITCH_SET2()⸤register bool defer_break_flag = false;⸥/*^*/__BLOCKSTATE_SET1(DEFER_BREAK_STATE)
⎔ define DEFER_SWITCH_START_2()
⎔ define DEFER_SWITCH_START_3()

⎔ define DEFER_SWITCH_END() __MULTI__(DEFER_SWITCH_END, DEFER_SWITCH_TST(), DEFER_MODE)()

⎔ define DEFER_SWITCH_END_0_0()
⎔ define DEFER_SWITCH_END_0_1()
⎔ define DEFER_SWITCH_END_0_2()

⎔ define DEFER_SWITCH_END_1_0() DEFER_SWITCH_SET0()
⎔ define DEFER_SWITCH_END_1_1() DEFER_SWITCH_SET0()
⎔ define DEFER_SWITCH_END_1_2() DEFER_SWITCH_SET0()
⎔ define DEFER_SWITCH_END_2_0() DEFER_SWITCH_SET0()
⎔ define DEFER_SWITCH_END_2_1() DEFER_SWITCH_SET0()
⎔ define DEFER_SWITCH_END_2_2() DEFER_SWITCH_SET0()

⎔ define DEFER_SWITCH_END_3_0() DEFER_SWITCH_SET0()
⎔ define DEFER_SWITCH_END_3_1() DEFER_SWITCH_SET0()
⎔ define DEFER_SWITCH_END_3_2() DEFER_SWITCH_SET0()

⎔ ifndef do
⎔ define do do
⎔ endif
⎔ ifndef while
⎔ define while while
⎔ endif
⎔ ifndef switch
⎔ define switch switch
⎔ endif
⎔ ifndef for
⎔ define for for
⎔ endif
⎔ ifndef if
⎔ define if if
⎔ endif
⎔ ifndef else
⎔ define else else
⎔ endif

⎔ define _DEFER_REPLAY_FOR(...)    /**/ DEFER_CONSTRUCT_SWITCH_CANCEL()DEFER_CONSTRUCT_LOOP ELLIPSIS_FORCEBRACE(__VA_ARGS__)
⎔ define _DEFER_REPLAY_WHILE(...)  /**/ DEFER_CONSTRUCT_SWITCH_CANCEL()DEFER_CONSTRUCT_LOOP ELLIPSIS_FORCEBRACE_WHILE(__VA_ARGS__)
⎔ define _DEFER_REPLAY_SWITCH(...) /**/ DEFER_CONSTRUCT_LOOP_CANCEL()DEFER_CONSTRUCT_SWITCH ELLIPSIS_FORCEBRACE(__VA_ARGS__)

⎔ bind _DEFER_FOR    _DEFER_REPLAY_FOR
⎔ bind _DEFER_WHILE  _DEFER_REPLAY_WHILE
⎔ bind _DEFER_SWITCH _DEFER_REPLAY_SWITCH
⎔ bind _DEFER_DO     /**/ DEFER_CONSTRUCT_SWITCH_CANCEL()DEFER_CONSTRUCT_LOOP ELLIPSIS_FORCEBRACE_DO

⎔ gather for    _DEFER_FOR
⎔ gather while  _DEFER_WHILE
⎔ gather switch _DEFER_SWITCH
⎔ gather do     _DEFER_DO


⎔ define DEFER_TMP DEFER_CONSTRUCT_SWITCH_CANCEL()DEFER_CONSTRUCT_LOOP_CANCEL()
⎔ gather ; DEFER_TMP
⎔ undef DEFER_TMP

⎔ define DEFER_TMP DEFER_RETURN_DONE()
⎔ gather DEFER_TMP ;
⎔ gather ; DEFER_TMP
⎔ undef DEFER_TMP

⎔ define DEFER_TMP DEFER_CONSTRUCT_SWITCH_SHOW() DEFER_CONSTRUCT_LOOP_SHOW()
⎔ gather { DEFER_TMP
⎔ undef DEFER_TMP

⎔ define DEFER_TMP DEFER_CONSTRUCT_SWITCH_CANCEL()DEFER_CONSTRUCT_LOOP_CANCEL()
⎔ gather DEFER_TMP if
⎔ gather if DEFER_TMP
⎔ undef DEFER_TMP

⎔ define DEFER_TMP DEFER_CONSTRUCT_SWITCH_CANCEL()DEFER_CONSTRUCT_LOOP_CANCEL()
⎔ gather DEFER_TMP else
⎔ gather else DEFER_TMP
⎔ undef DEFER_TMP

<|
#define DEFER_RETURN_TO() ELLIPSIS_MARK_LINE⸤do {⸥
/*^*/ELLIPSIS_MARK_LINE/*! ⸤return mode 2⸥ !*/
/*^*/ELLIPSIS_MARK_LINE⸤defer_return_flag = true;⸥
/*^*/ELLIPSIS_MARK_LINE DEFER_MODIFIABLE  =
__BLOCKSTATE_SET1(DEFER_RETURN_STATE)ELLIPSIS_MARK_LINE
|>

#define DEFER_RETURN_DONE() DEFER_RETURN_DONE_(__BLOCKSTATE_TST(DEFER_RETURN_STATE))
#define DEFER_RETURN_DONE_(S) __MULTI__(DEFER_RETURN_DONE, S)()

<|
#define DEFER_RETURN_DONE_1() ⸤;⸥
/*^*/ELLIPSIS_MARK_LINE   defer_skip⸤;⸥
/*^*/ELLIPSIS_MARK_LINE⸤} while(false)⸥__BLOCKSTATE_SET0(DEFER_RETURN_STATE)
|>

#define DEFER_RETURN_DONE_0()

#define DEFER_TRIGGER_START(TARGET, LOCATION) DEFER_TRIGGER_START_(TARGET, LOCATION)

<|
#define DEFER_TRIGGER_START_(TARGET, LOCATION)
 ⸤[[__maybe_unused__, __deprecated__("dummy variable for better diagnostics")]]⸥
 /*^*/⸤unsigned⸥ (*DEFER_LOC_NEW)⸤[⸥DEFER_ONE⸤]⸥ ⸤= {};⸥
 /*^*/⸤if (false) {⸥
 /*^*/ELLIPSIS_MARK_LINE/*>*/LOCATION⸤: {⸥
 /*^*/ELLIPSIS_MARK_LINE/*>*/⸤[[__maybe_unused__, __deprecated__("invalid termination of a deferred block")]]⸥
 /*^*/ELLIPSIS_MARK_LINE/*>*/⸤register bool const defer_return_flag = false, defer_break_flag = false, defer_continue_flag = false;⸥
 /*^*/ELLIPSIS_MARK_LINE
|>

<|
⎔ define DEFER_TRIGGER_CLOSE()
    /*^*/ELLIPSIS_MARK_LINE/*>*/⸤}⸥
      DEFER_SWITCH_BREAK()DEFER_LOOP_BREAK()DEFER_LOOP_CONTINUE()DEFER_RET⸤⸥__BLOCKSTATE_SET0(DEFER_BREAK_STATE)
    /*^*/ELLIPSIS_MARK_LINE⸤goto⸥ DEFER_DONE()⸤;⸥
    /*^*/⸤} else⸥ DEFER_OPENBR()
    /*^*/⸤(void)0⸥ /*^ ⸤defer needs braces and a semicolon⸥ ^*/
|>

⎔ define defer_break_down() /*! downgraded !*/ ⸤break⸥

<|
⎔ define defer_break_up()
/*^*/⸤do {⸥
/*^*/  ⸤defer_break_flag = true;⸥
/*^*/  defer_skip⸤;⸥
/*^*/⸤} while(false)⸥
|>

⎔ define defer_break_(ML, MS) __MULTI__(defer_break, ML, MS)()
⎔ define defer_break_0_0() defer_break_down()
⎔ define defer_break_0_1() defer_break_down()
⎔ define defer_break_1_0() defer_break_down()
⎔ define defer_break_1_1() defer_break_down()
⎔ define defer_break_2_1() defer_break_down()
⎔ define defer_break_3_1() defer_break_down()

⎔ define defer_break_0_2() defer_break_up()
⎔ define defer_break_0_3() defer_break_up()
⎔ define defer_break_1_2() defer_break_up()
⎔ define defer_break_1_3() defer_break_up()
⎔ define defer_break_2_0() defer_break_up()
⎔ define defer_break_2_2() defer_break_up()
⎔ define defer_break_2_3() defer_break_up()
⎔ define defer_break_3_0() defer_break_up()
⎔ define defer_break_3_2() defer_break_up()
⎔ define defer_break_3_3() defer_break_up()

⎔ define defer_continue(X) __MULTI__(defer_continue, X)()
⎔ define defer_continue_0() /*! downgraded !*/ ⸤continue⸥
⎔ define defer_continue_1() /*! downgraded !*/ ⸤continue⸥

<|
⎔ define defer_continue_2()
/*^*/⸤do {⸥
/*^*/  ⸤defer_continue_flag = true;⸥
/*^*/  defer_skip⸤;⸥
/*^*/⸤} while(false)⸥
|>

⎔ define defer_continue_3() defer_continue_2()


⎔ endif

⎔ define DEFER_MODE 0

<|
⎔ define DEFER_TYPE(...)
  __SET1__(DEFER_MODE)
  __VA_OPT__(
             __SET2__(DEFER_MODE)
             /*>*/⸤typeof(__VA_ARGS__)⸥ DEFER_MODIFIABLE⸤;⸥
             /*^*/⸤if (__func__[0] ≡ 'm' ∧ __func__[1] ≡ 'a' ∧ __func__[2] ≡ 'i' ∧  __func__[3] ≡ 'n' ∧  ¬__func__[4]) {⸥
             /*^*/ DEFER_MODIFIABLE ⸤= (typeof(__VA_ARGS__)){};⸥
             /*^*/ ⸤}⸥
             )
  /*^*/⸤[[__maybe_unused__]] register unsigned⸥ DEFER_ONE ⸤= 1U;⸥
  /*^*/⸤[[__maybe_unused__]] register bool defer_return_flag = false;⸥
  /*^*/DEFER_DEF()DEFER_TOP
|>

⎔ define defer_return_value ((typeof(DEFER_MODIFIABLE))DEFER_MODIFIABLE)

⎔ define defer_skip goto DEFER()

⎔ define return defer_return(DEFER_MODE)

⎔ define break defer_break_(DEFER_LOOP_TST(), DEFER_SWITCH_TST())
⎔ define continue defer_continue(DEFER_LOOP_TST())

⎔ define defer /*>*//*! ⸤defer⸥ !*//*^*//*>*/DEFER_DEF()DEFER_TRIGGER(DEFER_DO(), DEFER_NEW)
