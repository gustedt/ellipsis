#include_directives<ellipsis-trigger.h> __prefix__(bind NAME LAMBDA)

#define CAPTURE(...) __VA_OPT__(CAPTURE_(CAPTURE_NAME(__INCREMENT__(__CAPTURE_COUNT__)), __VA_ARGS__))
#define RECAP(...) __VA_OPT__(RECAP_(CAPTURE_NAME(__CAPTURE_COUNT__), __VA_ARGS__))

#ifdef DOXYGEN_SPECIAL
#define LAMBDA(RET, ...)
#else
#define CAPTURE_(V, ...)                                                \
  /*^*//*! capture !*/                                                  \
  __BIND__((C, ...) /*^*/ [[__maybe_unused__]] auto const __MULTI__(V, C) = C __BIND_OPT__(;) __BIND_TAIL__())(__VA_ARGS__)

#define RECAP_(V, ...)                                                  \
  /*^*//*! recap !*/                                                    \
  __BIND__((C, ...) /*^*/ [[__maybe_unused__]] auto const C = __MULTI__(V, C) __BIND_OPT__(;) __BIND_TAIL__())(__VA_ARGS__)

#define CAPTURE_NAME(X) _CAPTURE_ ## X

#define LAMBDA_START(RET, ...) (⸤{⸥ /*^*/ [[gnu::always_inline]] inline RET LAMBDA_NAME(__INCREMENT__(__LAMBDA_COUNT__)) (__VA_ARGS__)
#define LAMBDA_CLOSE() /*^*/ LAMBDA_NAME(__LAMBDA_COUNT__); /*^*/ ⸤}⸥)
#define LAMBDA_NAME(X)  _LAMBDA_  ## X

#endif
