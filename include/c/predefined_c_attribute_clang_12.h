#ifndef __has_c_attribute‿deprecated
#xdefine __has_c_attribute‿deprecated __EVALUATE__(202311L)
#endif
#ifndef __has_c_attribute‿__deprecated__
#xdefine __has_c_attribute‿__deprecated__ __EVALUATE__(202311L)
#endif
#ifndef __has_c_attribute‿fallthrough
#xdefine __has_c_attribute‿fallthrough __EVALUATE__(202311L)
#endif
#ifndef __has_c_attribute‿__fallthrough__
#xdefine __has_c_attribute‿__fallthrough__ __EVALUATE__(202311L)
#endif
#ifndef __has_c_attribute‿maybe_unused
#xdefine __has_c_attribute‿maybe_unused __EVALUATE__(202311L)
#endif
#ifndef __has_c_attribute‿__maybe_unused__
#xdefine __has_c_attribute‿__maybe_unused__ __EVALUATE__(202311L)
#endif
#ifndef __has_c_attribute‿nodiscard
#xdefine __has_c_attribute‿nodiscard __EVALUATE__(202311L)
#endif
#ifndef __has_c_attribute‿__nodiscard__
#xdefine __has_c_attribute‿__nodiscard__ __EVALUATE__(202311L)
#endif
#xdefine __HAS_C_ATTRIBUTE(gnu,alias)
#xdefine __HAS_C_ATTRIBUTE(gnu,aligned)
#xdefine __HAS_C_ATTRIBUTE(gnu,alloc_align)
#xdefine __HAS_C_ATTRIBUTE(gnu,alloc_size)
#xdefine __HAS_C_ATTRIBUTE(gnu,always_inline)
#xdefine __HAS_C_ATTRIBUTE(gnu,artificial)
#xdefine __HAS_C_ATTRIBUTE(gnu,assume_aligned)
#xdefine __HAS_C_ATTRIBUTE(gnu,cleanup)
#xdefine __HAS_C_ATTRIBUTE(gnu,cold)
#xdefine __HAS_C_ATTRIBUTE(gnu,common)
#xdefine __HAS_C_ATTRIBUTE(gnu,const)
#xdefine __HAS_C_ATTRIBUTE(gnu,constructor)
#xdefine __HAS_C_ATTRIBUTE(gnu,deprecated)
#xdefine __HAS_C_ATTRIBUTE(gnu,destructor)
#xdefine __HAS_C_ATTRIBUTE(gnu,flatten)
#xdefine __HAS_C_ATTRIBUTE(gnu,format)
#xdefine __HAS_C_ATTRIBUTE(gnu,format_arg)
#xdefine __HAS_C_ATTRIBUTE(gnu,gnu_inline)
#xdefine __HAS_C_ATTRIBUTE(gnu,hot)
#xdefine __HAS_C_ATTRIBUTE(gnu,ifunc)
#xdefine __HAS_C_ATTRIBUTE(gnu,leaf)
#xdefine __HAS_C_ATTRIBUTE(gnu,malloc)
#xdefine __HAS_C_ATTRIBUTE(gnu,mode)
#xdefine __HAS_C_ATTRIBUTE(gnu,no_address_safety_analysis)
#xdefine __HAS_C_ATTRIBUTE(gnu,no_instrument_function)
#xdefine __HAS_C_ATTRIBUTE(gnu,no_sanitize_address)
#xdefine __HAS_C_ATTRIBUTE(gnu,no_sanitize_thread)
#xdefine __HAS_C_ATTRIBUTE(gnu,no_split_stack)
#xdefine __HAS_C_ATTRIBUTE(gnu,nocommon)
#xdefine __HAS_C_ATTRIBUTE(gnu,nonnull)
#xdefine __HAS_C_ATTRIBUTE(gnu,noreturn)
#xdefine __HAS_C_ATTRIBUTE(gnu,nothrow)
#xdefine __HAS_C_ATTRIBUTE(gnu,packed)
#xdefine __HAS_C_ATTRIBUTE(gnu,patchable_function_entry)
#xdefine __HAS_C_ATTRIBUTE(gnu,pure)
#xdefine __HAS_C_ATTRIBUTE(gnu,returns_nonnull)
#xdefine __HAS_C_ATTRIBUTE(gnu,returns_twice)
#xdefine __HAS_C_ATTRIBUTE(gnu,section)
#xdefine __HAS_C_ATTRIBUTE(gnu,sentinel)
#xdefine __HAS_C_ATTRIBUTE(gnu,target)
#xdefine __HAS_C_ATTRIBUTE(gnu,tls_model)
#xdefine __HAS_C_ATTRIBUTE(gnu,unused)
#xdefine __HAS_C_ATTRIBUTE(gnu,used)
#xdefine __HAS_C_ATTRIBUTE(gnu,vector_size)
#xdefine __HAS_C_ATTRIBUTE(gnu,visibility)
#xdefine __HAS_C_ATTRIBUTE(gnu,warn_unused_result)
#xdefine __HAS_C_ATTRIBUTE(gnu,weak)
#xdefine __HAS_C_ATTRIBUTE(gnu,weakref)
