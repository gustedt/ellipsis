⎔ once ELLIPSIS_MOVE
⎔ include_directives ⟨ellipsis-loc.h⟩

⎔⎔ @brief Free the contents of `TARGET` and move the contents of `SOURCE` to
⎔⎔ `TARGET`.
⎔⎔
⎔⎔ @hideinitializer
⎔ define ELLIPSIS_MOVE(TARGET, SOURCE, FREE)                            \
/*>*/if (*TARGET)                                                  /*^*/\
/*>*//*>*/FREE((void*)*TARGET);                                    /*^*/\
/*>*/if (SOURCE) {                                                 /*^*/\
/*>*//*>*/*TARGET = *SOURCE;                                       /*^*/\
/*>*//*>*/*SOURCE = nullptr;                                       /*^*/\
/*>*/} else {                                                      /*^*/\
/*>*//*>*/*TARGET = nullptr;                                       /*^*/\
/*>*/}                                                             /*^*/\

