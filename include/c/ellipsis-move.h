#unit ¤¤∷move
#include_directives <ellipsis-move-macro.h>

/*^
 ** @brief Move a `¤¤` pointed to by the second parameter to the one pointed to by the first.
 **
 ** If `target` is not null before, the old pointed-to object is
 ** deleted.
 **
 ** @memberof ¤¤
 ^*/
inline void ¤(¤¤* __LOC_NEW[restrict static 1], ¤¤**restrict __LOC_NEW) {
  ELLIPSIS_MOVE(__LOC(1, 1), __LOC(0, 1), ¤¤∷delete)
}
