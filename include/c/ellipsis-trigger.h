#ifndef DOXYGEN_SPECIAL

#include_directives<ellipsis-blockstate.h>

/* Fix macro names depending on the NAME argument. */
#xbind _TRIGGER_NAME_(SUFFIX) ⸤__MULTI__⸥(, NAME, SUFFIX)
#xbind _TRIGGER_NAME( SUFFIX) ⸤__MULTI__⸥(NAME, SUFFIX)
#xbind _STATE_         _TRIGGER_NAME_(⸤STATE_⸥)

/* This will be appended to the closing brace. */
#xbind   _TRIGGER_CLOSE _TRIGGER_NAME_(⸤CLOSE_⸥)()

/* A cascade of evaluations to trigger the user macro, or do nothing
   depending on the state on this brace level. */
#xdefine _TRIGGER_NAME_(⸤CLOSE_⸥)() _TRIGGER_NAME_(⸤CLOSE⸥)(⸤__BLOCKSTATE_TST⸥(_STATE_))

#xdefine _TRIGGER_NAME_(⸤CLOSE⸥)(STATE) ⸤__MULTI__⸥(_TRIGGER_NAME_(⸤CLOSE⸥), STATE)()

/* If the state was 1, invoke the user's close macro. */
#xdefine _TRIGGER_NAME_(⸤CLOSE_1⸥)() _TRIGGER_NAME(⸤CLOSE⸥)()⸤__BLOCKSTATE_DEC⸥(_STATE_)

/* If the state was 0, do nothing. */
#xdefine _TRIGGER_NAME_(⸤CLOSE_0⸥)()

/* Ensure to clear the state at the end of each block */
#xbind _TRIGGER_TMP ⸤__BLOCKSTATE_CLR⸥(_STATE_)
#gather _TRIGGER_TMP } _TRIGGER_CLOSE
#gather } _TRIGGER_TMP

#xdefine _TRIGGER_NAME_(⸤CHECK_⸥)() _TRIGGER_NAME_(⸤CHECK⸥)(⸤__BLOCKSTATE_TST⸥(_STATE_))
#xdefine _TRIGGER_NAME_(⸤CHECK⸥)(STATE) ⸤__MULTI__⸥(_TRIGGER_NAME_(⸤CHECK⸥), STATE)()

/* If the state was 1 issue a warning */
#xdefine _TRIGGER_NAME_(⸤CHECK_1⸥)() ⸤__WARNING__⸥(__EXPAND_STRINGIFY__(NAME repeated before properly opening braces))

/* If the state was 0, do nothing. */
#xdefine _TRIGGER_NAME_(⸤CHECK_0⸥)()

/* Set the state to 1 and invoke the user's start macro. */
#xdefine NAME _TRIGGER_NAME_(⸤CHECK_⸥)⸤()__BLOCKSTATE_SET1⸥(_STATE_)_TRIGGER_NAME(⸤START⸥)

#endif
