#ifdef __has_c_attribute
# if __has_c_attribute(_Noreturn)
#  define __has_c_attribute‿_Noreturn 202311L
#  define __has_c_attribute‿___Noreturn__ 202311L
# endif
# if __has_c_attribute(deprecated)
#  define __has_c_attribute‿deprecated 202311L
#  define __has_c_attribute‿__deprecated__ 202311L
# endif
# if __has_c_attribute(fallthrough)
#  define __has_c_attribute‿fallthrough 202311L
#  define __has_c_attribute‿__fallthrough__ 202311L
# endif
# if __has_c_attribute(maybe_unused)
#  define __has_c_attribute‿maybe_unused 202311L
#  define __has_c_attribute‿__maybe_unused__ 202311L
# endif
# if __has_c_attribute(nodiscard)
#  define __has_c_attribute‿nodiscard 202311L
#  define __has_c_attribute‿__nodiscard__ 202311L
# endif
# if __has_c_attribute(noreturn)
#  define __has_c_attribute‿noreturn 202311L
#  define __has_c_attribute‿__noreturn__ 202311L
# endif
# if __has_c_attribute(reproducible)
#  define __has_c_attribute‿reproducible 202311L
#  define __has_c_attribute‿__reproducible__ 202311L
# endif
# if __has_c_attribute(unsequenced)
#  define __has_c_attribute‿unsequenced 202311L
#  define __has_c_attribute‿__unsequenced__ 202311L
# endif
# if __has_c_attribute(gnu::acces)
#  define __has_c_attribute‿gnu‿acces	1
#  define __has_c_attribute‿gnu‿__acces__	1
#  define __has_c_attribute‿__gnu__‿acces	1
#  define __has_c_attribute‿__gnu__‿__acces__	1
# endif
# if __has_c_attribute(gnu::alias)
#  define __has_c_attribute‿gnu‿alias	1
#  define __has_c_attribute‿gnu‿__alias__	1
#  define __has_c_attribute‿__gnu__‿alias	1
#  define __has_c_attribute‿__gnu__‿__alias__	1
# endif
# if __has_c_attribute(gnu::aligned)
#  define __has_c_attribute‿gnu‿aligned	1
#  define __has_c_attribute‿gnu‿__aligned__	1
#  define __has_c_attribute‿__gnu__‿aligned	1
#  define __has_c_attribute‿__gnu__‿__aligned__	1
# endif
# if __has_c_attribute(gnu::alloc_align)
#  define __has_c_attribute‿gnu‿alloc_align	1
#  define __has_c_attribute‿gnu‿__alloc_align__	1
#  define __has_c_attribute‿__gnu__‿alloc_align	1
#  define __has_c_attribute‿__gnu__‿__alloc_align__	1
# endif
# if __has_c_attribute(gnu::alloc_size)
#  define __has_c_attribute‿gnu‿alloc_size	1
#  define __has_c_attribute‿gnu‿__alloc_size__	1
#  define __has_c_attribute‿__gnu__‿alloc_size	1
#  define __has_c_attribute‿__gnu__‿__alloc_size__	1
# endif
# if __has_c_attribute(gnu::always_inline)
#  define __has_c_attribute‿gnu‿always_inline	1
#  define __has_c_attribute‿gnu‿__always_inline__	1
#  define __has_c_attribute‿__gnu__‿always_inline	1
#  define __has_c_attribute‿__gnu__‿__always_inline__	1
# endif
# if __has_c_attribute(gnu::artificial)
#  define __has_c_attribute‿gnu‿artificial	1
#  define __has_c_attribute‿gnu‿__artificial__	1
#  define __has_c_attribute‿__gnu__‿artificial	1
#  define __has_c_attribute‿__gnu__‿__artificial__	1
# endif
# if __has_c_attribute(gnu::assume_aligned)
#  define __has_c_attribute‿gnu‿assume_aligned	1
#  define __has_c_attribute‿gnu‿__assume_aligned__	1
#  define __has_c_attribute‿__gnu__‿assume_aligned	1
#  define __has_c_attribute‿__gnu__‿__assume_aligned__	1
# endif
# if __has_c_attribute(gnu::cleanup)
#  define __has_c_attribute‿gnu‿cleanup	1
#  define __has_c_attribute‿gnu‿__cleanup__	1
#  define __has_c_attribute‿__gnu__‿cleanup	1
#  define __has_c_attribute‿__gnu__‿__cleanup__	1
# endif
# if __has_c_attribute(gnu::cold)
#  define __has_c_attribute‿gnu‿cold	1
#  define __has_c_attribute‿gnu‿__cold__	1
#  define __has_c_attribute‿__gnu__‿cold	1
#  define __has_c_attribute‿__gnu__‿__cold__	1
# endif
# if __has_c_attribute(gnu::common)
#  define __has_c_attribute‿gnu‿common	1
#  define __has_c_attribute‿gnu‿__common__	1
#  define __has_c_attribute‿__gnu__‿common	1
#  define __has_c_attribute‿__gnu__‿__common__	1
# endif
# if __has_c_attribute(gnu::const)
#  define __has_c_attribute‿gnu‿const	1
#  define __has_c_attribute‿gnu‿__const__	1
#  define __has_c_attribute‿__gnu__‿const	1
#  define __has_c_attribute‿__gnu__‿__const__	1
# endif
# if __has_c_attribute(gnu::constructor)
#  define __has_c_attribute‿gnu‿constructor	1
#  define __has_c_attribute‿gnu‿__constructor__	1
#  define __has_c_attribute‿__gnu__‿constructor	1
#  define __has_c_attribute‿__gnu__‿__constructor__	1
# endif
# if __has_c_attribute(gnu::copy)
#  define __has_c_attribute‿gnu‿copy	1
#  define __has_c_attribute‿gnu‿__copy__	1
#  define __has_c_attribute‿__gnu__‿copy	1
#  define __has_c_attribute‿__gnu__‿__copy__	1
# endif
# if __has_c_attribute(gnu::counted_by)
#  define __has_c_attribute‿gnu‿counted_by	1
#  define __has_c_attribute‿gnu‿__counted_by__	1
#  define __has_c_attribute‿__gnu__‿counted_by	1
#  define __has_c_attribute‿__gnu__‿__counted_by__	1
# endif
# if __has_c_attribute(gnu::deprecated)
#  define __has_c_attribute‿gnu‿deprecated	1
#  define __has_c_attribute‿gnu‿__deprecated__	1
#  define __has_c_attribute‿__gnu__‿deprecated	1
#  define __has_c_attribute‿__gnu__‿__deprecated__	1
# endif
# if __has_c_attribute(gnu::destructor)
#  define __has_c_attribute‿gnu‿destructor	1
#  define __has_c_attribute‿gnu‿__destructor__	1
#  define __has_c_attribute‿__gnu__‿destructor	1
#  define __has_c_attribute‿__gnu__‿__destructor__	1
# endif
# if __has_c_attribute(gnu::error)
#  define __has_c_attribute‿gnu‿error	1
#  define __has_c_attribute‿gnu‿__error__	1
#  define __has_c_attribute‿__gnu__‿error	1
#  define __has_c_attribute‿__gnu__‿__error__	1
# endif
# if __has_c_attribute(gnu::expected_throw)
#  define __has_c_attribute‿gnu‿expected_throw	1
#  define __has_c_attribute‿gnu‿__expected_throw__	1
#  define __has_c_attribute‿__gnu__‿expected_throw	1
#  define __has_c_attribute‿__gnu__‿__expected_throw__	1
# endif
# if __has_c_attribute(gnu::externally_visible)
#  define __has_c_attribute‿gnu‿externally_visible	1
#  define __has_c_attribute‿gnu‿__externally_visible__	1
#  define __has_c_attribute‿__gnu__‿externally_visible	1
#  define __has_c_attribute‿__gnu__‿__externally_visible__	1
# endif
# if __has_c_attribute(gnu::fd_arg)
#  define __has_c_attribute‿gnu‿fd_arg	1
#  define __has_c_attribute‿gnu‿__fd_arg__	1
#  define __has_c_attribute‿__gnu__‿fd_arg	1
#  define __has_c_attribute‿__gnu__‿__fd_arg__	1
# endif
# if __has_c_attribute(gnu::fd_arg_read)
#  define __has_c_attribute‿gnu‿fd_arg_read	1
#  define __has_c_attribute‿gnu‿__fd_arg_read__	1
#  define __has_c_attribute‿__gnu__‿fd_arg_read	1
#  define __has_c_attribute‿__gnu__‿__fd_arg_read__	1
# endif
# if __has_c_attribute(gnu::fd_arg_write)
#  define __has_c_attribute‿gnu‿fd_arg_write	1
#  define __has_c_attribute‿gnu‿__fd_arg_write__	1
#  define __has_c_attribute‿__gnu__‿fd_arg_write	1
#  define __has_c_attribute‿__gnu__‿__fd_arg_write__	1
# endif
# if __has_c_attribute(gnu::flatten)
#  define __has_c_attribute‿gnu‿flatten	1
#  define __has_c_attribute‿gnu‿__flatten__	1
#  define __has_c_attribute‿__gnu__‿flatten	1
#  define __has_c_attribute‿__gnu__‿__flatten__	1
# endif
# if __has_c_attribute(gnu::format)
#  define __has_c_attribute‿gnu‿format	1
#  define __has_c_attribute‿gnu‿__format__	1
#  define __has_c_attribute‿__gnu__‿format	1
#  define __has_c_attribute‿__gnu__‿__format__	1
# endif
# if __has_c_attribute(gnu::format_arg)
#  define __has_c_attribute‿gnu‿format_arg	1
#  define __has_c_attribute‿gnu‿__format_arg__	1
#  define __has_c_attribute‿__gnu__‿format_arg	1
#  define __has_c_attribute‿__gnu__‿__format_arg__	1
# endif
# if __has_c_attribute(gnu::gnu_inline)
#  define __has_c_attribute‿gnu‿gnu_inline	1
#  define __has_c_attribute‿gnu‿__gnu_inline__	1
#  define __has_c_attribute‿__gnu__‿gnu_inline	1
#  define __has_c_attribute‿__gnu__‿__gnu_inline__	1
# endif
# if __has_c_attribute(gnu::hot)
#  define __has_c_attribute‿gnu‿hot	1
#  define __has_c_attribute‿gnu‿__hot__	1
#  define __has_c_attribute‿__gnu__‿hot	1
#  define __has_c_attribute‿__gnu__‿__hot__	1
# endif
# if __has_c_attribute(gnu::ifunc)
#  define __has_c_attribute‿gnu‿ifunc	1
#  define __has_c_attribute‿gnu‿__ifunc__	1
#  define __has_c_attribute‿__gnu__‿ifunc	1
#  define __has_c_attribute‿__gnu__‿__ifunc__	1
# endif
# if __has_c_attribute(gnu::interrupt)
#  define __has_c_attribute‿gnu‿interrupt	1
#  define __has_c_attribute‿gnu‿__interrupt__	1
#  define __has_c_attribute‿__gnu__‿interrupt	1
#  define __has_c_attribute‿__gnu__‿__interrupt__	1
# endif
# if __has_c_attribute(gnu::interrupt_handler)
#  define __has_c_attribute‿gnu‿interrupt_handler	1
#  define __has_c_attribute‿gnu‿__interrupt_handler__	1
#  define __has_c_attribute‿__gnu__‿interrupt_handler	1
#  define __has_c_attribute‿__gnu__‿__interrupt_handler__	1
# endif
# if __has_c_attribute(gnu::leaf)
#  define __has_c_attribute‿gnu‿leaf	1
#  define __has_c_attribute‿gnu‿__leaf__	1
#  define __has_c_attribute‿__gnu__‿leaf	1
#  define __has_c_attribute‿__gnu__‿__leaf__	1
# endif
# if __has_c_attribute(gnu::malloc)
#  define __has_c_attribute‿gnu‿malloc	1
#  define __has_c_attribute‿gnu‿__malloc__	1
#  define __has_c_attribute‿__gnu__‿malloc	1
#  define __has_c_attribute‿__gnu__‿__malloc__	1
# endif
# if __has_c_attribute(gnu::mode)
#  define __has_c_attribute‿gnu‿mode	1
#  define __has_c_attribute‿gnu‿__mode__	1
#  define __has_c_attribute‿__gnu__‿mode	1
#  define __has_c_attribute‿__gnu__‿__mode__	1
# endif
# if __has_c_attribute(gnu::no_address_safety_analysis)
#  define __has_c_attribute‿gnu‿no_address_safety_analysis	1
#  define __has_c_attribute‿gnu‿__no_address_safety_analysis__	1
#  define __has_c_attribute‿__gnu__‿no_address_safety_analysis	1
#  define __has_c_attribute‿__gnu__‿__no_address_safety_analysis__	1
# endif
# if __has_c_attribute(gnu::no_icf)
#  define __has_c_attribute‿gnu‿no_icf	1
#  define __has_c_attribute‿gnu‿__no_icf__	1
#  define __has_c_attribute‿__gnu__‿no_icf	1
#  define __has_c_attribute‿__gnu__‿__no_icf__	1
# endif
# if __has_c_attribute(gnu::no_instrument_function)
#  define __has_c_attribute‿gnu‿no_instrument_function	1
#  define __has_c_attribute‿gnu‿__no_instrument_function__	1
#  define __has_c_attribute‿__gnu__‿no_instrument_function	1
#  define __has_c_attribute‿__gnu__‿__no_instrument_function__	1
# endif
# if __has_c_attribute(gnu::no_profile_instrument_function)
#  define __has_c_attribute‿gnu‿no_profile_instrument_function	1
#  define __has_c_attribute‿gnu‿__no_profile_instrument_function__	1
#  define __has_c_attribute‿__gnu__‿no_profile_instrument_function	1
#  define __has_c_attribute‿__gnu__‿__no_profile_instrument_function__	1
# endif
# if __has_c_attribute(gnu::no_reorder)
#  define __has_c_attribute‿gnu‿no_reorder	1
#  define __has_c_attribute‿gnu‿__no_reorder__	1
#  define __has_c_attribute‿__gnu__‿no_reorder	1
#  define __has_c_attribute‿__gnu__‿__no_reorder__	1
# endif
# if __has_c_attribute(gnu::no_sanitize)
#  define __has_c_attribute‿gnu‿no_sanitize	1
#  define __has_c_attribute‿gnu‿__no_sanitize__	1
#  define __has_c_attribute‿__gnu__‿no_sanitize	1
#  define __has_c_attribute‿__gnu__‿__no_sanitize__	1
# endif
# if __has_c_attribute(gnu::no_sanitize_address)
#  define __has_c_attribute‿gnu‿no_sanitize_address	1
#  define __has_c_attribute‿gnu‿__no_sanitize_address__	1
#  define __has_c_attribute‿__gnu__‿no_sanitize_address	1
#  define __has_c_attribute‿__gnu__‿__no_sanitize_address__	1
# endif
# if __has_c_attribute(gnu::no_sanitize_coverage)
#  define __has_c_attribute‿gnu‿no_sanitize_coverage	1
#  define __has_c_attribute‿gnu‿__no_sanitize_coverage__	1
#  define __has_c_attribute‿__gnu__‿no_sanitize_coverage	1
#  define __has_c_attribute‿__gnu__‿__no_sanitize_coverage__	1
# endif
# if __has_c_attribute(gnu::no_sanitize_thread)
#  define __has_c_attribute‿gnu‿no_sanitize_thread	1
#  define __has_c_attribute‿gnu‿__no_sanitize_thread__	1
#  define __has_c_attribute‿__gnu__‿no_sanitize_thread	1
#  define __has_c_attribute‿__gnu__‿__no_sanitize_thread__	1
# endif
# if __has_c_attribute(gnu::no_sanitize_undefined)
#  define __has_c_attribute‿gnu‿no_sanitize_undefined	1
#  define __has_c_attribute‿gnu‿__no_sanitize_undefined__	1
#  define __has_c_attribute‿__gnu__‿no_sanitize_undefined	1
#  define __has_c_attribute‿__gnu__‿__no_sanitize_undefined__	1
# endif
# if __has_c_attribute(gnu::no_split_stack)
#  define __has_c_attribute‿gnu‿no_split_stack	1
#  define __has_c_attribute‿gnu‿__no_split_stack__	1
#  define __has_c_attribute‿__gnu__‿no_split_stack	1
#  define __has_c_attribute‿__gnu__‿__no_split_stack__	1
# endif
# if __has_c_attribute(gnu::no_stack_limit)
#  define __has_c_attribute‿gnu‿no_stack_limit	1
#  define __has_c_attribute‿gnu‿__no_stack_limit__	1
#  define __has_c_attribute‿__gnu__‿no_stack_limit	1
#  define __has_c_attribute‿__gnu__‿__no_stack_limit__	1
# endif
# if __has_c_attribute(gnu::no_stack_protector)
#  define __has_c_attribute‿gnu‿no_stack_protector	1
#  define __has_c_attribute‿gnu‿__no_stack_protector__	1
#  define __has_c_attribute‿__gnu__‿no_stack_protector	1
#  define __has_c_attribute‿__gnu__‿__no_stack_protector__	1
# endif
# if __has_c_attribute(gnu::noclone)
#  define __has_c_attribute‿gnu‿noclone	1
#  define __has_c_attribute‿gnu‿__noclone__	1
#  define __has_c_attribute‿__gnu__‿noclone	1
#  define __has_c_attribute‿__gnu__‿__noclone__	1
# endif
# if __has_c_attribute(gnu::nocommon)
#  define __has_c_attribute‿gnu‿nocommon	1
#  define __has_c_attribute‿gnu‿__nocommon__	1
#  define __has_c_attribute‿__gnu__‿nocommon	1
#  define __has_c_attribute‿__gnu__‿__nocommon__	1
# endif
# if __has_c_attribute(gnu::noinit)
#  define __has_c_attribute‿gnu‿noinit	1
#  define __has_c_attribute‿gnu‿__noinit__	1
#  define __has_c_attribute‿__gnu__‿noinit	1
#  define __has_c_attribute‿__gnu__‿__noinit__	1
# endif
# if __has_c_attribute(gnu::noipa)
#  define __has_c_attribute‿gnu‿noipa	1
#  define __has_c_attribute‿gnu‿__noipa__	1
#  define __has_c_attribute‿__gnu__‿noipa	1
#  define __has_c_attribute‿__gnu__‿__noipa__	1
# endif
# if __has_c_attribute(gnu::nonnull)
#  define __has_c_attribute‿gnu‿nonnull	1
#  define __has_c_attribute‿gnu‿__nonnull__	1
#  define __has_c_attribute‿__gnu__‿nonnull	1
#  define __has_c_attribute‿__gnu__‿__nonnull__	1
# endif
# if __has_c_attribute(gnu::nonstring)
#  define __has_c_attribute‿gnu‿nonstring	1
#  define __has_c_attribute‿gnu‿__nonstring__	1
#  define __has_c_attribute‿__gnu__‿nonstring	1
#  define __has_c_attribute‿__gnu__‿__nonstring__	1
# endif
# if __has_c_attribute(gnu::noplt)
#  define __has_c_attribute‿gnu‿noplt	1
#  define __has_c_attribute‿gnu‿__noplt__	1
#  define __has_c_attribute‿__gnu__‿noplt	1
#  define __has_c_attribute‿__gnu__‿__noplt__	1
# endif
# if __has_c_attribute(gnu::noreturn)
#  define __has_c_attribute‿gnu‿noreturn	1
#  define __has_c_attribute‿gnu‿__noreturn__	1
#  define __has_c_attribute‿__gnu__‿noreturn	1
#  define __has_c_attribute‿__gnu__‿__noreturn__	1
# endif
# if __has_c_attribute(gnu::nothrow)
#  define __has_c_attribute‿gnu‿nothrow	1
#  define __has_c_attribute‿gnu‿__nothrow__	1
#  define __has_c_attribute‿__gnu__‿nothrow	1
#  define __has_c_attribute‿__gnu__‿__nothrow__	1
# endif
# if __has_c_attribute(gnu::optimize)
#  define __has_c_attribute‿gnu‿optimize	1
#  define __has_c_attribute‿gnu‿__optimize__	1
#  define __has_c_attribute‿__gnu__‿optimize	1
#  define __has_c_attribute‿__gnu__‿__optimize__	1
# endif
# if __has_c_attribute(gnu::packed)
#  define __has_c_attribute‿gnu‿packed	1
#  define __has_c_attribute‿gnu‿__packed__	1
#  define __has_c_attribute‿__gnu__‿packed	1
#  define __has_c_attribute‿__gnu__‿__packed__	1
# endif
# if __has_c_attribute(gnu::patchable_function_entry)
#  define __has_c_attribute‿gnu‿patchable_function_entry	1
#  define __has_c_attribute‿gnu‿__patchable_function_entry__	1
#  define __has_c_attribute‿__gnu__‿patchable_function_entry	1
#  define __has_c_attribute‿__gnu__‿__patchable_function_entry__	1
# endif
# if __has_c_attribute(gnu::persistent)
#  define __has_c_attribute‿gnu‿persistent	1
#  define __has_c_attribute‿gnu‿__persistent__	1
#  define __has_c_attribute‿__gnu__‿persistent	1
#  define __has_c_attribute‿__gnu__‿__persistent__	1
# endif
# if __has_c_attribute(gnu::pure)
#  define __has_c_attribute‿gnu‿pure	1
#  define __has_c_attribute‿gnu‿__pure__	1
#  define __has_c_attribute‿__gnu__‿pure	1
#  define __has_c_attribute‿__gnu__‿__pure__	1
# endif
# if __has_c_attribute(gnu::retain)
#  define __has_c_attribute‿gnu‿retain	1
#  define __has_c_attribute‿gnu‿__retain__	1
#  define __has_c_attribute‿__gnu__‿retain	1
#  define __has_c_attribute‿__gnu__‿__retain__	1
# endif
# if __has_c_attribute(gnu::returns_nonnull)
#  define __has_c_attribute‿gnu‿returns_nonnull	1
#  define __has_c_attribute‿gnu‿__returns_nonnull__	1
#  define __has_c_attribute‿__gnu__‿returns_nonnull	1
#  define __has_c_attribute‿__gnu__‿__returns_nonnull__	1
# endif
# if __has_c_attribute(gnu::returns_twice)
#  define __has_c_attribute‿gnu‿returns_twice	1
#  define __has_c_attribute‿gnu‿__returns_twice__	1
#  define __has_c_attribute‿__gnu__‿returns_twice	1
#  define __has_c_attribute‿__gnu__‿__returns_twice__	1
# endif
# if __has_c_attribute(gnu::section)
#  define __has_c_attribute‿gnu‿section	1
#  define __has_c_attribute‿gnu‿__section__	1
#  define __has_c_attribute‿__gnu__‿section	1
#  define __has_c_attribute‿__gnu__‿__section__	1
# endif
# if __has_c_attribute(gnu::sentinel)
#  define __has_c_attribute‿gnu‿sentinel	1
#  define __has_c_attribute‿gnu‿__sentinel__	1
#  define __has_c_attribute‿__gnu__‿sentinel	1
#  define __has_c_attribute‿__gnu__‿__sentinel__	1
# endif
# if __has_c_attribute(gnu::simd)
#  define __has_c_attribute‿gnu‿simd	1
#  define __has_c_attribute‿gnu‿__simd__	1
#  define __has_c_attribute‿__gnu__‿simd	1
#  define __has_c_attribute‿__gnu__‿__simd__	1
# endif
# if __has_c_attribute(gnu::stack_protect)
#  define __has_c_attribute‿gnu‿stack_protect	1
#  define __has_c_attribute‿gnu‿__stack_protect__	1
#  define __has_c_attribute‿__gnu__‿stack_protect	1
#  define __has_c_attribute‿__gnu__‿__stack_protect__	1
# endif
# if __has_c_attribute(gnu::strict_flex_array)
#  define __has_c_attribute‿gnu‿strict_flex_array	1
#  define __has_c_attribute‿gnu‿__strict_flex_array__	1
#  define __has_c_attribute‿__gnu__‿strict_flex_array	1
#  define __has_c_attribute‿__gnu__‿__strict_flex_array__	1
# endif
# if __has_c_attribute(gnu::symver)
#  define __has_c_attribute‿gnu‿symver	1
#  define __has_c_attribute‿gnu‿__symver__	1
#  define __has_c_attribute‿__gnu__‿symver	1
#  define __has_c_attribute‿__gnu__‿__symver__	1
# endif
# if __has_c_attribute(gnu::tainted_args)
#  define __has_c_attribute‿gnu‿tainted_args	1
#  define __has_c_attribute‿gnu‿__tainted_args__	1
#  define __has_c_attribute‿__gnu__‿tainted_args	1
#  define __has_c_attribute‿__gnu__‿__tainted_args__	1
# endif
# if __has_c_attribute(gnu::target)
#  define __has_c_attribute‿gnu‿target	1
#  define __has_c_attribute‿gnu‿__target__	1
#  define __has_c_attribute‿__gnu__‿target	1
#  define __has_c_attribute‿__gnu__‿__target__	1
# endif
# if __has_c_attribute(gnu::target_clones)
#  define __has_c_attribute‿gnu‿target_clones	1
#  define __has_c_attribute‿gnu‿__target_clones__	1
#  define __has_c_attribute‿__gnu__‿target_clones	1
#  define __has_c_attribute‿__gnu__‿__target_clones__	1
# endif
# if __has_c_attribute(gnu::tls_model)
#  define __has_c_attribute‿gnu‿tls_model	1
#  define __has_c_attribute‿gnu‿__tls_model__	1
#  define __has_c_attribute‿__gnu__‿tls_model	1
#  define __has_c_attribute‿__gnu__‿__tls_model__	1
# endif
# if __has_c_attribute(gnu::unavailable)
#  define __has_c_attribute‿gnu‿unavailable	1
#  define __has_c_attribute‿gnu‿__unavailable__	1
#  define __has_c_attribute‿__gnu__‿unavailable	1
#  define __has_c_attribute‿__gnu__‿__unavailable__	1
# endif
# if __has_c_attribute(gnu::uninitialized)
#  define __has_c_attribute‿gnu‿uninitialized	1
#  define __has_c_attribute‿gnu‿__uninitialized__	1
#  define __has_c_attribute‿__gnu__‿uninitialized	1
#  define __has_c_attribute‿__gnu__‿__uninitialized__	1
# endif
# if __has_c_attribute(gnu::unused)
#  define __has_c_attribute‿gnu‿unused	1
#  define __has_c_attribute‿gnu‿__unused__	1
#  define __has_c_attribute‿__gnu__‿unused	1
#  define __has_c_attribute‿__gnu__‿__unused__	1
# endif
# if __has_c_attribute(gnu::used)
#  define __has_c_attribute‿gnu‿used	1
#  define __has_c_attribute‿gnu‿__used__	1
#  define __has_c_attribute‿__gnu__‿used	1
#  define __has_c_attribute‿__gnu__‿__used__	1
# endif
# if __has_c_attribute(gnu::vector_size)
#  define __has_c_attribute‿gnu‿vector_size	1
#  define __has_c_attribute‿gnu‿__vector_size__	1
#  define __has_c_attribute‿__gnu__‿vector_size	1
#  define __has_c_attribute‿__gnu__‿__vector_size__	1
# endif
# if __has_c_attribute(gnu::visibility)
#  define __has_c_attribute‿gnu‿visibility	1
#  define __has_c_attribute‿gnu‿__visibility__	1
#  define __has_c_attribute‿__gnu__‿visibility	1
#  define __has_c_attribute‿__gnu__‿__visibility__	1
# endif
# if __has_c_attribute(gnu::warn_if_not_aligned)
#  define __has_c_attribute‿gnu‿warn_if_not_aligned	1
#  define __has_c_attribute‿gnu‿__warn_if_not_aligned__	1
#  define __has_c_attribute‿__gnu__‿warn_if_not_aligned	1
#  define __has_c_attribute‿__gnu__‿__warn_if_not_aligned__	1
# endif
# if __has_c_attribute(gnu::warn_unused_result)
#  define __has_c_attribute‿gnu‿warn_unused_result	1
#  define __has_c_attribute‿gnu‿__warn_unused_result__	1
#  define __has_c_attribute‿__gnu__‿warn_unused_result	1
#  define __has_c_attribute‿__gnu__‿__warn_unused_result__	1
# endif
# if __has_c_attribute(gnu::warning)
#  define __has_c_attribute‿gnu‿warning	1
#  define __has_c_attribute‿gnu‿__warning__	1
#  define __has_c_attribute‿__gnu__‿warning	1
#  define __has_c_attribute‿__gnu__‿__warning__	1
# endif
# if __has_c_attribute(gnu::weak)
#  define __has_c_attribute‿gnu‿weak	1
#  define __has_c_attribute‿gnu‿__weak__	1
#  define __has_c_attribute‿__gnu__‿weak	1
#  define __has_c_attribute‿__gnu__‿__weak__	1
# endif
# if __has_c_attribute(gnu::weakref)
#  define __has_c_attribute‿gnu‿weakref	1
#  define __has_c_attribute‿gnu‿__weakref__	1
#  define __has_c_attribute‿__gnu__‿weakref	1
#  define __has_c_attribute‿__gnu__‿__weakref__	1
# endif
# if __has_c_attribute(gnu::zero_call_used_regs)
#  define __has_c_attribute‿gnu‿zero_call_used_regs	1
#  define __has_c_attribute‿gnu‿__zero_call_used_regs__	1
#  define __has_c_attribute‿__gnu__‿zero_call_used_regs	1
#  define __has_c_attribute‿__gnu__‿__zero_call_used_regs__	1
# endif
# if __has_c_attribute(clang::aarch64_sve_pcs)
#  define __has_c_attribute‿clang‿aarch64_sve_pcs	1
#  define __has_c_attribute‿__clang__‿aarch64_sve_pcs	1
#  define __has_c_attribute‿clang‿__aarch64_sve_pcs__	1
#  define __has_c_attribute‿__clang__‿__aarch64_sve_pcs__	1
#  define __has_c_attribute‿_Clang‿aarch64_sve_pcs	1
#  define __has_c_attribute‿_Clang‿__aarch64_sve_pcs__	1
# endif
# if __has_c_attribute(clang::aarch64_vector_pcs)
#  define __has_c_attribute‿clang‿aarch64_vector_pcs	1
#  define __has_c_attribute‿__clang__‿aarch64_vector_pcs	1
#  define __has_c_attribute‿clang‿__aarch64_vector_pcs__	1
#  define __has_c_attribute‿__clang__‿__aarch64_vector_pcs__	1
#  define __has_c_attribute‿_Clang‿aarch64_vector_pcs	1
#  define __has_c_attribute‿_Clang‿__aarch64_vector_pcs__	1
# endif
# if __has_c_attribute(clang::acquire_handle)
#  define __has_c_attribute‿clang‿acquire_handle	1
#  define __has_c_attribute‿__clang__‿acquire_handle	1
#  define __has_c_attribute‿clang‿__acquire_handle__	1
#  define __has_c_attribute‿__clang__‿__acquire_handle__	1
#  define __has_c_attribute‿_Clang‿acquire_handle	1
#  define __has_c_attribute‿_Clang‿__acquire_handle__	1
# endif
# if __has_c_attribute(clang::acquire_shared_capability)
#  define __has_c_attribute‿clang‿acquire_shared_capability	1
#  define __has_c_attribute‿__clang__‿acquire_shared_capability	1
#  define __has_c_attribute‿clang‿__acquire_shared_capability__	1
#  define __has_c_attribute‿__clang__‿__acquire_shared_capability__	1
#  define __has_c_attribute‿_Clang‿acquire_shared_capability	1
#  define __has_c_attribute‿_Clang‿__acquire_shared_capability__	1
# endif
# if __has_c_attribute(clang::address_space)
#  define __has_c_attribute‿clang‿address_space	1
#  define __has_c_attribute‿__clang__‿address_space	1
#  define __has_c_attribute‿clang‿__address_space__	1
#  define __has_c_attribute‿__clang__‿__address_space__	1
#  define __has_c_attribute‿_Clang‿address_space	1
#  define __has_c_attribute‿_Clang‿__address_space__	1
# endif
# if __has_c_attribute(clang::allocating)
#  define __has_c_attribute‿clang‿allocating	1
#  define __has_c_attribute‿__clang__‿allocating	1
#  define __has_c_attribute‿clang‿__allocating__	1
#  define __has_c_attribute‿__clang__‿__allocating__	1
#  define __has_c_attribute‿_Clang‿allocating	1
#  define __has_c_attribute‿_Clang‿__allocating__	1
# endif
# if __has_c_attribute(clang::always_destroy)
#  define __has_c_attribute‿clang‿always_destroy	1
#  define __has_c_attribute‿__clang__‿always_destroy	1
#  define __has_c_attribute‿clang‿__always_destroy__	1
#  define __has_c_attribute‿__clang__‿__always_destroy__	1
#  define __has_c_attribute‿_Clang‿always_destroy	1
#  define __has_c_attribute‿_Clang‿__always_destroy__	1
# endif
# if __has_c_attribute(clang::always_inline)
#  define __has_c_attribute‿clang‿always_inline	1
#  define __has_c_attribute‿__clang__‿always_inline	1
#  define __has_c_attribute‿clang‿__always_inline__	1
#  define __has_c_attribute‿__clang__‿__always_inline__	1
#  define __has_c_attribute‿_Clang‿always_inline	1
#  define __has_c_attribute‿_Clang‿__always_inline__	1
# endif
# if __has_c_attribute(clang::amdgpu_flat_work_group_size)
#  define __has_c_attribute‿clang‿amdgpu_flat_work_group_size	1
#  define __has_c_attribute‿__clang__‿amdgpu_flat_work_group_size	1
#  define __has_c_attribute‿clang‿__amdgpu_flat_work_group_size__	1
#  define __has_c_attribute‿__clang__‿__amdgpu_flat_work_group_size__	1
#  define __has_c_attribute‿_Clang‿amdgpu_flat_work_group_size	1
#  define __has_c_attribute‿_Clang‿__amdgpu_flat_work_group_size__	1
# endif
# if __has_c_attribute(clang::amdgpu_kernel)
#  define __has_c_attribute‿clang‿amdgpu_kernel	1
#  define __has_c_attribute‿__clang__‿amdgpu_kernel	1
#  define __has_c_attribute‿clang‿__amdgpu_kernel__	1
#  define __has_c_attribute‿__clang__‿__amdgpu_kernel__	1
#  define __has_c_attribute‿_Clang‿amdgpu_kernel	1
#  define __has_c_attribute‿_Clang‿__amdgpu_kernel__	1
# endif
# if __has_c_attribute(clang::amdgpu_max_num_work_groups)
#  define __has_c_attribute‿clang‿amdgpu_max_num_work_groups	1
#  define __has_c_attribute‿__clang__‿amdgpu_max_num_work_groups	1
#  define __has_c_attribute‿clang‿__amdgpu_max_num_work_groups__	1
#  define __has_c_attribute‿__clang__‿__amdgpu_max_num_work_groups__	1
#  define __has_c_attribute‿_Clang‿amdgpu_max_num_work_groups	1
#  define __has_c_attribute‿_Clang‿__amdgpu_max_num_work_groups__	1
# endif
# if __has_c_attribute(clang::amdgpu_num_sgpr)
#  define __has_c_attribute‿clang‿amdgpu_num_sgpr	1
#  define __has_c_attribute‿__clang__‿amdgpu_num_sgpr	1
#  define __has_c_attribute‿clang‿__amdgpu_num_sgpr__	1
#  define __has_c_attribute‿__clang__‿__amdgpu_num_sgpr__	1
#  define __has_c_attribute‿_Clang‿amdgpu_num_sgpr	1
#  define __has_c_attribute‿_Clang‿__amdgpu_num_sgpr__	1
# endif
# if __has_c_attribute(clang::amdgpu_num_vgpr)
#  define __has_c_attribute‿clang‿amdgpu_num_vgpr	1
#  define __has_c_attribute‿__clang__‿amdgpu_num_vgpr	1
#  define __has_c_attribute‿clang‿__amdgpu_num_vgpr__	1
#  define __has_c_attribute‿__clang__‿__amdgpu_num_vgpr__	1
#  define __has_c_attribute‿_Clang‿amdgpu_num_vgpr	1
#  define __has_c_attribute‿_Clang‿__amdgpu_num_vgpr__	1
# endif
# if __has_c_attribute(clang::amdgpu_waves_per_eu)
#  define __has_c_attribute‿clang‿amdgpu_waves_per_eu	1
#  define __has_c_attribute‿__clang__‿amdgpu_waves_per_eu	1
#  define __has_c_attribute‿clang‿__amdgpu_waves_per_eu__	1
#  define __has_c_attribute‿__clang__‿__amdgpu_waves_per_eu__	1
#  define __has_c_attribute‿_Clang‿amdgpu_waves_per_eu	1
#  define __has_c_attribute‿_Clang‿__amdgpu_waves_per_eu__	1
# endif
# if __has_c_attribute(clang::annotate)
#  define __has_c_attribute‿clang‿annotate	1
#  define __has_c_attribute‿__clang__‿annotate	1
#  define __has_c_attribute‿clang‿__annotate__	1
#  define __has_c_attribute‿__clang__‿__annotate__	1
#  define __has_c_attribute‿_Clang‿annotate	1
#  define __has_c_attribute‿_Clang‿__annotate__	1
# endif
# if __has_c_attribute(clang::annotate_type)
#  define __has_c_attribute‿clang‿annotate_type	1
#  define __has_c_attribute‿__clang__‿annotate_type	1
#  define __has_c_attribute‿clang‿__annotate_type__	1
#  define __has_c_attribute‿__clang__‿__annotate_type__	1
#  define __has_c_attribute‿_Clang‿annotate_type	1
#  define __has_c_attribute‿_Clang‿__annotate_type__	1
# endif
# if __has_c_attribute(clang::assert_shared_capability)
#  define __has_c_attribute‿clang‿assert_shared_capability	1
#  define __has_c_attribute‿__clang__‿assert_shared_capability	1
#  define __has_c_attribute‿clang‿__assert_shared_capability__	1
#  define __has_c_attribute‿__clang__‿__assert_shared_capability__	1
#  define __has_c_attribute‿_Clang‿assert_shared_capability	1
#  define __has_c_attribute‿_Clang‿__assert_shared_capability__	1
# endif
# if __has_c_attribute(clang::assume)
#  define __has_c_attribute‿clang‿assume	1
#  define __has_c_attribute‿__clang__‿assume	1
#  define __has_c_attribute‿clang‿__assume__	1
#  define __has_c_attribute‿__clang__‿__assume__	1
#  define __has_c_attribute‿_Clang‿assume	1
#  define __has_c_attribute‿_Clang‿__assume__	1
# endif
# if __has_c_attribute(clang::availability)
#  define __has_c_attribute‿clang‿availability	1
#  define __has_c_attribute‿__clang__‿availability	1
#  define __has_c_attribute‿clang‿__availability__	1
#  define __has_c_attribute‿__clang__‿__availability__	1
#  define __has_c_attribute‿_Clang‿availability	1
#  define __has_c_attribute‿_Clang‿__availability__	1
# endif
# if __has_c_attribute(clang::available_only_in_default_eval_method)
#  define __has_c_attribute‿clang‿available_only_in_default_eval_method	1
#  define __has_c_attribute‿__clang__‿available_only_in_default_eval_method	1
#  define __has_c_attribute‿clang‿__available_only_in_default_eval_method__	1
#  define __has_c_attribute‿__clang__‿__available_only_in_default_eval_method__	1
#  define __has_c_attribute‿_Clang‿available_only_in_default_eval_method	1
#  define __has_c_attribute‿_Clang‿__available_only_in_default_eval_method__	1
# endif
# if __has_c_attribute(clang::blocking)
#  define __has_c_attribute‿clang‿blocking	1
#  define __has_c_attribute‿__clang__‿blocking	1
#  define __has_c_attribute‿clang‿__blocking__	1
#  define __has_c_attribute‿__clang__‿__blocking__	1
#  define __has_c_attribute‿_Clang‿blocking	1
#  define __has_c_attribute‿_Clang‿__blocking__	1
# endif
# if __has_c_attribute(clang::blocks)
#  define __has_c_attribute‿clang‿blocks	1
#  define __has_c_attribute‿__clang__‿blocks	1
#  define __has_c_attribute‿clang‿__blocks__	1
#  define __has_c_attribute‿__clang__‿__blocks__	1
#  define __has_c_attribute‿_Clang‿blocks	1
#  define __has_c_attribute‿_Clang‿__blocks__	1
# endif
# if __has_c_attribute(clang::bpf_fastcall)
#  define __has_c_attribute‿clang‿bpf_fastcall	1
#  define __has_c_attribute‿__clang__‿bpf_fastcall	1
#  define __has_c_attribute‿clang‿__bpf_fastcall__	1
#  define __has_c_attribute‿__clang__‿__bpf_fastcall__	1
#  define __has_c_attribute‿_Clang‿bpf_fastcall	1
#  define __has_c_attribute‿_Clang‿__bpf_fastcall__	1
# endif
# if __has_c_attribute(clang::btf_decl_tag)
#  define __has_c_attribute‿clang‿btf_decl_tag	1
#  define __has_c_attribute‿__clang__‿btf_decl_tag	1
#  define __has_c_attribute‿clang‿__btf_decl_tag__	1
#  define __has_c_attribute‿__clang__‿__btf_decl_tag__	1
#  define __has_c_attribute‿_Clang‿btf_decl_tag	1
#  define __has_c_attribute‿_Clang‿__btf_decl_tag__	1
# endif
# if __has_c_attribute(clang::btf_type_tag)
#  define __has_c_attribute‿clang‿btf_type_tag	1
#  define __has_c_attribute‿__clang__‿btf_type_tag	1
#  define __has_c_attribute‿clang‿__btf_type_tag__	1
#  define __has_c_attribute‿__clang__‿__btf_type_tag__	1
#  define __has_c_attribute‿_Clang‿btf_type_tag	1
#  define __has_c_attribute‿_Clang‿__btf_type_tag__	1
# endif
# if __has_c_attribute(clang::builtin_alias)
#  define __has_c_attribute‿clang‿builtin_alias	1
#  define __has_c_attribute‿__clang__‿builtin_alias	1
#  define __has_c_attribute‿clang‿__builtin_alias__	1
#  define __has_c_attribute‿__clang__‿__builtin_alias__	1
#  define __has_c_attribute‿_Clang‿builtin_alias	1
#  define __has_c_attribute‿_Clang‿__builtin_alias__	1
# endif
# if __has_c_attribute(clang::callable_when)
#  define __has_c_attribute‿clang‿callable_when	1
#  define __has_c_attribute‿__clang__‿callable_when	1
#  define __has_c_attribute‿clang‿__callable_when__	1
#  define __has_c_attribute‿__clang__‿__callable_when__	1
#  define __has_c_attribute‿_Clang‿callable_when	1
#  define __has_c_attribute‿_Clang‿__callable_when__	1
# endif
# if __has_c_attribute(clang::callback)
#  define __has_c_attribute‿clang‿callback	1
#  define __has_c_attribute‿__clang__‿callback	1
#  define __has_c_attribute‿clang‿__callback__	1
#  define __has_c_attribute‿__clang__‿__callback__	1
#  define __has_c_attribute‿_Clang‿callback	1
#  define __has_c_attribute‿_Clang‿__callback__	1
# endif
# if __has_c_attribute(clang::called_once)
#  define __has_c_attribute‿clang‿called_once	1
#  define __has_c_attribute‿__clang__‿called_once	1
#  define __has_c_attribute‿clang‿__called_once__	1
#  define __has_c_attribute‿__clang__‿__called_once__	1
#  define __has_c_attribute‿_Clang‿called_once	1
#  define __has_c_attribute‿_Clang‿__called_once__	1
# endif
# if __has_c_attribute(clang::cf_audited_transfer)
#  define __has_c_attribute‿clang‿cf_audited_transfer	1
#  define __has_c_attribute‿__clang__‿cf_audited_transfer	1
#  define __has_c_attribute‿clang‿__cf_audited_transfer__	1
#  define __has_c_attribute‿__clang__‿__cf_audited_transfer__	1
#  define __has_c_attribute‿_Clang‿cf_audited_transfer	1
#  define __has_c_attribute‿_Clang‿__cf_audited_transfer__	1
# endif
# if __has_c_attribute(clang::cf_consumed)
#  define __has_c_attribute‿clang‿cf_consumed	1
#  define __has_c_attribute‿__clang__‿cf_consumed	1
#  define __has_c_attribute‿clang‿__cf_consumed__	1
#  define __has_c_attribute‿__clang__‿__cf_consumed__	1
#  define __has_c_attribute‿_Clang‿cf_consumed	1
#  define __has_c_attribute‿_Clang‿__cf_consumed__	1
# endif
# if __has_c_attribute(clang::cfi_canonical_jump_table)
#  define __has_c_attribute‿clang‿cfi_canonical_jump_table	1
#  define __has_c_attribute‿__clang__‿cfi_canonical_jump_table	1
#  define __has_c_attribute‿clang‿__cfi_canonical_jump_table__	1
#  define __has_c_attribute‿__clang__‿__cfi_canonical_jump_table__	1
#  define __has_c_attribute‿_Clang‿cfi_canonical_jump_table	1
#  define __has_c_attribute‿_Clang‿__cfi_canonical_jump_table__	1
# endif
# if __has_c_attribute(clang::cf_returns_not_retained)
#  define __has_c_attribute‿clang‿cf_returns_not_retained	1
#  define __has_c_attribute‿__clang__‿cf_returns_not_retained	1
#  define __has_c_attribute‿clang‿__cf_returns_not_retained__	1
#  define __has_c_attribute‿__clang__‿__cf_returns_not_retained__	1
#  define __has_c_attribute‿_Clang‿cf_returns_not_retained	1
#  define __has_c_attribute‿_Clang‿__cf_returns_not_retained__	1
# endif
# if __has_c_attribute(clang::cf_returns_retained)
#  define __has_c_attribute‿clang‿cf_returns_retained	1
#  define __has_c_attribute‿__clang__‿cf_returns_retained	1
#  define __has_c_attribute‿clang‿__cf_returns_retained__	1
#  define __has_c_attribute‿__clang__‿__cf_returns_retained__	1
#  define __has_c_attribute‿_Clang‿cf_returns_retained	1
#  define __has_c_attribute‿_Clang‿__cf_returns_retained__	1
# endif
# if __has_c_attribute(clang::cf_unknown_transfer)
#  define __has_c_attribute‿clang‿cf_unknown_transfer	1
#  define __has_c_attribute‿__clang__‿cf_unknown_transfer	1
#  define __has_c_attribute‿clang‿__cf_unknown_transfer__	1
#  define __has_c_attribute‿__clang__‿__cf_unknown_transfer__	1
#  define __has_c_attribute‿_Clang‿cf_unknown_transfer	1
#  define __has_c_attribute‿_Clang‿__cf_unknown_transfer__	1
# endif
# if __has_c_attribute(clang::__clang_arm_builtin_alias)
#  define __has_c_attribute‿clang‿__clang_arm_builtin_alias	1
#  define __has_c_attribute‿__clang__‿__clang_arm_builtin_alias	1
#  define __has_c_attribute‿clang‿____clang_arm_builtin_alias__	1
#  define __has_c_attribute‿__clang__‿____clang_arm_builtin_alias__	1
#  define __has_c_attribute‿_Clang‿__clang_arm_builtin_alias	1
#  define __has_c_attribute‿_Clang‿____clang_arm_builtin_alias__	1
# endif
# if __has_c_attribute(clang::__clang_arm_mve_strict_polymorphism)
#  define __has_c_attribute‿clang‿__clang_arm_mve_strict_polymorphism	1
#  define __has_c_attribute‿__clang__‿__clang_arm_mve_strict_polymorphism	1
#  define __has_c_attribute‿clang‿____clang_arm_mve_strict_polymorphism__	1
#  define __has_c_attribute‿__clang__‿____clang_arm_mve_strict_polymorphism__	1
#  define __has_c_attribute‿_Clang‿__clang_arm_mve_strict_polymorphism	1
#  define __has_c_attribute‿_Clang‿____clang_arm_mve_strict_polymorphism__	1
# endif
# if __has_c_attribute(clang::clspv_libclc_builtin)
#  define __has_c_attribute‿clang‿clspv_libclc_builtin	1
#  define __has_c_attribute‿__clang__‿clspv_libclc_builtin	1
#  define __has_c_attribute‿clang‿__clspv_libclc_builtin__	1
#  define __has_c_attribute‿__clang__‿__clspv_libclc_builtin__	1
#  define __has_c_attribute‿_Clang‿clspv_libclc_builtin	1
#  define __has_c_attribute‿_Clang‿__clspv_libclc_builtin__	1
# endif
# if __has_c_attribute(clang::code_align)
#  define __has_c_attribute‿clang‿code_align	1
#  define __has_c_attribute‿__clang__‿code_align	1
#  define __has_c_attribute‿clang‿__code_align__	1
#  define __has_c_attribute‿__clang__‿__code_align__	1
#  define __has_c_attribute‿_Clang‿code_align	1
#  define __has_c_attribute‿_Clang‿__code_align__	1
# endif
# if __has_c_attribute(clang::consumable)
#  define __has_c_attribute‿clang‿consumable	1
#  define __has_c_attribute‿__clang__‿consumable	1
#  define __has_c_attribute‿clang‿__consumable__	1
#  define __has_c_attribute‿__clang__‿__consumable__	1
#  define __has_c_attribute‿_Clang‿consumable	1
#  define __has_c_attribute‿_Clang‿__consumable__	1
# endif
# if __has_c_attribute(clang::consumable_auto_cast_state)
#  define __has_c_attribute‿clang‿consumable_auto_cast_state	1
#  define __has_c_attribute‿__clang__‿consumable_auto_cast_state	1
#  define __has_c_attribute‿clang‿__consumable_auto_cast_state__	1
#  define __has_c_attribute‿__clang__‿__consumable_auto_cast_state__	1
#  define __has_c_attribute‿_Clang‿consumable_auto_cast_state	1
#  define __has_c_attribute‿_Clang‿__consumable_auto_cast_state__	1
# endif
# if __has_c_attribute(clang::consumable_set_state_on_read)
#  define __has_c_attribute‿clang‿consumable_set_state_on_read	1
#  define __has_c_attribute‿__clang__‿consumable_set_state_on_read	1
#  define __has_c_attribute‿clang‿__consumable_set_state_on_read__	1
#  define __has_c_attribute‿__clang__‿__consumable_set_state_on_read__	1
#  define __has_c_attribute‿_Clang‿consumable_set_state_on_read	1
#  define __has_c_attribute‿_Clang‿__consumable_set_state_on_read__	1
# endif
# if __has_c_attribute(clang::convergent)
#  define __has_c_attribute‿clang‿convergent	1
#  define __has_c_attribute‿__clang__‿convergent	1
#  define __has_c_attribute‿clang‿__convergent__	1
#  define __has_c_attribute‿__clang__‿__convergent__	1
#  define __has_c_attribute‿_Clang‿convergent	1
#  define __has_c_attribute‿_Clang‿__convergent__	1
# endif
# if __has_c_attribute(clang::coro_await_elidable)
#  define __has_c_attribute‿clang‿coro_await_elidable	1
#  define __has_c_attribute‿__clang__‿coro_await_elidable	1
#  define __has_c_attribute‿clang‿__coro_await_elidable__	1
#  define __has_c_attribute‿__clang__‿__coro_await_elidable__	1
#  define __has_c_attribute‿_Clang‿coro_await_elidable	1
#  define __has_c_attribute‿_Clang‿__coro_await_elidable__	1
# endif
# if __has_c_attribute(clang::coro_await_elidable_argument)
#  define __has_c_attribute‿clang‿coro_await_elidable_argument	1
#  define __has_c_attribute‿__clang__‿coro_await_elidable_argument	1
#  define __has_c_attribute‿clang‿__coro_await_elidable_argument__	1
#  define __has_c_attribute‿__clang__‿__coro_await_elidable_argument__	1
#  define __has_c_attribute‿_Clang‿coro_await_elidable_argument	1
#  define __has_c_attribute‿_Clang‿__coro_await_elidable_argument__	1
# endif
# if __has_c_attribute(clang::coro_disable_lifetimebound)
#  define __has_c_attribute‿clang‿coro_disable_lifetimebound	1
#  define __has_c_attribute‿__clang__‿coro_disable_lifetimebound	1
#  define __has_c_attribute‿clang‿__coro_disable_lifetimebound__	1
#  define __has_c_attribute‿__clang__‿__coro_disable_lifetimebound__	1
#  define __has_c_attribute‿_Clang‿coro_disable_lifetimebound	1
#  define __has_c_attribute‿_Clang‿__coro_disable_lifetimebound__	1
# endif
# if __has_c_attribute(clang::coro_lifetimebound)
#  define __has_c_attribute‿clang‿coro_lifetimebound	1
#  define __has_c_attribute‿__clang__‿coro_lifetimebound	1
#  define __has_c_attribute‿clang‿__coro_lifetimebound__	1
#  define __has_c_attribute‿__clang__‿__coro_lifetimebound__	1
#  define __has_c_attribute‿_Clang‿coro_lifetimebound	1
#  define __has_c_attribute‿_Clang‿__coro_lifetimebound__	1
# endif
# if __has_c_attribute(clang::coro_only_destroy_when_complete)
#  define __has_c_attribute‿clang‿coro_only_destroy_when_complete	1
#  define __has_c_attribute‿__clang__‿coro_only_destroy_when_complete	1
#  define __has_c_attribute‿clang‿__coro_only_destroy_when_complete__	1
#  define __has_c_attribute‿__clang__‿__coro_only_destroy_when_complete__	1
#  define __has_c_attribute‿_Clang‿coro_only_destroy_when_complete	1
#  define __has_c_attribute‿_Clang‿__coro_only_destroy_when_complete__	1
# endif
# if __has_c_attribute(clang::coro_return_type)
#  define __has_c_attribute‿clang‿coro_return_type	1
#  define __has_c_attribute‿__clang__‿coro_return_type	1
#  define __has_c_attribute‿clang‿__coro_return_type__	1
#  define __has_c_attribute‿__clang__‿__coro_return_type__	1
#  define __has_c_attribute‿_Clang‿coro_return_type	1
#  define __has_c_attribute‿_Clang‿__coro_return_type__	1
# endif
# if __has_c_attribute(clang::coro_wrapper)
#  define __has_c_attribute‿clang‿coro_wrapper	1
#  define __has_c_attribute‿__clang__‿coro_wrapper	1
#  define __has_c_attribute‿clang‿__coro_wrapper__	1
#  define __has_c_attribute‿__clang__‿__coro_wrapper__	1
#  define __has_c_attribute‿_Clang‿coro_wrapper	1
#  define __has_c_attribute‿_Clang‿__coro_wrapper__	1
# endif
# if __has_c_attribute(clang::counted_by)
#  define __has_c_attribute‿clang‿counted_by	1
#  define __has_c_attribute‿__clang__‿counted_by	1
#  define __has_c_attribute‿clang‿__counted_by__	1
#  define __has_c_attribute‿__clang__‿__counted_by__	1
#  define __has_c_attribute‿_Clang‿counted_by	1
#  define __has_c_attribute‿_Clang‿__counted_by__	1
# endif
# if __has_c_attribute(clang::counted_by_or_null)
#  define __has_c_attribute‿clang‿counted_by_or_null	1
#  define __has_c_attribute‿__clang__‿counted_by_or_null	1
#  define __has_c_attribute‿clang‿__counted_by_or_null__	1
#  define __has_c_attribute‿__clang__‿__counted_by_or_null__	1
#  define __has_c_attribute‿_Clang‿counted_by_or_null	1
#  define __has_c_attribute‿_Clang‿__counted_by_or_null__	1
# endif
# if __has_c_attribute(clang::cpu_dispatch)
#  define __has_c_attribute‿clang‿cpu_dispatch	1
#  define __has_c_attribute‿__clang__‿cpu_dispatch	1
#  define __has_c_attribute‿clang‿__cpu_dispatch__	1
#  define __has_c_attribute‿__clang__‿__cpu_dispatch__	1
#  define __has_c_attribute‿_Clang‿cpu_dispatch	1
#  define __has_c_attribute‿_Clang‿__cpu_dispatch__	1
# endif
# if __has_c_attribute(clang::cpu_specific)
#  define __has_c_attribute‿clang‿cpu_specific	1
#  define __has_c_attribute‿__clang__‿cpu_specific	1
#  define __has_c_attribute‿clang‿__cpu_specific__	1
#  define __has_c_attribute‿__clang__‿__cpu_specific__	1
#  define __has_c_attribute‿_Clang‿cpu_specific	1
#  define __has_c_attribute‿_Clang‿__cpu_specific__	1
# endif
# if __has_c_attribute(clang::diagnose_as_builtin)
#  define __has_c_attribute‿clang‿diagnose_as_builtin	1
#  define __has_c_attribute‿__clang__‿diagnose_as_builtin	1
#  define __has_c_attribute‿clang‿__diagnose_as_builtin__	1
#  define __has_c_attribute‿__clang__‿__diagnose_as_builtin__	1
#  define __has_c_attribute‿_Clang‿diagnose_as_builtin	1
#  define __has_c_attribute‿_Clang‿__diagnose_as_builtin__	1
# endif
# if __has_c_attribute(clang::disable_sanitizer_instrumentation)
#  define __has_c_attribute‿clang‿disable_sanitizer_instrumentation	1
#  define __has_c_attribute‿__clang__‿disable_sanitizer_instrumentation	1
#  define __has_c_attribute‿clang‿__disable_sanitizer_instrumentation__	1
#  define __has_c_attribute‿__clang__‿__disable_sanitizer_instrumentation__	1
#  define __has_c_attribute‿_Clang‿disable_sanitizer_instrumentation	1
#  define __has_c_attribute‿_Clang‿__disable_sanitizer_instrumentation__	1
# endif
# if __has_c_attribute(clang::disable_tail_calls)
#  define __has_c_attribute‿clang‿disable_tail_calls	1
#  define __has_c_attribute‿__clang__‿disable_tail_calls	1
#  define __has_c_attribute‿clang‿__disable_tail_calls__	1
#  define __has_c_attribute‿__clang__‿__disable_tail_calls__	1
#  define __has_c_attribute‿_Clang‿disable_tail_calls	1
#  define __has_c_attribute‿_Clang‿__disable_tail_calls__	1
# endif
# if __has_c_attribute(clang::enforce_read_only_placement)
#  define __has_c_attribute‿clang‿enforce_read_only_placement	1
#  define __has_c_attribute‿__clang__‿enforce_read_only_placement	1
#  define __has_c_attribute‿clang‿__enforce_read_only_placement__	1
#  define __has_c_attribute‿__clang__‿__enforce_read_only_placement__	1
#  define __has_c_attribute‿_Clang‿enforce_read_only_placement	1
#  define __has_c_attribute‿_Clang‿__enforce_read_only_placement__	1
# endif
# if __has_c_attribute(clang::enforce_tcb)
#  define __has_c_attribute‿clang‿enforce_tcb	1
#  define __has_c_attribute‿__clang__‿enforce_tcb	1
#  define __has_c_attribute‿clang‿__enforce_tcb__	1
#  define __has_c_attribute‿__clang__‿__enforce_tcb__	1
#  define __has_c_attribute‿_Clang‿enforce_tcb	1
#  define __has_c_attribute‿_Clang‿__enforce_tcb__	1
# endif
# if __has_c_attribute(clang::enforce_tcb_leaf)
#  define __has_c_attribute‿clang‿enforce_tcb_leaf	1
#  define __has_c_attribute‿__clang__‿enforce_tcb_leaf	1
#  define __has_c_attribute‿clang‿__enforce_tcb_leaf__	1
#  define __has_c_attribute‿__clang__‿__enforce_tcb_leaf__	1
#  define __has_c_attribute‿_Clang‿enforce_tcb_leaf	1
#  define __has_c_attribute‿_Clang‿__enforce_tcb_leaf__	1
# endif
# if __has_c_attribute(clang::enum_extensibility)
#  define __has_c_attribute‿clang‿enum_extensibility	1
#  define __has_c_attribute‿__clang__‿enum_extensibility	1
#  define __has_c_attribute‿clang‿__enum_extensibility__	1
#  define __has_c_attribute‿__clang__‿__enum_extensibility__	1
#  define __has_c_attribute‿_Clang‿enum_extensibility	1
#  define __has_c_attribute‿_Clang‿__enum_extensibility__	1
# endif
# if __has_c_attribute(clang::exclude_from_explicit_instantiation)
#  define __has_c_attribute‿clang‿exclude_from_explicit_instantiation	1
#  define __has_c_attribute‿__clang__‿exclude_from_explicit_instantiation	1
#  define __has_c_attribute‿clang‿__exclude_from_explicit_instantiation__	1
#  define __has_c_attribute‿__clang__‿__exclude_from_explicit_instantiation__	1
#  define __has_c_attribute‿_Clang‿exclude_from_explicit_instantiation	1
#  define __has_c_attribute‿_Clang‿__exclude_from_explicit_instantiation__	1
# endif
# if __has_c_attribute(clang::export_name)
#  define __has_c_attribute‿clang‿export_name	1
#  define __has_c_attribute‿__clang__‿export_name	1
#  define __has_c_attribute‿clang‿__export_name__	1
#  define __has_c_attribute‿__clang__‿__export_name__	1
#  define __has_c_attribute‿_Clang‿export_name	1
#  define __has_c_attribute‿_Clang‿__export_name__	1
# endif
# if __has_c_attribute(clang::external_source_symbol)
#  define __has_c_attribute‿clang‿external_source_symbol	1
#  define __has_c_attribute‿__clang__‿external_source_symbol	1
#  define __has_c_attribute‿clang‿__external_source_symbol__	1
#  define __has_c_attribute‿__clang__‿__external_source_symbol__	1
#  define __has_c_attribute‿_Clang‿external_source_symbol	1
#  define __has_c_attribute‿_Clang‿__external_source_symbol__	1
# endif
# if __has_c_attribute(clang::fallthrough)
#  define __has_c_attribute‿clang‿fallthrough	1
#  define __has_c_attribute‿__clang__‿fallthrough	1
#  define __has_c_attribute‿clang‿__fallthrough__	1
#  define __has_c_attribute‿__clang__‿__fallthrough__	1
#  define __has_c_attribute‿_Clang‿fallthrough	1
#  define __has_c_attribute‿_Clang‿__fallthrough__	1
# endif
# if __has_c_attribute(clang::flag_enum)
#  define __has_c_attribute‿clang‿flag_enum	1
#  define __has_c_attribute‿__clang__‿flag_enum	1
#  define __has_c_attribute‿clang‿__flag_enum__	1
#  define __has_c_attribute‿__clang__‿__flag_enum__	1
#  define __has_c_attribute‿_Clang‿flag_enum	1
#  define __has_c_attribute‿_Clang‿__flag_enum__	1
# endif
# if __has_c_attribute(clang::guard)
#  define __has_c_attribute‿clang‿guard	1
#  define __has_c_attribute‿__clang__‿guard	1
#  define __has_c_attribute‿clang‿__guard__	1
#  define __has_c_attribute‿__clang__‿__guard__	1
#  define __has_c_attribute‿_Clang‿guard	1
#  define __has_c_attribute‿_Clang‿__guard__	1
# endif
# if __has_c_attribute(clang::guarded_var)
#  define __has_c_attribute‿clang‿guarded_var	1
#  define __has_c_attribute‿__clang__‿guarded_var	1
#  define __has_c_attribute‿clang‿__guarded_var__	1
#  define __has_c_attribute‿__clang__‿__guarded_var__	1
#  define __has_c_attribute‿_Clang‿guarded_var	1
#  define __has_c_attribute‿_Clang‿__guarded_var__	1
# endif
# if __has_c_attribute(clang::hybrid_patchable)
#  define __has_c_attribute‿clang‿hybrid_patchable	1
#  define __has_c_attribute‿__clang__‿hybrid_patchable	1
#  define __has_c_attribute‿clang‿__hybrid_patchable__	1
#  define __has_c_attribute‿__clang__‿__hybrid_patchable__	1
#  define __has_c_attribute‿_Clang‿hybrid_patchable	1
#  define __has_c_attribute‿_Clang‿__hybrid_patchable__	1
# endif
# if __has_c_attribute(clang::ibaction)
#  define __has_c_attribute‿clang‿ibaction	1
#  define __has_c_attribute‿__clang__‿ibaction	1
#  define __has_c_attribute‿clang‿__ibaction__	1
#  define __has_c_attribute‿__clang__‿__ibaction__	1
#  define __has_c_attribute‿_Clang‿ibaction	1
#  define __has_c_attribute‿_Clang‿__ibaction__	1
# endif
# if __has_c_attribute(clang::iboutlet)
#  define __has_c_attribute‿clang‿iboutlet	1
#  define __has_c_attribute‿__clang__‿iboutlet	1
#  define __has_c_attribute‿clang‿__iboutlet__	1
#  define __has_c_attribute‿__clang__‿__iboutlet__	1
#  define __has_c_attribute‿_Clang‿iboutlet	1
#  define __has_c_attribute‿_Clang‿__iboutlet__	1
# endif
# if __has_c_attribute(clang::iboutletcollection)
#  define __has_c_attribute‿clang‿iboutletcollection	1
#  define __has_c_attribute‿__clang__‿iboutletcollection	1
#  define __has_c_attribute‿clang‿__iboutletcollection__	1
#  define __has_c_attribute‿__clang__‿__iboutletcollection__	1
#  define __has_c_attribute‿_Clang‿iboutletcollection	1
#  define __has_c_attribute‿_Clang‿__iboutletcollection__	1
# endif
# if __has_c_attribute(clang::import_module)
#  define __has_c_attribute‿clang‿import_module	1
#  define __has_c_attribute‿__clang__‿import_module	1
#  define __has_c_attribute‿clang‿__import_module__	1
#  define __has_c_attribute‿__clang__‿__import_module__	1
#  define __has_c_attribute‿_Clang‿import_module	1
#  define __has_c_attribute‿_Clang‿__import_module__	1
# endif
# if __has_c_attribute(clang::import_name)
#  define __has_c_attribute‿clang‿import_name	1
#  define __has_c_attribute‿__clang__‿import_name	1
#  define __has_c_attribute‿clang‿__import_name__	1
#  define __has_c_attribute‿__clang__‿__import_name__	1
#  define __has_c_attribute‿_Clang‿import_name	1
#  define __has_c_attribute‿_Clang‿__import_name__	1
# endif
# if __has_c_attribute(clang::intel_ocl_bicc)
#  define __has_c_attribute‿clang‿intel_ocl_bicc	1
#  define __has_c_attribute‿__clang__‿intel_ocl_bicc	1
#  define __has_c_attribute‿clang‿__intel_ocl_bicc__	1
#  define __has_c_attribute‿__clang__‿__intel_ocl_bicc__	1
#  define __has_c_attribute‿_Clang‿intel_ocl_bicc	1
#  define __has_c_attribute‿_Clang‿__intel_ocl_bicc__	1
# endif
# if __has_c_attribute(clang::internal_linkage)
#  define __has_c_attribute‿clang‿internal_linkage	1
#  define __has_c_attribute‿__clang__‿internal_linkage	1
#  define __has_c_attribute‿clang‿__internal_linkage__	1
#  define __has_c_attribute‿__clang__‿__internal_linkage__	1
#  define __has_c_attribute‿_Clang‿internal_linkage	1
#  define __has_c_attribute‿_Clang‿__internal_linkage__	1
# endif
# if __has_c_attribute(clang::lifetimebound)
#  define __has_c_attribute‿clang‿lifetimebound	1
#  define __has_c_attribute‿__clang__‿lifetimebound	1
#  define __has_c_attribute‿clang‿__lifetimebound__	1
#  define __has_c_attribute‿__clang__‿__lifetimebound__	1
#  define __has_c_attribute‿_Clang‿lifetimebound	1
#  define __has_c_attribute‿_Clang‿__lifetimebound__	1
# endif
# if __has_c_attribute(clang::likely)
#  define __has_c_attribute‿clang‿likely	1
#  define __has_c_attribute‿__clang__‿likely	1
#  define __has_c_attribute‿clang‿__likely__	1
#  define __has_c_attribute‿__clang__‿__likely__	1
#  define __has_c_attribute‿_Clang‿likely	1
#  define __has_c_attribute‿_Clang‿__likely__	1
# endif
# if __has_c_attribute(clang::loader_uninitialized)
#  define __has_c_attribute‿clang‿loader_uninitialized	1
#  define __has_c_attribute‿__clang__‿loader_uninitialized	1
#  define __has_c_attribute‿clang‿__loader_uninitialized__	1
#  define __has_c_attribute‿__clang__‿__loader_uninitialized__	1
#  define __has_c_attribute‿_Clang‿loader_uninitialized	1
#  define __has_c_attribute‿_Clang‿__loader_uninitialized__	1
# endif
# if __has_c_attribute(clang::lto_visibility_public)
#  define __has_c_attribute‿clang‿lto_visibility_public	1
#  define __has_c_attribute‿__clang__‿lto_visibility_public	1
#  define __has_c_attribute‿clang‿__lto_visibility_public__	1
#  define __has_c_attribute‿__clang__‿__lto_visibility_public__	1
#  define __has_c_attribute‿_Clang‿lto_visibility_public	1
#  define __has_c_attribute‿_Clang‿__lto_visibility_public__	1
# endif
# if __has_c_attribute(clang::m68k_rtd)
#  define __has_c_attribute‿clang‿m68k_rtd	1
#  define __has_c_attribute‿__clang__‿m68k_rtd	1
#  define __has_c_attribute‿clang‿__m68k_rtd__	1
#  define __has_c_attribute‿__clang__‿__m68k_rtd__	1
#  define __has_c_attribute‿_Clang‿m68k_rtd	1
#  define __has_c_attribute‿_Clang‿__m68k_rtd__	1
# endif
# if __has_c_attribute(clang::matrix_type)
#  define __has_c_attribute‿clang‿matrix_type	1
#  define __has_c_attribute‿__clang__‿matrix_type	1
#  define __has_c_attribute‿clang‿__matrix_type__	1
#  define __has_c_attribute‿__clang__‿__matrix_type__	1
#  define __has_c_attribute‿_Clang‿matrix_type	1
#  define __has_c_attribute‿_Clang‿__matrix_type__	1
# endif
# if __has_c_attribute(clang::maybe_undef)
#  define __has_c_attribute‿clang‿maybe_undef	1
#  define __has_c_attribute‿__clang__‿maybe_undef	1
#  define __has_c_attribute‿clang‿__maybe_undef__	1
#  define __has_c_attribute‿__clang__‿__maybe_undef__	1
#  define __has_c_attribute‿_Clang‿maybe_undef	1
#  define __has_c_attribute‿_Clang‿__maybe_undef__	1
# endif
# if __has_c_attribute(clang::mig_server_routine)
#  define __has_c_attribute‿clang‿mig_server_routine	1
#  define __has_c_attribute‿__clang__‿mig_server_routine	1
#  define __has_c_attribute‿clang‿__mig_server_routine__	1
#  define __has_c_attribute‿__clang__‿__mig_server_routine__	1
#  define __has_c_attribute‿_Clang‿mig_server_routine	1
#  define __has_c_attribute‿_Clang‿__mig_server_routine__	1
# endif
# if __has_c_attribute(clang::minsize)
#  define __has_c_attribute‿clang‿minsize	1
#  define __has_c_attribute‿__clang__‿minsize	1
#  define __has_c_attribute‿clang‿__minsize__	1
#  define __has_c_attribute‿__clang__‿__minsize__	1
#  define __has_c_attribute‿_Clang‿minsize	1
#  define __has_c_attribute‿_Clang‿__minsize__	1
# endif
# if __has_c_attribute(clang::min_vector_width)
#  define __has_c_attribute‿clang‿min_vector_width	1
#  define __has_c_attribute‿__clang__‿min_vector_width	1
#  define __has_c_attribute‿clang‿__min_vector_width__	1
#  define __has_c_attribute‿__clang__‿__min_vector_width__	1
#  define __has_c_attribute‿_Clang‿min_vector_width	1
#  define __has_c_attribute‿_Clang‿__min_vector_width__	1
# endif
# if __has_c_attribute(clang::musttail)
#  define __has_c_attribute‿clang‿musttail	1
#  define __has_c_attribute‿__clang__‿musttail	1
#  define __has_c_attribute‿clang‿__musttail__	1
#  define __has_c_attribute‿__clang__‿__musttail__	1
#  define __has_c_attribute‿_Clang‿musttail	1
#  define __has_c_attribute‿_Clang‿__musttail__	1
# endif
# if __has_c_attribute(clang::neon_polyvector_type)
#  define __has_c_attribute‿clang‿neon_polyvector_type	1
#  define __has_c_attribute‿__clang__‿neon_polyvector_type	1
#  define __has_c_attribute‿clang‿__neon_polyvector_type__	1
#  define __has_c_attribute‿__clang__‿__neon_polyvector_type__	1
#  define __has_c_attribute‿_Clang‿neon_polyvector_type	1
#  define __has_c_attribute‿_Clang‿__neon_polyvector_type__	1
# endif
# if __has_c_attribute(clang::neon_vector_type)
#  define __has_c_attribute‿clang‿neon_vector_type	1
#  define __has_c_attribute‿__clang__‿neon_vector_type	1
#  define __has_c_attribute‿clang‿__neon_vector_type__	1
#  define __has_c_attribute‿__clang__‿__neon_vector_type__	1
#  define __has_c_attribute‿_Clang‿neon_vector_type	1
#  define __has_c_attribute‿_Clang‿__neon_vector_type__	1
# endif
# if __has_c_attribute(clang::no_builtin)
#  define __has_c_attribute‿clang‿no_builtin	1
#  define __has_c_attribute‿__clang__‿no_builtin	1
#  define __has_c_attribute‿clang‿__no_builtin__	1
#  define __has_c_attribute‿__clang__‿__no_builtin__	1
#  define __has_c_attribute‿_Clang‿no_builtin	1
#  define __has_c_attribute‿_Clang‿__no_builtin__	1
# endif
# if __has_c_attribute(clang::noconvergent)
#  define __has_c_attribute‿clang‿noconvergent	1
#  define __has_c_attribute‿__clang__‿noconvergent	1
#  define __has_c_attribute‿clang‿__noconvergent__	1
#  define __has_c_attribute‿__clang__‿__noconvergent__	1
#  define __has_c_attribute‿_Clang‿noconvergent	1
#  define __has_c_attribute‿_Clang‿__noconvergent__	1
# endif
# if __has_c_attribute(clang::noderef)
#  define __has_c_attribute‿clang‿noderef	1
#  define __has_c_attribute‿__clang__‿noderef	1
#  define __has_c_attribute‿clang‿__noderef__	1
#  define __has_c_attribute‿__clang__‿__noderef__	1
#  define __has_c_attribute‿_Clang‿noderef	1
#  define __has_c_attribute‿_Clang‿__noderef__	1
# endif
# if __has_c_attribute(clang::no_destroy)
#  define __has_c_attribute‿clang‿no_destroy	1
#  define __has_c_attribute‿__clang__‿no_destroy	1
#  define __has_c_attribute‿clang‿__no_destroy__	1
#  define __has_c_attribute‿__clang__‿__no_destroy__	1
#  define __has_c_attribute‿_Clang‿no_destroy	1
#  define __has_c_attribute‿_Clang‿__no_destroy__	1
# endif
# if __has_c_attribute(clang::noduplicate)
#  define __has_c_attribute‿clang‿noduplicate	1
#  define __has_c_attribute‿__clang__‿noduplicate	1
#  define __has_c_attribute‿clang‿__noduplicate__	1
#  define __has_c_attribute‿__clang__‿__noduplicate__	1
#  define __has_c_attribute‿_Clang‿noduplicate	1
#  define __has_c_attribute‿_Clang‿__noduplicate__	1
# endif
# if __has_c_attribute(clang::noescape)
#  define __has_c_attribute‿clang‿noescape	1
#  define __has_c_attribute‿__clang__‿noescape	1
#  define __has_c_attribute‿clang‿__noescape__	1
#  define __has_c_attribute‿__clang__‿__noescape__	1
#  define __has_c_attribute‿_Clang‿noescape	1
#  define __has_c_attribute‿_Clang‿__noescape__	1
# endif
# if __has_c_attribute(clang::noinline)
#  define __has_c_attribute‿clang‿noinline	1
#  define __has_c_attribute‿__clang__‿noinline	1
#  define __has_c_attribute‿clang‿__noinline__	1
#  define __has_c_attribute‿__clang__‿__noinline__	1
#  define __has_c_attribute‿_Clang‿noinline	1
#  define __has_c_attribute‿_Clang‿__noinline__	1
# endif
# if __has_c_attribute(clang::nomerge)
#  define __has_c_attribute‿clang‿nomerge	1
#  define __has_c_attribute‿__clang__‿nomerge	1
#  define __has_c_attribute‿clang‿__nomerge__	1
#  define __has_c_attribute‿__clang__‿__nomerge__	1
#  define __has_c_attribute‿_Clang‿nomerge	1
#  define __has_c_attribute‿_Clang‿__nomerge__	1
# endif
# if __has_c_attribute(clang::nonallocating)
#  define __has_c_attribute‿clang‿nonallocating	1
#  define __has_c_attribute‿__clang__‿nonallocating	1
#  define __has_c_attribute‿clang‿__nonallocating__	1
#  define __has_c_attribute‿__clang__‿__nonallocating__	1
#  define __has_c_attribute‿_Clang‿nonallocating	1
#  define __has_c_attribute‿_Clang‿__nonallocating__	1
# endif
# if __has_c_attribute(clang::nonblocking)
#  define __has_c_attribute‿clang‿nonblocking	1
#  define __has_c_attribute‿__clang__‿nonblocking	1
#  define __has_c_attribute‿clang‿__nonblocking__	1
#  define __has_c_attribute‿__clang__‿__nonblocking__	1
#  define __has_c_attribute‿_Clang‿nonblocking	1
#  define __has_c_attribute‿_Clang‿__nonblocking__	1
# endif
# if __has_c_attribute(clang::no_sanitize)
#  define __has_c_attribute‿clang‿no_sanitize	1
#  define __has_c_attribute‿__clang__‿no_sanitize	1
#  define __has_c_attribute‿clang‿__no_sanitize__	1
#  define __has_c_attribute‿__clang__‿__no_sanitize__	1
#  define __has_c_attribute‿_Clang‿no_sanitize	1
#  define __has_c_attribute‿_Clang‿__no_sanitize__	1
# endif
# if __has_c_attribute(clang::no_sanitize_memory)
#  define __has_c_attribute‿clang‿no_sanitize_memory	1
#  define __has_c_attribute‿__clang__‿no_sanitize_memory	1
#  define __has_c_attribute‿clang‿__no_sanitize_memory__	1
#  define __has_c_attribute‿__clang__‿__no_sanitize_memory__	1
#  define __has_c_attribute‿_Clang‿no_sanitize_memory	1
#  define __has_c_attribute‿_Clang‿__no_sanitize_memory__	1
# endif
# if __has_c_attribute(clang::no_speculative_load_hardening)
#  define __has_c_attribute‿clang‿no_speculative_load_hardening	1
#  define __has_c_attribute‿__clang__‿no_speculative_load_hardening	1
#  define __has_c_attribute‿clang‿__no_speculative_load_hardening__	1
#  define __has_c_attribute‿__clang__‿__no_speculative_load_hardening__	1
#  define __has_c_attribute‿_Clang‿no_speculative_load_hardening	1
#  define __has_c_attribute‿_Clang‿__no_speculative_load_hardening__	1
# endif
# if __has_c_attribute(clang::no_stack_protector)
#  define __has_c_attribute‿clang‿no_stack_protector	1
#  define __has_c_attribute‿__clang__‿no_stack_protector	1
#  define __has_c_attribute‿clang‿__no_stack_protector__	1
#  define __has_c_attribute‿__clang__‿__no_stack_protector__	1
#  define __has_c_attribute‿_Clang‿no_stack_protector	1
#  define __has_c_attribute‿_Clang‿__no_stack_protector__	1
# endif
# if __has_c_attribute(clang::no_thread_safety_analysis)
#  define __has_c_attribute‿clang‿no_thread_safety_analysis	1
#  define __has_c_attribute‿__clang__‿no_thread_safety_analysis	1
#  define __has_c_attribute‿clang‿__no_thread_safety_analysis__	1
#  define __has_c_attribute‿__clang__‿__no_thread_safety_analysis__	1
#  define __has_c_attribute‿_Clang‿no_thread_safety_analysis	1
#  define __has_c_attribute‿_Clang‿__no_thread_safety_analysis__	1
# endif
# if __has_c_attribute(clang::not_tail_called)
#  define __has_c_attribute‿clang‿not_tail_called	1
#  define __has_c_attribute‿__clang__‿not_tail_called	1
#  define __has_c_attribute‿clang‿__not_tail_called__	1
#  define __has_c_attribute‿__clang__‿__not_tail_called__	1
#  define __has_c_attribute‿_Clang‿not_tail_called	1
#  define __has_c_attribute‿_Clang‿__not_tail_called__	1
# endif
# if __has_c_attribute(clang::nouwtable)
#  define __has_c_attribute‿clang‿nouwtable	1
#  define __has_c_attribute‿__clang__‿nouwtable	1
#  define __has_c_attribute‿clang‿__nouwtable__	1
#  define __has_c_attribute‿__clang__‿__nouwtable__	1
#  define __has_c_attribute‿_Clang‿nouwtable	1
#  define __has_c_attribute‿_Clang‿__nouwtable__	1
# endif
# if __has_c_attribute(clang::ns_consumed)
#  define __has_c_attribute‿clang‿ns_consumed	1
#  define __has_c_attribute‿__clang__‿ns_consumed	1
#  define __has_c_attribute‿clang‿__ns_consumed__	1
#  define __has_c_attribute‿__clang__‿__ns_consumed__	1
#  define __has_c_attribute‿_Clang‿ns_consumed	1
#  define __has_c_attribute‿_Clang‿__ns_consumed__	1
# endif
# if __has_c_attribute(clang::ns_consumes_self)
#  define __has_c_attribute‿clang‿ns_consumes_self	1
#  define __has_c_attribute‿__clang__‿ns_consumes_self	1
#  define __has_c_attribute‿clang‿__ns_consumes_self__	1
#  define __has_c_attribute‿__clang__‿__ns_consumes_self__	1
#  define __has_c_attribute‿_Clang‿ns_consumes_self	1
#  define __has_c_attribute‿_Clang‿__ns_consumes_self__	1
# endif
# if __has_c_attribute(clang::NSObject)
#  define __has_c_attribute‿clang‿NSObject	1
#  define __has_c_attribute‿__clang__‿NSObject	1
#  define __has_c_attribute‿clang‿__NSObject__	1
#  define __has_c_attribute‿__clang__‿__NSObject__	1
#  define __has_c_attribute‿_Clang‿NSObject	1
#  define __has_c_attribute‿_Clang‿__NSObject__	1
# endif
# if __has_c_attribute(clang::ns_returns_autoreleased)
#  define __has_c_attribute‿clang‿ns_returns_autoreleased	1
#  define __has_c_attribute‿__clang__‿ns_returns_autoreleased	1
#  define __has_c_attribute‿clang‿__ns_returns_autoreleased__	1
#  define __has_c_attribute‿__clang__‿__ns_returns_autoreleased__	1
#  define __has_c_attribute‿_Clang‿ns_returns_autoreleased	1
#  define __has_c_attribute‿_Clang‿__ns_returns_autoreleased__	1
# endif
# if __has_c_attribute(clang::ns_returns_not_retained)
#  define __has_c_attribute‿clang‿ns_returns_not_retained	1
#  define __has_c_attribute‿__clang__‿ns_returns_not_retained	1
#  define __has_c_attribute‿clang‿__ns_returns_not_retained__	1
#  define __has_c_attribute‿__clang__‿__ns_returns_not_retained__	1
#  define __has_c_attribute‿_Clang‿ns_returns_not_retained	1
#  define __has_c_attribute‿_Clang‿__ns_returns_not_retained__	1
# endif
# if __has_c_attribute(clang::ns_returns_retained)
#  define __has_c_attribute‿clang‿ns_returns_retained	1
#  define __has_c_attribute‿__clang__‿ns_returns_retained	1
#  define __has_c_attribute‿clang‿__ns_returns_retained__	1
#  define __has_c_attribute‿__clang__‿__ns_returns_retained__	1
#  define __has_c_attribute‿_Clang‿ns_returns_retained	1
#  define __has_c_attribute‿_Clang‿__ns_returns_retained__	1
# endif
# if __has_c_attribute(clang::nvptx_kernel)
#  define __has_c_attribute‿clang‿nvptx_kernel	1
#  define __has_c_attribute‿__clang__‿nvptx_kernel	1
#  define __has_c_attribute‿clang‿__nvptx_kernel__	1
#  define __has_c_attribute‿__clang__‿__nvptx_kernel__	1
#  define __has_c_attribute‿_Clang‿nvptx_kernel	1
#  define __has_c_attribute‿_Clang‿__nvptx_kernel__	1
# endif
# if __has_c_attribute(clang::objc_arc_weak_reference_unavailable)
#  define __has_c_attribute‿clang‿objc_arc_weak_reference_unavailable	1
#  define __has_c_attribute‿__clang__‿objc_arc_weak_reference_unavailable	1
#  define __has_c_attribute‿clang‿__objc_arc_weak_reference_unavailable__	1
#  define __has_c_attribute‿__clang__‿__objc_arc_weak_reference_unavailable__	1
#  define __has_c_attribute‿_Clang‿objc_arc_weak_reference_unavailable	1
#  define __has_c_attribute‿_Clang‿__objc_arc_weak_reference_unavailable__	1
# endif
# if __has_c_attribute(clang::objc_boxable)
#  define __has_c_attribute‿clang‿objc_boxable	1
#  define __has_c_attribute‿__clang__‿objc_boxable	1
#  define __has_c_attribute‿clang‿__objc_boxable__	1
#  define __has_c_attribute‿__clang__‿__objc_boxable__	1
#  define __has_c_attribute‿_Clang‿objc_boxable	1
#  define __has_c_attribute‿_Clang‿__objc_boxable__	1
# endif
# if __has_c_attribute(clang::objc_bridge)
#  define __has_c_attribute‿clang‿objc_bridge	1
#  define __has_c_attribute‿__clang__‿objc_bridge	1
#  define __has_c_attribute‿clang‿__objc_bridge__	1
#  define __has_c_attribute‿__clang__‿__objc_bridge__	1
#  define __has_c_attribute‿_Clang‿objc_bridge	1
#  define __has_c_attribute‿_Clang‿__objc_bridge__	1
# endif
# if __has_c_attribute(clang::objc_bridge_mutable)
#  define __has_c_attribute‿clang‿objc_bridge_mutable	1
#  define __has_c_attribute‿__clang__‿objc_bridge_mutable	1
#  define __has_c_attribute‿clang‿__objc_bridge_mutable__	1
#  define __has_c_attribute‿__clang__‿__objc_bridge_mutable__	1
#  define __has_c_attribute‿_Clang‿objc_bridge_mutable	1
#  define __has_c_attribute‿_Clang‿__objc_bridge_mutable__	1
# endif
# if __has_c_attribute(clang::objc_bridge_related)
#  define __has_c_attribute‿clang‿objc_bridge_related	1
#  define __has_c_attribute‿__clang__‿objc_bridge_related	1
#  define __has_c_attribute‿clang‿__objc_bridge_related__	1
#  define __has_c_attribute‿__clang__‿__objc_bridge_related__	1
#  define __has_c_attribute‿_Clang‿objc_bridge_related	1
#  define __has_c_attribute‿_Clang‿__objc_bridge_related__	1
# endif
# if __has_c_attribute(clang::objc_class_stub)
#  define __has_c_attribute‿clang‿objc_class_stub	1
#  define __has_c_attribute‿__clang__‿objc_class_stub	1
#  define __has_c_attribute‿clang‿__objc_class_stub__	1
#  define __has_c_attribute‿__clang__‿__objc_class_stub__	1
#  define __has_c_attribute‿_Clang‿objc_class_stub	1
#  define __has_c_attribute‿_Clang‿__objc_class_stub__	1
# endif
# if __has_c_attribute(clang::objc_designated_initializer)
#  define __has_c_attribute‿clang‿objc_designated_initializer	1
#  define __has_c_attribute‿__clang__‿objc_designated_initializer	1
#  define __has_c_attribute‿clang‿__objc_designated_initializer__	1
#  define __has_c_attribute‿__clang__‿__objc_designated_initializer__	1
#  define __has_c_attribute‿_Clang‿objc_designated_initializer	1
#  define __has_c_attribute‿_Clang‿__objc_designated_initializer__	1
# endif
# if __has_c_attribute(clang::objc_direct)
#  define __has_c_attribute‿clang‿objc_direct	1
#  define __has_c_attribute‿__clang__‿objc_direct	1
#  define __has_c_attribute‿clang‿__objc_direct__	1
#  define __has_c_attribute‿__clang__‿__objc_direct__	1
#  define __has_c_attribute‿_Clang‿objc_direct	1
#  define __has_c_attribute‿_Clang‿__objc_direct__	1
# endif
# if __has_c_attribute(clang::objc_direct_members)
#  define __has_c_attribute‿clang‿objc_direct_members	1
#  define __has_c_attribute‿__clang__‿objc_direct_members	1
#  define __has_c_attribute‿clang‿__objc_direct_members__	1
#  define __has_c_attribute‿__clang__‿__objc_direct_members__	1
#  define __has_c_attribute‿_Clang‿objc_direct_members	1
#  define __has_c_attribute‿_Clang‿__objc_direct_members__	1
# endif
# if __has_c_attribute(clang::objc_exception)
#  define __has_c_attribute‿clang‿objc_exception	1
#  define __has_c_attribute‿__clang__‿objc_exception	1
#  define __has_c_attribute‿clang‿__objc_exception__	1
#  define __has_c_attribute‿__clang__‿__objc_exception__	1
#  define __has_c_attribute‿_Clang‿objc_exception	1
#  define __has_c_attribute‿_Clang‿__objc_exception__	1
# endif
# if __has_c_attribute(clang::objc_externally_retained)
#  define __has_c_attribute‿clang‿objc_externally_retained	1
#  define __has_c_attribute‿__clang__‿objc_externally_retained	1
#  define __has_c_attribute‿clang‿__objc_externally_retained__	1
#  define __has_c_attribute‿__clang__‿__objc_externally_retained__	1
#  define __has_c_attribute‿_Clang‿objc_externally_retained	1
#  define __has_c_attribute‿_Clang‿__objc_externally_retained__	1
# endif
# if __has_c_attribute(clang::objc_gc)
#  define __has_c_attribute‿clang‿objc_gc	1
#  define __has_c_attribute‿__clang__‿objc_gc	1
#  define __has_c_attribute‿clang‿__objc_gc__	1
#  define __has_c_attribute‿__clang__‿__objc_gc__	1
#  define __has_c_attribute‿_Clang‿objc_gc	1
#  define __has_c_attribute‿_Clang‿__objc_gc__	1
# endif
# if __has_c_attribute(clang::objc_independent_class)
#  define __has_c_attribute‿clang‿objc_independent_class	1
#  define __has_c_attribute‿__clang__‿objc_independent_class	1
#  define __has_c_attribute‿clang‿__objc_independent_class__	1
#  define __has_c_attribute‿__clang__‿__objc_independent_class__	1
#  define __has_c_attribute‿_Clang‿objc_independent_class	1
#  define __has_c_attribute‿_Clang‿__objc_independent_class__	1
# endif
# if __has_c_attribute(clang::objc_method_family)
#  define __has_c_attribute‿clang‿objc_method_family	1
#  define __has_c_attribute‿__clang__‿objc_method_family	1
#  define __has_c_attribute‿clang‿__objc_method_family__	1
#  define __has_c_attribute‿__clang__‿__objc_method_family__	1
#  define __has_c_attribute‿_Clang‿objc_method_family	1
#  define __has_c_attribute‿_Clang‿__objc_method_family__	1
# endif
# if __has_c_attribute(clang::objc_nonlazy_class)
#  define __has_c_attribute‿clang‿objc_nonlazy_class	1
#  define __has_c_attribute‿__clang__‿objc_nonlazy_class	1
#  define __has_c_attribute‿clang‿__objc_nonlazy_class__	1
#  define __has_c_attribute‿__clang__‿__objc_nonlazy_class__	1
#  define __has_c_attribute‿_Clang‿objc_nonlazy_class	1
#  define __has_c_attribute‿_Clang‿__objc_nonlazy_class__	1
# endif
# if __has_c_attribute(clang::objc_non_runtime_protocol)
#  define __has_c_attribute‿clang‿objc_non_runtime_protocol	1
#  define __has_c_attribute‿__clang__‿objc_non_runtime_protocol	1
#  define __has_c_attribute‿clang‿__objc_non_runtime_protocol__	1
#  define __has_c_attribute‿__clang__‿__objc_non_runtime_protocol__	1
#  define __has_c_attribute‿_Clang‿objc_non_runtime_protocol	1
#  define __has_c_attribute‿_Clang‿__objc_non_runtime_protocol__	1
# endif
# if __has_c_attribute(clang::objc_ownership)
#  define __has_c_attribute‿clang‿objc_ownership	1
#  define __has_c_attribute‿__clang__‿objc_ownership	1
#  define __has_c_attribute‿clang‿__objc_ownership__	1
#  define __has_c_attribute‿__clang__‿__objc_ownership__	1
#  define __has_c_attribute‿_Clang‿objc_ownership	1
#  define __has_c_attribute‿_Clang‿__objc_ownership__	1
# endif
# if __has_c_attribute(clang::objc_precise_lifetime)
#  define __has_c_attribute‿clang‿objc_precise_lifetime	1
#  define __has_c_attribute‿__clang__‿objc_precise_lifetime	1
#  define __has_c_attribute‿clang‿__objc_precise_lifetime__	1
#  define __has_c_attribute‿__clang__‿__objc_precise_lifetime__	1
#  define __has_c_attribute‿_Clang‿objc_precise_lifetime	1
#  define __has_c_attribute‿_Clang‿__objc_precise_lifetime__	1
# endif
# if __has_c_attribute(clang::objc_protocol_requires_explicit_implementation)
#  define __has_c_attribute‿clang‿objc_protocol_requires_explicit_implementation	1
#  define __has_c_attribute‿__clang__‿objc_protocol_requires_explicit_implementation	1
#  define __has_c_attribute‿clang‿__objc_protocol_requires_explicit_implementation__	1
#  define __has_c_attribute‿__clang__‿__objc_protocol_requires_explicit_implementation__	1
#  define __has_c_attribute‿_Clang‿objc_protocol_requires_explicit_implementation	1
#  define __has_c_attribute‿_Clang‿__objc_protocol_requires_explicit_implementation__	1
# endif
# if __has_c_attribute(clang::objc_requires_property_definitions)
#  define __has_c_attribute‿clang‿objc_requires_property_definitions	1
#  define __has_c_attribute‿__clang__‿objc_requires_property_definitions	1
#  define __has_c_attribute‿clang‿__objc_requires_property_definitions__	1
#  define __has_c_attribute‿__clang__‿__objc_requires_property_definitions__	1
#  define __has_c_attribute‿_Clang‿objc_requires_property_definitions	1
#  define __has_c_attribute‿_Clang‿__objc_requires_property_definitions__	1
# endif
# if __has_c_attribute(clang::objc_requires_super)
#  define __has_c_attribute‿clang‿objc_requires_super	1
#  define __has_c_attribute‿__clang__‿objc_requires_super	1
#  define __has_c_attribute‿clang‿__objc_requires_super__	1
#  define __has_c_attribute‿__clang__‿__objc_requires_super__	1
#  define __has_c_attribute‿_Clang‿objc_requires_super	1
#  define __has_c_attribute‿_Clang‿__objc_requires_super__	1
# endif
# if __has_c_attribute(clang::objc_returns_inner_pointer)
#  define __has_c_attribute‿clang‿objc_returns_inner_pointer	1
#  define __has_c_attribute‿__clang__‿objc_returns_inner_pointer	1
#  define __has_c_attribute‿clang‿__objc_returns_inner_pointer__	1
#  define __has_c_attribute‿__clang__‿__objc_returns_inner_pointer__	1
#  define __has_c_attribute‿_Clang‿objc_returns_inner_pointer	1
#  define __has_c_attribute‿_Clang‿__objc_returns_inner_pointer__	1
# endif
# if __has_c_attribute(clang::objc_root_class)
#  define __has_c_attribute‿clang‿objc_root_class	1
#  define __has_c_attribute‿__clang__‿objc_root_class	1
#  define __has_c_attribute‿clang‿__objc_root_class__	1
#  define __has_c_attribute‿__clang__‿__objc_root_class__	1
#  define __has_c_attribute‿_Clang‿objc_root_class	1
#  define __has_c_attribute‿_Clang‿__objc_root_class__	1
# endif
# if __has_c_attribute(clang::objc_runtime_name)
#  define __has_c_attribute‿clang‿objc_runtime_name	1
#  define __has_c_attribute‿__clang__‿objc_runtime_name	1
#  define __has_c_attribute‿clang‿__objc_runtime_name__	1
#  define __has_c_attribute‿__clang__‿__objc_runtime_name__	1
#  define __has_c_attribute‿_Clang‿objc_runtime_name	1
#  define __has_c_attribute‿_Clang‿__objc_runtime_name__	1
# endif
# if __has_c_attribute(clang::objc_runtime_visible)
#  define __has_c_attribute‿clang‿objc_runtime_visible	1
#  define __has_c_attribute‿__clang__‿objc_runtime_visible	1
#  define __has_c_attribute‿clang‿__objc_runtime_visible__	1
#  define __has_c_attribute‿__clang__‿__objc_runtime_visible__	1
#  define __has_c_attribute‿_Clang‿objc_runtime_visible	1
#  define __has_c_attribute‿_Clang‿__objc_runtime_visible__	1
# endif
# if __has_c_attribute(clang::objc_subclassing_restricted)
#  define __has_c_attribute‿clang‿objc_subclassing_restricted	1
#  define __has_c_attribute‿__clang__‿objc_subclassing_restricted	1
#  define __has_c_attribute‿clang‿__objc_subclassing_restricted__	1
#  define __has_c_attribute‿__clang__‿__objc_subclassing_restricted__	1
#  define __has_c_attribute‿_Clang‿objc_subclassing_restricted	1
#  define __has_c_attribute‿_Clang‿__objc_subclassing_restricted__	1
# endif
# if __has_c_attribute(clang::opencl_constant)
#  define __has_c_attribute‿clang‿opencl_constant	1
#  define __has_c_attribute‿__clang__‿opencl_constant	1
#  define __has_c_attribute‿clang‿__opencl_constant__	1
#  define __has_c_attribute‿__clang__‿__opencl_constant__	1
#  define __has_c_attribute‿_Clang‿opencl_constant	1
#  define __has_c_attribute‿_Clang‿__opencl_constant__	1
# endif
# if __has_c_attribute(clang::opencl_generic)
#  define __has_c_attribute‿clang‿opencl_generic	1
#  define __has_c_attribute‿__clang__‿opencl_generic	1
#  define __has_c_attribute‿clang‿__opencl_generic__	1
#  define __has_c_attribute‿__clang__‿__opencl_generic__	1
#  define __has_c_attribute‿_Clang‿opencl_generic	1
#  define __has_c_attribute‿_Clang‿__opencl_generic__	1
# endif
# if __has_c_attribute(clang::opencl_global)
#  define __has_c_attribute‿clang‿opencl_global	1
#  define __has_c_attribute‿__clang__‿opencl_global	1
#  define __has_c_attribute‿clang‿__opencl_global__	1
#  define __has_c_attribute‿__clang__‿__opencl_global__	1
#  define __has_c_attribute‿_Clang‿opencl_global	1
#  define __has_c_attribute‿_Clang‿__opencl_global__	1
# endif
# if __has_c_attribute(clang::opencl_global_device)
#  define __has_c_attribute‿clang‿opencl_global_device	1
#  define __has_c_attribute‿__clang__‿opencl_global_device	1
#  define __has_c_attribute‿clang‿__opencl_global_device__	1
#  define __has_c_attribute‿__clang__‿__opencl_global_device__	1
#  define __has_c_attribute‿_Clang‿opencl_global_device	1
#  define __has_c_attribute‿_Clang‿__opencl_global_device__	1
# endif
# if __has_c_attribute(clang::opencl_global_host)
#  define __has_c_attribute‿clang‿opencl_global_host	1
#  define __has_c_attribute‿__clang__‿opencl_global_host	1
#  define __has_c_attribute‿clang‿__opencl_global_host__	1
#  define __has_c_attribute‿__clang__‿__opencl_global_host__	1
#  define __has_c_attribute‿_Clang‿opencl_global_host	1
#  define __has_c_attribute‿_Clang‿__opencl_global_host__	1
# endif
# if __has_c_attribute(clang::opencl_local)
#  define __has_c_attribute‿clang‿opencl_local	1
#  define __has_c_attribute‿__clang__‿opencl_local	1
#  define __has_c_attribute‿clang‿__opencl_local__	1
#  define __has_c_attribute‿__clang__‿__opencl_local__	1
#  define __has_c_attribute‿_Clang‿opencl_local	1
#  define __has_c_attribute‿_Clang‿__opencl_local__	1
# endif
# if __has_c_attribute(clang::opencl_private)
#  define __has_c_attribute‿clang‿opencl_private	1
#  define __has_c_attribute‿__clang__‿opencl_private	1
#  define __has_c_attribute‿clang‿__opencl_private__	1
#  define __has_c_attribute‿__clang__‿__opencl_private__	1
#  define __has_c_attribute‿_Clang‿opencl_private	1
#  define __has_c_attribute‿_Clang‿__opencl_private__	1
# endif
# if __has_c_attribute(clang::optnone)
#  define __has_c_attribute‿clang‿optnone	1
#  define __has_c_attribute‿__clang__‿optnone	1
#  define __has_c_attribute‿clang‿__optnone__	1
#  define __has_c_attribute‿__clang__‿__optnone__	1
#  define __has_c_attribute‿_Clang‿optnone	1
#  define __has_c_attribute‿_Clang‿__optnone__	1
# endif
# if __has_c_attribute(clang::os_consumed)
#  define __has_c_attribute‿clang‿os_consumed	1
#  define __has_c_attribute‿__clang__‿os_consumed	1
#  define __has_c_attribute‿clang‿__os_consumed__	1
#  define __has_c_attribute‿__clang__‿__os_consumed__	1
#  define __has_c_attribute‿_Clang‿os_consumed	1
#  define __has_c_attribute‿_Clang‿__os_consumed__	1
# endif
# if __has_c_attribute(clang::os_consumes_this)
#  define __has_c_attribute‿clang‿os_consumes_this	1
#  define __has_c_attribute‿__clang__‿os_consumes_this	1
#  define __has_c_attribute‿clang‿__os_consumes_this__	1
#  define __has_c_attribute‿__clang__‿__os_consumes_this__	1
#  define __has_c_attribute‿_Clang‿os_consumes_this	1
#  define __has_c_attribute‿_Clang‿__os_consumes_this__	1
# endif
# if __has_c_attribute(clang::os_returns_not_retained)
#  define __has_c_attribute‿clang‿os_returns_not_retained	1
#  define __has_c_attribute‿__clang__‿os_returns_not_retained	1
#  define __has_c_attribute‿clang‿__os_returns_not_retained__	1
#  define __has_c_attribute‿__clang__‿__os_returns_not_retained__	1
#  define __has_c_attribute‿_Clang‿os_returns_not_retained	1
#  define __has_c_attribute‿_Clang‿__os_returns_not_retained__	1
# endif
# if __has_c_attribute(clang::os_returns_retained)
#  define __has_c_attribute‿clang‿os_returns_retained	1
#  define __has_c_attribute‿__clang__‿os_returns_retained	1
#  define __has_c_attribute‿clang‿__os_returns_retained__	1
#  define __has_c_attribute‿__clang__‿__os_returns_retained__	1
#  define __has_c_attribute‿_Clang‿os_returns_retained	1
#  define __has_c_attribute‿_Clang‿__os_returns_retained__	1
# endif
# if __has_c_attribute(clang::os_returns_retained_on_non_zero)
#  define __has_c_attribute‿clang‿os_returns_retained_on_non_zero	1
#  define __has_c_attribute‿__clang__‿os_returns_retained_on_non_zero	1
#  define __has_c_attribute‿clang‿__os_returns_retained_on_non_zero__	1
#  define __has_c_attribute‿__clang__‿__os_returns_retained_on_non_zero__	1
#  define __has_c_attribute‿_Clang‿os_returns_retained_on_non_zero	1
#  define __has_c_attribute‿_Clang‿__os_returns_retained_on_non_zero__	1
# endif
# if __has_c_attribute(clang::os_returns_retained_on_zero)
#  define __has_c_attribute‿clang‿os_returns_retained_on_zero	1
#  define __has_c_attribute‿__clang__‿os_returns_retained_on_zero	1
#  define __has_c_attribute‿clang‿__os_returns_retained_on_zero__	1
#  define __has_c_attribute‿__clang__‿__os_returns_retained_on_zero__	1
#  define __has_c_attribute‿_Clang‿os_returns_retained_on_zero	1
#  define __has_c_attribute‿_Clang‿__os_returns_retained_on_zero__	1
# endif
# if __has_c_attribute(clang::overloadable)
#  define __has_c_attribute‿clang‿overloadable	1
#  define __has_c_attribute‿__clang__‿overloadable	1
#  define __has_c_attribute‿clang‿__overloadable__	1
#  define __has_c_attribute‿__clang__‿__overloadable__	1
#  define __has_c_attribute‿_Clang‿overloadable	1
#  define __has_c_attribute‿_Clang‿__overloadable__	1
# endif
# if __has_c_attribute(clang::ownership_takes)
#  define __has_c_attribute‿clang‿ownership_takes	1
#  define __has_c_attribute‿__clang__‿ownership_takes	1
#  define __has_c_attribute‿clang‿__ownership_takes__	1
#  define __has_c_attribute‿__clang__‿__ownership_takes__	1
#  define __has_c_attribute‿_Clang‿ownership_takes	1
#  define __has_c_attribute‿_Clang‿__ownership_takes__	1
# endif
# if __has_c_attribute(clang::param_typestate)
#  define __has_c_attribute‿clang‿param_typestate	1
#  define __has_c_attribute‿__clang__‿param_typestate	1
#  define __has_c_attribute‿clang‿__param_typestate__	1
#  define __has_c_attribute‿__clang__‿__param_typestate__	1
#  define __has_c_attribute‿_Clang‿param_typestate	1
#  define __has_c_attribute‿_Clang‿__param_typestate__	1
# endif
# if __has_c_attribute(clang::pascal)
#  define __has_c_attribute‿clang‿pascal	1
#  define __has_c_attribute‿__clang__‿pascal	1
#  define __has_c_attribute‿clang‿__pascal__	1
#  define __has_c_attribute‿__clang__‿__pascal__	1
#  define __has_c_attribute‿_Clang‿pascal	1
#  define __has_c_attribute‿_Clang‿__pascal__	1
# endif
# if __has_c_attribute(clang::pass_dynamic_object_size)
#  define __has_c_attribute‿clang‿pass_dynamic_object_size	1
#  define __has_c_attribute‿__clang__‿pass_dynamic_object_size	1
#  define __has_c_attribute‿clang‿__pass_dynamic_object_size__	1
#  define __has_c_attribute‿__clang__‿__pass_dynamic_object_size__	1
#  define __has_c_attribute‿_Clang‿pass_dynamic_object_size	1
#  define __has_c_attribute‿_Clang‿__pass_dynamic_object_size__	1
# endif
# if __has_c_attribute(clang::pointer_with_type_tag)
#  define __has_c_attribute‿clang‿pointer_with_type_tag	1
#  define __has_c_attribute‿__clang__‿pointer_with_type_tag	1
#  define __has_c_attribute‿clang‿__pointer_with_type_tag__	1
#  define __has_c_attribute‿__clang__‿__pointer_with_type_tag__	1
#  define __has_c_attribute‿_Clang‿pointer_with_type_tag	1
#  define __has_c_attribute‿_Clang‿__pointer_with_type_tag__	1
# endif
# if __has_c_attribute(clang::preferred_name)
#  define __has_c_attribute‿clang‿preferred_name	1
#  define __has_c_attribute‿__clang__‿preferred_name	1
#  define __has_c_attribute‿clang‿__preferred_name__	1
#  define __has_c_attribute‿__clang__‿__preferred_name__	1
#  define __has_c_attribute‿_Clang‿preferred_name	1
#  define __has_c_attribute‿_Clang‿__preferred_name__	1
# endif
# if __has_c_attribute(clang::preferred_type)
#  define __has_c_attribute‿clang‿preferred_type	1
#  define __has_c_attribute‿__clang__‿preferred_type	1
#  define __has_c_attribute‿clang‿__preferred_type__	1
#  define __has_c_attribute‿__clang__‿__preferred_type__	1
#  define __has_c_attribute‿_Clang‿preferred_type	1
#  define __has_c_attribute‿_Clang‿__preferred_type__	1
# endif
# if __has_c_attribute(clang::preserve_access_index)
#  define __has_c_attribute‿clang‿preserve_access_index	1
#  define __has_c_attribute‿__clang__‿preserve_access_index	1
#  define __has_c_attribute‿clang‿__preserve_access_index__	1
#  define __has_c_attribute‿__clang__‿__preserve_access_index__	1
#  define __has_c_attribute‿_Clang‿preserve_access_index	1
#  define __has_c_attribute‿_Clang‿__preserve_access_index__	1
# endif
# if __has_c_attribute(clang::preserve_all)
#  define __has_c_attribute‿clang‿preserve_all	1
#  define __has_c_attribute‿__clang__‿preserve_all	1
#  define __has_c_attribute‿clang‿__preserve_all__	1
#  define __has_c_attribute‿__clang__‿__preserve_all__	1
#  define __has_c_attribute‿_Clang‿preserve_all	1
#  define __has_c_attribute‿_Clang‿__preserve_all__	1
# endif
# if __has_c_attribute(clang::preserve_most)
#  define __has_c_attribute‿clang‿preserve_most	1
#  define __has_c_attribute‿__clang__‿preserve_most	1
#  define __has_c_attribute‿clang‿__preserve_most__	1
#  define __has_c_attribute‿__clang__‿__preserve_most__	1
#  define __has_c_attribute‿_Clang‿preserve_most	1
#  define __has_c_attribute‿_Clang‿__preserve_most__	1
# endif
# if __has_c_attribute(clang::preserve_none)
#  define __has_c_attribute‿clang‿preserve_none	1
#  define __has_c_attribute‿__clang__‿preserve_none	1
#  define __has_c_attribute‿clang‿__preserve_none__	1
#  define __has_c_attribute‿__clang__‿__preserve_none__	1
#  define __has_c_attribute‿_Clang‿preserve_none	1
#  define __has_c_attribute‿_Clang‿__preserve_none__	1
# endif
# if __has_c_attribute(clang::preserve_static_offset)
#  define __has_c_attribute‿clang‿preserve_static_offset	1
#  define __has_c_attribute‿__clang__‿preserve_static_offset	1
#  define __has_c_attribute‿clang‿__preserve_static_offset__	1
#  define __has_c_attribute‿__clang__‿__preserve_static_offset__	1
#  define __has_c_attribute‿_Clang‿preserve_static_offset	1
#  define __has_c_attribute‿_Clang‿__preserve_static_offset__	1
# endif
# if __has_c_attribute(clang::pt_guarded_var)
#  define __has_c_attribute‿clang‿pt_guarded_var	1
#  define __has_c_attribute‿__clang__‿pt_guarded_var	1
#  define __has_c_attribute‿clang‿__pt_guarded_var__	1
#  define __has_c_attribute‿__clang__‿__pt_guarded_var__	1
#  define __has_c_attribute‿_Clang‿pt_guarded_var	1
#  define __has_c_attribute‿_Clang‿__pt_guarded_var__	1
# endif
# if __has_c_attribute(clang::ptrauth_vtable_pointer)
#  define __has_c_attribute‿clang‿ptrauth_vtable_pointer	1
#  define __has_c_attribute‿__clang__‿ptrauth_vtable_pointer	1
#  define __has_c_attribute‿clang‿__ptrauth_vtable_pointer__	1
#  define __has_c_attribute‿__clang__‿__ptrauth_vtable_pointer__	1
#  define __has_c_attribute‿_Clang‿ptrauth_vtable_pointer	1
#  define __has_c_attribute‿_Clang‿__ptrauth_vtable_pointer__	1
# endif
# if __has_c_attribute(clang::reinitializes)
#  define __has_c_attribute‿clang‿reinitializes	1
#  define __has_c_attribute‿__clang__‿reinitializes	1
#  define __has_c_attribute‿clang‿__reinitializes__	1
#  define __has_c_attribute‿__clang__‿__reinitializes__	1
#  define __has_c_attribute‿_Clang‿reinitializes	1
#  define __has_c_attribute‿_Clang‿__reinitializes__	1
# endif
# if __has_c_attribute(clang::release_handle)
#  define __has_c_attribute‿clang‿release_handle	1
#  define __has_c_attribute‿__clang__‿release_handle	1
#  define __has_c_attribute‿clang‿__release_handle__	1
#  define __has_c_attribute‿__clang__‿__release_handle__	1
#  define __has_c_attribute‿_Clang‿release_handle	1
#  define __has_c_attribute‿_Clang‿__release_handle__	1
# endif
# if __has_c_attribute(clang::require_constant_initialization)
#  define __has_c_attribute‿clang‿require_constant_initialization	1
#  define __has_c_attribute‿__clang__‿require_constant_initialization	1
#  define __has_c_attribute‿clang‿__require_constant_initialization__	1
#  define __has_c_attribute‿__clang__‿__require_constant_initialization__	1
#  define __has_c_attribute‿_Clang‿require_constant_initialization	1
#  define __has_c_attribute‿_Clang‿__require_constant_initialization__	1
# endif
# if __has_c_attribute(clang::return_typestate)
#  define __has_c_attribute‿clang‿return_typestate	1
#  define __has_c_attribute‿__clang__‿return_typestate	1
#  define __has_c_attribute‿clang‿__return_typestate__	1
#  define __has_c_attribute‿__clang__‿__return_typestate__	1
#  define __has_c_attribute‿_Clang‿return_typestate	1
#  define __has_c_attribute‿_Clang‿__return_typestate__	1
# endif
# if __has_c_attribute(clang::riscv_vector_cc)
#  define __has_c_attribute‿clang‿riscv_vector_cc	1
#  define __has_c_attribute‿__clang__‿riscv_vector_cc	1
#  define __has_c_attribute‿clang‿__riscv_vector_cc__	1
#  define __has_c_attribute‿__clang__‿__riscv_vector_cc__	1
#  define __has_c_attribute‿_Clang‿riscv_vector_cc	1
#  define __has_c_attribute‿_Clang‿__riscv_vector_cc__	1
# endif
# if __has_c_attribute(clang::scoped_lockable)
#  define __has_c_attribute‿clang‿scoped_lockable	1
#  define __has_c_attribute‿__clang__‿scoped_lockable	1
#  define __has_c_attribute‿clang‿__scoped_lockable__	1
#  define __has_c_attribute‿__clang__‿__scoped_lockable__	1
#  define __has_c_attribute‿_Clang‿scoped_lockable	1
#  define __has_c_attribute‿_Clang‿__scoped_lockable__	1
# endif
# if __has_c_attribute(clang::set_typestate)
#  define __has_c_attribute‿clang‿set_typestate	1
#  define __has_c_attribute‿__clang__‿set_typestate	1
#  define __has_c_attribute‿clang‿__set_typestate__	1
#  define __has_c_attribute‿__clang__‿__set_typestate__	1
#  define __has_c_attribute‿_Clang‿set_typestate	1
#  define __has_c_attribute‿_Clang‿__set_typestate__	1
# endif
# if __has_c_attribute(clang::shared_capability)
#  define __has_c_attribute‿clang‿shared_capability	1
#  define __has_c_attribute‿__clang__‿shared_capability	1
#  define __has_c_attribute‿clang‿__shared_capability__	1
#  define __has_c_attribute‿__clang__‿__shared_capability__	1
#  define __has_c_attribute‿_Clang‿shared_capability	1
#  define __has_c_attribute‿_Clang‿__shared_capability__	1
# endif
# if __has_c_attribute(clang::shared_locks_required)
#  define __has_c_attribute‿clang‿shared_locks_required	1
#  define __has_c_attribute‿__clang__‿shared_locks_required	1
#  define __has_c_attribute‿clang‿__shared_locks_required__	1
#  define __has_c_attribute‿__clang__‿__shared_locks_required__	1
#  define __has_c_attribute‿_Clang‿shared_locks_required	1
#  define __has_c_attribute‿_Clang‿__shared_locks_required__	1
# endif
# if __has_c_attribute(clang::sized_by)
#  define __has_c_attribute‿clang‿sized_by	1
#  define __has_c_attribute‿__clang__‿sized_by	1
#  define __has_c_attribute‿clang‿__sized_by__	1
#  define __has_c_attribute‿__clang__‿__sized_by__	1
#  define __has_c_attribute‿_Clang‿sized_by	1
#  define __has_c_attribute‿_Clang‿__sized_by__	1
# endif
# if __has_c_attribute(clang::sized_by_or_null)
#  define __has_c_attribute‿clang‿sized_by_or_null	1
#  define __has_c_attribute‿__clang__‿sized_by_or_null	1
#  define __has_c_attribute‿clang‿__sized_by_or_null__	1
#  define __has_c_attribute‿__clang__‿__sized_by_or_null__	1
#  define __has_c_attribute‿_Clang‿sized_by_or_null	1
#  define __has_c_attribute‿_Clang‿__sized_by_or_null__	1
# endif
# if __has_c_attribute(clang::speculative_load_hardening)
#  define __has_c_attribute‿clang‿speculative_load_hardening	1
#  define __has_c_attribute‿__clang__‿speculative_load_hardening	1
#  define __has_c_attribute‿clang‿__speculative_load_hardening__	1
#  define __has_c_attribute‿__clang__‿__speculative_load_hardening__	1
#  define __has_c_attribute‿_Clang‿speculative_load_hardening	1
#  define __has_c_attribute‿_Clang‿__speculative_load_hardening__	1
# endif
# if __has_c_attribute(clang::standalone_debug)
#  define __has_c_attribute‿clang‿standalone_debug	1
#  define __has_c_attribute‿__clang__‿standalone_debug	1
#  define __has_c_attribute‿clang‿__standalone_debug__	1
#  define __has_c_attribute‿__clang__‿__standalone_debug__	1
#  define __has_c_attribute‿_Clang‿standalone_debug	1
#  define __has_c_attribute‿_Clang‿__standalone_debug__	1
# endif
# if __has_c_attribute(clang::suppress)
#  define __has_c_attribute‿clang‿suppress	1
#  define __has_c_attribute‿__clang__‿suppress	1
#  define __has_c_attribute‿clang‿__suppress__	1
#  define __has_c_attribute‿__clang__‿__suppress__	1
#  define __has_c_attribute‿_Clang‿suppress	1
#  define __has_c_attribute‿_Clang‿__suppress__	1
# endif
# if __has_c_attribute(clang::swift_async)
#  define __has_c_attribute‿clang‿swift_async	1
#  define __has_c_attribute‿__clang__‿swift_async	1
#  define __has_c_attribute‿clang‿__swift_async__	1
#  define __has_c_attribute‿__clang__‿__swift_async__	1
#  define __has_c_attribute‿_Clang‿swift_async	1
#  define __has_c_attribute‿_Clang‿__swift_async__	1
# endif
# if __has_c_attribute(clang::swiftasynccall)
#  define __has_c_attribute‿clang‿swiftasynccall	1
#  define __has_c_attribute‿__clang__‿swiftasynccall	1
#  define __has_c_attribute‿clang‿__swiftasynccall__	1
#  define __has_c_attribute‿__clang__‿__swiftasynccall__	1
#  define __has_c_attribute‿_Clang‿swiftasynccall	1
#  define __has_c_attribute‿_Clang‿__swiftasynccall__	1
# endif
# if __has_c_attribute(clang::swift_async_context)
#  define __has_c_attribute‿clang‿swift_async_context	1
#  define __has_c_attribute‿__clang__‿swift_async_context	1
#  define __has_c_attribute‿clang‿__swift_async_context__	1
#  define __has_c_attribute‿__clang__‿__swift_async_context__	1
#  define __has_c_attribute‿_Clang‿swift_async_context	1
#  define __has_c_attribute‿_Clang‿__swift_async_context__	1
# endif
# if __has_c_attribute(clang::swift_async_error)
#  define __has_c_attribute‿clang‿swift_async_error	1
#  define __has_c_attribute‿__clang__‿swift_async_error	1
#  define __has_c_attribute‿clang‿__swift_async_error__	1
#  define __has_c_attribute‿__clang__‿__swift_async_error__	1
#  define __has_c_attribute‿_Clang‿swift_async_error	1
#  define __has_c_attribute‿_Clang‿__swift_async_error__	1
# endif
# if __has_c_attribute(clang::swiftcall)
#  define __has_c_attribute‿clang‿swiftcall	1
#  define __has_c_attribute‿__clang__‿swiftcall	1
#  define __has_c_attribute‿clang‿__swiftcall__	1
#  define __has_c_attribute‿__clang__‿__swiftcall__	1
#  define __has_c_attribute‿_Clang‿swiftcall	1
#  define __has_c_attribute‿_Clang‿__swiftcall__	1
# endif
# if __has_c_attribute(clang::swift_context)
#  define __has_c_attribute‿clang‿swift_context	1
#  define __has_c_attribute‿__clang__‿swift_context	1
#  define __has_c_attribute‿clang‿__swift_context__	1
#  define __has_c_attribute‿__clang__‿__swift_context__	1
#  define __has_c_attribute‿_Clang‿swift_context	1
#  define __has_c_attribute‿_Clang‿__swift_context__	1
# endif
# if __has_c_attribute(clang::swift_error_result)
#  define __has_c_attribute‿clang‿swift_error_result	1
#  define __has_c_attribute‿__clang__‿swift_error_result	1
#  define __has_c_attribute‿clang‿__swift_error_result__	1
#  define __has_c_attribute‿__clang__‿__swift_error_result__	1
#  define __has_c_attribute‿_Clang‿swift_error_result	1
#  define __has_c_attribute‿_Clang‿__swift_error_result__	1
# endif
# if __has_c_attribute(clang::swift_indirect_result)
#  define __has_c_attribute‿clang‿swift_indirect_result	1
#  define __has_c_attribute‿__clang__‿swift_indirect_result	1
#  define __has_c_attribute‿clang‿__swift_indirect_result__	1
#  define __has_c_attribute‿__clang__‿__swift_indirect_result__	1
#  define __has_c_attribute‿_Clang‿swift_indirect_result	1
#  define __has_c_attribute‿_Clang‿__swift_indirect_result__	1
# endif
# if __has_c_attribute(clang::sycl_kernel)
#  define __has_c_attribute‿clang‿sycl_kernel	1
#  define __has_c_attribute‿__clang__‿sycl_kernel	1
#  define __has_c_attribute‿clang‿__sycl_kernel__	1
#  define __has_c_attribute‿__clang__‿__sycl_kernel__	1
#  define __has_c_attribute‿_Clang‿sycl_kernel	1
#  define __has_c_attribute‿_Clang‿__sycl_kernel__	1
# endif
# if __has_c_attribute(clang::sycl_special_class)
#  define __has_c_attribute‿clang‿sycl_special_class	1
#  define __has_c_attribute‿__clang__‿sycl_special_class	1
#  define __has_c_attribute‿clang‿__sycl_special_class__	1
#  define __has_c_attribute‿__clang__‿__sycl_special_class__	1
#  define __has_c_attribute‿_Clang‿sycl_special_class	1
#  define __has_c_attribute‿_Clang‿__sycl_special_class__	1
# endif
# if __has_c_attribute(clang::test_typestate)
#  define __has_c_attribute‿clang‿test_typestate	1
#  define __has_c_attribute‿__clang__‿test_typestate	1
#  define __has_c_attribute‿clang‿__test_typestate__	1
#  define __has_c_attribute‿__clang__‿__test_typestate__	1
#  define __has_c_attribute‿_Clang‿test_typestate	1
#  define __has_c_attribute‿_Clang‿__test_typestate__	1
# endif
# if __has_c_attribute(clang::trivial_abi)
#  define __has_c_attribute‿clang‿trivial_abi	1
#  define __has_c_attribute‿__clang__‿trivial_abi	1
#  define __has_c_attribute‿clang‿__trivial_abi__	1
#  define __has_c_attribute‿__clang__‿__trivial_abi__	1
#  define __has_c_attribute‿_Clang‿trivial_abi	1
#  define __has_c_attribute‿_Clang‿__trivial_abi__	1
# endif
# if __has_c_attribute(clang::try_acquire_shared_capability)
#  define __has_c_attribute‿clang‿try_acquire_shared_capability	1
#  define __has_c_attribute‿__clang__‿try_acquire_shared_capability	1
#  define __has_c_attribute‿clang‿__try_acquire_shared_capability__	1
#  define __has_c_attribute‿__clang__‿__try_acquire_shared_capability__	1
#  define __has_c_attribute‿_Clang‿try_acquire_shared_capability	1
#  define __has_c_attribute‿_Clang‿__try_acquire_shared_capability__	1
# endif
# if __has_c_attribute(clang::type_tag_for_datatype)
#  define __has_c_attribute‿clang‿type_tag_for_datatype	1
#  define __has_c_attribute‿__clang__‿type_tag_for_datatype	1
#  define __has_c_attribute‿clang‿__type_tag_for_datatype__	1
#  define __has_c_attribute‿__clang__‿__type_tag_for_datatype__	1
#  define __has_c_attribute‿_Clang‿type_tag_for_datatype	1
#  define __has_c_attribute‿_Clang‿__type_tag_for_datatype__	1
# endif
# if __has_c_attribute(clang::type_visibility)
#  define __has_c_attribute‿clang‿type_visibility	1
#  define __has_c_attribute‿__clang__‿type_visibility	1
#  define __has_c_attribute‿clang‿__type_visibility__	1
#  define __has_c_attribute‿__clang__‿__type_visibility__	1
#  define __has_c_attribute‿_Clang‿type_visibility	1
#  define __has_c_attribute‿_Clang‿__type_visibility__	1
# endif
# if __has_c_attribute(clang::unavailable)
#  define __has_c_attribute‿clang‿unavailable	1
#  define __has_c_attribute‿__clang__‿unavailable	1
#  define __has_c_attribute‿clang‿__unavailable__	1
#  define __has_c_attribute‿__clang__‿__unavailable__	1
#  define __has_c_attribute‿_Clang‿unavailable	1
#  define __has_c_attribute‿_Clang‿__unavailable__	1
# endif
# if __has_c_attribute(clang::uninitialized)
#  define __has_c_attribute‿clang‿uninitialized	1
#  define __has_c_attribute‿__clang__‿uninitialized	1
#  define __has_c_attribute‿clang‿__uninitialized__	1
#  define __has_c_attribute‿__clang__‿__uninitialized__	1
#  define __has_c_attribute‿_Clang‿uninitialized	1
#  define __has_c_attribute‿_Clang‿__uninitialized__	1
# endif
# if __has_c_attribute(clang::unlikely)
#  define __has_c_attribute‿clang‿unlikely	1
#  define __has_c_attribute‿__clang__‿unlikely	1
#  define __has_c_attribute‿clang‿__unlikely__	1
#  define __has_c_attribute‿__clang__‿__unlikely__	1
#  define __has_c_attribute‿_Clang‿unlikely	1
#  define __has_c_attribute‿_Clang‿__unlikely__	1
# endif
# if __has_c_attribute(clang::unlock_function)
#  define __has_c_attribute‿clang‿unlock_function	1
#  define __has_c_attribute‿__clang__‿unlock_function	1
#  define __has_c_attribute‿clang‿__unlock_function__	1
#  define __has_c_attribute‿__clang__‿__unlock_function__	1
#  define __has_c_attribute‿_Clang‿unlock_function	1
#  define __has_c_attribute‿_Clang‿__unlock_function__	1
# endif
# if __has_c_attribute(clang::unsafe_buffer_usage)
#  define __has_c_attribute‿clang‿unsafe_buffer_usage	1
#  define __has_c_attribute‿__clang__‿unsafe_buffer_usage	1
#  define __has_c_attribute‿clang‿__unsafe_buffer_usage__	1
#  define __has_c_attribute‿__clang__‿__unsafe_buffer_usage__	1
#  define __has_c_attribute‿_Clang‿unsafe_buffer_usage	1
#  define __has_c_attribute‿_Clang‿__unsafe_buffer_usage__	1
# endif
# if __has_c_attribute(clang::use_handle)
#  define __has_c_attribute‿clang‿use_handle	1
#  define __has_c_attribute‿__clang__‿use_handle	1
#  define __has_c_attribute‿clang‿__use_handle__	1
#  define __has_c_attribute‿__clang__‿__use_handle__	1
#  define __has_c_attribute‿_Clang‿use_handle	1
#  define __has_c_attribute‿_Clang‿__use_handle__	1
# endif
# if __has_c_attribute(clang::using_if_exists)
#  define __has_c_attribute‿clang‿using_if_exists	1
#  define __has_c_attribute‿__clang__‿using_if_exists	1
#  define __has_c_attribute‿clang‿__using_if_exists__	1
#  define __has_c_attribute‿__clang__‿__using_if_exists__	1
#  define __has_c_attribute‿_Clang‿using_if_exists	1
#  define __has_c_attribute‿_Clang‿__using_if_exists__	1
# endif
# if __has_c_attribute(clang::vecreturn)
#  define __has_c_attribute‿clang‿vecreturn	1
#  define __has_c_attribute‿__clang__‿vecreturn	1
#  define __has_c_attribute‿clang‿__vecreturn__	1
#  define __has_c_attribute‿__clang__‿__vecreturn__	1
#  define __has_c_attribute‿_Clang‿vecreturn	1
#  define __has_c_attribute‿_Clang‿__vecreturn__	1
# endif
# if __has_c_attribute(clang::vectorcall)
#  define __has_c_attribute‿clang‿vectorcall	1
#  define __has_c_attribute‿__clang__‿vectorcall	1
#  define __has_c_attribute‿clang‿__vectorcall__	1
#  define __has_c_attribute‿__clang__‿__vectorcall__	1
#  define __has_c_attribute‿_Clang‿vectorcall	1
#  define __has_c_attribute‿_Clang‿__vectorcall__	1
# endif
# if __has_c_attribute(clang::warn_unused_result)
#  define __has_c_attribute‿clang‿warn_unused_result	1
#  define __has_c_attribute‿__clang__‿warn_unused_result	1
#  define __has_c_attribute‿clang‿__warn_unused_result__	1
#  define __has_c_attribute‿__clang__‿__warn_unused_result__	1
#  define __has_c_attribute‿_Clang‿warn_unused_result	1
#  define __has_c_attribute‿_Clang‿__warn_unused_result__	1
# endif
# if __has_c_attribute(clang::weak_import)
#  define __has_c_attribute‿clang‿weak_import	1
#  define __has_c_attribute‿__clang__‿weak_import	1
#  define __has_c_attribute‿clang‿__weak_import__	1
#  define __has_c_attribute‿__clang__‿__weak_import__	1
#  define __has_c_attribute‿_Clang‿weak_import	1
#  define __has_c_attribute‿_Clang‿__weak_import__	1
# endif
# if __has_c_attribute(clang::xray_always_instrument)
#  define __has_c_attribute‿clang‿xray_always_instrument	1
#  define __has_c_attribute‿__clang__‿xray_always_instrument	1
#  define __has_c_attribute‿clang‿__xray_always_instrument__	1
#  define __has_c_attribute‿__clang__‿__xray_always_instrument__	1
#  define __has_c_attribute‿_Clang‿xray_always_instrument	1
#  define __has_c_attribute‿_Clang‿__xray_always_instrument__	1
# endif
# if __has_c_attribute(clang::xray_log_args)
#  define __has_c_attribute‿clang‿xray_log_args	1
#  define __has_c_attribute‿__clang__‿xray_log_args	1
#  define __has_c_attribute‿clang‿__xray_log_args__	1
#  define __has_c_attribute‿__clang__‿__xray_log_args__	1
#  define __has_c_attribute‿_Clang‿xray_log_args	1
#  define __has_c_attribute‿_Clang‿__xray_log_args__	1
# endif
# if __has_c_attribute(clang::xray_never_instrument)
#  define __has_c_attribute‿clang‿xray_never_instrument	1
#  define __has_c_attribute‿__clang__‿xray_never_instrument	1
#  define __has_c_attribute‿clang‿__xray_never_instrument__	1
#  define __has_c_attribute‿__clang__‿__xray_never_instrument__	1
#  define __has_c_attribute‿_Clang‿xray_never_instrument	1
#  define __has_c_attribute‿_Clang‿__xray_never_instrument__	1
# endif
#endif /*__has_c_attribute*/
