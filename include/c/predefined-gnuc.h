// eĿlipsis specific implementations of macros
#include_source <ellipsis-predefined.h>
// determine the compiler that we are emulating
#ifndef __ARCH__
#environment __ARCH__ x86_64
#endif
#ifndef __OS__
#environment __OS__ linux
#endif
#ifndef __LIBC__
#environment __LIBC__ gnu
#endif
#ifndef __COMPN__
#environment __COMPN__ gcc
#endif
#ifndef __COMPV__
#environment __COMPV__ 13
#endif
//
#bind __PRE_FILE(P, C, N) <P ## _ ## C ## _ ## N.h>
#include_source <ellipsis-predefined-c-attribute.h>                     \
  /* Add support for gcc specific attributes. */                        \
  __suffix__(expand include_source __PRE_FILE(predefined_c_attribute, __COMPN__, __COMPV__) \
             __prefix__(bind HPRE __EXPAND__)                           \
             __prefix__(bind HSUF __EXPAND__))                          \
  __suffix__(expand include_source __PRE_FILE(predefined_c_attribute, __COMPN__, __COMPV__) \
             __prefix__(bind HPRE __UGLIFY__)                           \
             __prefix__(bind HSUF __EXPAND__))                          \
  __suffix__(expand include_source __PRE_FILE(predefined_c_attribute, __COMPN__, __COMPV__) \
             __prefix__(bind HPRE __EXPAND__)                           \
             __prefix__(bind HSUF __UGLIFY__))                          \
  __suffix__(expand include_source __PRE_FILE(predefined_c_attribute, __COMPN__, __COMPV__) \
             __prefix__(bind HPRE __UGLIFY__)                           \
             __prefix__(bind HSUF __UGLIFY__))
// The system directories. The first expands the version number inside
// the directory path.
#expand include_source <predefined-__COMPN__-__COMPV__-__ARCH__-__OS__-__LIBC__.h>
