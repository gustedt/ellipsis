⎔ once ELLIPSIS_PREDEFINED_H

/**
 ** @file
 ** @brief predefined macros needed by eĿlipsis
 **
 **/
# define __BRACE_LEVEL__ 0
# define __BRACKET_LEVEL__ 0
# define __ATTR_LEVEL__ 0

⎔ ifndef DOXYGEN_SPECIAL
⎔ define __has_c_attribute __has_c_attribute
⎔ define __building_module(…) 0
⎔ define __IGNORE__(…) __IGNORE(__VA_ARGS__)
⎔ define __IGNORE(…)
/* instrument braces */
⎔ define { \u007b
⎔ define } \u007d
⎔ bind __BRACE_INC __IGNORE__(__INCREMENT__(__BRACE_LEVEL__))
⎔ bind __BRACE_DEC __IGNORE__(__DECREMENT__(__BRACE_LEVEL__))
⎔ gather { __BRACE_INC
⎔ gather __BRACE_DEC }
⎔ gather } __BRACE_DEC
/* instrument brackets */
⎔ define [ \u005b
⎔ define ] \u005d
⎔ bind __BRACKET_INC __IGNORE__(__INCREMENT__(__BRACKET_LEVEL__))
⎔ bind __BRACKET_DEC __IGNORE__(__DECREMENT__(__BRACKET_LEVEL__))
⎔ gather [ __BRACKET_INC
⎔ gather __BRACKET_DEC ]
⎔ gather ] __BRACKET_DEC
⎔ define = \u003d
⎔ define ; \u003b
/* instrument attribute: this is special because of its interaction
   with brackets. In fact ]] may end an attribute or be ending two
   subscripts as in a[b[9]]. */
⎔ define [[ \u005b\u005b
⎔ define ]] \u005d\u005d
⎔ bind __ATTR_START __IGNORE__(__INCREMENT__(__ATTR_LEVEL__)__INCREMENT__(__BRACKET_LEVEL__)__INCREMENT__(__BRACKET_LEVEL__))
⎔ bind __ATTR_END __IGNORE__(__DECREMENT__(__BRACKET_LEVEL__)__DECREMENT__(__BRACKET_LEVEL__)__ATTR_END_(__BRACKET_LEVEL__, __ATTR_LEVEL__))
⎔ define __ATTR_END_(B, A) __MULTI__(__ATTR, END, B, A)
/* Only do something if all [] are closed and there had been an opening [[ */
⎔ define __ATTR_END_0_1 __DECREMENT__(__ATTR_LEVEL__)
⎔ gather [[ __ATTR_START
⎔ gather __ATTR_END ]]
⎔ gather ]] __ATTR_END
⎔ define ⟦ [[
⎔ define ⟧ ]]
/* Instrument ::. This has two different meanings according to context. */
⎔ define __ATTR_DCOLON_0 ,
⎔ define __DCOLON__ __ATTR_DCOLON__(__ATTR_LEVEL__)
⎔ define __ATTR_DCOLON__(C) __ATTR_DCOLON_(__MULTI__(⸤__ATTR_DCOLON⸥, C)0)
⎔ define __ATTR_DCOLON_(…) __ATTR_DCOLON(__VA_ARGS__)
⎔ define __ATTR_DCOLON(X,…) __ATTR_DCOLON_I ## __VA_OPT__(plus)
/* general context: name composition */
⎔ define __ATTR_DCOLON_Iplus ∷
/* attribute context: separate prefix and suffix */
⎔ define __ATTR_DCOLON_I \u003a\u003a
⎔ define :: __DCOLON__
⎔ endif

