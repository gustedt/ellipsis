#ifndef ELLIPSIS_ONCE_H
#define ELLIPSIS_ONCE_H

#include_directives <ellipsis-loc.h> __prefix__(bind LOC_NAME ONCE)
#include <stdlib.h>
#if __STDC_VERSION_STDLIB_H__ < 202311L
#include <threads.h>
#endif

/**
 ** @file
 **
 ** @brief Simple TU initialization and cleanup handling with
 ** dependencies.
 **
 ** This is a header only library as a small wrapper around
 ** `call_once` and `atexit`. It has just a handful user
 ** interfaces. For triggered dependencies these are
 **
 ** - @ref ONCE_DEFINE
 ** - @ref ONCE_DEPEND
 ** - @ref ONCE_ATEXIT
 ** - @ref ONCE_AT_QUICK_EXIT
 **
 ** For unconditional dependencies (that should always run) there are
 **
 ** - @ref ONCE_DEFINE_STRONG
 ** - @ref ONCE_DEPEND_WEAK
 **
 ** To fully work `ONCE_DEFINE_STRONG` needs compiler magic, here we
 ** use a gnu feature. `ONCE_DEPEND_WEAK` then triggers a dependency
 ** just as `ONCE_DEPEND` but only if running code unconditionally at
 ** startup is not supported. Otherwise this is a no-op.
 **
 ** Currently there is no detection of dependency cycles, but a graph
 ** can be extracted from the strings of the binary. It should only
 ** have cycles between a symbolic dependency and the TU in which is
 ** found.
 **
 ** There is a script called `dot_extract.sh` that filters the string
 ** of the executable to obtain the graph, which basically looks as
 ** follows:
 **
 ** ```
 ** echo "digraph \"$1\" {"
 ** strings $1 | sed '
 **        /ONCE_DEPEND_MARKER:/!d
 **        s!ONCE_DEPEND_MARKER:!\t!
 **        s![a-zA-Z_][-+./a-zA-Z0-9_]*!"&"!g
 **'
 **echo "}"
 ** ```
 **
 **/

/**
 ** @name Internal macros
 **/
/**@{*/
/** @internal*/
#define _ONCE_INIT(NAME) NAME ∷_Once∷init
#define _ONCE_USER(NAME) NAME ∷_Once∷init∷user
#define _ONCE_CALLBACK(NAME) NAME ∷_Once∷init∷callback
#define _ONCE_ATEXIT_POINTER(NAME) NAME ∷_Once∷atexit∷pointer
#define _ONCE_ATEXIT_CALLBACK(NAME) NAME ∷_Once∷atexit∷callback
#define _ONCE_AT_QUICK_EXIT_POINTER(NAME) NAME ∷_Once∷at_quick_exit∷pointer
#define _ONCE_AT_QUICK_EXIT_CALLBACK(NAME) NAME ∷_Once∷at_quick_exit∷callback
#define _ONCE_STRONG_FLAG(NAME) NAME ∷_Once∷strong

#define _ONCE_DOTTY_ARC(A, B) "ONCE_DEPEND_MARKER: " A  " -> " B
#define _ONCE_DOTTY_NODE(A, ...) "ONCE_DEPEND_MARKER: " A  " [" #__VA_ARGS__ "]"


#ifdef __ELLIPSIS__
# define _ONCE_DOTTY_FILE ⸤__FILE__⸥
#else
# define _ONCE_DOTTY_FILE __FILE__
#endif

<|
#define _ONCE_DECLARE(NAME)
  [[maybe_unused]] extern bool const _ONCE_STRONG_FLAG(NAME);
  void _ONCE_INIT(NAME)(void)
|>

#define _ONCE_DEPEND(...) _ONCE_DEPEND_I(__VA_ARGS__ __VA_OPT__(,) __UNIT__,)

<|
#define _ONCE_DEPEND_I(NAME, ...)
 _ONCE_DECLARE(NAME);
 GNU_ATTR_USED [[__maybe_unused__]]
 static char const ONCE_NEW[]
                       = _ONCE_DOTTY_ARC(_ONCE_DOTTY_FILE, __STRINGIFY__(NAME));
 _ONCE_INIT(NAME)()
|>

#define _ONCE_DEFINE(...) _ONCE_DEFINE_I(__VA_ARGS__ __VA_OPT__(,) __UNIT__,)

<|
#define _ONCE_DEFINE_I(NAME, ...)
/*^*/  static void _ONCE_USER(NAME)(void);
/*^*/  static void (*const _ONCE_ATEXIT_POINTER(NAME))(void);
/*^*/  static void (*const _ONCE_AT_QUICK_EXIT_POINTER(NAME))(void);
/*^*/  static void _ONCE_CALLBACK(NAME)(void) {
/*^*/    GNU_ATTR_USED [[__maybe_unused__]]
/*^*/      static char const ONCE_NEW[]
/*^*/      = _ONCE_DOTTY_ARC(__STRINGIFY__(NAME), _ONCE_DOTTY_FILE)
/*^*/      "\n" _ONCE_DOTTY_NODE(__STRINGIFY__(NAME), shape=box,color=blue);
/*^*/    _ONCE_USER(NAME)();
/*^*/    if (_ONCE_ATEXIT_POINTER(NAME)) {
/*^*/      atexit(_ONCE_ATEXIT_POINTER(NAME));
/*^*/    }
/*^*/    if (_ONCE_AT_QUICK_EXIT_POINTER(NAME)) {
/*^*/      at_quick_exit(_ONCE_AT_QUICK_EXIT_POINTER(NAME));
/*^*/    }
/*^*/  }
/*^*/  void _ONCE_INIT(NAME)(void) {
/*^*/    static once_flag ONCE_NEW = ONCE_FLAG_INIT;
/*^*/    call_once(&ONCE(), _ONCE_CALLBACK(NAME));
/*^*/  }
/*^*/  static void _ONCE_USER(NAME)(void)
|>

#define _ONCE_DEPEND_WEAK(...) _ONCE_DEPEND_WEAK_I(__VA_ARGS__ __VA_OPT__(,) __UNIT__,)

#if __has_c_attribute(__gnu__::__constructor__)
<|
#define _ONCE_DEPEND_WEAK_I(NAME, ...)
/*^*/  _ONCE_DECLARE(NAME);
/*^*/  GNU_ATTR_USED [[__maybe_unused__]]
/*^*/  static char const ONCE_NEW[]
/*^*/    = _ONCE_DOTTY_ARC(_ONCE_DOTTY_FILE, __STRINGIFY__(NAME)) " [color=red] ";
/*^*/  GNU_ATTR_USED [[__maybe_unused__]]
/*^*/  static bool const*const ONCE_NEW = &_ONCE_STRONG_FLAG(NAME)
|>

#define _ONCE_IS_STRONG
#define _ONCE_STRONG_INIT = true
#else

<|
#define _ONCE_DEPEND_WEAK_I(NAME, ...)
/*^*/  _ONCE_DECLARE(NAME);
/*^*/  GNU_ATTR_USED [[__maybe_unused__]]
/*^*/  static char const ONCE_NEW[]
/*^*/    = _ONCE_DOTTY_ARC(_ONCE_DOTTY_FILE, __STRINGIFY__(NAME)) " [color=red] ";
/*^*/  ONCE_DEPEND(NAME)
|>

#define _ONCE_IS_STRONG extern
#define _ONCE_STRONG_INIT
#endif

#define _ONCE_DEFINE_STRONG(...) _ONCE_DEFINE_STRONG_I(__VA_ARGS__ __VA_OPT__(,) __UNIT__,)

<|
#define _ONCE_DEFINE_STRONG_I(NAME, ...)
/*^*/  static void _ONCE_USER(NAME)(void);
/*^*/  static void (*const _ONCE_ATEXIT_POINTER(NAME))(void);
/*^*/  static void _ONCE_CALLBACK(NAME)(void) {
/*^*/    GNU_ATTR_USED [[__maybe_unused__]]
/*^*/      static char const ONCE_NEW[]
/*^*/      = _ONCE_DOTTY_ARC(__STRINGIFY__(NAME), _ONCE_DOTTY_FILE)
/*^*/      "\n" _ONCE_DOTTY_NODE(__STRINGIFY__(NAME), shape=box,color=red);
/*^*/    _ONCE_USER(NAME)();
/*^*/    if (_ONCE_ATEXIT_POINTER(NAME)) {
/*^*/      atexit(_ONCE_ATEXIT_POINTER(NAME));
/*^*/    }
/*^*/  }
/*^*/  [[__maybe_unused__]] _ONCE_IS_STRONG bool const _ONCE_STRONG_FLAG(NAME) _ONCE_STRONG_INIT;
/*^*/  [[__gnu__::__constructor__]]
/*^*/  void _ONCE_INIT(NAME)(void) {
/*^*/    static once_flag ONCE_NEW = ONCE_FLAG_INIT;
/*^*/    call_once(&ONCE(), _ONCE_CALLBACK(NAME));
/*^*/  }
/*^*/  static void _ONCE_USER(NAME)(void)
|>

#define _ONCE_ATEXIT(...) _ONCE_ATEXIT_I(__VA_ARGS__ __VA_OPT__(,) __UNIT__,)

<|
#define _ONCE_ATEXIT_I(NAME, ...)
/*^*/  static void _ONCE_ATEXIT_CALLBACK(NAME)(void);
/*^*/  static void (*const _ONCE_ATEXIT_POINTER(NAME))(void)
/*^*/  = _ONCE_ATEXIT_CALLBACK(NAME);
/*^*/  static void _ONCE_ATEXIT_CALLBACK(NAME)(void)
|>

#define _ONCE_AT_QUICK_EXIT(...) _ONCE_AT_QUICK_EXIT_I(__VA_ARGS__ __VA_OPT__(,) __UNIT__,)

<|
#define _ONCE_AT_QUICK_EXIT_I(NAME, ...)
/*^*/  static void _ONCE_AT_QUICK_EXIT_CALLBACK(NAME)(void);
/*^*/  static void (*const _ONCE_AT_QUICK_EXIT_POINTER(NAME))(void)
/*^*/  = _ONCE_AT_QUICK_EXIT_CALLBACK(NAME);
/*^*/  static void _ONCE_AT_QUICK_EXIT_CALLBACK(NAME)(void)
|>


/**@}*/

/**
 ** @brief Declare that this TU depends on a initialization and
 ** cleanup feature named `NAME`
 **
 ** Use this in block scope to refer to the one external symbol that
 ** can be used by other TU to indicate a dependency of this TU here.
 **
 ** ```
 ** ONCE_DEPEND(toto);
 ** ```
 **
 ** The external name is not portable between different implementations.
 **
 ** Under the hood that is a simple function call of a `void`
 ** function.
 **
 ** This can be placed anywhere where a declaration is allowed.
 **/
#define ONCE_DEPEND(...) _ONCE_DEPEND(__VA_ARGS__)

/**
 ** @brief Define the initialization code that goes with feature named
 ** `NAME`
 **
 ** Use this as a prefix to a function definition such as in
 **
 ** ```
 ** ONCE_DEFINE(toto) {
 **   ONCE_DEPEND(tata);
 **   // do your stuff
 ** }
 ** ```
 **
 ** This creates some symbols with internal linkage and one external
 ** symbol, but with a naming that is not portable between different
 ** implementations.
 **
 ** Technically this creates separate functions and a `once_flag` that
 ** are then used with `call_once`.
 **
 ** Use this, even in the same TU, as `ONCE_DEPEND(toto)` to be sure
 ** that your initialization code is guaranteed to have run before any
 ** of your other code.
 **/
#define ONCE_DEFINE(...) _ONCE_DEFINE(__VA_ARGS__)

/**
 ** @brief Define optional cleanup code that goes with feature
 ** named `NAME` for `atexit`
 **
 ** Use this as a prefix to a function definition such as in
 **
 ** ```
 ** ONCE_ATEXIT(toto) {
 **   // do your stuff
 ** }
 ** ```
 **
 ** This creates some symbols with internal linkage in the current
 ** TU. For this to work `ONCE_DEFINE(toto)` has to be defined in the
 ** same TU. The other way around, this is not mandatory:
 ** `ONCE_DEFINE(toto)` can work without having `ONCE_ATEXIT(toto)`.
 **
 ** Technically this creates a functions and a function pointer is
 ** then used with `atexit`. All implicitly defined symbols have
 ** internal linkage.
 **/
#define ONCE_ATEXIT(...) _ONCE_ATEXIT(__VA_ARGS__)

/**
 ** @brief Define optional cleanup code that goes with feature
 ** named `NAME` for `quick_exit`
 **
 ** Use this as a prefix to a function definition such as in
 **
 ** ```
 ** ONCE_AT_QUICK_EXIT(toto) {
 **   // do your stuff
 ** }
 ** ```
 **
 ** This creates some symbols with internal linkage in the current
 ** TU. For this to work `ONCE_DEFINE(toto)` has to be defined in the
 ** same TU. The other way around, this is not mandatory:
 ** `ONCE_DEFINE(toto)` can work without having `ONCE_AT_QUICK_EXIT(toto)`.
 **
 ** Technically this creates a functions and a function pointer is
 ** then used with `at_quick_exit`. All implicitly defined symbols
 ** have internal linkage.
 **/
#define ONCE_AT_QUICK_EXIT(...) _ONCE_AT_QUICK_EXIT(__VA_ARGS__)

/**
 ** @brief Similar to `ONCE_DEPEND` but only triggers if
 ** `ONCE_DEFINE_STRONG` is not able to guarantee unconditional
 ** initialization.
 **
 ** This can be placed anywhere where a declaration is allowed.
 **/
#define ONCE_DEPEND_WEAK(...) _ONCE_DEPEND_WEAK(__VA_ARGS__)

/**
 ** @brief Similar to `ONCE_DEFINE` but triggers unconditionally if
 ** the platform supports this.
 **/
#define ONCE_DEFINE_STRONG(...) _ONCE_DEFINE_STRONG(__VA_ARGS__)

#endif

