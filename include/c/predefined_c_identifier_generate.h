#ifdef __is_identifier
# if !__is_identifier(_Atomic)
#  define __is_keyword‿_Atomic	,
# endif
# if !__is_identifier(_BitInt)
#  define __is_keyword‿_BitInt	,
# endif
# if !__is_identifier(_Complex)
#  define __is_keyword‿_Complex	,
# endif
# if !__is_identifier(_Decimal128)
#  define __is_keyword‿_Decimal128	,
# endif
# if !__is_identifier(_Decimal32)
#  define __is_keyword‿_Decimal32	,
# endif
# if !__is_identifier(_Decimal64)
#  define __is_keyword‿_Decimal64	,
# endif
# if !__is_identifier(_Generic)
#  define __is_keyword‿_Generic	,
# endif
# if !__is_identifier(_Imaginary)
#  define __is_keyword‿_Imaginary	,
# endif
# if !__is_identifier(_Noreturn)
#  define __is_keyword‿_Noreturn	,
# endif
# if !__is_identifier(alignas)
#  define __is_keyword‿alignas	,
# endif
# if !__is_identifier(alignof)
#  define __is_keyword‿alignof	,
# endif
# if !__is_identifier(auto)
#  define __is_keyword‿auto	,
# endif
# if !__is_identifier(bool)
#  define __is_keyword‿bool	,
# endif
# if !__is_identifier(break)
#  define __is_keyword‿break	,
# endif
# if !__is_identifier(case)
#  define __is_keyword‿case	,
# endif
# if !__is_identifier(char)
#  define __is_keyword‿char	,
# endif
# if !__is_identifier(const)
#  define __is_keyword‿const	,
# endif
# if !__is_identifier(constexpr)
#  define __is_keyword‿constexpr	,
# endif
# if !__is_identifier(continue)
#  define __is_keyword‿continue	,
# endif
# if !__is_identifier(default)
#  define __is_keyword‿default	,
# endif
# if !__is_identifier(do)
#  define __is_keyword‿do	,
# endif
# if !__is_identifier(double)
#  define __is_keyword‿double	,
# endif
# if !__is_identifier(else)
#  define __is_keyword‿else	,
# endif
# if !__is_identifier(enum)
#  define __is_keyword‿enum	,
# endif
# if !__is_identifier(extern)
#  define __is_keyword‿extern	,
# endif
# if !__is_identifier(false)
#  define __is_keyword‿false	,
# endif
# if !__is_identifier(float)
#  define __is_keyword‿float	,
# endif
# if !__is_identifier(for)
#  define __is_keyword‿for	,
# endif
# if !__is_identifier(goto)
#  define __is_keyword‿goto	,
# endif
# if !__is_identifier(if)
#  define __is_keyword‿if	,
# endif
# if !__is_identifier(inline)
#  define __is_keyword‿inline	,
# endif
# if !__is_identifier(int)
#  define __is_keyword‿int	,
# endif
# if !__is_identifier(long)
#  define __is_keyword‿long	,
# endif
# if !__is_identifier(nullptr)
#  define __is_keyword‿nullptr	,
# endif
# if !__is_identifier(register)
#  define __is_keyword‿register	,
# endif
# if !__is_identifier(restrict)
#  define __is_keyword‿restrict	,
# endif
# if !__is_identifier(return)
#  define __is_keyword‿return	,
# endif
# if !__is_identifier(short)
#  define __is_keyword‿short	,
# endif
# if !__is_identifier(signed)
#  define __is_keyword‿signed	,
# endif
# if !__is_identifier(sizeof)
#  define __is_keyword‿sizeof	,
# endif
# if !__is_identifier(static)
#  define __is_keyword‿static	,
# endif
# if !__is_identifier(static_assert)
#  define __is_keyword‿static_assert	,
# endif
# if !__is_identifier(struct)
#  define __is_keyword‿struct	,
# endif
# if !__is_identifier(switch)
#  define __is_keyword‿switch	,
# endif
# if !__is_identifier(thread_local)
#  define __is_keyword‿thread_local	,
# endif
# if !__is_identifier(true)
#  define __is_keyword‿true	,
# endif
# if !__is_identifier(typedef)
#  define __is_keyword‿typedef	,
# endif
# if !__is_identifier(typeof)
#  define __is_keyword‿typeof	,
# endif
# if !__is_identifier(typeof_unqual)
#  define __is_keyword‿typeof_unqual	,
# endif
# if !__is_identifier(union)
#  define __is_keyword‿union	,
# endif
# if !__is_identifier(unsigned)
#  define __is_keyword‿unsigned	,
# endif
# if !__is_identifier(void)
#  define __is_keyword‿void	,
# endif
# if !__is_identifier(volatile)
#  define __is_keyword‿volatile	,
# endif
# if !__is_identifier(while)
#  define __is_keyword‿while	,
# endif
#endif /*__is_identifier*/
