
#include <stdio.h>
int main(void) {

#if defined(__has_c_attribute)
# if __has_c_attribute(_Noreturn)
	puts("#ifndef __has_c_attribute‿_Noreturn\n#xdefine __has_c_attribute‿_Noreturn __EVALUATE__(202311L)\n#endif\n#ifndef __has_c_attribute‿___Noreturn__\n#xdefine __has_c_attribute‿___Noreturn__ __EVALUATE__(202311L)\n#endif");
# endif
#endif
#if defined(__has_c_attribute)
# if __has_c_attribute(deprecated)
	puts("#ifndef __has_c_attribute‿deprecated\n#xdefine __has_c_attribute‿deprecated __EVALUATE__(202311L)\n#endif\n#ifndef __has_c_attribute‿__deprecated__\n#xdefine __has_c_attribute‿__deprecated__ __EVALUATE__(202311L)\n#endif");
# endif
#endif
#if defined(__has_c_attribute)
# if __has_c_attribute(fallthrough)
	puts("#ifndef __has_c_attribute‿fallthrough\n#xdefine __has_c_attribute‿fallthrough __EVALUATE__(202311L)\n#endif\n#ifndef __has_c_attribute‿__fallthrough__\n#xdefine __has_c_attribute‿__fallthrough__ __EVALUATE__(202311L)\n#endif");
# endif
#endif
#if defined(__has_c_attribute)
# if __has_c_attribute(maybe_unused)
	puts("#ifndef __has_c_attribute‿maybe_unused\n#xdefine __has_c_attribute‿maybe_unused __EVALUATE__(202311L)\n#endif\n#ifndef __has_c_attribute‿__maybe_unused__\n#xdefine __has_c_attribute‿__maybe_unused__ __EVALUATE__(202311L)\n#endif");
# endif
#endif
#if defined(__has_c_attribute)
# if __has_c_attribute(nodiscard)
	puts("#ifndef __has_c_attribute‿nodiscard\n#xdefine __has_c_attribute‿nodiscard __EVALUATE__(202311L)\n#endif\n#ifndef __has_c_attribute‿__nodiscard__\n#xdefine __has_c_attribute‿__nodiscard__ __EVALUATE__(202311L)\n#endif");
# endif
#endif
#if defined(__has_c_attribute)
# if __has_c_attribute(noreturn)
	puts("#ifndef __has_c_attribute‿noreturn\n#xdefine __has_c_attribute‿noreturn __EVALUATE__(202311L)\n#endif\n#ifndef __has_c_attribute‿__noreturn__\n#xdefine __has_c_attribute‿__noreturn__ __EVALUATE__(202311L)\n#endif");
# endif
#endif
#if defined(__has_c_attribute)
# if __has_c_attribute(reproducible)
	puts("#ifndef __has_c_attribute‿reproducible\n#xdefine __has_c_attribute‿reproducible __EVALUATE__(202311L)\n#endif\n#ifndef __has_c_attribute‿__reproducible__\n#xdefine __has_c_attribute‿__reproducible__ __EVALUATE__(202311L)\n#endif");
# endif
#endif
#if defined(__has_c_attribute)
# if __has_c_attribute(unsequenced)
	puts("#ifndef __has_c_attribute‿unsequenced\n#xdefine __has_c_attribute‿unsequenced __EVALUATE__(202311L)\n#endif\n#ifndef __has_c_attribute‿__unsequenced__\n#xdefine __has_c_attribute‿__unsequenced__ __EVALUATE__(202311L)\n#endif");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::acces)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,acces)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::alias)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,alias)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::aligned)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,aligned)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::alloc_align)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,alloc_align)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::alloc_size)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,alloc_size)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::always_inline)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,always_inline)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::artificial)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,artificial)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::assume_aligned)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,assume_aligned)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::cleanup)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,cleanup)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::cold)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,cold)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::common)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,common)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::const)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,const)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::constructor)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,constructor)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::copy)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,copy)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::counted_by)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,counted_by)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::deprecated)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,deprecated)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::destructor)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,destructor)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::error)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,error)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::expected_throw)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,expected_throw)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::externally_visible)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,externally_visible)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::fd_arg)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,fd_arg)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::fd_arg_read)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,fd_arg_read)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::fd_arg_write)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,fd_arg_write)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::flatten)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,flatten)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::format)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,format)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::format_arg)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,format_arg)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::gnu_inline)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,gnu_inline)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::hot)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,hot)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::ifunc)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,ifunc)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::interrupt)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,interrupt)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::interrupt_handler)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,interrupt_handler)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::leaf)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,leaf)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::malloc)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,malloc)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::mode)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,mode)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_address_safety_analysis)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_address_safety_analysis)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_icf)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_icf)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_instrument_function)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_instrument_function)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_profile_instrument_function)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_profile_instrument_function)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_reorder)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_reorder)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_sanitize)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_sanitize)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_sanitize_address)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_sanitize_address)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_sanitize_coverage)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_sanitize_coverage)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_sanitize_thread)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_sanitize_thread)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_sanitize_undefined)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_sanitize_undefined)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_split_stack)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_split_stack)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_stack_limit)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_stack_limit)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::no_stack_protector)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,no_stack_protector)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::noclone)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,noclone)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::nocommon)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,nocommon)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::noinit)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,noinit)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::noipa)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,noipa)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::nonnull)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,nonnull)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::nonstring)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,nonstring)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::noplt)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,noplt)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::noreturn)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,noreturn)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::nothrow)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,nothrow)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::optimize)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,optimize)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::packed)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,packed)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::patchable_function_entry)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,patchable_function_entry)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::persistent)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,persistent)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::pure)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,pure)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::retain)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,retain)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::returns_nonnull)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,returns_nonnull)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::returns_twice)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,returns_twice)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::section)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,section)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::sentinel)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,sentinel)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::simd)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,simd)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::stack_protect)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,stack_protect)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::strict_flex_array)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,strict_flex_array)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::symver)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,symver)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::tainted_args)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,tainted_args)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::target)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,target)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::target_clones)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,target_clones)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::tls_model)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,tls_model)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::unavailable)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,unavailable)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::uninitialized)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,uninitialized)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::unused)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,unused)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::used)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,used)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::vector_size)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,vector_size)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::visibility)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,visibility)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::warn_if_not_aligned)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,warn_if_not_aligned)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::warn_unused_result)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,warn_unused_result)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::warning)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,warning)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::weak)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,weak)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::weakref)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,weakref)");
# endif
#endif
#ifdef __has_c_attribute
# if __has_c_attribute(gnu::zero_call_used_regs)
	puts("#xdefine __HAS_C_ATTRIBUTE(gnu,zero_call_used_regs)");
# endif
#endif

}

