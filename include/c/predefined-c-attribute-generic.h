// This installs the feature and ensures that eĿlipsis is conforming to C23.
#define __has_c_attribute___ __has_c_attribute___

// This establishes a mechanism to register attributes. This
// distinguishes two cases, standard attributes and prefixed
// attributes. For both we have a mechanism such that also the double
// underscore versions are handled. For standard attributes this is
// achieved by binding the macro HSUF to __EXPAND__ and __UGLIFY__,
// respectively, to produce the two different variants. For prefixed
// attributes additionally HPRE is bound to these two values, such
// that in total the four variants are produced.
//
// The macro may be called with one, two, or three arguments, where
// additionally the second and third may be empty.
//
#define __HAS_C_ATTRIBUTE(PRE, ...) __HAS_C_ATTRIBUTE_I ## __VA_OPT__(plus) (PRE __VA_OPT__(,) __VA_ARGS__)
#define __HAS_C_ATTRIBUTE_Iplus(PRE, SUF, ...) __HAS_C_ATTRIBUTE_II ## __VA_OPT__(plus) (PRE, SUF __VA_OPT__(,) __VA_ARGS__)

// If the second argument is empty, the to-be-defined attribute is a
// standard attribute, named by the first argument. It is an error if
// the third argument is also empty because standard attributes should
// always expand to a number that corresponds to a version number
// date; we can't guess that by ourselves.
#define __HAS_C_ATTRIBUTE_I(PRE) __HAS_C_ATTRIBUTE_I_(HPRE(PRE), __STRINGIFY__(PRE))
#define __HAS_C_ATTRIBUTE_I_(PRE, SPRE)                                 \
  __HAS_C_ATTRIBUTE_MANGLE                                              \
  (PRE,                                                                 \
   ,                                                                    \
   __ERROR__(standard attribute SPRE: configuration shall be of the form yyyymmL))


// If the first and second arguments are present this is a prefixed
// attribute. A value of 1 is guessed if there is no third argument,
// because that seems to be the general policy of vendors, here.
#bind __HAS_C_ATTRIBUTE_II(PRE, SUF) __HAS_C_ATTRIBUTE_MANGLE(PRE, SUF, __ONE__)
#bind __HAS_C_ATTRIBUTE_IIplus(PRE, SUF, ...) __HAS_C_ATTRIBUTE_MANGLE(PRE, SUF, __VA_ARGS__)


#bind __HAS_C_ATTRIBUTE_MANGLE(PRE, SUF, VAL) __HAS_C_ATTRIBUTE_XDEFINE(__HAS_C_ATTRIBUTE_MANGLE2(PRE, SUF), VAL)
#bind __HAS_C_ATTRIBUTE_XDEFINE(NAME, VAL) __has_c_attribute‿ ## NAME __EVALUATE__(VAL)

#bind __HAS_C_ATTRIBUTE_MANGLE2(PRE, ...) __HAS_C_ATTRIBUTE_MANGLE2_I ## __VA_OPT__(plus) (PRE __VA_OPT__(,) __VA_ARGS__)
#bind __HAS_C_ATTRIBUTE_MANGLE2_I(NAME) __HAS_C_ATTRIBUTE_MANGLE_(HSUF(NAME))
#bind __HAS_C_ATTRIBUTE_MANGLE2_Iplus(PRE, ...) __HAS_C_ATTRIBUTE_MANGLE_(HPRE(PRE) :: HSUF(__VA_ARGS__))
#bind __HAS_C_ATTRIBUTE_MANGLE_(...) __MANGLE__(__VA_ARGS__)
