#ifdef __has_feature
# if __has_extension(address_sanitizer)
#  define __has_extension‿address_sanitizer	,
# endif
# if __has_feature(address_sanitizer)
#  define __has_feature‿address_sanitizer	,
# endif
# if __has_extension(attribute_deprecated_with_message)
#  define __has_extension‿attribute_deprecated_with_message	,
# endif
# if __has_feature(attribute_deprecated_with_message)
#  define __has_feature‿attribute_deprecated_with_message	,
# endif
# if __has_extension(attribute_unavailable_with_message)
#  define __has_extension‿attribute_unavailable_with_message	,
# endif
# if __has_feature(attribute_unavailable_with_message)
#  define __has_feature‿attribute_unavailable_with_message	,
# endif
# if __has_extension(blocks)
#  define __has_extension‿blocks	,
# endif
# if __has_feature(blocks)
#  define __has_feature‿blocks	,
# endif
# if __has_extension(c_alignas)
#  define __has_extension‿c_alignas	,
# endif
# if __has_feature(c_alignas)
#  define __has_feature‿c_alignas	,
# endif
# if __has_extension(c_alignof)
#  define __has_extension‿c_alignof	,
# endif
# if __has_feature(c_alignof)
#  define __has_feature‿c_alignof	,
# endif
# if __has_extension(c_atomic)
#  define __has_extension‿c_atomic	,
# endif
# if __has_feature(c_atomic)
#  define __has_feature‿c_atomic	,
# endif
# if __has_extension(c_generic_selections)
#  define __has_extension‿c_generic_selections	,
# endif
# if __has_feature(c_generic_selections)
#  define __has_feature‿c_generic_selections	,
# endif
# if __has_extension(c_generic_selection_with_controlling_type)
#  define __has_extension‿c_generic_selection_with_controlling_type	,
# endif
# if __has_feature(c_generic_selection_with_controlling_type)
#  define __has_feature‿c_generic_selection_with_controlling_type	,
# endif
# if __has_extension(c_static_assert)
#  define __has_extension‿c_static_assert	,
# endif
# if __has_feature(c_static_assert)
#  define __has_feature‿c_static_assert	,
# endif
# if __has_extension(c_thread_local)
#  define __has_extension‿c_thread_local	,
# endif
# if __has_feature(c_thread_local)
#  define __has_feature‿c_thread_local	,
# endif
# if __has_extension(cxx_access_control_sfinae)
#  define __has_extension‿cxx_access_control_sfinae	,
# endif
# if __has_feature(cxx_access_control_sfinae)
#  define __has_feature‿cxx_access_control_sfinae	,
# endif
# if __has_extension(cxx_aggregate_nsdmi)
#  define __has_extension‿cxx_aggregate_nsdmi	,
# endif
# if __has_feature(cxx_aggregate_nsdmi)
#  define __has_feature‿cxx_aggregate_nsdmi	,
# endif
# if __has_extension(cxx_alias_templates)
#  define __has_extension‿cxx_alias_templates	,
# endif
# if __has_feature(cxx_alias_templates)
#  define __has_feature‿cxx_alias_templates	,
# endif
# if __has_extension(cxx_alignas)
#  define __has_extension‿cxx_alignas	,
# endif
# if __has_feature(cxx_alignas)
#  define __has_feature‿cxx_alignas	,
# endif
# if __has_extension(cxx_alignof)
#  define __has_extension‿cxx_alignof	,
# endif
# if __has_feature(cxx_alignof)
#  define __has_feature‿cxx_alignof	,
# endif
# if __has_extension(cxx_attributes)
#  define __has_extension‿cxx_attributes	,
# endif
# if __has_feature(cxx_attributes)
#  define __has_feature‿cxx_attributes	,
# endif
# if __has_extension(cxx_attributes_on_using_declarations)
#  define __has_extension‿cxx_attributes_on_using_declarations	,
# endif
# if __has_feature(cxx_attributes_on_using_declarations)
#  define __has_feature‿cxx_attributes_on_using_declarations	,
# endif
# if __has_extension(cxx_auto_type)
#  define __has_extension‿cxx_auto_type	,
# endif
# if __has_feature(cxx_auto_type)
#  define __has_feature‿cxx_auto_type	,
# endif
# if __has_extension(cxx_binary_literals)
#  define __has_extension‿cxx_binary_literals	,
# endif
# if __has_feature(cxx_binary_literals)
#  define __has_feature‿cxx_binary_literals	,
# endif
# if __has_extension(cxx_constexpr)
#  define __has_extension‿cxx_constexpr	,
# endif
# if __has_feature(cxx_constexpr)
#  define __has_feature‿cxx_constexpr	,
# endif
# if __has_extension(cxx_constexpr_string_builtins)
#  define __has_extension‿cxx_constexpr_string_builtins	,
# endif
# if __has_feature(cxx_constexpr_string_builtins)
#  define __has_feature‿cxx_constexpr_string_builtins	,
# endif
# if __has_extension(cxx_contextual_conversions)
#  define __has_extension‿cxx_contextual_conversions	,
# endif
# if __has_feature(cxx_contextual_conversions)
#  define __has_feature‿cxx_contextual_conversions	,
# endif
# if __has_extension(cxx_decltype)
#  define __has_extension‿cxx_decltype	,
# endif
# if __has_feature(cxx_decltype)
#  define __has_feature‿cxx_decltype	,
# endif
# if __has_extension(cxx_decltype_auto)
#  define __has_extension‿cxx_decltype_auto	,
# endif
# if __has_feature(cxx_decltype_auto)
#  define __has_feature‿cxx_decltype_auto	,
# endif
# if __has_extension(cxx_decltype_incomplete_return_types)
#  define __has_extension‿cxx_decltype_incomplete_return_types	,
# endif
# if __has_feature(cxx_decltype_incomplete_return_types)
#  define __has_feature‿cxx_decltype_incomplete_return_types	,
# endif
# if __has_extension(cxx_defaulted_functions)
#  define __has_extension‿cxx_defaulted_functions	,
# endif
# if __has_feature(cxx_defaulted_functions)
#  define __has_feature‿cxx_defaulted_functions	,
# endif
# if __has_extension(cxx_default_function_template_args)
#  define __has_extension‿cxx_default_function_template_args	,
# endif
# if __has_feature(cxx_default_function_template_args)
#  define __has_feature‿cxx_default_function_template_args	,
# endif
# if __has_extension(cxx_delegating_constructors)
#  define __has_extension‿cxx_delegating_constructors	,
# endif
# if __has_feature(cxx_delegating_constructors)
#  define __has_feature‿cxx_delegating_constructors	,
# endif
# if __has_extension(cxx_deleted_functions)
#  define __has_extension‿cxx_deleted_functions	,
# endif
# if __has_feature(cxx_deleted_functions)
#  define __has_feature‿cxx_deleted_functions	,
# endif
# if __has_extension(cxx_exceptions)
#  define __has_extension‿cxx_exceptions	,
# endif
# if __has_feature(cxx_exceptions)
#  define __has_feature‿cxx_exceptions	,
# endif
# if __has_extension(cxx_explicit_conversions)
#  define __has_extension‿cxx_explicit_conversions	,
# endif
# if __has_feature(cxx_explicit_conversions)
#  define __has_feature‿cxx_explicit_conversions	,
# endif
# if __has_extension(cxx_generalized_initializers)
#  define __has_extension‿cxx_generalized_initializers	,
# endif
# if __has_feature(cxx_generalized_initializers)
#  define __has_feature‿cxx_generalized_initializers	,
# endif
# if __has_extension(cxx_generic_lambdas)
#  define __has_extension‿cxx_generic_lambdas	,
# endif
# if __has_feature(cxx_generic_lambdas)
#  define __has_feature‿cxx_generic_lambdas	,
# endif
# if __has_extension(cxx_implicit_moves)
#  define __has_extension‿cxx_implicit_moves	,
# endif
# if __has_feature(cxx_implicit_moves)
#  define __has_feature‿cxx_implicit_moves	,
# endif
# if __has_extension(cxx_inheriting_constructors)
#  define __has_extension‿cxx_inheriting_constructors	,
# endif
# if __has_feature(cxx_inheriting_constructors)
#  define __has_feature‿cxx_inheriting_constructors	,
# endif
# if __has_extension(cxx_init_captures)
#  define __has_extension‿cxx_init_captures	,
# endif
# if __has_feature(cxx_init_captures)
#  define __has_feature‿cxx_init_captures	,
# endif
# if __has_extension(cxx_inline_namespaces)
#  define __has_extension‿cxx_inline_namespaces	,
# endif
# if __has_feature(cxx_inline_namespaces)
#  define __has_feature‿cxx_inline_namespaces	,
# endif
# if __has_extension(cxx_lambdas)
#  define __has_extension‿cxx_lambdas	,
# endif
# if __has_feature(cxx_lambdas)
#  define __has_feature‿cxx_lambdas	,
# endif
# if __has_extension(cxx_local_type_template_args)
#  define __has_extension‿cxx_local_type_template_args	,
# endif
# if __has_feature(cxx_local_type_template_args)
#  define __has_feature‿cxx_local_type_template_args	,
# endif
# if __has_extension(cxx_noexcept)
#  define __has_extension‿cxx_noexcept	,
# endif
# if __has_feature(cxx_noexcept)
#  define __has_feature‿cxx_noexcept	,
# endif
# if __has_extension(cxx_nonstatic_member_init)
#  define __has_extension‿cxx_nonstatic_member_init	,
# endif
# if __has_feature(cxx_nonstatic_member_init)
#  define __has_feature‿cxx_nonstatic_member_init	,
# endif
# if __has_extension(cxx_nullptr)
#  define __has_extension‿cxx_nullptr	,
# endif
# if __has_feature(cxx_nullptr)
#  define __has_feature‿cxx_nullptr	,
# endif
# if __has_extension(cxx_override_control)
#  define __has_extension‿cxx_override_control	,
# endif
# if __has_feature(cxx_override_control)
#  define __has_feature‿cxx_override_control	,
# endif
# if __has_extension(cxx_range_for)
#  define __has_extension‿cxx_range_for	,
# endif
# if __has_feature(cxx_range_for)
#  define __has_feature‿cxx_range_for	,
# endif
# if __has_extension(cxx_raw_string_literals)
#  define __has_extension‿cxx_raw_string_literals	,
# endif
# if __has_feature(cxx_raw_string_literals)
#  define __has_feature‿cxx_raw_string_literals	,
# endif
# if __has_extension(cxx_reference_qualified_functions)
#  define __has_extension‿cxx_reference_qualified_functions	,
# endif
# if __has_feature(cxx_reference_qualified_functions)
#  define __has_feature‿cxx_reference_qualified_functions	,
# endif
# if __has_extension(cxx_relaxed_constexpr)
#  define __has_extension‿cxx_relaxed_constexpr	,
# endif
# if __has_feature(cxx_relaxed_constexpr)
#  define __has_feature‿cxx_relaxed_constexpr	,
# endif
# if __has_extension(cxx_return_type_deduction)
#  define __has_extension‿cxx_return_type_deduction	,
# endif
# if __has_feature(cxx_return_type_deduction)
#  define __has_feature‿cxx_return_type_deduction	,
# endif
# if __has_extension(cxx_rtti)
#  define __has_extension‿cxx_rtti	,
# endif
# if __has_feature(cxx_rtti)
#  define __has_feature‿cxx_rtti	,
# endif
# if __has_extension(cxx_runtime_array)
#  define __has_extension‿cxx_runtime_array	,
# endif
# if __has_feature(cxx_runtime_array)
#  define __has_feature‿cxx_runtime_array	,
# endif
# if __has_extension(cxx_rvalue_references)
#  define __has_extension‿cxx_rvalue_references	,
# endif
# if __has_feature(cxx_rvalue_references)
#  define __has_feature‿cxx_rvalue_references	,
# endif
# if __has_extension(cxx_static_assert)
#  define __has_extension‿cxx_static_assert	,
# endif
# if __has_feature(cxx_static_assert)
#  define __has_feature‿cxx_static_assert	,
# endif
# if __has_extension(cxx_strong_enums)
#  define __has_extension‿cxx_strong_enums	,
# endif
# if __has_feature(cxx_strong_enums)
#  define __has_feature‿cxx_strong_enums	,
# endif
# if __has_extension(cxx_thread_local)
#  define __has_extension‿cxx_thread_local	,
# endif
# if __has_feature(cxx_thread_local)
#  define __has_feature‿cxx_thread_local	,
# endif
# if __has_extension(cxx_trailing_return)
#  define __has_extension‿cxx_trailing_return	,
# endif
# if __has_feature(cxx_trailing_return)
#  define __has_feature‿cxx_trailing_return	,
# endif
# if __has_extension(cxx_unicode_literals)
#  define __has_extension‿cxx_unicode_literals	,
# endif
# if __has_feature(cxx_unicode_literals)
#  define __has_feature‿cxx_unicode_literals	,
# endif
# if __has_extension(cxx_unrestricted_unions)
#  define __has_extension‿cxx_unrestricted_unions	,
# endif
# if __has_feature(cxx_unrestricted_unions)
#  define __has_feature‿cxx_unrestricted_unions	,
# endif
# if __has_extension(cxx_user_literals)
#  define __has_extension‿cxx_user_literals	,
# endif
# if __has_feature(cxx_user_literals)
#  define __has_feature‿cxx_user_literals	,
# endif
# if __has_extension(cxx_variable_templates)
#  define __has_extension‿cxx_variable_templates	,
# endif
# if __has_feature(cxx_variable_templates)
#  define __has_feature‿cxx_variable_templates	,
# endif
# if __has_extension(cxx_variadic_templates)
#  define __has_extension‿cxx_variadic_templates	,
# endif
# if __has_feature(cxx_variadic_templates)
#  define __has_feature‿cxx_variadic_templates	,
# endif
# if __has_extension(dataflow_sanitizer)
#  define __has_extension‿dataflow_sanitizer	,
# endif
# if __has_feature(dataflow_sanitizer)
#  define __has_feature‿dataflow_sanitizer	,
# endif
# if __has_extension(enumerator_attributes)
#  define __has_extension‿enumerator_attributes	,
# endif
# if __has_feature(enumerator_attributes)
#  define __has_feature‿enumerator_attributes	,
# endif
# if __has_extension(gnu_asm_goto_with_outputs)
#  define __has_extension‿gnu_asm_goto_with_outputs	,
# endif
# if __has_feature(gnu_asm_goto_with_outputs)
#  define __has_feature‿gnu_asm_goto_with_outputs	,
# endif
# if __has_extension(gnu_asm_goto_with_outputs_full)
#  define __has_extension‿gnu_asm_goto_with_outputs_full	,
# endif
# if __has_feature(gnu_asm_goto_with_outputs_full)
#  define __has_feature‿gnu_asm_goto_with_outputs_full	,
# endif
# if __has_extension(memory_sanitizer)
#  define __has_extension‿memory_sanitizer	,
# endif
# if __has_feature(memory_sanitizer)
#  define __has_feature‿memory_sanitizer	,
# endif
# if __has_extension(modules)
#  define __has_extension‿modules	,
# endif
# if __has_feature(modules)
#  define __has_feature‿modules	,
# endif
# if __has_extension(objc_arc)
#  define __has_extension‿objc_arc	,
# endif
# if __has_feature(objc_arc)
#  define __has_feature‿objc_arc	,
# endif
# if __has_extension(objc_arc_fields)
#  define __has_extension‿objc_arc_fields	,
# endif
# if __has_feature(objc_arc_fields)
#  define __has_feature‿objc_arc_fields	,
# endif
# if __has_extension(objc_arc_weak)
#  define __has_extension‿objc_arc_weak	,
# endif
# if __has_feature(objc_arc_weak)
#  define __has_feature‿objc_arc_weak	,
# endif
# if __has_extension(objc_array_literals)
#  define __has_extension‿objc_array_literals	,
# endif
# if __has_feature(objc_array_literals)
#  define __has_feature‿objc_array_literals	,
# endif
# if __has_extension(objc_default_synthesize_properties)
#  define __has_extension‿objc_default_synthesize_properties	,
# endif
# if __has_feature(objc_default_synthesize_properties)
#  define __has_feature‿objc_default_synthesize_properties	,
# endif
# if __has_extension(objc_dictionary_literals)
#  define __has_extension‿objc_dictionary_literals	,
# endif
# if __has_feature(objc_dictionary_literals)
#  define __has_feature‿objc_dictionary_literals	,
# endif
# if __has_extension(objc_fixed_enum)
#  define __has_extension‿objc_fixed_enum	,
# endif
# if __has_feature(objc_fixed_enum)
#  define __has_feature‿objc_fixed_enum	,
# endif
# if __has_extension(objc_instancetype)
#  define __has_extension‿objc_instancetype	,
# endif
# if __has_feature(objc_instancetype)
#  define __has_feature‿objc_instancetype	,
# endif
# if __has_extension(objc_protocol_qualifier_mangling)
#  define __has_extension‿objc_protocol_qualifier_mangling	,
# endif
# if __has_feature(objc_protocol_qualifier_mangling)
#  define __has_feature‿objc_protocol_qualifier_mangling	,
# endif
# if __has_extension(objc_subscripting)
#  define __has_extension‿objc_subscripting	,
# endif
# if __has_feature(objc_subscripting)
#  define __has_feature‿objc_subscripting	,
# endif
# if __has_extension(pragma_clang_attribute_namespaces)
#  define __has_extension‿pragma_clang_attribute_namespaces	,
# endif
# if __has_feature(pragma_clang_attribute_namespaces)
#  define __has_feature‿pragma_clang_attribute_namespaces	,
# endif
# if __has_extension(safe_stack)
#  define __has_extension‿safe_stack	,
# endif
# if __has_feature(safe_stack)
#  define __has_feature‿safe_stack	,
# endif
# if __has_extension(thread_sanitizer)
#  define __has_extension‿thread_sanitizer	,
# endif
# if __has_feature(thread_sanitizer)
#  define __has_feature‿thread_sanitizer	,
# endif
#endif /*__has_feature*/
