⎔ if defined(DOXYGEN_SPECIAL)
⎔  define __LOC(POS0, POS1)
⎔  define __LOC_NEW
⎔  define LOC_NAME __LOC
⎔ endif
⎔ if ¬defined(LOC_NAME)
⎔  define LOC_NAME __LOC
⎔ endif
⎔ expand __if__ ¬⸤defined⸥(LOC_NAME)

⎔ expand __bind__ UNIQUE_NAME LOC_NAME

⎔ expand __bind__ __TMP_UP   __MULTI__(LOC_NAME, ⸤UP⸥)
⎔ expand __bind__ __TMP_DOWN __MULTI__(LOC_NAME, ⸤DOWN⸥)
⎔ gather { __TMP_UP
⎔ gather __TMP_DOWN }
⎔ gather } __TMP_DOWN

⎔ include_directives ⟨ellipsis-unique.dirs⟩
⎔ endif
