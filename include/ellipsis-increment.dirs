⎔⎔ @file
⎔⎔
⎔⎔ @brief An eĿlipsis extension to increment a macro holding an integer
⎔⎔
⎔⎔ Use this such as in
⎔⎔
⎔⎔ ```{.C}
⎔⎔  %:include_directives ⟨ellipsis-increment.dirs⟩           \
⎔⎔     __prefix__(bind VAR A)
⎔⎔ ```
⎔⎔
⎔⎔ If prior to this `A()` evaluates to the integer value `19` this assigns the token `20` to the macro `A()`.

⎔ ifdef DOXYGEN_SPECIAL
⎔⎔ @brief The name of the macro to which we will assign
⎔⎔
⎔⎔ Note that the macro is a functional macro. For its evaluation you
⎔⎔ have to add parentheses as in `VAR()`.
⎔⎔
⎔ define VAR()

⎔ else

⎔ ifndef VAR
⎔ error the macro VAR needs to be bound to the name of the name of a functional macro

⎔ endif

⎔ undef EXPR
⎔ include_directives ⟨ellipsis-assign.dirs⟩                      \
  prefix(bind EXPR __EXPAND_DEC__(VAR() + 1))

⎔ endif
