# This makefile is only needed to produce the documentation
#
# Only produce the tar file when on the branch doxygen
#
# There run
#     make doxygen
#     make tar
#
# Then commit the file "doxy-html.tar" and push to the remote.

tar :
	tar cf doxy-html.tar doxy-html

doxy :
	bin/ellipsis -I. -I../.. -DDOXYGEN_SPECIAL -DELLIPSIS_CODE_LANGUAGE -Iinclude/html -x html ellipsis-footer.raw > ellipsis-footer.html
	bin/ellipsis -I. -I../.. -DDOXYGEN_SPECIAL -DELLIPSIS_CODE_LANGUAGE -x md README.dirs > README.md
	doxygen Doxyfile

README.md : README.dirs
	bin/ellipsis -I. -I../.. -DELLIPSIS_CODE_LANGUAGE -x md $< > $@
