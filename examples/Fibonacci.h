/** @file
 ** @brief Compute Fibonacci numbers by recursive inclusion
 **
 ** This shows how real recursive inclusion can be easily with tools
 ** like @ref gather and @ref move, and by evaluating expressions by
 ** means of @ref expand and @ref __EXPAND_DEC_U__ (expand, decimal,
 ** unsigned).
 **
 ** See the source code by following the link above and the result in
 ** a compact view by simply launching
 **
 ** ```{.shell}
 ** ellipsis Fibonacci.h
 ** ```
 **
 ** If you want to see how the compiler would see a file preprocess by
 ** ellipsis-gnuc.sh
 **
 ** ```{.shell}
 ** ellipsis-gnuc.sh -E Fibonacci.h
 ** ```
 **
 ** The output here is annotated by directives with the source line
 ** number. Here, since most of the numbers come from the same source
 ** line this just resets the current line number to that same number
 ** again and again.
 **/
# ifndef FIBONACCI
/*
 * This the initial execution that sets up all the macros. They are
 * created with @ref bind so they will cease to exist, once this upper
 * level of recursion is terminated.
 */

#  bind FIB0() 0U
#  bind FIB1() 1U
#  bind FIB2()

/*
 * This macro collects all the values by means of @ref gather,
 * later. There are special comments, that help to indent (the ones
 * with a >) and to skip to the next line (the one with the ^).
 */
#  bind FIBONACCI() /*>*/0U, /*^*//*>*/1U

/*
 * And then the top level recursive call. We only want to expand the
 * directives, so we use @ref include_directives, but we also want to
 * expand the name of the file to this same file so we prefix this
 * directive with @ref expand.
 */
#  expand include_directives __FILE__

/*
 * @brief An array with the first __NARGS__(FIBONACCI()) Fibonacci numbers
 *
 * It calls @ref FIBONACCI twice, once for the number of elements
 * (counted with @ref __NARGS__) and then for the initializer. Most
 * probably you will see that the integers here are 64 bit wide and
 * that there are 94 Fibonacci numbers that fit into 64 bit.
 *
 * But what do we know about your platform? So we compute the value of
 * the biggest unsigned number and ask C23's `typeof` to give us the
 * correct type where all these Fibonacci numbers fit.
 */
constexpr typeof(__EXPAND_DEC_U__(-1U)) Fibonacci[__NARGS__(FIBONACCI())] = {
FIBONACCI()
};
/*
 * Check for possible overflow. We check if the sum of the two current
 * values would be greater than the biggest unsigned value that the
 * preprocessor can handle.
 */
# elif FIB1() <= ((-1U)-FIB0())
/*
 * Set FIB2() to that new value which is the sum of the two current
 * values. This sum is evaluated and expanded before being assigned to
 * FIB2().
 */
#  undef FIB2
#  expand bind FIB2() __EXPAND_DEC_U__(FIB0() + FIB1())
/* collect the contents of the expansion of FIB2() to the end of
   FIBONACCI. Here the macro @ref ELLIPSIS_MARK_LINE is either empty
   (normal operation of `ellipsis`) or adds the line number operation
   (when using ellipsis-gnuc.sh) */
#  expand gather FIBONACCI ,/*^*/ELLIPSIS_MARK_LINE/*>*/FIB2()
/* shift of FIB2 → FIB1 → FIB0. Here these macros are not expanded, it
   is their definitions that are shifted around. */
#  move FIB0 FIB1
#  move FIB1 FIB2
/*
 * And then next recursive call. The ideas are the same as for the
 * first.
 */
#  expand include_directives __FILE__
/*
 * There is no else clause, so recursion stops as soon as the @ref
 * elif condition is not fulfilled any more.
 */
# endif
