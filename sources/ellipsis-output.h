
#ifndef ELLIPSIS_OUTPUT_H
#define ELLIPSIS_OUTPUT_H
#include "utils/ellipsis-str32.h"
#include <stdio.h>

extern int ellipsis‿c_punctuators‿fputs(ellipsis‿str32 const*restrict s, FILE *stream);
extern int (*ellipsis‿output‿punctuators)(ellipsis‿str32 const*restrict s, FILE *stream);
extern int (*ellipsis‿output‿others)(ellipsis‿str32 const*restrict s, FILE *stream);
int ellipsis‿output‿cont(ellipsis‿str32 const s32[static restrict 1], FILE* out);
int ellipsis‿output‿space(ellipsis‿str32 const s32[static restrict 1], FILE* out);

thrd_t ellipsis‿output‿thread(ellipsis‿token‿list[static 1]);
thrd_t ellipsis‿output‿verbose(ellipsis‿token‿list[static 1]);

#endif
