
/**
 ** @file
 **
 ** @warning This is a file generated by eĿlipsis version 20250219, do not modify
 **/
#include <stdlib.h>
#ifndef ONCE_INIT
#include <threads.h>
#endif
#include "language/ellipsis-special.h"

#include "ellipsis-token.h"
#include "ellipsis-tlist.h"
#include "utils/ellipsis-error.h"

/** @cond NEVER_EVER **/
typeof(ellipsis‿token‿alloc) ellipsis‿token‿alloc;
typeof(ellipsis‿token‿cmove) ellipsis‿token‿cmove;
typeof(ellipsis‿token‿cpy) ellipsis‿token‿cpy;
typeof(ellipsis‿token‿deepcpy) ellipsis‿token‿deepcpy;
typeof(ellipsis‿token‿delete) ellipsis‿token‿delete;
typeof(ellipsis‿token‿delete_all) ellipsis‿token‿delete_all;
typeof(ellipsis‿token‿destroy) ellipsis‿token‿destroy;
typeof(ellipsis‿token‿drop) ellipsis‿token‿drop;
typeof(ellipsis‿token‿equiv) ellipsis‿token‿equiv;
typeof(ellipsis‿token‿is_inner) ellipsis‿token‿is_inner;
typeof(ellipsis‿token‿move) ellipsis‿token‿move;
typeof(ellipsis‿token‿new) ellipsis‿token‿new;
typeof(ellipsis‿token‿nominal) ellipsis‿token‿nominal;
typeof(ellipsis‿token‿number) ellipsis‿token‿number;
typeof(ellipsis‿token‿pop) ellipsis‿token‿pop;
typeof(ellipsis‿token‿push) ellipsis‿token‿push;
typeof(ellipsis‿token‿repeat) ellipsis‿token‿repeat;
typeof(ellipsis‿token‿string) ellipsis‿token‿string
/** @endcond **/
/** @file **/
/** @remark This translation unit implements @ref ellipsis‿token::ellipsis‿token‿alloc, @ref ellipsis‿token::ellipsis‿token‿cmove, @ref ellipsis‿token::ellipsis‿token‿cpy, @ref ellipsis‿token::ellipsis‿token‿deepcpy, @ref ellipsis‿token::ellipsis‿token‿delete, @ref ellipsis‿token::ellipsis‿token‿delete_all, @ref ellipsis‿token::ellipsis‿token‿destroy, @ref ellipsis‿token::ellipsis‿token‿drop, @ref ellipsis‿token::ellipsis‿token‿equiv, @ref ellipsis‿token::ellipsis‿token‿is_inner, @ref ellipsis‿token::ellipsis‿token‿move, @ref ellipsis‿token::ellipsis‿token‿new, @ref ellipsis‿token::ellipsis‿token‿nominal, @ref ellipsis‿token::ellipsis‿token‿number, @ref ellipsis‿token::ellipsis‿token‿pop, @ref ellipsis‿token::ellipsis‿token‿push, @ref ellipsis‿token::ellipsis‿token‿repeat, @ref ellipsis‿token::ellipsis‿token‿string **/;

void ellipsis‿token‿destroy(ellipsis‿token t[static 1]) {
  ellipsis‿str32‿delete(t->contents);
  ellipsis‿str32‿delete(t->space);
  if (t->payload) {
    switch (t->is_what) {
    default:
      ellipsis‿token‿list‿delete(t->payload);
      break;
    case ellipsis‿token‿what‿binary:
      ellipsis‿str8‿delete(t->data);
      break;
    case ellipsis‿token‿what‿callback:
      break;
    }
  }
  *t  = (ellipsis‿token) {};
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfree-nonheap-object"

static tss_t garb_key;
static thread_local bool garb_canary;
static thread_local ellipsis‿token* garb;

static
void garb_dtor(void* t) {
  ellipsis‿token** g  = t;
  while (*g) {
    free(ellipsis‿token‿pop(g));
  }
}

static void ellipsis‿token‿_Once‿init‿user(void);
static void (*const ellipsis‿token‿_Once‿atexit‿pointer)(void);
static void (*const ellipsis‿token‿_Once‿at_quick_exit‿pointer)(void);
static void ellipsis‿token‿_Once‿init‿callback(void) {
  GNU_ATTR_USED [[__maybe_unused__]]
  static char const ONCE_ID_1_1[]
    = "ONCE_DEPEND_MARKER: " "ellipsis∷token"" -> " __FILE__
      "\n" "ONCE_DEPEND_MARKER: " "ellipsis∷token"" [" "shape=box,color=blue" "]";
  ellipsis‿token‿_Once‿init‿user();
  if (ellipsis‿token‿_Once‿atexit‿pointer) {
    atexit(ellipsis‿token‿_Once‿atexit‿pointer);
  }
  if (ellipsis‿token‿_Once‿at_quick_exit‿pointer) {
    at_quick_exit(ellipsis‿token‿_Once‿at_quick_exit‿pointer);
  }
}
void ellipsis‿token‿_Once‿init(void) {
  static once_flag ONCE_ID_1_2 = {0 };
  call_once(&ONCE_ID_1_2, ellipsis‿token‿_Once‿init‿callback);
}
static void ellipsis‿token‿_Once‿init‿user(void) {
  tss_create(&garb_key, garb_dtor);
}

static void ellipsis‿token‿_Once‿atexit‿callback(void);
static void (*const ellipsis‿token‿_Once‿atexit‿pointer)(void)
  = ellipsis‿token‿_Once‿atexit‿callback;
static void ellipsis‿token‿_Once‿atexit‿callback(void) {

  garb_dtor(&garb);
  tss_delete(garb_key);
}

#define garb_start() \
 do {\
  \
 if (!garb_canary) {\
  \
 [[maybe_unused]] extern bool const ellipsis‿token‿_Once‿strong; void ellipsis‿token‿_Once‿init(void); GNU_ATTR_USED [[__maybe_unused__]] static char const ONCE_ID_2_1[] = "ONCE_DEPEND_MARKER: " __FILE__" -> " "ellipsis∷token"; ellipsis‿token‿_Once‿init(); \
 tss_set(garb_key, &garb); \
 garb_canary  = true; \
 } \
 } while(false)

ellipsis‿token* ellipsis‿token‿alloc(void) {
  garb_start();
  ellipsis‿token* nt  = ellipsis‿token‿pop(&garb);
  if (!nt) {
    nt  = malloc(sizeof(ellipsis‿token));
  }
  return nt;
}

void ellipsis‿token‿delete(ellipsis‿token t[static 1]) {
  garb_start();
  ellipsis‿token‿destroy(t);
  ellipsis‿token‿push(&garb, t);
}

#pragma GCC diagnostic pop

ellipsis‿token* ellipsis‿token‿cpy(ellipsis‿token const* t) {
  ellipsis‿token* ret = nullptr;
  if (t) {
    ret = TOKEN_NEW(
            .contents = (t->contents) ? ellipsis‿str32‿cpy_const(t->contents) : nullptr,
            .space  = (t->space) ? ellipsis‿str32‿cpy_const(t->space) : nullptr,
            .line = t->line,
            .file = t->file,
            .cat  = t->cat,
            .value  = t->value,
          );
  } else {
    ret = TOKEN_NEW();
  }
  return ret;
}

static
ellipsis‿token* ellipsis‿token‿local_cpy(ellipsis‿token const* t) {
  ellipsis‿token* ret = ellipsis‿token‿cpy(t);
  if (t && ret) {
    ret->is_what  = t->is_what;

    if (t->payload || t->callback || t->data) {
      switch (t->is_what) {
      default:
        ret->payload  = ellipsis‿token‿list‿cpy(t->payload);
        if (!ret->payload) {
          ELLIPSIS_ERROR("unable to copy payload member during deep token copy");
        }
        break;
      case ellipsis‿token‿what‿callback:
        ret->callback = t->callback;
        break;
      case ellipsis‿token‿what‿binary:
        ret->data = ellipsis‿str8‿cpy(t->data);
        if (!ret->data) {
          ELLIPSIS_ERROR("unable to copy data member during deep token copy");
        }
        break;
      }
    }
  }
  return ret;
}

ellipsis‿token* ellipsis‿token‿deepcpy(ellipsis‿token const* t) {
  ellipsis‿token* ret = ellipsis‿token‿local_cpy(t);
  if (t) {
    for (ellipsis‿token* act  = ret; t->next; act = act->next, t  = t->next) {
      act->next = ellipsis‿token‿local_cpy(t->next);
      if (!act->next) {
        ELLIPSIS_ERROR("unable to copy token list during deep token copy");
      }
    }
  }
  return ret;
}

bool ellipsis‿token‿equiv(ellipsis‿token* a, ellipsis‿token* b) {
  if (!a && !b) return true;
  if (!a || !b) return false;
  if (a->contents || b->contents) {
    if (a->contents && b->contents) {
      if (ellipsis‿str32‿compare(a->contents, b->contents)) {
        if (a->cat == parameter && b->cat == parameter) {
          if (a->value == b->value) {
            ELLIPSIS_WARNING("test for token equivalence failed because of different parameter names\n");
            fprintf(stderr, "\t\t");
            ellipsis‿str32‿fputs(a->contents, stderr);
            fprintf(stderr, " and ");
            ellipsis‿str32‿fputs(b->contents, stderr);
            fprintf(stderr, "\n");
          }
        }
        return false;
      }
    } else {
      return false;
    }
  }

  return ellipsis‿token‿list‿equiv(a->payload, b->payload);
}

ellipsis‿token* ellipsis‿token‿string(char const str[static 1]) {
  size_t blen = strlen(str) + 3;
  char buf[blen];
  size_t len  = snprintf(buf, sizeof buf, "\"%s\"", str);
  return TOKEN_NEW(.contents  = ellipsis‿str32‿construct(len, buf),
                   .cat = string);
}

ellipsis‿token* ellipsis‿token‿nominal(char const id[static 1]) {
  return TOKEN_NEW(.contents  = ellipsis‿str32‿construct(strlen(id), id),
                   .cat = nominal);
}

ellipsis‿token* ellipsis‿token‿number(vtype numb) {
  char buf[sizeof(vtype)*3+1];
  size_t len  = snprintf(buf, sizeof buf, "%llu", (long long unsigned)numb);
  return TOKEN_NEW(
           .contents  = ellipsis‿str32‿construct(len, buf),
           .cat = numeral);
}

void ellipsis‿token‿delete_all(ellipsis‿token act[static 1]) {
  for (ellipsis‿token* t  = ellipsis‿token‿pop(&act); t; t  = ellipsis‿token‿pop(&act)) {
    ellipsis‿token‿delete(t);
  }
}
