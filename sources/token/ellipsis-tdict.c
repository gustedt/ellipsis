
/**
 ** @file
 **
 ** @warning This is a file generated by eĿlipsis version 20250219, do not modify
 **/

#include "ellipsis-tdict.h"
#include "utils/ellipsis-hash.h"
#include "utils/ellipsis-error.h"
#include "ellipsis-tlist.h"
#include "ellipsis-macros.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#define ELLIPSIS_FA_UNDEPRECATE [[__deprecated__("you should not see this")]]

constexpr auto qot  = 9U;
constexpr auto num  = 17U;
constexpr auto φ = 7540113804746346429.0L / 12200160415121876738.0L;

static
void ellipsis‿token‿dictionary‿maintain(ellipsis‿token‿dictionary* dict);

/**
 ** @brief Compute gcd and relative inverse `(r, inv)` of two numbers
 ** `(a, m)` and return them in place
 **
 ** `inv` fulfills `inv * a == r (m)`, so in particular if `r` is `1`,
 ** `inv` is the multiplicative inverse of `a` in the ring Zₘ.
 **
 ** If `m` is `0`, the computation is done as if it where `max+1`,
 ** where `max` is the maximum value of the base type.
 **/
static
void ellipsis‿token‿dictionary‿inverse_inplace(uint64_t res[static 2]) {
  typedef typeof(res[0]) inv_type;
enum : inv_type {inv_max  = (inv_type)-1, };
  static_assert(0 < inv_max, "base type must be an unsigned type");
  inv_type const a  = res[0];
  inv_type const m  = res[1];

  inv_type q
    = m
      ? m
      : inv_max/a + ((inv_max%a) == (a-1));

  inv_type tmp[2] = {a, 1, };
  res[0]  = m;
  res[1]  = 0;
  for (;;) {
    inv_type const rem[2] = {
      res[0] - q * tmp[0],
      res[1] - q * tmp[1],
    };
    res[0]  = tmp[0];
    res[1]  = tmp[1];
    tmp[0]  = rem[0];
    tmp[1]  = rem[1];
    if (!tmp[0]) /* downgraded */ break;
    q = res[0] / tmp[0];
  };
  if (m) res[1] %= m;
}

ELLIPSIS_FA_UNDEPRECATE
static
size_t ellipsis‿token‿dictionary‿find_pos(ellipsis‿token‿dictionary* dict, ellipsis‿str32 const* s) {

  size_t h  = -1;
  if (dict) {
    ++dict->tries;
    if (s) {
      if (!s->length) {
        ELLIPSIS_BUG("dictionary search with empty string");
      }
      h = (ellipsis‿hash(s->length, s->array)*dict->fibfac) % dict->tab->length;
      for (; dict->tab->array[h]; h = (h+1) % dict->tab->length) {
        ++dict->comparisons;
        if (!ellipsis‿str32‿compare(dict->tab->array[h]->contents, s))
          /* downgraded */ break;
      }
    }
  }
  return h;
}

ELLIPSIS_FA_UNDEPRECATE
token* ellipsis‿token‿dictionary‿find(ellipsis‿token‿dictionary* dict, ellipsis‿str32 const* s) {
  token* ret  = nullptr;
  if (dict && s) {
    size_t h  = ellipsis‿token‿dictionary‿find_pos(dict, s);
    ret = dict->tab->array[h];
    if (ret && ret->cat == removed) {
      ret = nullptr;
    }
  }
  return ret;
}

ELLIPSIS_FA_UNDEPRECATE
bool ellipsis‿token‿dictionary‿insert(ellipsis‿token‿dictionary* dict, token* t) {
  if (!dict || !t) return false;
  size_t h  = ellipsis‿token‿dictionary‿find_pos(dict, t->contents);
  if (dict->tab->array[h]) {
    if ((dict->tab->array[h]->cat == removed) || ellipsis‿token‿equiv(t, dict->tab->array[h])) {
      if (dict->tab->array[h]->cat != removed) {
        ELLIPSIS_DIAGNOSTIC("inserting equivalent contents for key into dictionary, ignored");
        if (ellipsis‿error‿verb) {
          fputs("\tkey is: ", stderr);
          ellipsis‿str32‿fputs(t->contents, stderr);
          fputc('\n', stderr);
        }
      }
      ellipsis‿token‿move(&dict->tab->array[h], &t);
      return true;
    } else {
      return false;
    }
  }
  ellipsis‿token‿dictionary‿maintain(dict);

  h = ellipsis‿token‿dictionary‿find_pos(dict, t->contents);
  dict->tab->array[h] = t;
  dict->keys++;
  return true;
}

ELLIPSIS_FA_UNDEPRECATE
static
token* ellipsis‿token‿dictionary‿catch(ellipsis‿token‿dictionary* dict, ellipsis‿str32 const* s) {
  token* ret  = nullptr;
  if (dict && s) {
    size_t h  = ellipsis‿token‿dictionary‿find_pos(dict, s);
    ret = dict->tab->array[h];
    if (ret) {
      ret->cat  = removed;
    }
  }
  return ret;
}

ELLIPSIS_FA_UNDEPRECATE
void ellipsis‿token‿dictionary‿remove(ellipsis‿token‿dictionary* dict, ellipsis‿str32 const* s) {
  if (dict && s) {
    token* t  = ellipsis‿token‿dictionary‿catch(dict, s);
    if (t) {
      if (t->is_undefinable) {
        --dict->keys;
        ++dict->xeys;
      } else {
        ELLIPSIS_ERROR("attempt to undefine a predefined macro");
      }
    }
  }
}

ELLIPSIS_FA_UNDEPRECATE
static
void ellipsis‿token‿dictionary‿resize(ellipsis‿token‿dictionary* dict, size_t nlen) {
  size_t keys = 0;
  for (size_t i = 0; i < dict->tab->length; ++i) {
    if (dict->tab->array[i]
        && dict->tab->array[i]->cat != removed) {
      ++keys;
    }
  }
  if (nlen < (keys*num)/qot) {
    nlen  = (keys*num)/qot;
  }
  ellipsis‿token‿dictionary cp  = {};
  ellipsis‿token‿dictionary‿init(&cp, nlen);
  for (size_t i = 0; i < dict->tab->length; ++i) {
    if (dict->tab->array[i]) {
      if (dict->tab->array[i]->cat == removed) {
        ellipsis‿token‿delete(dict->tab->array[i]);
      } else {
        ellipsis‿token‿dictionary‿insert(&cp, dict->tab->array[i]);
      }
    }
    dict->tab->array[i] = nullptr;
  }
  ellipsis‿token‿array‿move(&dict->tab, &cp.tab);
  dict->keys  = cp.keys;
  dict->fibfac  = cp.fibfac;
}

ELLIPSIS_FA_UNDEPRECATE
static
void ellipsis‿token‿dictionary‿maintain(ellipsis‿token‿dictionary* dict) {
  while (dict->keys > (dict->tab->length*qot)/num || (dict->xeys > dict->tab->length/5)) {
    ellipsis‿token‿dictionary‿resize(dict, (dict->tab->length*num)/qot);
  }
}

ELLIPSIS_FA_UNDEPRECATE
void ellipsis‿token‿dictionary‿destroy(ellipsis‿token‿dictionary* dict) {
  if (dict->tab) {
    for (size_t i = 0; i < dict->tab->length; ++i) {
      if (dict->tab->array[i]) {
        ellipsis‿token‿move(&dict->tab->array[i], nullptr);
      }
    }
    ellipsis‿token‿array‿delete(dict->tab);
  }
  dict->tab = nullptr;
}

ELLIPSIS_FA_UNDEPRECATE
void ellipsis‿token‿dictionary‿fputs(ellipsis‿token‿dictionary dict[static 1], FILE* outf, char const prefix[static 1]) {
  if (!outf) outf = stderr;
  if (!dict->tab) {
    fprintf(outf, "// %zu keys in 0 slots\n", dict->keys);
    return;
  }
  fprintf(outf, "// %zu keys in %zu slots, %zu tries with %zu comparisons\n",
          dict->keys, dict->tab->length,
          dict->tries, dict->comparisons);
  ellipsis‿token‿array* tab = ellipsis‿token‿array‿cpy(dict->tab);
  ellipsis‿token‿array‿sort(tab, 0, tab->length);
  /* defer */
  [[__maybe_unused__]] register unsigned DEFER_LOC_ID_0_2 = 1U;
  [[__maybe_unused__]] register bool defer_return_flag  = false;/* return context forced */
  [[__maybe_unused__, __deprecated__("dummy variable for better diagnostics")]]
  unsigned (*DEFER_LOC_ID_1_1)[DEFER_LOC_ID_0_2]  = {};
  if (false) {
  DEFER_ID_1_1: {
      [[__maybe_unused__, __deprecated__("invalid termination of a deferred block")]]
      register bool const defer_return_flag = false, defer_break_flag = false, defer_continue_flag  = false;
      {ellipsis‿token‿array‿delete(tab);}
    }
    goto DEFER_END_ID_1_1;
  } else { /** start inner defer anchor at level 1 **/
    (void)0 /** defer needs braces and a semicolon **/;
    for (size_t i = tab->length - dict->keys; i < tab->length; ++i) {
      if (tab->array[i]
          && tab->array[i]->cat != removed) {
        if (tab->array[i]->contents) {
          fputs(prefix, outf);
          ellipsis‿str32‿fputs(tab->array[i]->contents, outf);
          if (tab->array[i]->is_what == ellipsis‿token‿what‿callback) {
            fprintf(outf, "\tcallback %p", tab->array[i]->payload);
          } else if ((tab->array[i]->is_what == ellipsis‿token‿what‿else) && tab->array[i]->payload) {
            token* t  = tab->array[i]->payload->first;
            size_t parms  = 0;
            if (tab->array[i]->is_functional) {
              fprintf(outf, "(");
              bool var  = is_variable(t->value);
              parms = var ? (-t->value)-1 : t->value;
              if (parms)
                for (size_t i = 0; i < parms-1; i++) {
                  fprintf(outf, "_%zu,\t", i);
                }
              if (var) {
                if (parms) {
                  fprintf(outf, "_%zu,\t...)\t", parms-1);
                } else {
                  fprintf(outf, "...)\t");
                }
              } else {
                if (parms) {
                  fprintf(outf, "_%zu)\t", parms-1);
                } else {
                  fprintf(outf, ")\t");
                }
              }
              t = t->next;
            } else {
              fprintf(outf, "\t");
            }
            for (; t; t = t->next) {
              if (t->cat == parameter) {
                if (t->value > parms) {
                  fprintf(outf, "__VA_OPT_%llu__", (unsigned long long)(t->value - parms));
                } else {
                  fprintf(outf, "_%llu", (unsigned long long)t->value);
                }
              } else if (t->cat == stringifier) {
                if (t->value > parms) {
                  fprintf(outf, "⌗__VA_OPT_%llu__", (unsigned long long)(t->value - parms));
                } else {
                  fprintf(outf, "⌗_%llu", (unsigned long long)t->value);
                }
              } else {
                if (t->contents) {
                  ellipsis‿str32‿fputs(t->contents, outf);
                }
              }
              if (t->space) {
                ellipsis‿str32‿fputs(t->space, outf);
              }
            }
          } else if (tab->array[i]->is_what == ellipsis‿token‿what‿binary) {
            fprintf(outf, "\tdata %p", (void*)tab->array[i]->data);
          } else if (tab->array[i]->cat == exp_unsigned) {
            fprintf(outf, "\t%llu", (unsigned long long)tab->array[i]->value);
          } else if (tab->array[i]->cat == exp_signed) {
            fprintf(outf, "\t%lld", (unsigned long long)tab->array[i]->value);
          }
          fputc('\n', outf);
        }
      }
    }

    goto DEFER_ID_1_1;
  } /** end inner defer anchor, level 1 **/
[[__maybe_unused__]] DEFER_END_ID_1_1:;
}

void ellipsis‿token‿dictionary‿init(ellipsis‿token‿dictionary*const d, size_t len) {

  uint64_t fact = len*φ;
  uint64_t inv[2] = {fact, len, };
  for (; inv[0] != 1; ++fact) {
    ellipsis‿token‿dictionary‿inverse_inplace(inv);
  }
  ellipsis_init(1, d, ellipsis‿token‿dictionary, .fibfac  = fact, .tab  = ellipsis‿token‿array‿alloc(len));

}

#pragma GCC diagnostic pop
