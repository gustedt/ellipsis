
/**
 ** @file
 **
 ** @warning This is a file generated by eĿlipsis version 20250219, do not modify
 **/

#ifndef ELLIPSIS_MACROS_H
#define ELLIPSIS_MACROS_H 1

#include "token/ellipsis-tlist.h"
#include "token/ellipsis-tdict.h"

ellipsis‿token* ellipsis‿macros‿find(ellipsis‿str32 const* n);
ellipsis‿token* ellipsis‿macros‿functions_find(ellipsis‿str32 const* n);

void ellipsis‿macros‿insert(ellipsis‿token* t);
void ellipsis‿macros‿remove(ellipsis‿str32 const* n);

void ellipsis‿macros‿deactivate(ellipsis‿str32* n);
void ellipsis‿macros‿activate(ellipsis‿str32* n);

void ellipsis‿macros‿freeze(void);
void ellipsis‿macros‿unfreeze(void);

ellipsis‿token* ellipsis‿macros‿mangle(ellipsis‿token* tk);

#define ellipsis_macros_mangle(TK) \
({\
 ellipsis‿token* _token = ellipsis‿macros‿mangle(TK); \
 if (!_token) {\
 ELLIPSIS_ERROR("%s:%lld: unable to mangle token", __FILE__, (long long)__LINE__); \
 } \
 _token; \
 })

static inline
bool is_variable(size_t count) {
  return count > 0x100;
}

void functions_insert(char const* name, ellipsis‿token* (*cb)(ellipsis‿token*));

void ellipsis‿macros‿repair(ellipsis‿str32 name[static 1]);

ellipsis‿token‿list* ellipsis‿macros‿unbind_get(void);

ellipsis‿token‿list* ellipsis‿macros‿unbind_set(ellipsis‿token‿list*);

size_t ellipsis‿macros‿paren‿get(void);
void ellipsis‿macros‿paren‿set(size_t);
void ellipsis‿macros‿paren‿inc(void);
void ellipsis‿macros‿paren‿dec(void);

#endif
