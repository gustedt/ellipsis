
#include "ellipsis-options.h"
#include <string.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

/**
 ** The internal state of option character processing as stored in the member ellipsis‿options‿pos
 **/
enum : size_t {
  ellipsis‿options‿start  = 0, /**< processing is just starting for the current parameter **/
  ellipsis‿options‿other  = (size_t)-1, /**< an argument to the option character was detected as the next argument **/
  ellipsis‿options‿here = (size_t)-2, /**< an argument to the option character was detected as continuation of the argument **/
  ellipsis‿options‿more = (size_t)-3, /**< option character processing is terminated, only non-options remain **/
  ellipsis‿options‿over = (size_t)-4, /**< movind to the next option character would overflow **/
};

[[deprecated]]
char ellipsis‿options‿get(ellipsis‿options* p) {

  if (!p->cur) {
    if (p->argv[0]) {
      p->cur  = (char const*const*)(p->argv+1);
    } else {
      p->cur  = (char const*const*)p->argv;
    }
  }
RETRY:
  if (!p->cur[0]) return ellipsis‿options‿end;
  switch (p->pos) {
  case ellipsis‿options‿here:
  case ellipsis‿options‿other:
    return (*p->cur)[1];
  case ellipsis‿options‿start:
    if ((*p->cur)[0] == '-') {
      switch ((*p->cur)[1]) {
      case ellipsis‿options‿longoption:
        if (!(*p->cur)[2]) {

          p->pos  = ellipsis‿options‿more;
          p->cur++;
          goto RETRY;
        }
        break;
      case 0:

        return ellipsis‿options‿none;
      }
      p->pos  = 1;
    } else {

    case ellipsis‿options‿more:
      return ellipsis‿options‿none;
    }
  }
  return (*p->cur)[p->pos];
}

[[deprecated]]
void ellipsis‿options‿next(ellipsis‿options* p) {
  char const c  = ellipsis‿options‿get(p);
  switch (c) {
  case ellipsis‿options‿end:
    return;
  case ellipsis‿options‿none:

    p->cur++;
    return;
  case ellipsis‿options‿longoption:
    if (!strchr(p->cur[0]+2, '='))
      goto OTHER;
    break;
  default:
    switch (p->pos) {
    case ellipsis‿options‿other:
    OTHER:

      p->cur++;
      if (!p->cur[0])
        return;
      break;
    default:
      p->pos++;
      if ((*p->cur)[p->pos])
        return;

    case ellipsis‿options‿start:
    case ellipsis‿options‿more:
    case ellipsis‿options‿over:
    case ellipsis‿options‿here:
    }
  }
  p->pos  = 0;
  p->cur++;
}

[[deprecated]]
char const* ellipsis‿options‿progname(ellipsis‿options* p) {
  return p->argv[0];
}

[[deprecated]]
char const* ellipsis‿options‿current(ellipsis‿options* p) {
  return p->cur[0];
}

[[deprecated]]
char const* ellipsis‿options‿argument(ellipsis‿options* p) {
  switch (p->pos) {
  case ellipsis‿options‿here:
    if (p->cur[0][1] == ellipsis‿options‿longoption) goto HERE;
    return p->cur[0]+2;
  default:
    if (p->pos == 1) {
      if (p->cur[0][1] == ellipsis‿options‿longoption) {
      HERE:
        char const* st  = p->cur[0]+2;
        char const* eq  = strchr(st, '=');
        if (eq) {
          p->pos  = ellipsis‿options‿here;
          return eq+1;
        } else {
          goto OTHER;
        }
      } else if (p->cur[0][2]) {
        p->pos  = ellipsis‿options‿here;
        return p->cur[0]+2;
      } else {
      OTHER:
        p->pos  = ellipsis‿options‿other;
      case ellipsis‿options‿other:
        return p->cur[1];
      }
    } else {
    case ellipsis‿options‿more:
    case ellipsis‿options‿over:
      return nullptr;
    }
  }
}

[[deprecated]]
char const* ellipsis‿options‿longname(ellipsis‿options* p) {
  if (ellipsis‿options‿get(p) != ellipsis‿options‿longoption) return nullptr;
  switch (p->pos) {
  default: return nullptr;
  case ellipsis‿options‿other: return p->cur[0]+2;
  case 1:
    break;
  case ellipsis‿options‿here:
    break;
  }
  char const* st  = p->cur[0]+2;
  char const* eq  = strchr(st, '=');
  if (!eq) {
    return st;
  }
  p->pos  = ellipsis‿options‿here;
  size_t len  = eq - st;
  if (len >= ellipsis‿options‿longname‿max)
    len = ellipsis‿options‿longname‿max - 1;
  memcpy(p->lname, st, len);
  p->lname[len] = 0;
  return p->lname;
}

#pragma GCC diagnostic pop
