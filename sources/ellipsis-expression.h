
#ifndef ELLIPSIS_EXPRESSION_H
#define ELLIPSIS_EXPRESSION_H

#include "token/ellipsis-tlist.h"

STDC_ATTR_NODISCARD() ellipsis‿token* ellipsis‿expression‿evaluate(ellipsis‿token‿list*);
void ellipsis‿expression‿resolve_defined(ellipsis‿token‿list* in, ellipsis‿token‿list* out);
bool ellipsis‿expression‿make_integer(ellipsis‿token* t);

#endif
