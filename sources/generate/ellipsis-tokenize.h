⎔ unit ellipsis∷tokenize
⎔ define DISCLAIMER file generated by eĿlipsis version __ELLIPSIS__, do not modify
/*^
 ** @file
 **
 ** @warning This is a DISCLAIMER
 ^*/
⎔ bind token ellipsis∷token
⎔ bind tlist token∷list
%%ifndef ELLIPSIS_TOKENIZE_H
%%define ELLIPSIS_TOKENIZE_H 1

%%include "token/ellipsis-tlist.h"
%%include "ellipsis-lexer.h"

extern ellipsis∷special∷find* tokenizer_find;
extern ellipsis∷special∷find ellipsis∷special∷decoration∷find;
extern void ¤(tlist* out, FILE* inp, size_t limit);
extern void ¤∷string(tlist tl[restrict static 1], char const all8[restrict static 1], ellipsis∷special∷find* find);
thrd_t ¤∷thread(tlist out[static 1], FILE* inp);
extern ellipsis∷special∷find* tokenizer_find;

%%endif
