// Source file for code expansion with eĿlipsis.
⎔ unit ¤¤∷array
⎔ define DISCLAIMER file generated by eĿlipsis version __ELLIPSIS__, do not modify
/*^
 ** @file
 **
 ** @warning This is a DISCLAIMER
 ^*/

⎔ include_directives "generate/xfiles/ellipsis-fa.eLh"
%%include "token/ellipsis-tarray.h"

%%pragma GCC diagnostic push
%%pragma GCC diagnostic ignored "-Wdeprecated-declarations"

⎔ include_source "generate/xfiles/ellipsis-fa-xcode.eLc"        \
  __prefix__(bind ELLIPSIS_FA_NAME ellipsis_tarray)

%%pragma GCC diagnostic pop

