// Source file for code expansion with eĿlipsis.
⎔ unit ¤¤∷bitset
⎔ define DISCLAIMER file generated by eĿlipsis version __ELLIPSIS__, do not modify
/*^
 ** @file
 **
 ** @warning This is a DISCLAIMER
 ^*/
%%include "utils/ellipsis-bitset.h"

¤∷base ¤∷baseset(size_t);
void ¤∷insert(size_t tabsize, ¤∷base table[tabsize], size_t val);
bool ¤∷member(size_t tabsize, ¤∷base table[tabsize], size_t val);
