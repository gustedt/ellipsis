⎔⎔ Source file for code expansion with eĿlipsis.
⎔ unit ellipsis∷fibfac
⎔ define DISCLAIMER file generated by eĿlipsis version __ELLIPSIS__, do not modify
%%ifndef ELLIPSIS_FIBFAC_H
%%define ELLIPSIS_FIBFAC_H 1

%%include <stddef.h>

/*^
 ** @file
 **
 ** @brief Fibonacci factors
 **
 ** These are chosen to be close to the binary expansion of the golden
 ** ratio.  It is rounded to the next odd integer to ensure that the
 ** number is invertible; namely every odd number is co-prime to a
 ** given power of 2. Such a choice then warrants that multiplication
 ** with this number is a bijection on the multiplication group and
 ** that the images of successive values are even spread in the
 ** domain.
 **
 ** Factors are provided for widths 3 to 64: hashing just `2¹ = 2` or
 ** `2² = 4` buckets makes not much sense, and generally we do not
 ** have numbers wider than 64 bit in the preprocessor.
 **
 ** @warning This is a DISCLAIMER
 ^*/

⎔ xdefine ELLIPSIS_FIB() 11400714819323198485U

⎔ bind  ELLIPSIS_FIB0() /*>*/ ** the real value is rounded up such that the result is odd

⎔ bind  ELLIPSIS_FIB1() /*>*/ ** the real value is rounded down such that the result is odd
⎔ bind  ELLIPSIS_FIBx(N) (ELLIPSIS_FIBN(N)%2)
⎔ bind  ELLIPSIS_FIBN(N) (ELLIPSIS_FIB()>>(64 - N))
⎔ bind  ELLIPSIS_FIBX_(A, B) __CONCAT__(A, B)
⎔ bind  ELLIPSIS_FIBX(N) ELLIPSIS_FIBX_(ELLIPSIS_FIB, __EXPAND_DEC__(ELLIPSIS_FIBx(N)))


⎔ xbind KEY ¤∷key
⎔ define DO_BODY(N) /*^*//*^*/ /*>*//*^/*^*/                            \
/*>*/ ** @brief Fibonacci factor for hash with N bit value/*^*/         \
/*>*/ **/*^*/                                                           \
  ELLIPSIS_FIBX(N)()/*^*/                                   \
/*>*/ ^*/                                                               \
     /*>*/ENUM_KEY(__CONCAT__(KEY, N))         \
     ENUM_VAL(__CONCAT__(__EXPAND_BIN__((ELLIPSIS_FIB()>>(64 - N))+(¬((ELLIPSIS_FIB()>>(64 - N))%2))),ull))

⎔ include_directives <ellipsis-do.dirs>         \
  __prefix__(bind DO_BOUNDS 3, 65)              \
  __prefix__(bind DO_SEPARATOR ,)               \
  __prefix__(bind DO_RESULT elist)

/*^
 ** @memberof ¤
 ^*/
typedef size_t ¤∷base;

⎔  xbind ENUM_LIST elist
⎔  include_source "generate/xfiles/ellipsis-enum-xcode.eLh"

⎔ undef DO_BODY
⎔ define DO_BODY(N)                                     \
  typeof(char(*)[N]): __CONCAT__(KEY, N)

⎔ include_directives <ellipsis-do.dirs>         \
  __prefix__(bind DO_BOUNDS 3, 65)              \
  __prefix__(bind DO_SEPARATOR ,)               \
  __prefix__(bind DO_RESULT assocs)

/*^
 ** @brief The Fibonacci factor for `N` bits as an integer constant expression
 ^*/
%%define ELLIPSIS_FIBFAC(N) _Generic((char(*)[N]){}, assocs)

%%endif
