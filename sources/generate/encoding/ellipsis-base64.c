// Source file for code expansion with eĿlipsis.
⎔ unit ¤¤∷base64
⎔ define DISCLAIMER file generated by eĿlipsis version __ELLIPSIS__, do not modify
/*^
 ** @file
 **
 ** @warning This is a DISCLAIMER
 ^*/
%%include <stdio.h>
%%include <stdlib.h>
%%include <string.h>
%%include "ellipsis-base64.h"
%%include "utils/ellipsis-error.h"

constexpr uint32_t ¤∷esign = '=';
constexpr char32_t ¤∷enc[] = U"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

%%define DEC_POS(X) dec_pos_ ## X
enum ellipsis_dec_pos {
  DEC_POS(A),
  DEC_POS(B),
  DEC_POS(C),
  DEC_POS(D),
  DEC_POS(E),
  DEC_POS(F),
  DEC_POS(G),
  DEC_POS(H),
  DEC_POS(I),
  DEC_POS(J),
  DEC_POS(K),
  DEC_POS(L),
  DEC_POS(M),
  DEC_POS(N),
  DEC_POS(O),
  DEC_POS(P),
  DEC_POS(Q),
  DEC_POS(R),
  DEC_POS(S),
  DEC_POS(T),
  DEC_POS(U),
  DEC_POS(V),
  DEC_POS(W),
  DEC_POS(X),
  DEC_POS(Y),
  DEC_POS(Z),
  DEC_POS(a),
  DEC_POS(b),
  DEC_POS(c),
  DEC_POS(d),
  DEC_POS(e),
  DEC_POS(f),
  DEC_POS(g),
  DEC_POS(h),
  DEC_POS(i),
  DEC_POS(j),
  DEC_POS(k),
  DEC_POS(l),
  DEC_POS(m),
  DEC_POS(n),
  DEC_POS(o),
  DEC_POS(p),
  DEC_POS(q),
  DEC_POS(r),
  DEC_POS(s),
  DEC_POS(t),
  DEC_POS(u),
  DEC_POS(v),
  DEC_POS(w),
  DEC_POS(x),
  DEC_POS(y),
  DEC_POS(z),
  DEC_POS(0),
  DEC_POS(1),
  DEC_POS(2),
  DEC_POS(3),
  DEC_POS(4),
  DEC_POS(5),
  DEC_POS(6),
  DEC_POS(7),
  DEC_POS(8),
  DEC_POS(9),
  DEC_POS(plus),
  DEC_POS(slash),
};

%%define DEC_POS_INIT(C, X) [C] = DEC_POS(X)
constexpr unsigned char ¤∷dec[256] = {
  DEC_POS_INIT('A', A),
  DEC_POS_INIT('B', B),
  DEC_POS_INIT('C', C),
  DEC_POS_INIT('D', D),
  DEC_POS_INIT('E', E),
  DEC_POS_INIT('F', F),
  DEC_POS_INIT('G', G),
  DEC_POS_INIT('H', H),
  DEC_POS_INIT('I', I),
  DEC_POS_INIT('J', J),
  DEC_POS_INIT('K', K),
  DEC_POS_INIT('L', L),
  DEC_POS_INIT('M', M),
  DEC_POS_INIT('N', N),
  DEC_POS_INIT('O', O),
  DEC_POS_INIT('P', P),
  DEC_POS_INIT('Q', Q),
  DEC_POS_INIT('R', R),
  DEC_POS_INIT('S', S),
  DEC_POS_INIT('T', T),
  DEC_POS_INIT('U', U),
  DEC_POS_INIT('V', V),
  DEC_POS_INIT('W', W),
  DEC_POS_INIT('X', X),
  DEC_POS_INIT('Y', Y),
  DEC_POS_INIT('Z', Z),
  DEC_POS_INIT('a', a),
  DEC_POS_INIT('b', b),
  DEC_POS_INIT('c', c),
  DEC_POS_INIT('d', d),
  DEC_POS_INIT('e', e),
  DEC_POS_INIT('f', f),
  DEC_POS_INIT('g', g),
  DEC_POS_INIT('h', h),
  DEC_POS_INIT('i', i),
  DEC_POS_INIT('j', j),
  DEC_POS_INIT('k', k),
  DEC_POS_INIT('l', l),
  DEC_POS_INIT('m', m),
  DEC_POS_INIT('n', n),
  DEC_POS_INIT('o', o),
  DEC_POS_INIT('p', p),
  DEC_POS_INIT('q', q),
  DEC_POS_INIT('r', r),
  DEC_POS_INIT('s', s),
  DEC_POS_INIT('t', t),
  DEC_POS_INIT('u', u),
  DEC_POS_INIT('v', v),
  DEC_POS_INIT('w', w),
  DEC_POS_INIT('x', x),
  DEC_POS_INIT('y', y),
  DEC_POS_INIT('z', z),
  DEC_POS_INIT('0', 0),
  DEC_POS_INIT('1', 1),
  DEC_POS_INIT('2', 2),
  DEC_POS_INIT('3', 3),
  DEC_POS_INIT('4', 4),
  DEC_POS_INIT('5', 5),
  DEC_POS_INIT('6', 6),
  DEC_POS_INIT('7', 7),
  DEC_POS_INIT('8', 8),
  DEC_POS_INIT('9', 9),
  DEC_POS_INIT('+', plus),
  DEC_POS_INIT('/', slash),
};

static
uint32_t ¤∷encode_up(uint32_t inp) {
  uint32_t ret = 0;
  ret ∪= ¤∷enc[(inp >> 18)∩0b0011'1111] << 24;
  ret ∪= ¤∷enc[(inp >> 12)∩0b0011'1111] << 16;
  ret ∪= ¤∷enc[(inp >>  6)∩0b0011'1111] <<  8;
  ret ∪= ¤∷enc[(inp >>  0)∩0b0011'1111] <<  0;
  return ret;
}

static
uint32_t ¤∷encode_3(uint32_t c0, uint32_t c1, uint32_t c2) {
  uint32_t inp = (c0<<16) ∪ (c1<<8) ∪ c2;
  return ¤∷encode_up(inp);
}

static
uint32_t ¤∷encode_2(uint32_t c0, uint32_t c1) {
  uint32_t inp = (c0<<16) ∪ (c1<<8);
  uint32_t ret = ¤∷encode_up(inp) ∩ 0xFFFFFF00;
  return ret;
}

static
uint32_t ¤∷encode_1(uint32_t c0) {
  uint32_t inp = (c0<<16);
  uint32_t ret = ¤∷encode_up(inp) ∩ 0xFFFF0000;
  return ret;
}

static
void ¤∷print_3(char tar[4], uint32_t enc) {
  union {
    char byte[4];
  } buf = {
    .byte[0] = (enc>>24)∩0b0111'1111,
    .byte[1] = (enc>>16)∩0b0111'1111,
    .byte[2] = (enc>> 8)∩0b0111'1111,
    .byte[3] = (enc>> 0)∩0b0111'1111,
  };
  memcpy(tar, buf.byte, 4);
}

static
void ¤∷print_2(char tar[4], uint32_t enc) {
  union {
    char byte[4];
  } buf = {
    .byte[0] = (enc>>24)∩0b0111'1111,
    .byte[1] = (enc>>16)∩0b0111'1111,
    .byte[2] = (enc>> 8)∩0b0111'1111,
    .byte[3] = ¤∷esign,
  };
  memcpy(tar, buf.byte, 4);
}

static
void ¤∷print_1(char tar[4], uint32_t enc) {
  union {
    char byte[4];
  } buf = {
    .byte[0] = (enc>>24)∩0b0111'1111,
    .byte[1] = (enc>>16)∩0b0111'1111,
    .byte[2] = ¤∷esign,
    .byte[3] = ¤∷esign,
  };
  memcpy(tar, buf.byte, 4);
}

char* ¤∷encode(ellipsis∷str8 const buf[static 1]) {
  size_t len = buf->length;
  size_t const packs = len/3 + ¬¬(len%3);
  size_t const nlen  = 4*packs;
  char* ret = malloc(nlen+1);
  if (¬ret) {
    ELLIPSIS_ERROR("unable to allocate target string in base64 encoding");
  }
  ret[nlen] = 0;
  uint8_t const* s = buf->array;
  char* t = ret;
  uint32_t coded;
  while (len ≥ 3) {
    coded = ¤∷encode_3(s[0], s[1], s[2]);
    ¤∷print_3(t, coded);
    t += 4;
    s += 3;
    len -= 3;
  }
  switch (len) {
  case 2:
    coded = ¤∷encode_2(s[0], s[1]);
    ¤∷print_2(t, coded);
    break;
  case 1:
    coded = ¤∷encode_1(s[0]);
    ¤∷print_1(t, coded);
    break;
  }
  return ret;
}

static
uint32_t ¤∷decode_down(uint32_t inp) {
  uint32_t ret = 0;
  ret ∪= ¤∷dec[(inp >> 24)∩0b1111'1111] << 18;
  ret ∪= ¤∷dec[(inp >> 16)∩0b1111'1111] << 12;
  ret ∪= ¤∷dec[(inp >>  8)∩0b1111'1111] <<  6;
  ret ∪= ¤∷dec[(inp >>  0)∩0b1111'1111] <<  0;
  return ret;
}

static
uint32_t ¤∷decode_4(uint32_t c0, uint32_t c1, uint32_t c2, uint32_t c3) {
  uint32_t inp = (c0<<24) ∪ (c1<<16) ∪ (c2<<8) ∪ c3;
  return ¤∷decode_down(inp);
}

static
void ¤∷print_4(uint8_t tar[static 3], uint32_t enc) {
  tar[0] = (enc>>16)∩0b1111'1111;
  tar[1] = (enc>> 8)∩0b1111'1111;
  tar[2] = (enc>> 0)∩0b1111'1111;
}

ellipsis∷str8* ¤∷decode(size_t len, char const buf[len]) {
  if (len % 4) {
    ELLIPSIS_WARNING("trying to base64 decode a string with a length that is not a multiple of four");
  }
  size_t const packs = len/4;
  size_t nlen  = 3*packs;
  ellipsis∷str8* ret = ellipsis∷str8∷alloc(nlen);
  if (¬ret) {
    ELLIPSIS_ERROR("unable to allocate target string in base64 decoding");
  }
  char const* s = buf;
  uint8_t* t = ret->array;
  uint32_t coded;
  while (len ≥ 4) {
    coded = ¤∷decode_4(s[0], s[1], s[2], s[3]);
    ¤∷print_4(t, coded);
    t += 3;
    len -= 4;
    if (¬len) {
      break;
    }
    s += 4;
  }
  // readjust the length if there is padding at the end
  if (s[3] ≡ '=') {
    if (s[2] ≡ '=') {
      nlen -= 2;
    } else {
      nlen -= 1;
    }
  }
  ret = ellipsis∷str8∷shrink(ret, nlen);
  return ret;
}
