// Source file for code expansion with eĿlipsis.
⎔ unit ¤¤∷xml
⎔ define DISCLAIMER file generated by eĿlipsis version __ELLIPSIS__, do not modify
/*^
 ** @file
 **
 ** @warning This is a DISCLAIMER
 ^*/
%%include "ellipsis-entity.h"
%%include "utils/ellipsis-error.h"
%%include <stdlib.h>
%%ifndef ONCE_INIT
%%include <threads.h>
%%endif
%%include "language/ellipsis-special.h"
#include_directives <ellipsis-once.h>
%%include <string.h>
%%include <stdlib.h>
%%include <stdint.h>
%%include <stddef.h>
%%include <ctype.h>

/*^
 ** @file
 **
 ** @warning This is a DISCLAIMER
 **
 ** @brief Map xml entities such as `&gt;` to their code points and
 ** vice versa.
 **
 ** This implementation possibly trades size over speed. The lookup
 ** tables only hold offsets and thus there are some indirections or
 ** even double indirections. Also, for this we use a mechanism with
 ** `thread_local` to be able to use `bsearch`. Whenever the platform
 ** allows indirect addressing this should be mostly ok.
 ^*/

/*^
 ** @brief A big `char` array that contains all the strings together
 ** with their null termination.
 ^*/
static char const strings[] = {
⎔ include_source "generate/xfiles/xml-entities-xcode.c" __prefix__(bind ENTRY(N, E) __STRINGIFY__(N) "\0")
};

constexpr size_t ent_length =
⎔ include_source "generate/xfiles/xml-entities-xcode.c"  __prefix__(bind ENTRY(N, E) +1)
  ;

static char32_t const entities[ent_length] = {
⎔ include_source "generate/xfiles/xml-entities-xcode.c"  __prefix__(bind ENTRY(N, E) E,)
};

/*^
 ** @brief Compute start and end positions of strings in the `strings`
 ** buffer
 ^*/
enum {
  first,
⎔ include_source "generate/xfiles/xml-entities-xcode.c"                 \
  __prefix__(                                                           \
  bind ENTRY(N, E)                                                      \
  __CONCAT__(N, _tmp),                                                  \
  __CONCAT__(N, _start) = __CONCAT__(N, _tmp) - 1,                      \
  __CONCAT__(N, _end) = __CONCAT__(N, _start) + sizeof(__STRINGIFY__(N)),)
  last,
};

static_assert(sizeof(strings) ≡ last);

/*^
 ** @brief Collect the start positions of the strings in the `strings`
 ** buffer.
 ^*/
static
uint_least16_t const entities_by_name[ent_length] = {
⎔ include_source "generate/xfiles/xml-entities-xcode.c" __prefix__(bind ENTRY(N, E) __CONCAT__(N, _start),)
};

static thread_local char const* bsearch_name = nullptr;

static
int comp_name(const void *a, const void *b) {
  uint_least16_t const* A = a;
  uint_least16_t const* B = b;
  char const*const α = (*A ≡ UINT_LEAST16_MAX) ? bsearch_name : strings+*A;
  char const*const β = (*B ≡ UINT_LEAST16_MAX) ? bsearch_name : strings+*B;
  return strcmp(α, β);
}

char32_t ¤∷entity(char const s[static 1]) {
  char32_t ret = 0;
  bsearch_name = s;
  static uint_least16_t const el = UINT_LEAST16_MAX;
  uint_least16_t const* entry = bsearch(&el, entities_by_name, ent_length, sizeof entities_by_name[0], comp_name);
  if (entry) {
    ptrdiff_t pos = entry-entities_by_name;
    ret = entities[pos];
  }
  return ret;
}

/*^
 ** @brief Compute positions of codes in the `entities` buffer
 ^*/
enum {
⎔ include_source "generate/xfiles/xml-entities-xcode.c" __prefix__(bind ENTRY(N, E) __CONCAT__(N, _pos),)
};

static
uint_least16_t entities_by_number[ent_length] = {
⎔ include_source "generate/xfiles/xml-entities-xcode.c" __prefix__(bind ENTRY(N, E) __CONCAT__(N, _pos),)
};

static thread_local char32_t bsearch_ent = 0;

static
int comp_number(const void *a, const void *b) {
  uint_least16_t const* A = a;
  uint_least16_t const* B = b;
  int_least32_t const α = (*A ≡ UINT_LEAST16_MAX) ? bsearch_ent : entities[*A];
  int_least32_t const β = (*B ≡ UINT_LEAST16_MAX) ? bsearch_ent : entities[*B];
⎔ if INT_LEAST32_MAX ≤ INT_MAX
  /* Valid code points don't produce overflow, here. */
  return α-β;
⎔ else
  return (α < β) ? -1 : ((α > β) ? +1 : 0);
⎔ endif
}

void ¤∷name(size_t len, char s[static len], char32_t c32) {
  // Only initialize if we really need this
  ONCE_DEPEND();
  if (c32 < (UINT32_C(1)<<21)) {
    bsearch_ent = c32;
    static uint_least16_t const el = UINT_LEAST16_MAX;
    uint_least16_t const* entry = bsearch(&el, entities_by_number, ent_length, sizeof entities_by_number[0], comp_number);
    if (entry) {
      ptrdiff_t pos = entities_by_name[*entry];
      size_t nlen = strlen(strings+pos);
      if (nlen < len) {
        memcpy(s, strings+pos, nlen+1);
        return;
      }
    }
  }
  s[0] = 0;
}

static
bool string_islower(char const* start) {
  bool lower = true;
  for (; *start; ++start) {
    if (¬islower(*start)) {
      lower = false;
      break;
    }
  }
  return lower;
}

ONCE_DEFINE() {
  qsort(entities_by_number, ent_length, sizeof entities_by_number[0], comp_number);
  bool changes = true;
  while (changes) {
    changes = false;
    for (uint_least16_t i = 0; i < ent_length; ++i) {
      uint_least16_t* entry = bsearch(&entities_by_number[i], entities_by_number, ent_length, sizeof entities_by_number[0], comp_number);
      if (entry ∧ strcmp(strings+entities_by_name[entities_by_number[i]], strings+entities_by_name[*entry])) {
        size_t lenI = strlen(strings+entities_by_name[entities_by_number[i]]);
        size_t lenE = strlen(strings+entities_by_name[*entry]);
        if (lenI < lenE) {
          *entry = entities_by_number[i];
          changes = true;
        } else if (lenI > lenE) {
          entities_by_number[i] = *entry;
          changes = true;
        } else if (i > (entry-entities_by_number)) {
          if (string_islower(strings+entities_by_name[entities_by_number[i]])) {
            *entry = entities_by_number[i];
            changes = true;
          } else {
            if (string_islower(strings+entities_by_name[*entry])) {
              entities_by_number[i] = *entry;
              changes = true;
            } else {
              ELLIPSIS_WARNING("ambiguous %u,%u : %s <-> %s", i, *entry, strings+entities_by_name[entities_by_number[i]], strings+entities_by_name[*entry]);
            }
          }
        }
      }
    }
  }
}
