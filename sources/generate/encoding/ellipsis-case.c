// Source file for code expansion with lip.
⎔ define DISCLAIMER file generated by eĿlipsis version __ELLIPSIS__, do not modify
⎔ unit ellipsis∷case

/*^
 ** @file
 **
 ** @warning This is a DISCLAIMER
 **
 ** @see ellipsis-case.h for a documentation of the case mappings
 ^*/

%%include "ellipsis-case.h"
%%include "utils/ellipsis-error.h"
%%include <stdlib.h>

%%ifndef DOXYGEN_SPECIAL

typedef struct cores cores;
struct cores {
  char32_t key;
  char32_t val;
};

static
cores lowercase[] = {
%%include "lowercase.c"
};

constexpr size_t len = sizeof lowercase/sizeof lowercase[0];

static
cores uppercase[len] = {
%%include "uppercase.c"
};

static
int ¤∷comp(void const* a, void const* b) {
  cores const* A = a;
  cores const* B = b;
  char32_t α = A→key;
  char32_t β = B→key;
  return (α < β)
         ? -1
         : ((α > β)
            ? +1
            : 0);
}

%%endif

char32_t ¤∷upper(char32_t a) {
  cores* ret = bsearch(&(cores const) { .key = a }, lowercase, len, sizeof lowercase[0], ¤∷comp);
  return ret ? ret→val : a;
}

char32_t ¤∷lower(char32_t a) {
  cores* ret = bsearch(&(cores const) { .key = a }, uppercase, len, sizeof uppercase[0], ¤∷comp);
  return ret ? ret→val : a;
}
