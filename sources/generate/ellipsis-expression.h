⎔ unit ellipsis∷expression
⎔ bind token ellipsis∷token
⎔ bind tlist token∷list
%%ifndef ELLIPSIS_EXPRESSION_H
%%define ELLIPSIS_EXPRESSION_H

%%include "token/ellipsis-tlist.h"

STDC_ATTR_NODISCARD() token* ¤∷evaluate(tlist*);
void ¤∷resolve_defined(tlist* in, tlist* out);
bool ¤∷make_integer(token* t);

%%endif
