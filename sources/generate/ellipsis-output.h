⎔ unit ellipsis∷output
%%ifndef ELLIPSIS_OUTPUT_H
%%define ELLIPSIS_OUTPUT_H
%%include "utils/ellipsis-str32.h"
%%include <stdio.h>

extern int ellipsis∷c_punctuators∷fputs(ellipsis∷str32 const*restrict s, FILE *stream);
extern int (*¤∷punctuators)(ellipsis∷str32 const*restrict s, FILE *stream);
extern int (*¤∷others)(ellipsis∷str32 const*restrict s, FILE *stream);
int ¤∷cont(ellipsis∷str32 const s32[static restrict 1], FILE* out);
int ¤∷space(ellipsis∷str32 const s32[static restrict 1], FILE* out);

thrd_t ¤∷thread(ellipsis∷token∷list[static 1]);
thrd_t ¤∷verbose(ellipsis∷token∷list[static 1]);


%%endif
