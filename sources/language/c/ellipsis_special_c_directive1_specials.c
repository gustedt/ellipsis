SPECIAL_TEXT0(### C directives for one stage parsing {#c_directive1})

  SPECIAL_TEXT0(In C, if we process in two steps, it is possible to mark directives)
    SPECIAL_TEXT0(for the second step with "%%" instead of "#".)
      SPECIAL_TEXT0(This means in particular, that the corresponding line will be expanded)
      SPECIAL_TEXT0(during the first step.)
      SPECIAL_TEXT0(To have the same effects when we process only in one step, under that mode)
      SPECIAL_TEXT0(the directive is prefixed with `expand` to make sure that expansion takes place as well.)

      SPECIAL_TEXT0(| start | end | category | note |)
      SPECIAL_TEXT0(|-|-|-|-|)
      SPECIAL_ADDON("%"":", "\n", directive)
      SPECIAL_ADDON("%%", "\n", directive, "expand")
      SPECIAL_ADDON("#", "\n", directive)
