PUNCTUATOR_TEXT0(### C punctuators for code structure {#c_structure})

  PUNCTUATOR_TEXT0(@warning the FULL STOP character should never be listed here. It
                   is always a punctuator unless it is followed by a digit, whence
                   it starts a numeral.)

  PUNCTUATOR_TEXT0(| sequence | replacement | note |)
  PUNCTUATOR_TEXT0(|-|-|-|)
  PUNCTUATOR_ADDON("%>", "}")
  PUNCTUATOR_ADDON("->", "→")
  PUNCTUATOR_ADDON("∷",, extension for composed names)
    PUNCTUATOR_ADDON(":>", "]")
    PUNCTUATOR_ADDON("<%", "{")
    PUNCTUATOR_ADDON("<:", "[")

    PUNCTUATOR_TEXT0(### eĿlipsis extension operators for C {#c_extension_operators})

      PUNCTUATOR_TEXT0(These are accepted as tokens in C and produce
                       the indicated effect on the output stream. This can e.g. be used
                       to "escape" directives that should be conserved for the output
                       or to preprocess comments such that they can be used
                       for documentation.)

          PUNCTUATOR_TEXT0(| sequence | replacement | output |)
          PUNCTUATOR_TEXT0(|-|-|-|)
          PUNCTUATOR_ADDON("%%",, a # token)
          PUNCTUATOR_ADDON("%%%%",, a ## token)
          SPECIAL_ADDON("<\x007c", "", keep, "⸤", start an unfiltered token sequence)
          SPECIAL_ADDON("\x007c>", "", peek, "⸥", end an unfiltered token sequence)
          PUNCTUATOR_ADDON("/*^*/",, a newline)
          PUNCTUATOR_ADDON("/*>*/",, a tab character)
          PUNCTUATOR_ADDON("/*^",, start a doxygen comment)
          PUNCTUATOR_ADDON("^*/",, end a doxygen comment)
          PUNCTUATOR_ADDON("/*!",, start a comment)
          PUNCTUATOR_ADDON("!*/",, end a comment)

          PUNCTUATOR_TEXT0(### instrument opening and closing braces in C {#c_extension_defer})

          PUNCTUATOR_TEXT0(These are accepted as tokens in C and can be used to add preprocessor callbacks
                           to these constructs. This is e.g used by hierarchically named identifiers and by the
                           defer extension.)

          PUNCTUATOR_TEXT0(| sequence | replacement | meaning |)
          PUNCTUATOR_TEXT0(|-|-|-|)
          NOMINAL_ADD("{",, instrumentation, implemented by a macro)
          NOMINAL_ADD("}",, instrumentation, implemented by a macro)
          NOMINAL_ADD("[",, instrumentation, implemented by a macro)
          NOMINAL_ADD("]",, instrumentation, implemented by a macro)
          NOMINAL_ADD("[[",, instrumentation, implemented by a macro)
          NOMINAL_ADD("]]",, instrumentation, implemented by a macro)
          NOMINAL_ADD("⟦",, instrumentation, implemented by a macro)
          NOMINAL_ADD("⟧",, instrumentation, implemented by a macro)
          NOMINAL_ADD("=",, instrumentation, implemented by a macro)
          NOMINAL_ADD(";",, instrumentation, implemented by a macro)
          NOMINAL_ADD("::",, used for attributes and composed names)
