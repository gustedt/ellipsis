PUNCTUATOR_TEXT0(### C operators with side effects {#c_sideeffects})

PUNCTUATOR_TEXT0(C has a whole set of operators that combine arithmetic with
                 side effects on objects that make no sense for the preprocessor.)

  PUNCTUATOR_TEXT0(| sequence | replacement | note |)
  PUNCTUATOR_TEXT0(|-|-|-|)
  PUNCTUATOR_ADDON("%=")
  PUNCTUATOR_ADDON("&=", "∩=")
  PUNCTUATOR_ADDON("*=", "×=")
  PUNCTUATOR_ADDON("++")
  PUNCTUATOR_ADDON("+=")
  PUNCTUATOR_ADDON("--")
  PUNCTUATOR_ADDON("-=", "−=")
  PUNCTUATOR_ADDON("/=", "∕=")
  PUNCTUATOR_ADDON("<<=", "◀=")
  PUNCTUATOR_ADDON(">>=", "▶=")
  PUNCTUATOR_ADDON("^=")
  PUNCTUATOR_ADDON("\x007c=", "∪=")
