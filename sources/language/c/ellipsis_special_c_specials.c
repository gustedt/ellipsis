/* collect all the specials that are needed for C */
#include "language/ellipsis-call.c"
#include "language/c/ellipsis-c-special.c"
#include "language/c/ellipsis-c-arithmetic.c"
#include "language/c/ellipsis-c-structure.c"
