SPECIAL_TEXT0(### Special include paths for C)

  SPECIAL_TEXT0(In C, less and greater signs are accepted complementing pointing angle brackets to determine include paths.)

  SPECIAL_TEXT0(| start | end | category | note |)
  SPECIAL_TEXT0(|-|-|-|-|)
  SPECIAL_ADDON("<", ">", string)
