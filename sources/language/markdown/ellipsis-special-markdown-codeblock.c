SPECIAL_TEXT0(### Markdown special that starts at the beginning of lines {#md_decoration})

SPECIAL_TEXT0(| start | end | category | note |)
SPECIAL_TEXT0(|-|-|-|-|)
SPECIAL_ADDON("```", "```", codeblock)
