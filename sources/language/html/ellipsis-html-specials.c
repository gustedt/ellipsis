SPECIAL_TEXT0(### Html constructs with different syntax elements)

SPECIAL_TEXT0(| start | end | category | note |)
SPECIAL_TEXT0(|-|-|-|-|)
SPECIAL_ADDON("<!", ">", special)
SPECIAL_ADDON("<style>", "</style>", special)
SPECIAL_ADDON("<script>", "</script>", special)
