PUNCTUATOR_TEXT0(#### Punctuators for directives)

  PUNCTUATOR_TEXT0(Directives form their own sublanguage with a specific set
                   of punctuators that are used as operators.)

  PUNCTUATOR_TEXT0(@see @ref ellipsis)

  PUNCTUATOR_TEXT0(| sequence | replacement | note |)
  PUNCTUATOR_TEXT0(|-|-|-|)
  PUNCTUATOR_ADDON("⌗",, the stringify operator)
  PUNCTUATOR_ADDON("⨝",, the token join operator)
  PUNCTUATOR_ADDON("#", "⌗", the stringify operator)
  PUNCTUATOR_ADDON("%:", "⌗", the stringify operator)
  PUNCTUATOR_ADDON("##", "⨝", the token join operator)
  PUNCTUATOR_ADDON("%:%:", "⨝", the token join operator)
