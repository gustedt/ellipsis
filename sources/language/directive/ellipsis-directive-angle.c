SPECIAL_TEXT0(### Special include paths for all languages {#special_include})

  SPECIAL_TEXT0(Pointing angle brackets (code points U27E8 and U27E9) are accepted as general syntax to determine include paths.)

  SPECIAL_TEXT0(| start | end | category | note |)
  SPECIAL_TEXT0(|-|-|-|-|)
  SPECIAL_ADDON("⟨", "⟩", string)
  SPECIAL_ADDON("〈", "〉", string)
