/*^
 ** @name C standard directives
 ^*/
/*^ @{!*/

/* These have to start in collumn 0 */

/*^ @brief The define directive as specified by the C standard ^*/
KEYWORD_ADD_ID(define)

/*^ @brief The elif directive as specified by the C standard ^*/
KEYWORD_ADD_ID(elif)

/*^ @brief The else directive as specified by the C standard ^*/
KEYWORD_ADD_ID(else)

/*^ @brief The embed directive as specified by the C standard plus extension
 **
 ** @see [extension to directives](@ref dir_extensions)
 ^*/
KEYWORD_ADD_ID(embed)

/*^ @brief The endif directive as specified by the C standard ^*/
KEYWORD_ADD_ID(endif)

/*^ @brief The error directive as specified by the C standard ^*/
KEYWORD_ADD_ID(error)

/*^ @brief The else directive as specified by the C standard ^*/
KEYWORD_ADD_ID(if)

/*^ @brief The include directive as specified by the C standard plus extensions
 **
 ** @see [extension to directives](@ref dir_extensions)
 ^*/
KEYWORD_ADD_ID(include)

/*^ @brief The line directive as specified by the C standard ^*/
KEYWORD_ADD_ID(line)

/*^ @brief The undef directive as specified by the C standard ^*/
KEYWORD_ADD_ID(undef)

/*^ @brief The warning directive as specified by the C standard ^*/
KEYWORD_ADD_ID(warning)

/*^ @}!*/

/*^
 ** @name C standard shortcuts for directives
 ^*/
/*^ @{!*/

KEYWORD_ADD_ID(elifdef, elif defined)
KEYWORD_ADD_ID(elifndef, elif !defined)
KEYWORD_ADD_ID(ifdef, if defined)
  KEYWORD_ADD_ID(ifndef, if !defined)

    /*^ @}!*/

    /*^
     ** @name EĿlipsis specific variations of directives
     ^*/
    /*^ @{!*/

    /*^
     **
     ** @brief Always expand the code up to the next [%%end](@ref end) or [%%endif](@ref endif)
     **
     ** This can be used for example to ensure that a set of
     ** [%%bind](@ref bind) directives is restricted to the lines up to the end of this
     ** construct.
     **
     ** ```{.C}
     ** %%always
     ** ...
     ** %%end
     ** ```
     **
     ** is the same as
     **
     ** ```{.C}
     ** %%if false
     ** %%else
     ** ...
     ** %%endif
     ** ```
     **
     ** @remark This is preferable over [%%if](@ref if) `true` because
     ** [%%elif](@ref elif) and [%%else](@ref else) are inhibited.
     ^*/
    KEYWORD_ADD_ID(always)

    /*^
     **
     ** @brief A local equivalent to [%%define](@ref define)
     **
     ** The difference is that the macro is undefined (as if by
     ** [%%undef](@ref undef)) at the end of the current
     ** [%%if](@ref if)/[%%elif](@ref elif)/[%%else](@ref else) block or source file.
     ^*/
    KEYWORD_ADD_ID(bind)

    /*^
     ** @brief Embed the data source file but without expanding the token list of the directive
     **
     ** Other arguments to this directive are not expanded, unless the
     ** [%%expand](@ref expand) prefix is applied.
     **
     ^*/
    KEYWORD_ADD_ID(embed_resource)

    /*^
     ** @brief Expand the remaining tokens on the line and re-interpret
     ** the result as a directive
     **
     ** ```{.C}
     ** %%expand tokens …
     ** ```
     **
     ** So, after expansion, the first token in the resulting list should
     ** be a directive. Here, the resulting directive may be uglified by
     ** appending double underscores in front and back to avoid expansion
     ** if that happens to be defined as a macro.
     **
     ** If for example `ARRAY(100)` is a macro and expands to
     ** `array_name_100` and `VALUE(100)` expands to `100*37` the
     ** following
     **
     ** ```{.C}
     ** %%expand __define__ ARRAY(100) VALUE(100)
     ** ```
     **
     ** leads to
     **
     ** ```{.C}
     ** __define__ array_name_100 100*37
     ** ```
     **
     ** which is the same as if we had the following directive
     **
     ** ```{.C}
     ** %%define array_name_100 100*37
     ** ```
     **
     ** which defines a new macro `array_name_100` with the indicated
     ** expression as expansion.
     ^*/
    KEYWORD_ADD_ID(expand)

    /*^
     ** @brief Execute only the directives in the given header file
     **
     ** All tokens that would be produced are discarded, only side effects
     ** such as macro definitions have an effect on the source that uses
     ** this directive.
     **
     ** Other arguments to this directive are not expanded, unless the
     ** [%%expand](@ref expand) prefix is applied.
     **
     ^*/
    KEYWORD_ADD_ID(include_directives)

    /*^
     ** @brief Include the source file but without expanding the token list of the directive
     **
     ** Other arguments to this directive are not expanded, unless the
     ** [%%expand](@ref expand) prefix is applied.
     **
     ^*/
    KEYWORD_ADD_ID(include_source)

    /*^
     ** @brief As the [%%line](@ref line) directive but without expanding
     ** the rest of the line
     ^*/
    KEYWORD_ADD_ID(linenumber)

    /*^
     **
     ** @brief Never expand the code up to the next [%%end](@ref end) or [%%endif](@ref endif)
     **
     ** This can be used for example to comment out a set of lines without
     ** having to change them individually.
     **
     ** @remark This is preferable over [%%if](@ref if) `false` because
     ** [%%elif](@ref elif) and [%%else](@ref else) are inhibited.
     ^*/
    KEYWORD_ADD_ID(never)

    /*^
     **
     ** @brief Only expand the code up to the next [%%end](@ref end),
     ** [%%endif](@ref endif) or the end of the header once.
     **
     ** In particular this can be used to skip over a whole header
     ** file that has already be seen.
     **
     ** The directive receives one optional argument, which is an
     ** identifier that helps to identify the source file (or
     ** [%%if](@ref if) construct) in question. If omitted, a hash of
     ** the base-name of the current file name is used.
     **
     ** @remark This is preferable over [%%ifdef](@ref ifdef) include
     ** guards because it avoid to read over the header, again.
     **
     ** @see @ref pragma This is also interfaced via the legacy @ref
     ** pragma `once`.
     ^*/
    KEYWORD_ADD_ID(once)

    /*^
     ** @brief Find an environment variable through the C library call
     ** `getenv` and define a macro of the same name.
     **
     ** The contents of the environment variable is tokenized as if it
     ** were given in a %%@ref define directive. The remaining token list
     ** (which may be empty) is the default setting if the environment
     ** variable is not found.
     **
     ** Example:
     **
     ** ```{.C}
     ** %%environment LOOP_DEPTH 4
     ** %%environment VALUES 5, 6, 78
     ** %%environment QUESTION
     ** %%environment HOME
     ** ```
     **
     ** inspects the environment variables and defines the macros
     ** `LOOP_DEPTH`, `VALUES`, `QUESTION` and `HOME`. If there is no such
     ** environment variable it is set to the token list as indicated. So
     ** if there is no `QUESTION` environment variable, the replacement
     ** list of that macro would be empty.
     **
     ** For `HOME` most operating systems provide a name of a directory so
     ** in general the macro `HOME` will not be empty. Note thought that
     ** eĿlipsis provides the contents of this variable as raw token
     ** sequence, not as a string. This allows for more flexibility when
     ** you have to compose path names.
     **
     ** ```{.C}
     ** %%environment HOME
     ** %%include <HOME/include/>
     ** ```
     **
     ** Would establish a subdirectory of the user′s home directory as a
     ** place to seek for include files.
     **
     ** Or, if you want something like this inside a string
     ^*/
    /* In the following there is a "zero width space" character within
       __STRINGIFY__ such that it survives for the documentation. */
    /*^
     ** ```
     ** %%include __EXPAND_STRING​IFY__(HOME/include/my_favorite.h)
     ** ```
     ^*/
    /*^
     ** @remark A macro such defined cannot be undefined.
     **
     ** @remark As for other macro definitions, redefining an existing
     ** macro of the same name is an error if that would result in a
     ** different replacement list.
     ^*/
    KEYWORD_ADD_ID(environment)

    /*^ @}!*/

    /*^
     ** @name EĿlipsis extensions to handle argument lists
     **
     ** Using recursive inclusion would quickly hit a complexity wall if we are
     ** not careful. This is because with "normal" directives we expand
     ** a whole argument lists, for example,
     ** just to split off the first argument and collect the remainder.
     ** This easily makes iterative algorithms quadratic because all the
     ** arguments are touched over and over again.
     **
     ** EĿlipsis provides specialized macro assignment directives that avoid this
     ** complexity.
     ^*/
    /*^ @{!*/

    /*^
     ** @brief Gather token lists into a macro
     **
     ** The general form of that directive is
     **
     ** ```{.C}
     ** %%gather NAME [token₁ [token₂ [ … tokenₖ ] ] ]
     ** ```
     **
     ** where `NAME` is a name of a possibly pre-existing macro, and
     ** `tokenᵢ` are tokens. The difference to `%%define` and similar are
     **
     ** - If one of the `tokenᵢ` is a name of a macro, the replacement
     **   list of that macro is removed from that macro (so it becomes
     **   empty) and spliced in place instead of `tokenᵢ`. But other than
     **   for `%%define` no further expansion takes place.

     ** - If `NAME` is not a pre-existing macro, a new macro is defined
     **   that has the resulting list as replacement list.
     **
     ** - If `NAME` is already a macro, the resulting list is appended to
     **   the current replacement list.
     **
     ** Consider the following example that uses three function-like macros
     **
     ** ```{.C}
     ** %%define A() a , 1
     ** %%define B() b
     ** %%define C() c , d
     ** %%gather C A B
     ** ```
     **
     ** Note that in the `%%gather` line, none of the macros is "invoked",
     ** there are no `()` parenthesis.
     **
     ** After that, the following three source lines
     **
     ** ```{.C}
     ** A: A()
     ** B: B()
     ** C: C()
     ** ```
     **
     ** expand to the replacements
     **
     ** ```{.C}
     ** A:
     ** B:
     ** C: c , d a , 1 b
     ** ```
     **
     ** That is, `C()` has the concatenation of the three lists in the
     ** order they appear in `%%gather`, and `A()` and `B()` are now empty
     ** macros that expand to nothing.
     **
     ** For a more complicated example, take the source of the
     ** ellipsis-foreach-loop.dirs Xfile, which is the bottom of the
     ** recursion of ellipsis-foreach.dirs
     **
     ** @include "ellipsis-foreach-loop.dirs"
     **
     ** If we have that
     **
     ** - `FOREACH_RESULT` is a function-like macro with no parameter that
     **    expands to the tokens &quot;`a , b , c`&quot;
     ** - `BODY` is function-like macro that receives one parameter `X`
     **   and returns the tokens &quot;`array [ X ]`&quot;
     ** - `FOREACH_FIR` expands to the single token &quot;`100`&quot;,
     **
     ** the line
     **
     ** @skipline gather
     **
     ** First expands the tokens on the line, to maybe result in something like
     **
     ** ```{.C}
     ** gather FOREACH_RESULT array [ 100 ]
     ** ```
     **
     ** Note that `gather` and `FOREACH_RESULT` are not expanded. This new
     ** token list is then taken as a directive and `FOREACH_RESULT` now
     ** contains `a, b, c, array[100]`
     **
     ** @remark The complexity of this directive is only related to the
     ** number of tokens `k` that are given, not on the lengths of their
     ** replacement lists or the length of the existing replacement list
     ** of `NAME`.
     ^*/
    KEYWORD_ADD_ID(gather)

    /*^
     ** @brief Delete the contents of `target` and move the contents of
     ** `source` into it.
     **
     ** ```{.C}
     ** %%move target source
     ** ```
     **
     **
     ** @remark The complexity of this directive is `O(1)`.
     ^*/
    KEYWORD_ADD_ID(move)

    /*^
     ** @brief Scatter comma-separated parts of a token list into other macros
     **
     ** The general form of that directive is
     **
     ** ```{.C}
     ** %%scatter [target₁ [target₂ [ … targetₖ] ] ] NAME
     ** ```
     **
     ** where `NAME` is a name of a pre-existing macro, and `targetᵢ` are
     ** identifiers. Here the starting sequence of tokens in `NAME` up to
     ** the first comma is removed from `NAME`, `target1` is made a macro
     ** if it was not before, and the replacement sequence of `target1` is
     ** set to (or replaced by) that starting sequence. Then the now
     ** leading comma is also removed from `NAME` and the procedure is
     ** performed in turn for `target2` etc. Once all `targetᵢ` macros are
     ** processed, `NAME` keeps the remaining token list (again without
     ** the possible comma).
     **
     ** ```{.C}
     ** %%define A()
     ** %%define B()
     ** %%define C() c , d a , 1 b
     ** %%scatter A B C
     ** ```
     **
     ** Note that in the `%%scatter` line, none of the macros is "invoked",
     ** there are no `()` parenthesis.
     **
     ** After that, the following three source lines
     **
     ** ```{.C}
     ** A: A()
     ** B: B()
     ** C: C()
     ** ```
     **
     ** expand to the replacements
     **
     ** ```{.C}
     ** A: c
     ** B: d a
     ** C: 1 b
     ** ```
     **
     ** That is, `A()` has the tokens up to the first comma, `B()` those
     ** up to the second then `C()` keeps the remaining list after the
     ** second comma.
     **
     ** For a more complicated example, take the source of the
     ** ellipsis-foreach-loop.dirs Xfile, which is the bottom of the
     ** recursion of ellipsis-foreach.dirs
     **
     ** @include "ellipsis-foreach-loop.dirs"
     **
     ** If we have that
     **
     ** - `FOREACH_ARGS` is a function-like macro with no parameter that
     **    expands to the tokens &quot;`17 , 9 , 100`&quot;

     ** - `FOREACH_FIR` is a function-like macro with no parameter that
     **   might have any contents
     **
     ** after the line
     **
     ** @skipline scatter
     **
     ** the snippet
     **
     ** ```{.C}
     ** FOREACH_FIR:  FOREACH_FIR()
     ** FOREACH_ARGS: FOREACH_ARGS()
     ** ```
     **
     ** would expand to
     **
     ** ```{.C}
     ** FOREACH_FIR:  17
     ** FOREACH_ARGS: 9 , 100
     ** ```
     **
     ** @remark The complexity of this directive is only related to the
     ** number of targets `k` that are given and on the length of the
     ** corresponding lists in `NAME`. If `n₁`, …, `nₖ` are the lengths of
     ** these lists, the complexity is `O(Σᵢ nᵢ)`. The remaining list in
     ** `NAME` stays otherwise untouched and may be arbitrarily long without
     ** influencing the complexity.
     ^*/
    KEYWORD_ADD_ID(scatter)

    /*^ @}!*/

    /*^
     ** @name Shortcuts for explicit expansion
     ^*/
    /*^ @{!*/

    KEYWORD_ADD_ID(end, endif)

    /*^
     ** @brief Define the name of the current unit.
     **
     ** Each source file that is included has its own unit name. The
     ** pseudo variables @ref ⸤__UNIT__⸥ and
     ** @ref ⸤__PARENT_UNIT__⸥ give access to that name and
     ** to the name of the unit that included the current one. They
     ** can also be abbreviated by `¤` and `..` respectively.
     **
     ** If you do not want to use composed names, this can be as simple
     ** as in
     **
     ** ```
     ** #unit bla
     ** ```
     **
     ** to name the current unit `bla`. If you want to compose the
     ** current name from the parent unit you can go with
     **
     ** ```
     ** #unit ⸤¤¤::kitten⸥
     ** ```
     **
     ** If the parent unit is `⸤my::favorite⸥`
     ** this would set the
     ** current unit to `⸤my::favorite::kitten⸥`;
     ** if there is no parent unit and if the environment variable @ref ⸤__PROJECT__⸥ is set to
     ** `⸤my::favorite⸥` the result for the current unit would be the
     ** same.
     **
     ** @remark The linker name used for this unit would then be
     ** `my∷favorite∷kitten`.
     **
     ** @warning This directive interacts with macros and keywords, so
     ** you should not use components of names that would expand or
     ** that is a keyword.
     **
     ** @see @ref ⸤__PROJECT__⸥
     ** @hideinitializer
     ^*/
    KEYWORD_ADD_ID(unit, expand __bind__ __UNIT__)
    KEYWORD_ADD_ID(xbind, expand __bind__)
    KEYWORD_ADD_ID(xdefine, expand __define__)
    KEYWORD_ADD_ID(xembed_resource, expand __embed_resource__)
    KEYWORD_ADD_ID(xinclude_source, expand __include_source__)
    KEYWORD_ADD_ID(xlinenumber, expand __linenumber__)

    /*^ @}!*/
