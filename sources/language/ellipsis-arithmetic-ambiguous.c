PUNCTUATOR_TEXT0(#### Ambigous C and C++ punctuators for artithmetic {#c_ambiguous})

  PUNCTUATOR_TEXT0(We cannot easily use Unicode replacements for these,
                   so they are not automatically transformed. The note
                   indicates the meaning of the replacement.)

    PUNCTUATOR_TEXT0(| sequence | replacement | note |)
    PUNCTUATOR_TEXT0(|-|-|-|)
    PUNCTUATOR_ADDON("&", "∩", bit-wise (not address))
    PUNCTUATOR_ADDON("*", "×", multiply (not reference))
