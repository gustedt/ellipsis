SPECIAL_TEXT0(### A generic directive and comment starter {#generic_directive})

SPECIAL_TEXT0(These two special directives on entire lines are recognized for all languages.)

  SPECIAL_TEXT0(| start | end | category | note |)
  SPECIAL_TEXT0(|-|-|-|-|)
  SPECIAL_ADDON("⎔", "\n", directive,, U2394, Software-Function Symbol)
  SPECIAL_ADDON("⎔⎔", "\n", comment,, two times U2394)
