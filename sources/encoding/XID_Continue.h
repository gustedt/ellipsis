
/**
 ** @file
 **
 ** @warning This is a file generated by eĿlipsis version 20250219, do not modify
 **/
#ifndef XID_CONTINUE_H
#define XID_CONTINUE_H 1

extern ellipsis‿bitset‿base ellipsis‿XID‿Continue[0x1000];

#endif
