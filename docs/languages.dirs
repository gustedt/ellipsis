# Supported languages {#languages}

[TOC]

Support for different programming and text processing languages is
still under construction. In general, these have a notion of string
and literals such as `"this is a string"` by using double quotes,
similar to C.

Single quotes for character constants are not generalized to other
languages, because they would conflict with the the ' character's
common use as apostrophe.

⎔ include_source "language.dirs" \
    __prefix__(bind LANG empty)

#### Language support

Even if the source and target language is not given (or not
recognized) eĿlipsis can directly be used as a macro preprocessor by
providing macro values on the command line

```{}
ellipsis -DTOTO=1 my-file.txt
```

this allows a basic level of configuration, for example, or an access
to external information such as the time (using `__TIME__`) or similar
builtin and predefined macros, see @ref ellipsis-builtins.dirs and
@ref ellipsis-predefined.dirs.

You may also use the character &#x2394; (codepoint 0x2394,
Software-Function Symbol) at the start of a line, much as you would
use `#` in C to start a line with a directive. Examples

<!-- Beware that in the following the ⎔ is hacked such that it appears -->
<!-- in the markdown output -->

```{}
​⎔⎔ This is a comment! It would just remove the line from the output.​
​⎔ define HEI(X) __EXPAND_STRINGIFY__(__CONCAT__(X, __COUNTER__))
​⎔ ifdef HEI
  do something nice HEI(you)
​⎔ endif

This here would insert the current time: __TIME__, but when protected
inside a string it wouldn't: "__TIME__".
```

This would produce something like the following, but probably with
more empty lines than are shown here:

~~~
⎔⎔ This is a comment! It would just remove the line from the output.​
⎔ define HEI(X) __EXPAND_STRINGIFY__(__CONCAT__(X, __COUNTER__))
⎔ ifdef HEI
  do something nice HEI(you)
⎔ endif

This here would insert the current time: __TIME__, but when protected
inside a string it wouldn't: "__TIME__".
~~~

This syntax works in all languages and allows to include macro
packages that are independent of the target language, see for example
the @ref include directory.

Note that this approach is line oriented and may not be properly
adapted for languages where new lines have not much of a significance,
see below.

If you'd like to avoid too many empty lines appearing in the output
you might collect all directives in a file (using e.g the extension
.dirs) and then just include this file

```{}
​⎔ include_directives "my-macros.dirs"
```

Other than #::include, this would throw away all produced output
lines. On the other hand, this still defines all macros that have
definitions, there. Only one empty line, for the #@ref include_directives
directive remains in the output.

In general, C's include syntax with `<` and `>` will not be supported
for other languages; these characters may have different syntactic
properties. Instead, we use left- and right-pointing angle brackets as
in

```{}
​⎔ include_directives ⟨ellipsis-foreach.dirs⟩
```

Since the `#` character has a special meaning in some languages that
we want to handle, we cannot use it for the special operators in macro
definitions, either. We use `⌗` (VIEWDATA SQUARE, `\u2317`) for
stringification, and `⨝` (JOIN, `\u2AD1`) for token concatenation.

```
⎔ define PAIR(X) toto_ ⨝ X = ⌗X
```

defines the macro `PAIR` suitable for all languages. The corresponding
C-only definition would be

```
# define PAIR(X) toto_ ## X = #X
```

@see @ref generic_directive
@see @ref special_include

⎔ include_source "language.dirs" \
    __prefix__(bind LANG c)

#### Language support

The support for the preprocessing part of the C language should be
quite complete, including comment syntax and string and character
literals.

eĿlipsis may be used for C in two different flawors:

- As an additional preprocessor that produces intermediate program
  text that can be saved and distributed with your sources. This is
  how it is currently used for eĿlipsis itself: this allows to use all
  the nice extensions by still guaranteeing that others may compile
  with existing C compilers. There are special operators that may be
  used to beautify these intermediate sources, see @ref
  c_extension_operators.
- As a replacement for the preprocessing phase of an existing C
  compiler. Note that this needs knowledge about the target
  architecture **and** the target compiler. For the latter, we have in
  particular to know about support for attributes, such that we can
  implement the @ref __has_c_attribute feature test correctly. The data
  that is needed for that can entirely be provided by predefined
  macros. The subdirectory "c" already contains a bunch of such header
  files with the necessary definitions for gcc and clang, and the
  Makefile there can be used to produce files for new versions of
  these.

@see @ref c_structure
@see @ref c_sideeffects
@see @ref c_extension_operators
@see @ref c_ambiguous
@see @ref c_unambiguous

⎔ include_source "language.dirs" \
    __prefix__(bind LANG lex)

#### Language support

Lex has no preprocessor that would already be defined. Our current
implementation only adds two features

- directives similar to C, but by using a `%`​`:` token instead of `#` to
  introduce the directive at the start of the line.
- C style comments between "/*" and "*/"

Everything that is not directive or comment is just passed through

@bug "\t" does not yet work correctly for lex

⎔ include_source "language.dirs" \
    __prefix__(bind LANG html)

#### Language support

Support for html is rudimentary.

- Html comments are between `<!--` and `-->` as usual.
- Directives are implemented as special comments with `<!--#` and `-->`
  markers. So here directives are *not* line oriented but just
  constructs that appear in the usual flow of text.

@see @ref html_comments
@see @ref html_directives

⎔ include_source "language.dirs" \
    __prefix__(bind LANG markdown)

#### Language support

eĿlipsis is quite usable already to be used as a filter for pandoc. In
my makefiles I have something similar to the following rule:

```{.make}
%.html : %.md
	${HOME}/build/ellipsis/bin/ellipsis $< | pandoc - --toc -s -o $@
```

I use this regularly to include some basic configuration such as
document numbers and other macros.

```{.html}
<!--# if __has_include("./document-numbers.md") -->
<!--#   include_source "./document-numbers.md"  -->
<!--# endif                                     -->
```

This can afterwards be use as the following

```{.md}
document number | date | comment
---|---|---
WG14DOC(__PreProOverview__) | **__EXPAND_DEC__(__INTEGER_DATE__/100)** | this paper, original proposal
```

Which results in the a table and links as follows

document number | date | comment
---|---|---
[n3190](https://open-std.org/JTC1/SC22/WG14/www/docs/n3190.htm) | **202406** | this paper, original proposal

Builtins and predefined macros are documented in the following source
files: @ref ellipsis-builtins.dirs and @ref ellipsis-predefined.dirs.

This is also partially used for the documentation here. This
documentation is written for doxygen, which supports a markdown
dialect that has some rough edges. Nevertheless, the collection of
information from different parts of the project (such as on this page
for the different languages) seems to work quite well.

Markdown inherits comments and directives from html. Html comments are
already commonly used for markdown, so this seemed the easiest way to
establish these features, here.

In addition to that, we support code snippets within the flow of text
with backticks as in "`func(34)`". Per default, these are supposed to
be code snippets written in the C programming language. This language
can be overwritten:

- per snippet by appending a `{}` construct (does not work for doxygen!)
- on the command line such as in `-DSOMETHING={.fortran}`
- in an environment variable, see ellipsis-environment.dirs

Additionally, features that are introduced in markdown starting at the
beginning of a new line are also supported:

- full line code snippets with "```" on a separate line
- head lines starting with `#`, `##` …
- citation lines starting with `>`, `>>`, …
- items lines starting with `-`, `+`, and `*`

@see @ref html_comments
@see @ref html_directives
@see @ref md_decoration
@see @ref md_code
@see @ref md_starter
@see @ref ellipsis-environment.dirs has the predefined environment variables


