
# Legal considerations {#legal}

@author Jens Gustedt
@copyright Jens Gustedt, 2024
@date "2025-02-10"
@version 20250219

- This software is licensed under a BSD 3-Clause License, see below.

- The documentation is licensed under a Creative Commons license, <a
 href="https://creativecommons.org/licenses/by-nc-nd/4.0/?ref=chooser-v1"
 target="_blank" rel="license noopener noreferrer"
 style="display:inline-block;">CC BY-NC-ND 4.0<img
 style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
 src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"
 alt=""><img
 style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
 src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"
 alt=""><img
 style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
 src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"
 alt=""><img
 style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
 src="https://mirrors.creativecommons.org/presskit/icons/nd.svg?ref=chooser-v1"
 alt=""></a>


## BSD 3-Clause License

Copyright (c) 2024, Jens Gustedt

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

