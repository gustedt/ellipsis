# Downloading, compiling, configuring and running eĿlipsis  {#compile_run}

## Downloading {#downloading}

To download eĿlipsis you need [git](https://git-scm.com/). The
repository can be found at

> https://gitlab.inria.fr/gustedt/ellipsis

You should probably not clone the whole repository, the branch that
holds the expanded documentation is quite large (&gt; 1GiB). The
`main` branch should be completely sufficient if you want to work with
eĿlipsis. To download the sources as a subdirectory `ellipsis` in your
current directory you could use

```{.shell}
git clone --single-branch https://gitlab.inria.fr/gustedt/ellipsis
```

This only downloads some MiB onto your disk.

## Compiling  {#compiling}

eĿlipsis is only tested on a Linux platform, but should work equally
well on other POSIX platforms.

Compiling eĿlipsis needs a compiler that is conforming to C23, for
example recent gcc or clang should do. Besides that not much is
needed. Just go into the `sources` directory and compile. For example
this one launches 4 parallel processes to compile an link eĿlipsis.

```{.shell}
cd sources
make distclean
CC=gcc-14 make -j 4
```

The `distclean` step is important when you compile for the first time,
because when checking the project out with `git` some time stamps for
the generated sources (see below) may be misleading.

As usual you may play around with the compiler you are using and with
compiler flags, for example

```{.shell}
CC=gcc-14 CFLAGS='-O3 -march=native -fanalyzer -g -Wall' make -j 4
```

to fully optimize and enable gcc's static analyzer. The resulting
executable `bin/ellipsis` has no binary dependencies other then
system libraries.

To test ellipsis on its own generic source in sources/generic you may
launch

```{.shell}
make -B generate-all
```

This should run eĿlipsis to produce many of the sources, but at the
end nothing in the sources/ directory but white space should have
changed. If you have [astyle](https://astyle.sourceforge.net/)
installed you may beautify the sources by doing

```{.shell}
make beautify
```

Then,

```{.shell}
git status
```

should show no changes. If you are not able to do the "beautify" step,
the difference should only be whitespace.

## Configuration {#configuration}

EĿlipsis configures itself a program startup time. That is where it is
decided for which language and compiler platform we are working
today. If you'd like to use eĿlipsis for a compiler that we don't
know, yet, you'd have to look into the configuration files in the
`"../include"` directory. In particular modern C needs some work here,
because since C23 knowledge about attributes (a language feature) has
to be integrated into the preprocessor.

To have run-time access to configuration files and extensions,
eĿlipsis uses several approaches:

- These files are embedded into the `ellipsis` executable by means of
  #@ref embed. It then has access to them via standard include notation
  such as `<ellipsis-do.dirs>`. By this, the executable can be placed
  anywhere in your file system and does not need any special
  configuration directory. This feature uses a POSIX specific function
  `fmemopen` to open a stream over a storage buffer.
- If you want to experiment or change these files you may switch of
  this default via the environment variable
  `ELLIPSIS_HASH_INCLUDE_DISABLE`.
  - At startup the executable then tries to determine is effective
    location in the file system and searches for `"../include"` from
    there. This feature uses OS specific extensions (POSIX and
    (untested) Windows).
  - If this is not found you should still be able to use an include
    commandline argument `-I` to point `ellipsis` in the right
    direction.

I personally use eĿlipsis mostly as separate phase producing
intermediate source files. If you want to use it as a direct
replacement of the preprocessor that comes with your compiler, there
still might be some rough edges.

There is now a replacement drop-in ellipsis-gnuc.sh that enables
eĿlipsis as a preprocessor for C compilers that that are compatible
with gcc to some extent, in particular all recent `gcc` and `clang`
versions. So when you use this, the painful creation of configuration
headers for new compiler versions is not needed.

## Running {#running}

The executable that is produced is named bin/ellipsis. For constraints
of moving this executable around, please see above.

EĿlipsis understands some of the usual command line arguments for C
compilers,

| option | needs argument | meaning                                                                                       |
|--------|----------------|-----------------------------------------------------------------------------------------------|
| `-D`   | yes            | Define a macro. Name and parameter list are separated from the definition by a `=` character. |
| `-U`   | yes            | Undefine a macro.                                                                             |
| `-I`   | yes            | Set an include directory.                                                                     |
| `-x`   | yes            | Set an input language.                                                                        |
| `-v`   | no             | Be verbose for the output.                                                                    |
| `-d`   | no             | Dump some debug information.                                                                  |

and may also react to the setting of some environment variables, see
@ref ellipsis-environment.dirs

An argument that is not one of the above options (or the associated
argument) is supposed to be a file name for the first source. A file
extension may be used to determine the input language. This can be
overwritten by the `-x` option.  Language independent source files may
be included as we go, e.g `〈ellipsis-do.dirs〉`.

@see @ref languages
@see ellipsis-do.dirs

