














# Punctuators {#punctuators_section}

[TOC]

Punctuators are special sequences of characters that have a specific
sematics in the target language. They are encoded as such and
terminate immediately. Eŀlipsis proposes single Unicode replacements for
those where a code point exists that has a description that matches
the semantics.

@remark As an extension, eĿlipsis also accepts these code points on input.

## General Punctuators

These are used by eĿlipsis in several contexts, independently of the
language that is processed

### Arithmetic {#arithmetic}

These punctuators are use in eĿlipsis expressions in #@ref if conditionals
and [__EVALUATE__](@ref __EVALUATE__) contexts.


#### Unambigous C and C++ punctuators for artithmetic {#c_unambiguous}

@note Some of these appear here as backslash encoded strings such that they do not disturb the markdown output.

| sequence | replacement | note |
|-|-|-|
| "!"| "¬"| |
| "!="| "≠"| |
| "&&"| "∧"| |
| "-"| "−"| |
| "/"| "∕"| |
| "<<"| "◀"| |
| "<="| "≤"| |
| "=="| "≡"| |
| ">="| "≥"| |
| ">>"| "▶"| |
| "❘"| "∪"| vertical bar|
| "❘❘"| "∨"| two vertical bars|
| "~"| "∁"| |



#### Ambigous C and C++ punctuators for artithmetic {#c_ambiguous}

We cannot easily use Unicode replacements for these,so they are not automatically transformed. The note indicates the meaning of the replacement.

| sequence | replacement | note |
|-|-|-|
| "&"| "∩"| bit-wise (not address)|
| "*"| "×"| multiply (not reference)|


### EĿlipsis specific punctuators for all languages {#specific}


#### Punctuators that are mandatory for all languages {#ellipsis}
| sequence | replacement | note |
|-|-|-|
| "..."| "…"| for variable argument lists|
| "⸤"| | @ref keep, start an unfiltered token sequence|
| "⸥"| | @ref peek, end an unfiltered token sequence|
| "¤"| | the full name of the current unit|
| "¤¤"| | the full name of the parent unit|
| "¤¤¤"| | the full name of the grand-parent unit|
| "¤¤¤¤"| | the full name of the great-grand-parent unit|
| "¤¤¤¤¤"| | the full name of the great-great-grand-parent unit|
| ".∷"| | prefix with the full name of the current unit, obsolete|
| ".::"| ".∷"| prefix with the full name of the current unit, obsolete|
| "..∷"| | prefix with the full name of the parent unit, obsolete|
| "..::"| "..∷"| prefix with the full name of the parent unit, obsolete|



#### Punctuators for directives

Directives form their own sublanguage with a specific set of punctuators that are used as operators.

@see @ref ellipsis

| sequence | replacement | note |
|-|-|-|
| "⌗"| | the stringify operator|
| "⨝"| | the token join operator|
| "#"| "⌗"| the stringify operator|
| "%:"| "⌗"| the stringify operator|
| "##"| "⨝"| the token join operator|
| "%:%:"| "⨝"| the token join operator|


## Punctuators for C

@see @ref arithmetic
@see @ref ellipsis


### C operators with side effects {#c_sideeffects}

C has a whole set of operators that combine arithmetic with side effects on objects that make no sense for the preprocessor.

| sequence | replacement | note |
|-|-|-|
| "%="| | |
| "&="| "∩="| |
| "*="| "×="| |
| "++"| | |
| "+="| | |
| "--"| | |
| "-="| "−="| |
| "/="| "∕="| |
| "<<="| "◀="| |
| ">>="| "▶="| |
| "^="| | |
| "❘="| "∪="| |



### C punctuators for code structure {#c_structure}

@warning the FULL STOP character should never be listed here. It is always a punctuator unless it is followed by a digit, whence it starts a numeral.

| sequence | replacement | note |
|-|-|-|
| "%>"| "}"| |
| "->"| "→"| |
| "∷"| | extension for composed names|
| ":>"| "]"| |
| "<%"| "{"| |
| "<:"| "["| |

### eĿlipsis extension operators for C {#c_extension_operators}

These are accepted as tokens in C and produce the indicated effect on the output stream. This can e.g. be used to "escape" directives that should be conserved for the output or to preprocess comments such that they can be used for documentation.

| sequence | replacement | output |
|-|-|-|
| "%%"| | a # token|
| "%%%%"| | a ## token|
| "<❘"| "⸤"| @ref keep, start an unfiltered token sequence|
| "❘>"| "⸥"| @ref peek, end an unfiltered token sequence|
| "/*^*/"| | a newline|
| "/*>*/"| | a tab character|
| "/*^"| | start a doxygen comment|
| "^*/"| | end a doxygen comment|
| "/*!"| | start a comment|
| "!*/"| | end a comment|

### instrument opening and closing braces in C {#c_extension_defer}

These are accepted as tokens in C and can be used to add preprocessor callbacks to these constructs. This is e.g used by hierarchically named identifiers and by the defer extension.

| sequence | replacement | meaning |
|-|-|-|
| "{"| | instrumentation, implemented by a macro|
| "}"| | instrumentation, implemented by a macro|
| "["| | instrumentation, implemented by a macro|
| "]"| | instrumentation, implemented by a macro|
| "[["| | instrumentation, implemented by a macro|
| "]]"| | instrumentation, implemented by a macro|
| "⟦"| | instrumentation, implemented by a macro|
| "⟧"| | instrumentation, implemented by a macro|
| "="| | instrumentation, implemented by a macro|
| ";"| | instrumentation, implemented by a macro|
| "::"| | used for attributes and composed names|


## Punctuators for markdown

@see @ref md_decoration
@see @ref md_code


### Markdown punctuators at the start of lines {#md_starter}

| sequence | replacement | note |
|-|-|-|
| "---"| | a horizontal line|
| "-"| | an item in a list|
| "+"| | an item in a list|
| "*"| | an item in a list|
| ">"| | citation, first level|
| ">>"| | citation, second level|
| ">>>"| | citation, third level|
| ">>>>"| | citation, fourth level|
| ">>>>>"| | citation, fifth level|
| ">>>>>>"| | citation, sixth level|
| "#"| | heading, first level|
| "##"| | heading, second level|
| "###"| | heading, third level|
| "####"| | heading, fourth level|
| "#####"| | heading, fifth level|
| "######"| | heading, sixth level|



