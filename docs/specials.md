










# Specials {#specials_section}

[TOC]

Specials are special sequences of characters that have a specific
sematics in the target language. They are encoded with two character
sequences, a start and a closing, and have contents between these two.

@remark Some special starters may not start at arbitrary positions but
have to be the first token on the line. This is not yet properly
handled by eĿlipsis.

@remark Many specials have the end of the line as closers. This is
encoded by using "\n" as a closer.

@see @ref decorations

## Specials valid for all languages


### A generic directive and comment starter {#generic_directive}

These two special directives on entire lines are recognized for all languages.

  | start | end | category | note |
  |-|-|-|-|
  | "⎔"| "\n"| [directive](@ref directive) | U2394, Software-Function Symbol|
  | "⎔⎔"| "\n"| [comment](@ref comment) | two times U2394|



### string literals that are mandatory for eĿlipsis

  | start | end | category | note |
  |-|-|-|-|
  | "U\""| "\""| [string](@ref string) | |
  | "\""| "\""| [string](@ref string) | |
  | "u8\""| "\""| [string](@ref string) | |
  | "u\""| "\""| [string](@ref string) | |
  | "x\""| "\""| [binary](@ref binary) | |
  | "B\""| "\""| [base64](@ref base64) | |
  | "R\""| "\""| [verbatim](@ref verbatim) | |
  | "uR\""| "\""| [verbatim](@ref verbatim) | |
  | "UR\""| "\""| [verbatim](@ref verbatim) | |
  | "u8R\""| "\""| [verbatim](@ref verbatim) | |
  | "LR\""| "\""| [verbatim](@ref verbatim) | |



### Special include paths for all languages {#special_include}

  Pointing angle brackets (code points U27E8 and U27E9) are accepted as general syntax to determine include paths.

  | start | end | category | note |
  |-|-|-|-|
  | "⟨"| "⟩"| [string](@ref string) | |
  | "〈"| "〉"| [string](@ref string) | |


## C specials


### C comments

| start | end | category | note |
|-|-|-|-|
| "//"| "\n"| [comment](@ref comment) | |
| "/*"| "*/"| [comment](@ref comment) | |



### Special include paths for C

  In C, less and greater signs are accepted complementing pointing angle brackets to determine include paths.

  | start | end | category | note |
  |-|-|-|-|
  | "<"| ">"| [string](@ref string) | |



### Character constants and wide strings

| start | end | category | note |
|-|-|-|-|
| "\'"| "\'"| [character](@ref character) | |
| "U\'"| "\'"| [character](@ref character) | |
| "u\'"| "\'"| [character](@ref character) | |
| "u8\'"| "\'"| [character](@ref character) | |
| "L\'"| "\'"| [character](@ref character) | |
| "L\""| "\""| [string](@ref string) | |


@see @ref c_directive

## Html specials


### Html constructs with different syntax elements

| start | end | category | note |
|-|-|-|-|
| "<!"| ">"| [special](@ref special) | |
| "<style>"| "</style>"| [special](@ref special) | |
| "<script>"| "</script>"| [special](@ref special) | |



### Html comments {#html_comments}

| start | end | category | note |
|-|-|-|-|
| "<!--"| "-->"| [comment](@ref comment) | |



### Directives in html {#html_directives}

| start | end | category | note |
|-|-|-|-|
| "<!--#"| "-->"| [directive](@ref directive) | |


## Markdown specials


### Verbatim code inclusion {#md_code}

| start | end | category | note |
|-|-|-|-|
| "`"| "`"| [code](@ref code) | |


@see @ref md_decoration
@see @ref html_comments
@see @ref html_directives

## Decorations: specials for line starts only {#decorations}


### A generic directive and comment starter {#generic_directive}

These two special directives on entire lines are recognized for all languages.

  | start | end | category | note |
  |-|-|-|-|
  | "⎔"| "\n"| [directive](@ref directive) | U2394, Software-Function Symbol|
  | "⎔⎔"| "\n"| [comment](@ref comment) | two times U2394|



### C directives {#c_directive}

| start | end | category | note |
|-|-|-|-|
| "%\r:"| "\n"| [directive](@ref directive) | |
| "#"| "\n"| [directive](@ref directive) | |



### C directives for one stage parsing {#c_directive1}

  In C, if we process in two steps, it is possible to mark directives
    for the second step with "%%" instead of "#".
      This means in particular, that the corresponding line will be expanded
      during the first step.
      To have the same effects when we process only in one step, under that mode
      the directive is prefixed with `expand` to make sure that expansion takes place as well.

      | start | end | category | note |
      |-|-|-|-|
      | "%"":"| "\n"| [directive](@ref directive) | |
      | "%%"| "\n"| [directive](@ref directive) | |
      | "#"| "\n"| [directive](@ref directive) | |



### Markdown special that starts at the beginning of lines {#md_decoration}

| start | end | category | note |
|-|-|-|-|
| "```"| "```"| [codeblock](@ref codeblock) | |

