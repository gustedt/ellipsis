/**
 ** @file
 **
 ** Pseudo source file that only holds documentation of predefined
 ** macros that come with eĿlipsis. Ignored otherwise.
 **/


/**
 ** @def ELLIPSIS_SUM(X, ...)
 ** @brief Sum up the arguments in the argument list.
 **/

/**
 ** @def ELLIPSIS_MUL(...)
 ** @brief Multiply the arguments in the argument list.
 **/

/**
 ** @def ELLIPSIS_XOR(...)
 ** @brief Bitwise exclusive or the arguments in the argument list.
 **/

/**
 ** @def ELLIPSIS_OR(...)
 ** @brief Logical or the arguments in the argument list.
 **/

/**
 ** @def ELLIPSIS_AND(...)
 ** @brief Logical and the arguments in the argument list.
 **/

/**
 ** @def ELLIPSIS_BOR(...)
 ** @brief Bitwise or the arguments in the argument list.
 **/

/**
 ** @def ELLIPSIS_BAND(...)
 ** @brief Bitwise and the arguments in the argument list.
 **/

/**
 ** @def ELLIPSIS_REF(...)
 ** @brief Chain of reference of the arguments in the argument list.
 **/

/**
 ** @def ELLIPSIS_SEMICOLON(...)
 ** @brief Separate the arguments in the argument list with a semicolon instead of a comma.
 **/

/**
 ** @def ELLIPSIS_SPACE(...)
 ** @brief Separate the arguments in the argument list with a space instead of a comma.
 **/

/**
 ** @def ELLIPSIS_MAP(FUNC, ...)
 ** @brief Map the macro or function `FUNC` over all arguments
 **/

/**
 ** @def ELLIPSIS_ISANY(X, ...)
 ** @brief Test equality for an expression against a set of expressions
 **
 ** For example in C
 **
 ** ```{.C}
 ** if (ELLIPSIS_ISANY(a, b, c, d, e)) {
 **  ...
 ** }
 ** ```
 **
 ** would expand to
 **
 ** ```{.C}
 ** if (((a) == (b)) ||((a) == (c)) ||((a) == (d)) ||((a) == (e))) {
 **   ...
 ** }
 ** ```
 **
 ** @warning The first argument is evaluated for each element in the list.
 **/

/**
 ** @def ELLIPSIS_VALID(...)
 ** @brief Test the validity of a chain of references
 **
 ** If any arguments, the first should resolve to an object and then the remaining arguments
 ** to names that recursively define members for the corresponding language.
 ** For example in C
 **
 ** ```{.C}
 ** if (ELLIPSIS_VALID(x, my, chain, of, members)) {
 **  ...
 ** }
 ** ```
 **
 ** would expand to
 **
 ** ```{.C}
 ** if ((x) ∧((x)→my) ∧((x)→my→chain) ∧((x)→my→chain→of) ∧((x)→my→chain→of→members)) {
 **   ...
 ** }
 ** ```
 **
 ** @warning The first argument is evaluated for each element in the list.
 ** @remark An empty list results in `(true)`
 **/


/**
 ** @def __COUNTER__
 ** @brief A global counter that advances at each evaluation
 **
 ** This uses a hidden state that can only be observed by modifying it.
 **
 ** @see __INCREMENT__
 **
 ** @predefined
 **/

/**
 ** @def __DATE__
 ** @brief The compilation date as required by the C standard
 **
 ** @predefined
 **/

/**
 ** @def __ISO_DATE__
 ** @brief The compilation date similar to [__DATE__](@ref __DATE__) but in the ISO date format
 **
 ** @predefined
 **/

/**
 ** @def __INTEGER_DATE__
 ** @brief The compilation date similar to [__DATE__](@ref __DATE__) but as an integer suitable for version numbers
 **
 ** @predefined
 **/

/**
 ** @def __FILE__
 ** @brief The current source file as required by the C standard
 **
 ** @predefined
 **/

/**
 ** @def __LINE__
 ** @brief The current physical source line as required by the C standard
 **
 ** @predefined
 **/

/**
 ** @def __ELLIPSIS__
 ** @brief EĿlipsis's version number
 **
 ** @predefined
 **/

/**
 ** @def __FILENO__
 ** @brief The current source file encoded in a unique number
 **
 ** @predefined
 **/

/**
 ** @def __LINENO__
 ** @brief The first physical source line of the current logical line
 **
 ** @predefined
 **/

/**
 ** @def __TIME__
 ** @brief The compilation time as required by the C standard
 **
 ** @predefined
 **/

/**
 ** @def defined
 ** @brief A place holder for the `defined` preprocessor test as required by the C standard
 **
 ** @predefined
 **/

/**
 ** @def __has_include
 ** @brief A feature test functionality to search for source files
 **
 ** Since C23.
 **
 ** You may use this to check if a particular file is in one of the paths used for
 ** @ref include, @ref include_source or @ref include_directives. For backwards compatibility
 ** with older version of C you may also use this in conditional preprocessing
 ** in the form of `defined(__has_include)` to see if this feature is implemented at all.
 **
 ** @predefined
 **/

/**
 ** @def __has_embed
 ** @brief A feature test functionality to search for data resources
 **
 ** Since C23.
 **
 ** You may use this to check if a particular file is in one of the paths used for
 ** @ref embed or @ref embed_resource. For backwards compatibility
 ** with older version of C you may also use this in conditional preprocessing
 ** in the form of `defined(__has_embed)` to see if this feature is implemented at all.
 **
 ** @predefined
 **/

/**
 ** @def __has_feature
 ** @brief A feature test specific for `clang`
 **
 ** @predefined
 **/

/**
 ** @def __has_extension
 ** @brief A test for compiler extensions specific for `clang`
 **
 ** @predefined
 **/

/**
 ** @def __is_identifier
 ** @brief A test specific for `clang` to see if a name is an identifier or a keyword
 **
 ** @predefined
 **/

/**
 ** @def __limit_include
 ** @brief A feature test functionality obtain the number of lines in a source files
 **
 ** You may use this to obtain the number of lines in a source file used for
 ** @ref include, @ref include_source or @ref include_directives. For backwards compatibility
 ** with older version of C you may also use this in conditional preprocessing
 ** in the form of `defined(__limit_include)` to see if this feature is implemented at all.
 **
 ** @predefined
 **/

/**
 ** @def __limit_embed
 ** @brief A feature test functionality obtain the number of bytes in a data resource
 **
 ** You may use this to obtain the number of bytes in a data resource used for
 ** @ref embed or @ref embed_resource. For backwards compatibility
 ** with older version of C you may also use this in conditional preprocessing
 ** in the form of `defined(__limit_embed)` to see if this feature is implemented at all.
 **
 ** @predefined
 **/

/**
 ** @def true
 ** @brief The constant `true` made accessible to the preprocessor
 **
 ** @predefined
 **/

/**
 ** @def false
 ** @brief The constant `false` made accessible to the preprocessor
 **
 ** @predefined
 **/

/* These don't expand their argument list. */

/**
 ** @def __ANY__(...)
 ** @brief Test if any of the tokens would remain after a first macro replacement,
 ** where undefined identifiers account as-if defined as empty.
 **
 ** Typically this would be used as in
 **
 ** ```{.C}
 ** %:if __ANY__(TOTO)
 ** ...
 ** %:endif
 ** ```
 ** where `TOTO` is some macro. This would be true if the expansion list of `TOTO` has
 ** any contents, regardless of whether or not multiple expansion would again reduce that
 ** to empty. If `TOTO` is not defined as a macro, this is false.
 **
 ** This can also be used with a list of names as in `__ANY__(TOTO TUTU TATA)` where
 ** this would test all three names and return `true` if any of them has contents as a macro. Note that
 ** a use as in `__ANY__(TOTO, TUTU, TATA)` would always return true, because of the commas.
 **
 ** @predefined
 **/

/**
 ** @def __EVALUATE__(...)
 ** @brief Evaluate the arguments as if in an #@ref if preprocessor conditional.
 **
 ** In most cases this probably is not what you want because this primitive does not
 ** expand the argument (so all identifier lead to value `0`). Also the result is
 ** not necessarily a literal but just an internal representation of the integer.
 **
 ** @predefined
 **/

/**
 ** @def __EVALUATE_DEC__(...)
 ** @brief Evaluate the arguments as if in an #@ref if preprocessor conditional and produce a decimal literal.
 **
 ** In most cases this probably is not what you want because this primitive does not
 ** expand the argument (so all identifier lead to value `0`). Most likely you would
 ** want to use [__EXPAND_DEC__](@ref __EXPAND_DEC__) or [__EXPAND_DEC_U__](@ref __EXPAND_DEC_U__) instead.
 **
 ** @predefined
 **/

/**
 ** @def __EVALUATE_HEX__(...)
 ** @brief Evaluate the arguments as if in an #@ref if preprocessor conditional and produce a hexdecimal literal.
 **
 ** In most cases this probably is not what you want because this primitive does not
 ** expand the argument (so all identifier lead to value `0`). Most likely you would
 ** want to use [__EXPAND_HEX__](@ref __EXPAND_HEX__) or [__EXPAND_HEX_U__](@ref __EXPAND_HEX_U__) instead.
 **
 ** @predefined
 **/

/**
 ** @def __EVALUATE_OCT__(...)
 ** @brief Evaluate the arguments as if in an #@ref if preprocessor conditional and produce a octal literal.
 **
 ** In most cases this probably is not what you want because this primitive does not
 ** expand the argument (so all identifier lead to value `0`). Most likely you would
 ** want to use [__EXPAND_OCT__](@ref __EXPAND_OCT__) or [__EXPAND_OCT_U__](@ref __EXPAND_OCT_U__) instead.
 **
 ** @predefined
 **/

/**
 ** @def __EVALUATE_BIN__(...)
 ** @brief Evaluate the arguments as if in an #@ref if preprocessor conditional and produce a binary literal.
 **
 ** In most cases this probably is not what you want because this primitive does not
 ** expand the argument (so all identifier lead to value `0`). Most likely you would
 ** want to use [__EXPAND_BIN__](@ref __EXPAND_BIN__) or [__EXPAND_BIN_U__](@ref __EXPAND_BIN_U__) instead.
 **
 ** @predefined
 **/

/**
 ** @def __ERROR__(...)
 ** @brief Use the `__VA_ARGS` as an error message and abord compilation.
 **
 ** @predefined
 **/

/**
 ** @def __FREEZE__(...)
 ** @brief Protect the identifier in the argument from being expanded
 **
 ** @predefined
 **/

/**
 ** @def __BIND__(...)
 ** @brief Generate an anonymous macro with a definition as if given by the argument list
 **
 ** This does almost the same thing as a #@ref bind directive. The differences are
 **
 ** - The macro that is defined has a random name and that random name
 **   is inserted in place.
 **
 ** - If you use `__BIND__` inside another macro definition, say
 **   `TOTO`, it is not expanded at the time of that definition but
 **   only later when `TOTO` is invoked. So any arguments that are
 **   passed into the invocation of `TOTO` are then replaced in the
 **   definition of the anonymous macro.
 **
 ** - `__BIND_ARGS__`, `__BIND_OPT__` and `__BIND_TAIL__` can be used
 **   instead of the corresponding `__VA_` features. This avoids that
 **   these would be interpreted by the surrounding macro definition.
 **
 ** It can be used when you locally want to apply a macro that saves
 ** some local state to a list of arguments. For example with
 **
 ** ```
 ** # define ELLIPSIS_ISANY(X, …) __BIND__((Y, …) ((X) ≡ (Y)) __BIND_OPT__(∨)__BIND_TAIL__())(__VA_ARGS__)
 ** ```
 **
 ** the invocation
 **
 ** ```
 ** ELLIPSIS_ISANY(precious, a, b, c)
 ** ```
 **
 ** would be as if a macro was bound
 **
 ** ```
 ** #bind HURLY(Y, …) ((precious) ≡ (Y)) __VA_OPT__(∨)__VA_TAIL__()
 ** ```
 **
 ** Note that here the first argument to `ELLIPSIS_ISANY`, `precious`,
 ** is fixed into the generated anonymous macro. Then that macro is in
 ** turned tail called as
 **
 ** ```
 ** HURLY(a, b, c)
 ** ```
 **
 ** with the end result of something like
 **
 ** ```
 ** ((precious) ≡ (a)) ∨ ((precious) ≡ (b)) ∨ ((precious) ≡ (c))
 ** ```
 **
 ** @see #@ref bind
 ** @see @ref tail
 **
 ** @warning If this construct is not followed by `(` as for all
 ** macros the crude internal name of this macro remains in the input
 ** stream. Although this could probably be used to some extent, this
 ** is not recommended.
 **
 ** @warning The validity of the constructed macro is limited in the
 ** same way as macros that are defined with #@ref bind.
 **
 ** @predefined
 **/

/**
 ** @def __MANGLE__(...)
 ** @brief Mangle identifiers
 **
 ** This uses an internal algorithm to produce indentifiers that are composed only of
 ** alphanumeric characters. E.g eĿlipsis converts `__MANGLE__(toto::hui)` into `_ZN4toto3huiE`.
 ** This functionality needed internally to produce macro names corresponding to C attributes,
 ** but could also be used by other features that need an abstraction to compose names
 ** hierarchically.
 **
 ** @predefined
 **/

/**
 ** @def __INCREMENT__(...)
 ** @brief Increment the value of a macro holding an integer
 **
 ** This builtin should receive exactly one parameter, the name of the
 ** macro for which the value is to be incremented. The result is a
 ** token with that incremented value.
 **
 ** @remark If the macro is not yet defined, the macro is first
 ** initialized with a signed zero and the result is a `1` token.
 **
 ** @predefined
 **/

/**
 ** @def __DECREMENT__(...)
 ** @brief Decrement the value of a macro holding an integer
 **
 ** This builtin should receive exactly one parameter, the name of the
 ** macro for which the value is to be decremented. The result is a
 ** token with that decremented value.
 **
 ** @remark If the macro is not yet defined, the macro is first
 ** initialized with a signed zero and the result is a `1` token.
 **
 ** @predefined
 **/

/**
 ** @def __INSTANT__(...)
 ** @brief Return the value of a macro holding an integer
 **
 ** This builtin should receive exactly one parameter, the name of the
 ** macro for which the value is to be sought. The result is a token
 ** with that value.
 **
 ** @remark If the macro is not yet defined, the macro is first
 ** initialized with a signed zero and the result is a `0` token.
 **
 ** @predefined
 **/

/**
 ** @def __CLEAR__(...)
 ** @brief Clear the value of a macro holding an integer
 **
 ** This builtin should receive exactly one parameter, the name of the
 ** macro for which the value is to be cleared. The result is a token
 ** with value `0` or `0U`.
 **
 ** @remark If the macro is not yet defined, the macro is initialized
 ** with a signed zero, `0`.
 **
 ** @predefined
 **/

/**
 ** @def __SET0__(...)
 ** @brief Set the value of a macro holding an integer to `0`.
 **
 ** This builtin should receive exactly one parameter, the name of the
 ** macro for which the value is to be set. The result is a empty token list.
 **
 ** @remark If the macro is not yet defined, the macro is initialized
 ** with a signed zero, `0`.
 **
 ** @predefined
 **/

/**
 ** @def __SET1__(...)
 ** @brief Set the value of a macro holding an integer to `1`.
 **
 ** @see __SET0__
 ** @predefined
 **/

/**
 ** @def __SET2__(...)
 ** @brief Set the value of a macro holding an integer to `2`.
 **
 ** @see __SET0__
 ** @predefined
 **/

/**
 ** @def __SET3__(...)
 ** @brief Set the value of a macro holding an integer to `3`.
 **
 ** @see __SET0__
 ** @predefined
 **/

/**
 ** @def __SET4__(...)
 ** @brief Set the value of a macro holding an integer to `4`.
 **
 ** @see __SET0__
 ** @predefined
 **/

/**
 ** @def __SET5__(...)
 ** @brief Set the value of a macro holding an integer to `5`.
 **
 ** @see __SET0__
 ** @predefined
 **/

/**
 ** @def __SET6__(...)
 ** @brief Set the value of a macro holding an integer to `6`.
 **
 ** @see __SET0__
 ** @predefined
 **/

/**
 ** @def __SET7__(...)
 ** @brief Set the value of a macro holding an integer to `7`.
 **
 ** @see __SET0__
 ** @predefined
 **/

/**
 ** @def __SET8__(...)
 ** @brief Set the value of a macro holding an integer to `8`.
 **
 ** @see __SET0__
 ** @predefined
 **/

/**
 ** @def __SET9__(...)
 ** @brief Set the value of a macro holding an integer to `9`.
 **
 ** @see __SET0__
 ** @predefined
 **/

/**
 ** @def __COMMAS__(...)
 ** @brief Count the number of top level commas in `__VA_ARGS`
 **
 ** Top level here refers to commas that are not enclosed in any parenthesis. E.g `__COMMAS__(,)`
 ** returns `1`, whereas in `__COMMAS__((,))` the comma is protected
 ** and the return is `0`.
 **
 ** @warning Parenthesis in `__VA_ARGS__` should be properly nested.
 **
 ** @predefined
 **/

/**
 ** @def __EMPTY__(...)
 ** @brief Test if `__VA_ARGS__` is empty or not
 **
 ** @predefined
 **/

/**
 ** @def __WARNING__(...)
 ** @brief Use the `__VA_ARGS__` as warning message and continue compilation.
 **
 ** @predefined
 **/

/**
 ** @def __TOUPPER__(...)
 ** @brief Convert the contents of the single token argument to all uppercase.
 **
 ** The argument may be any kind of token, in particular an
 ** identifier, a number, a string or character literal. It simply
 ** transforms all letters that have a case correspondence, even
 ** within a string or character literal. In particular it also
 ** transforms number representations that contain letters to their
 ** uppercase, e.g. `0xb.34afp+1` to `0XB.34AFP+1`.
 **
 ** This only concerns the default Unicode case mapping facilities, in
 ** particular those that are in ASCII. Language specific mappings
 ** such as the German ß → SS are not used.
 **
 ** @see @ref special_upper
 ** @see @ref bijections
 ** @predefined
 **/

/**
 ** @def __TOLOWER__(...)
 ** @brief Convert the contents of the single token argument to all lowercase.
 **
 ** The argument may be any kind of token, in particular an
 ** identifier, a number, a string or character literal. It simply
 ** transforms all letters that have a case correspondence, even
 ** within a string or character literal. In particular it also
 ** transforms number representations that contain letters to their
 ** lowercase, e.g. `0XB.34AFP+1` to `0xb.34afp+1`.
 **
 ** This only concerns the default Unicode case mapping facilities, in
 ** particular those that are in ASCII. Language specific mappings
 ** are not used.
 **
 ** @see @ref special_lower
 ** @see @ref bijections
 ** @predefined
 **/

/**
 ** @def __CONCAT__(…)
 ** @brief evaluate and then concatenate up to 10 arguments
 **
 ** @warning If any argument consists of several tokens, its first
 ** token of is concatenated with the previous argument and its last
 ** token is concatenated with the next argument, if any. Using of this
 ** property is deprecated; use only with arguments that are single
 ** tokens.
 **
 ** @predefined
 ** @hideinitializer
 **/

/**
 ** @def __EXPAND__(…)
 ** @brief expand the arguments
 **
 ** @predefined
 **/

/**
 ** @def __MAP__(F, …)
 ** @brief Construct the body of a macro mapping `F` over all variadic arguments
 **
 ** This can be used in combination with #@ref xdefine to declare a functional macro that
 ** maps `F` over all its other arguments, as in
 **
 ** ```{.C}
 ** #xdefine __STRINGS__(X, …) __MAP__(__STRINGIFY__, X)
 ** ```
 **
 ** If the macro `F` receives more than one argument list them all in
 ** the call to `__MAP__`.
 **
 ** ```{.C}
 ** #xdefine TWO_STRINGS(X, Y, …) __MAP__(__STRINGIFY__, X, Y)
 ** ```
 **
 ** @predefined
 **/

/**
 ** @def __STRINGIFY__(…)
 ** @brief put all the arguments without expanding them, including commas, into one string
 **
 ** @predefined
 **/

/**
 ** @def __EXPAND_STRINGIFY__(…)
 ** @brief expand all the arguments and put the result into one string
 **
 ** @predefined
 **/

/**
 ** @def __STRINGS__(X, …)
 ** @brief create a list of strings formed from all the arguments after they are expanded
 **
 ** @predefined
 **/

/**
 ** @def __COMPACT__(X, …)
 ** @brief Expand all arguments and remove whitespace surrounding them
 **
 ** @predefined
 **/

/**
 ** @def __UNLIST__(X, …)
 ** @brief Expand all arguments and remove commas between them
 **
 ** @predefined
 **/

/**
 ** @def __EXPAND_BIN__(…)
 ** @brief expand all the arguments, evaluate the resulting integer expression and produce a binary literal
 **
 ** @predefined
 **/

/**
 ** @def __EXPAND_BIN_U__(…)
 ** @brief expand all the arguments, evaluate the resulting integer expression and produce a binary literal
 **
 ** The result is a binary integer literal with a `U` suffix such that it alwas is interpreted as unsigned number.
 **
 ** @predefined
 **/

/**
 ** @def __EXPAND_OCT__(…)
 ** @brief expand all the arguments, evaluate the resulting integer expression and produce a octal literal
 ** @predefined
 **/

/**
 ** @def __EXPAND_OCT_U__(…)
 ** @brief expand all the arguments, evaluate the resulting integer expression and produce a octal literal
 **
 ** The result is a octal integer literal with a `U` suffix such that it alwas is interpreted as unsigned number.
 **
 ** @predefined
 **/

/**
 ** @def __EXPAND_DEC__(…)
 ** @brief expand all the arguments, evaluate the resulting integer expression and produce a decimal literal
 **
 ** The type of the constant is signed if it fits, otherwise unsigned.
 **
 ** @predefined
 **/

/**
 ** @def __EXPAND_DEC_U__(…)
 ** @brief expand all the arguments, evaluate the resulting integer expression and produce a decimal literal
 **
 ** The result is a decimal integer literal with a `U` suffix such that it alwas is interpreted as unsigned number.
 **
 ** @predefined
 **/

/**
 ** @def __EXPAND_HEX__(…)
 ** @brief expand all the arguments, evaluate the resulting integer expression and produce a hexadecimal literal
 **
 ** @predefined
 **/

/**
 ** @def __EXPAND_HEX_U__(…)
 ** @brief expand all the arguments, evaluate the resulting integer expression and produce a hexadecimal literal
 **
 ** The result is a hexadecimal integer literal with a `U` suffix such that it alwas is interpreted as unsigned number.
 **
 ** @predefined
 **/

/**
 ** @def __EXPAND_EMPTY__(…)
 ** @brief expand all the arguments and see if the result is empty
 **
 **
 ** @predefined
 **/

/**
 ** @def __ZERO__
 ** @brief A predefined and pre-evaluated `0`
 **
 ** @predefined
 **/

/**
 ** @def __ONE__
 ** @brief A predefined and pre-evaluated `1`
 **
 **
 ** @predefined
 **/

/**
 ** @def __STDC_EMBED_NOT_FOUND__
 ** @brief A predefined and pre-evaluated value as required by the C standard
 **
 **
 ** @predefined
 **/

/**
 ** @def __STDC_EMBED_FOUND__
 ** @brief A predefined and pre-evaluated value as required by the C standard
 **
 ** @predefined
 **/

/**
 ** @def __STDC_EMBED_EMPTY__
 ** @brief A predefined and pre-evaluated value as required by the C standard
 **
 ** @predefined
 **/

/**
 ** @def __NARGS__(…)
 ** @brief expand all the arguments and count the number of items in the resulting list
 **
 ** If there are no arguments or their expansion leads to nothing, the result is `0`.
 ** Otherwise the result is the number of toplevel commas in the expansion plus one.
 **
 ** @predefined
 **/

/**
 ** @def __EXPAND_COMMAS__(…)
 ** @brief expand all the arguments and count the number of toplevel commas in the resulting list
 **
 ** If there are no arguments, their expansion leads to nothing,
 ** or there are any but no toplevel comma, the result is `0`.
 **
 **
 ** @predefined
 **/

/**
 ** @def __UGLIFY__(X)
 ** @brief add two leading and trailing underscores to the identifier `X`
 **
 ** @predefined
 **/

/**
 ** @def __VA_OPT__
 ** @brief A pseudo macro that cannot be redefined
 **
 ** The `__VA_OPT__` construct is only active in the replacement list
 ** of a variadic macro and must be present at the point of
 ** definition. Any other occurence triggers this macro here and passes
 ** the token through.
 **
 ** @predefined
 **/

/**
 ** @def __VA_TAIL__
 ** @brief A pseudo macro that cannot be redefined
 **
 ** The `__VA_TAIL__` construct is only active in the replacement list
 ** of a variadic macro at the very end of expansion. Any other
 ** occurence triggers this macro here and passes the token
 ** through. @see @ref tail
 **
 ** @predefined
 **/

/**
 ** @def __EXPAND_ERROR__(...)
 ** @brief Evaluate the `__VA_ARGS__`, use them as an error message and abord compilation.
 **
 ** @predefined
 **/

/**
 ** @def ELLIPSIS_LOOP_WIDTH
 ** @brief The log₂ of the number of the per-level includes for the loop feature.
 **
 ** @see ellipsis-loop.dirs
 **
 ** @predefined
 **/

/**
 ** @def ELLIPSIS_LOOP_MAX
 ** @brief The maximum number of iterations of the loop feature.
 **
 ** @see ellipsis-loop.dirs
 ** @predefined
 **/

/**
 ** @def UNIQ(POS1, POS2)
 **
 ** @brief Refer to the `POS1`-last unique identifier recently created.
 **
 ** The second argument may chose identifiers from different levels,
 ** `0` is the current level, `1` is one level down and so on.
 **
 ** If omitted both parameters default to `0`.
 **
 ** @see @ref ellipsis-unique.dirs
 **/

/**
 ** @def UNIQ_NEW
 ** @brief Create and return a new unique identifier
 **
 ** @see @ref ellipsis-unique.dirs
 **/

/**
 ** @def UNIQ_UP
 **
 ** @brief Move up one level in the hierarchy of unique identifiers
 **
 ** Otherwise this macro has no visible effect.
 **
 ** @see @ref ellipsis-unique.dirs
 **/

/**
 ** @def UNIQ_DOWN
 **
 ** @brief Move down one level in the hierarchy of unique identifiers
 **
 ** Otherwise this macro has no visible effect.
 **
 ** @see @ref ellipsis-unique.dirs
 **/


/**
 ** @file ellipsis-defer.h
 **
 ** @brief Implement a @ref defer feature to postpone execution of specific blocks.
 **
 ** Using this header imposes that all loop and switch statements have
 ** a compound statement as secondary block.
 **
 ** @see @ref defer_desc
 **/

/**
 ** @def DEFER_TYPE
 **
 ** @brief Provide the return type to a function context that uses the @ref defer feature.
 **
 ** The argument list has to contain a type name that is compatible
 ** with the @ref return type of the function, or be empty if that
 ** @ref return type is `void`.
 **
 ** This must be used just after the opening brace of the function
 ** body. The body shall not contain preliminary entries or exits by
 **
 ** - `longjmp` out of the body.
 ** - `exit` or `quick_exit`
 **
 ** unless they are located before or within the first
 ** lexicographical @ref defer.
 **
 ** If any of these is used in other places, the cleanup code of
 ** pending @ref defer statements is not executed and thus the state is
 ** not clean.
 **
 ** @ref return constitutes a preliminary exit from the body and
 ** guarantees that all active @ref defer statements are executed.
 **
 ** @see @ref defer_desc
 ** @hideinitializer
 **/

/**
 ** @def defer_skip
 **
 ** @brief terminate the closest surrounding compound statement with
 ** @ref defer statements
 **
 ** If there is no such compound statement a compile time error
 ** occurs.
 **
 ** @see @ref defer_desc
 ** @hideinitializer
 **/

/**
 ** @def defer
 **
 ** @brief Mark the depending compound statement as being deferred
 ** until the current compound statement (*the anchor*) terminates.
 **
 ** You may use this construct only in a constrained setting:
 **
 ** - The depending statement must be a compound statement, then
 **   followed by a `;`, thus of the form
 **   ```
 **   defer { ... something ... };
 **   ```
 **
 ** - It must be placed into a compound statement, called its anchor
 **   statement. That anchor limits the validity of the @ref defer
 **   construct.
 **
 ** - It must not be hidden inside a `if`, `switch` or loop statement
 **   without using a compound statement. So in particular the @ref
 **   defer itself can not be conditioned. If you are tempted to do
 **   this, usually you should put the condition (`if` statement)
 **   inside the @ref defer, not in front. This implementation
 **   complains about all loop or switch bodies that are not compound
 **   statements.
 **
 ** - There must be no jumps into, out of or across a @ref defer
 **   statement. Jumps here are the most general, no `goto`,
 **   `longjmp`, `case` or `default` or other labels when accessed
 **   from outside the @ref defer block.
 **
 ** - The secondary block must be valid in place, only that it is
 **   guaranteed to be executed at the very end of the guarded function
 **   body or block, respectively.
 **
 ** - If a compound statement contains several @ref defer statements,
 **   these are then executed in the inverse lexicographical order.
 **
 ** - If a compound statement terminates before a specific @ref defer has
 **   been met, the depending secondary block is not executed at all.
 **
 ** The anchor should not contain preliminary entries or exits by
 **
 ** - `switch` cases, including `default`.
 ** - `goto` into or out of the compound statement.
 ** - `longjmp` out of the compound statement.
 ** - `exit` or `quick_exit`.
 **
 ** If any of these is used, the cleanup code of pending @ref defer
 ** statements is not executed and thus the state is not clean.
 **
 ** Several preliminary exits from the compound statement are possible,
 ** though:
 **
 ** - @ref return from the surrounding function context
 ** - @ref break for surrounding `switches` or loops
 ** - @ref continue for surrounding loops
 ** - If the top-most compound statement of a non-`void` function,
 **   @ref defer_skip can be used to just terminate this compound
 **   statement
 **
 ** @see @ref defer_desc
 ** @see DEFER_TYPE
 ** @hideinitializer
 **/

/**
 ** @def defer_return_value
 **
 ** @brief access the return value of a function from within a @ref defer
 ** statement
 **
 ** This can be used in particular in the very first @ref defer of a
 ** non-`void` function to control a non-regular jump out of the
 ** function.
 **
 ** @warning This can obviously only be used after the return value
 ** has been set, that is after a @ref return statement has been
 ** executed. The only points where this is accessible are during
 ** deferred statements that are executed after such a @ref return. A
 ** good static analyzer should always warn you of possible code paths
 ** that try to use this value before it exists.
 **
 ** @see @ref defer_desc
 ** @see DEFER_TYPE
 ** @hideinitializer
 **/

/**
 ** @def break
 **
 ** @brief `break` to the next loop or `switch` statement which is
 ** compatible with @ref defer
 **
 ** @see @ref defer_desc
 ** @see break
 ** @see defer
 ** @hideinitializer
 **/

/**
 ** @def continue
 **
 ** @brief `continue` the next loop statement which is compatible with
 ** @ref defer
 **
 ** @see @ref defer_desc
 ** @see continue
 ** @see defer
 ** @hideinitializer
 **/

/**
 ** @def DEFER_MODE
 **
 ** @brief The mode of the @ref defer feature for the current function.
 **
 ** If 0, we are in a function without @ref defer, if 1 in a void function
 ** and if 2 in a function with value return. Otherwise the
 ** compilation is erroneous.
 **
 ** @predefined
 ** @hideinitializer
 **/

/**
 ** @def return
 **
 ** @brief Return from a function. If it has active @ref defer clauses,
 ** unwind these.
 **
 ** If there is no visible @ref defer statement this macro just
 ** resolves to the normal keyword __return__.
 **
 ** If there is a visible @ref defer, the current list of deferred
 ** statements is executed in reverse order. If also a return type had
 ** been specified by means of @ref DEFER_TYPE, before unwinding the
 ** return expression is executed and its value is stored in a
 ** temporary variable. That value can be read by using
 ** @ref defer_return_value.
 **
 ** @warning If @ref defer is included in the unit, using `return` with a
 ** return expression must first use @ref DEFER_TYPE to instruct eĿlipsis
 ** about the return type of the function. Otherwise, the compilation
 ** will be on error.
 **
 ** @remark If the function has type `void`, no special precautions
 ** are necessary and both, @ref return without a return expression and
 ** falling off at the end of the function body, should work as usual.
 **
 ** @see @ref defer_desc
 ** @see DEFER_TYPE
 ** @see defer_return_value
 ** @hideinitializer
 **/

/**
 ** @file ellipsis-unique.dirs
 **
 ** @brief Provide a hierarchically organized set of identifers
 **
 ** This provides a set of macros prefixed with `UNIQ` that expand and
 ** manage unique identifiers. (For using other another name than
 ** `UNIQ`, see below.)  Central are macros `UNIQ_NEW` and `UNIQ` that
 ** create respectively refer to an identifier.
 **
 ** ```
 ** double UNIQ_NEW = 5;
 ** double UNIQ_NEW = 6;
 ** printf("%g %g\n", UNIQ(), UNIQ(1));
 ** ```
 **
 ** Note that here `UNIQ` needs parenthesis for the invokation and
 ** receives 0, 1, or 2 arguments. The above expands to something
 ** similar as the following
 **
 ** ```
 **  double UNIQ_ID_1_9	= 5;
 **  double UNIQ_ID_1_10	= 6;
 **  printf("%g %g\n", UNIQ_ID_1_10, UNIQ_ID_1_9);
 ** ```
 **
 ** Obivously here the concrete names that are chosen depend on the
 ** context where this code would be found. You should not try to
 ** recover such names manually.
 **
 ** The first argument to `UNIQ` indicates to which of the unique
 ** idenfier it refers: 0, the default, refers to the last identifer
 ** created with `UNIQ_NEW` (here the variable initialized with the
 ** value `6`), 1 to the one before (here the variable initialized with
 ** the value `5`).
 **
 ** So the above example should print the values
 ** `6 5` in that order.
 **
 ** Two additional macros, `UNIQ_UP` and `UNIQ_DOWN`, add a
 ** hierarchical aspect, namely nesting levels for identifiers.  To see
 ** that consider a macro `SWAPEM(A, B)` with the following expansion:
 ** ```
 ** do {
 **   UNIQ_UP
 **   register auto const UNIQ_NEW = &(A);
 **   register auto const UNIQ_NEW = &(B);
 **   register auto const UNIQ_NEW = *UNIQ(2);
 **   *UNIQ(2) = *UNIQ(1);
 **   *UNIQ(1) = UNIQ();
 **   UNIQ_DOWN
 **     } while(false)
 ** ```
 **
 ** Here we use `UNIQ_UP` to move one "level" up in the hierarchy if
 ** identifiers. This and the `UNIQ_DOWN` at the end have no visual
 ** effect on the code, only that they steps one level up or down in
 ** the identifier hierarchy. To see that in context let's amend our
 ** example from above by adding an invokation of `SWAPEM`.
 **
 ** ```
 ** double UNIQ_NEW = 5;
 ** double UNIQ_NEW = 6;
 ** SWAPEM(UNIQ(), UNIQ(1));
 ** printf("%g %g\n", UNIQ(), UNIQ(1));
 ** ```
 **
 ** The expansion would be looking similar to the following:
 **
 ** ```
 ** double UNIQ_ID_1_9	= 5;
 ** double UNIQ_ID_1_10	= 6;
 ** do { register auto const UNIQ_ID_2_7	= &(UNIQ_ID_1_10); register auto const UNIQ_ID_2_8	= &(UNIQ_ID_1_9); register auto const UNIQ_ID_2_9	= *UNIQ_ID_2_7; *UNIQ_ID_2_7= *UNIQ_ID_2_8; *UNIQ_ID_2_8	= UNIQ_ID_2_9; } while(false);
 ** printf("%g %g\n", UNIQ_ID_1_10, UNIQ_ID_1_9);
 ** ```
 **
 ** So here the expansion of the three lines that we had before has not
 ** changed at all; in the `printf` call we can easily refer to the
 ** identifiers as we did before without needing to worry about what
 ** effects the invokation of `SWAPEM` might have caused.
 **
 ** In contrast to that the call to `SWAPEM` itself sees a new set of
 ** names for its variables, here identifiers starting with
 ** `UNIQ_ID_2_`, and so the variables defined there are in no conflict
 ** with the identifiers in the surrounding scope.
 **
 ** As a result of this invokation, the contents of the two variables is
 ** swapped and the two values would be `5 6`, inverted compared to what
 ** we had seen above.
 **
 ** `UNIQ` has an optional second parameter that refers to the level in
 ** which the identifier is sought. As for the first paremeter, `0`,
 ** the default, refers to the current level, `1` refers to one level
 ** down and so on.
 **
 ** Typically you would include this feature with a directive such as
 **
 ** ```{.C}
 ** #include_directives <ellipsis-unique.dirs>
 ** ```
 **
 ** This provides you with a set of macros as described above that have
 ** names starting with `UNIQ`. If you wand to change that, you may
 ** bind the macro `UNIQUE_NAME` to a prefix of your liking. For
 ** example
 **
 ** ```{.C}
 ** #include_directives "ellipsis-unique.dirs" __prefix__(bind UNIQUE_NAME LABEL)
 ** ```
 **
 ** creates an alternative set of macros `LABEL`, `LABEL_NEW`,
 ** `LABEL_UP` and `LABEL_DOWN` that can be used in a similar way, but
 ** which result in identifiers that use "LABEL" instead of "UNIQ".
 **
 ** @see ellipsis-loc.h for a simpler feature based on `{}` scope (C only)
 **/

/**
 ** @file ellipsis-loc.h
 **
 ** @brief Provide locally scoped identifers
 **
 ** This provides two macros @ref __LOC and @ref __LOC_NEW that expand and
 ** manage identifiers that are unique within the current set of curly
 ** braces `{}`. (For using another prefix than @ref __LOC, see below.)
 **
 ** ```
 ** double __LOC_NEW = 5;
 ** double __LOC_NEW = 6;
 ** printf("%g %g\n", __LOC(), __LOC(1));
 ** ```
 **
 ** Note that here @ref __LOC needs parenthesis for the invokation and
 ** receives 0, 1, or 2 arguments. The above expands to something
 ** similar as the following
 **
 ** ```
 **  double __LOC_ID_1_9	= 5;
 **  double __LOC_ID_1_10	= 6;
 **  printf("%g %g\n", __LOC_ID_1_10, __LOC_ID_1_9);
 ** ```
 **
 ** Obivously here the concrete names that are chosen depend on the
 ** context where this code would be found. You should not try to
 ** recover such names manually.
 **
 ** The first argument to @ref __LOC indicates to which of the unique
 ** idenfier it refers: 0, the default, refers to the last identifer
 ** created with `__LOC_NEW` (here the variable initialized with the
 ** value `6`), 1 to the one before (here the variable initialized with
 ** the value `5`).
 **
 ** So the above example should print the values
 ** `6 5` in that order.
 **
 ** Curly braces add a hierarchical aspect, namely nesting levels for
 ** identifiers.  To see that consider a macro `SWAPEM(A, B)` with the
 ** following expansion:

 ** ```
 ** do {
 **   register auto const __LOC_NEW = &(A);
 **   register auto const __LOC_NEW = &(B);
 **   register auto const __LOC_NEW = *__LOC(2);
 **   *__LOC(2) = *__LOC(1);
 **   *__LOC(1) = __LOC();
 **     } while(false)
 ** ```
 **
 ** Here `{` moves one "level" up in the hierarchy of identifiers. To
 ** see that in context let's amend our example from above by adding
 ** an invokation of `SWAPEM`.
 **
 ** ```
 ** double __LOC_NEW = 5;
 ** double __LOC_NEW = 6;
 ** SWAPEM(__LOC(), __LOC(1));
 ** printf("%g %g\n", __LOC(), __LOC(1));
 ** ```
 **
 ** The expansion would be looking similar to the following:
 **
 ** ```
 ** double __LOC_ID_1_9	= 5;
 ** double __LOC_ID_1_10	= 6;
 ** do { register auto const __LOC_ID_2_7	= &(__LOC_ID_1_10); register auto const __LOC_ID_2_8	= &(__LOC_ID_1_9); register auto const __LOC_ID_2_9	= *__LOC_ID_2_7; *__LOC_ID_2_7= *__LOC_ID_2_8; *__LOC_ID_2_8	= __LOC_ID_2_9; } while(false);
 ** printf("%g %g\n", __LOC_ID_1_10, __LOC_ID_1_9);
 ** ```
 **
 ** So here the expansion of the three lines that we had before has not
 ** changed at all; in the `printf` call we can easily refer to the
 ** identifiers as we did before without needing to worry about what
 ** effects the invokation of `SWAPEM` might have caused.
 **
 ** In contrast to that the call to `SWAPEM` itself sees a new set of
 ** names for its variables, here identifiers starting with
 ** `__LOC_ID_2_`, and so the variables defined there are in no conflict
 ** with the identifiers in the surrounding scope.
 **
 ** As a result of this invokation, the contents of the two variables is
 ** swapped and the two values would be `5 6`, inverted compared to what
 ** we had seen above.
 **
 ** @ref __LOC has an optional second parameter that refers to the level in
 ** which the identifier is sought. As for the first paremeter, `0`,
 ** the default, refers to the current level, `1` refers to one level
 ** down and so on.
 **
 ** Typically you would include this feature with a directive such as
 **
 ** ```{.C}
 ** #include_directives <ellipsis-loc.h>
 ** ```
 **
 ** This provides you with a set of macros as described above that have
 ** names starting with @ref __LOC.
 **
 ** @see LOC_NAME
 **/

/**
 ** @def __LOC(POS0, POS1)
 **
 ** @brief refer to an identifier created with @ref __LOC_NEW
 **
 ** @param POS0 is the "distance" to the invocation of @ref __LOC_NEW:
 ** `0` refers to the latest, `1` to the one before and so on.
 **
 ** @param POS1 refers to the level of curly braces of the referred
 ** identifier, `0` is the current level, `1` is one level up and so
 ** on.
 **
 ** Let's see this with an example:
 **
 ** ```
 ** inline void swapem(double* __LOC_NEW, double* __LOC_NEW) {
 **     double const __LOC_NEW = __LOC(1, 1);
 **     *__LOC(1, 1) = *__LOC(0, 1);
 **     *__LOC(0, 1) = *__LOC(0, 0);
 ** }
 ** ```
 **
 ** Here, we create three variables, but in different scopes. Inside
 ** the function body, the two parameters are referred by
 ** `__LOC(1, 1)` and `__LOC(0, 1)` because they are the before last and last
 ** defined names in the scope outside the `{}` of the function body
 ** (therefore the second argument `1` for both). The variable
 ** declared inside the body is referred by `__LOC(0, 0)` (or
 ** `__LOC(0)` or `__LOC()`), with a `0` as second parameter referring
 ** to the current level of `{}`.
 **
 ** The above example will be replaced by something like the following.
 **
 ** ```
 ** inline void swapem(double* __LOC_ID_0_3, double* __LOC_ID_0_4) {
 **   double const __LOC_ID_1_5	= *__LOC_ID_0_3;
 **   *__LOC_ID_0_3	= *__LOC_ID_0_4;
 **   *__LOC_ID_0_4	= __LOC_ID_1_5;
 ** }
 ** ```
 **
 ** All three identifiers will be unique in the whole translation
 ** unit, and thus will not conflict with anything else.
 **
 ** Another use case for `__LOC_NEW` would be parameters in function
 ** prototypes, for which you want to avoid that they ever conflict
 ** with user defined identifiers.
 **
 ** ```
 ** void copyem(size_t __LOC_NEW, double [static restrict __LOC()], double const [static restrict __LOC()]);
 ** ```
 **
 ** Not that here the second an third parameters have no names; we
 ** don't need them so we omit them. In contrast to that the first
 ** parameter conveys information about the type of the two others and
 ** is used for their definition. After expansion this looks similar
 ** to the following
 **
 ** ```
 ** void copyem(size_t __LOC_ID_0_5, double [static restrict __LOC_ID_0_5], double const [static restrict __LOC_ID_0_5]);
 ** ```
 **
 ** using an identifier that is guaranteed not to be in conflict with
 ** any other identifier of the translation unit and that we just
 ** don't want to have to invent ourselves.
 **/

/**
 ** @def __LOC_NEW
 **
 ** @brief create a new identifier in the current scope of `{}`
 **
 ** @see __LOC for how to refer to such an identifier
 **
 ** @warning Don't use that feature to create names with external linkage.
 ** Otherwise, this could create name conflicts during linking with other units.
 **
 ** @warning Don't use that feature to create names for elements of
 ** `struct` or `union` types that you provide in headers, eve of
 ** those that you want to hide from the user. These would get
 ** instantiated differently in different translation units, and thus
 ** types would not be compatible across translation units.
 **/

/**
 ** @def LOC_NAME
 **
 ** If you want to use a different prefix for this feature, you may
 ** bind the macro `LOC_NAME` to a prefix of your liking. For example
 **
 ** ```{.C}
 ** #include_directives "ellipsis-loc.h" __prefix__(bind LOC_NAME LABEL)
 ** ```
 **
 ** creates an alternative set of macros `LABEL`, `LABEL_NEW` that can
 ** be used in a similar way, but which result in identifiers that use
 ** "LABEL" instead of "__LOC".
 **
 ** Each such new prefix is added to the instrumentation of the curly
 ** braces `{}`, so please don't exaggerate using too many
 ** prefixes. For normal use as local identifiers in `inline`
 ** functions and macros, the default should be largely sufficient.
 **/

/**
 ** @def __UNIT__
 **
 ** @brief A pseudo-macro that resolves to the name associated with
 ** the current translation unit, generally abbreviated with `¤`.
 **
 ** This is **not** set automatically, so if you want to use this
 ** feature you'd have to set this manually. Usually this would be
 ** done at the start of the translation unit. For a `.c` file this
 ** would typically look like
 **
 ** ```
 ** #unit hurli∷purtz
 ** ```
 **
 ** for an absolute (unrelated) name or
 **
 ** ```
 ** #unit ¤¤∷macros
 ** ```
 **
 ** for a name that is prefixed with either the name of the unit that
 ** included this source file or with the project name provided
 ** through @ref __PROJECT__.
 **
 ** In all cases the translation unit may then be abbreviated with
 ** `¤∷` or `__UNIT__∷` as in
 **
 ** ```
 ** void ¤∷hello(void);
 ** void __UNIT__∷hallo(void);
 ** ```
 **
 ** Note that the definition of the unit name is restricted to the
 ** current source file; when returning from an @ref include
 ** directive, the name is reset to the name there.
 **
 ** In a header file, that is generally included into completely
 ** unrelated code, you should prefer the first form, such that the
 ** contents of the header expands to the same, regardless of the
 ** context.
 **
 ** When ellipsis is used as a first phase preprocessor such names are
 ** all expanded into long forms that are usually separated by a `‿`
 ** character (**UNDERTIE**, U203F). For example if the project is
 ** `magn∷ificient` with the above subunit `__UNIT__` would expand to
 ** `magn‿ificient‿macros` and the function name would be
 ** `magn‿ificient‿macros‿hello`.
 **
 ** @see @ref composition
 ** @see `__PARENT_UNIT__`
 ** @see `__PROJECT__`
 ** @hideinitializer
 **
 ** @predefined
 **/

/**
 ** @def __PARENT_UNIT__
 **
 ** @brief A pseudo-macro that resolves to the name of the current
 ** parent unit, generally abbreviated as `¤¤`.
 **
 ** For a top-level C file the parent would be the project name, if
 ** that is set from `__PROJECT__`, for files that are deeper in the
 ** include hierarchy, other subunit names may already have been added
 ** to that.
 **
 ** @see @ref composition
 ** @see `__UNIT__`
 ** @see `__PROJECT__`
 ** @hideinitializer
 **
 ** @predefined
 **/

/**
 ** @def ¤
 ** @brief shortcut for @ref __UNIT__
 ** @see @ref composition
 ** @see @ref ellipsis
 ** @hideinitializer
 **
 ** @predefined
 **/

/**
 ** @def ¤¤
 ** @brief shortcut for @ref __PARENT_UNIT__
 ** @see @ref composition
 ** @see @ref ellipsis
 ** @hideinitializer
 **
 ** @predefined
 **/

/**
 ** @def ¤¤¤
 ** @see @ref composition
 ** @see @ref ellipsis
 ** @hideinitializer
 **
 ** @predefined
 **/

/**
 ** @def ¤¤¤¤
 ** @see @ref composition
 ** @see @ref ellipsis
 ** @hideinitializer
 **
 ** @predefined
 **/

/**
 ** @def ¤¤¤¤¤
 ** @see @ref composition
 ** @see @ref ellipsis
 ** @hideinitializer
 **
 ** @predefined
 **/

/**
 ** @def __INCLUDE_DEPTH__
 **
 ** @brief The current depth of nested includes.
 **
 ** @predefined
 **/

/**
 ** @def __PARENTHESIS_DEPTH__
 **
 ** @brief The current depth of nested parenthesis.
 **
 ** Note that counting parenthesis in the preprocessor is a delicate
 ** thing, because parenthesis also serve in the grammar of the
 ** preprocessor itself.
 **
 **
 ** ```
 ** #define hello(...) __VA_ARGS__
 ** void hollo(int);
 ** hello( __PARENTHESIS_DEPTH__ ( + __PARENTHESIS_DEPTH__ ) + __PARENTHESIS_DEPTH__ );
 ** hollo( __PARENTHESIS_DEPTH__ ( + __PARENTHESIS_DEPTH__ ) + __PARENTHESIS_DEPTH__ );
 ** ```
 **
 ** Here the parenthesis of the macro invocation are not counted,
 ** since they never are at the head of parsing; they are only
 ** encountered to complete the macro invocation. Therefore the resulting
 ** token sequence is
 **
 ** ```
 ** void hollo(int);
 ** 0( + 1) + 0;
 ** hollo( 1( + 2) + 1);
 ** ```
 **
 ** @warning A warning is issued if this value exceeds `LONG_MAX`.
 **
 ** @warning A warning is issued if this value falls below `0`.
 **
 ** @warning A warning is issued if an include file has different
 ** numbers of opening and closing parenthesis. If so, the depth is
 ** reset to the value from before the inclusion.
 **
 ** @predefined
 **/

/**
 ** @def __BRACE_LEVEL__
 **
 ** @brief The current depth of nested braces `{}`.
 **
 ** @predefined
 **/

/**
 ** @def __BRACKET_LEVEL__
 **
 ** @brief The current depth of nested brackets `[]`.
 **
 ** @predefined
 **/

/**
 ** @def __ATTR_LEVEL__
 **
 ** @brief `1` when inside an attribute `[[]]`, `0` otherwise.
 **
 ** @predefined
 **/

/**
 ** @def __IMPLEMENT__
 **
 ** @brief Implement the listed names as functions for the current
 ** translation unit.
 **
 ** This receives a list of identifiers and emits code that ensures
 ** that they are implemented in the current translation unit. For
 ** example
 **
 ** ```
 ** __IMPLEMENT__(cpy, destroy);
 ** ```
 **
 ** ensures that the symbols `¤∷cpy` and `¤∷destroy` are implemented.
 **
 ** This can be used for functions, in particular `inline` functions,
 ** and variables with external linkage that do not need an
 ** initializer. For non-`inline` functions this serves just as a
 ** consistency check; for `inline` functions this ensures that the
 ** function is always emitted, regardless of the optimization options
 ** for the compilation.
 **
 ** @remark The corresponding symbols must have previously been
 ** declared, usually in the corresponding header file.
 **
 ** @see @ref unit
 ** @see @ref __UNIT__
 **
 ** @hideinitializer
 **/



/**
 ** @file ellipsis-trigger.h
 **
 ** @brief Trigger a matching closing braces that ends on the same
 ** level of nestedness.
 **
 ** As a simple example suppose that we want to mark `{}` blocks that should always
 ** be surrounded by a `do` … `while(false)` construct. This can be achieved
 **
 ** ```
 ** #include_directives<ellipsis-trigger.h> __prefix__(bind NAME STATEMENT)
 ** #define STATEMENT_START do
 ** #define STATEMENT_CLOSE() while (false)
 ** ```
 **
 ** To write a macro `SWAP` that can be used anywhere a normal
 ** statement could be used such as in
 **
 ** ```
 ** if (something) SWAP(a, b);
 ** else SWAP(a, c);
 ** ```
 **
 ** you could use `STATEMENT` to mark the opening brace:
 **
 ** ```
 ** #define SWAP(X, Y)                              \
 **   STATEMENT {                                   \
 **     auto tmpY = (Y);                            \
 **     (Y) = (X);                                  \
 **     (X) = tmpY;                                 \
 **   }
 ** ```
 **
 ** The example from above would then be replaced by something like:
 **
 ** ```
 ** if (something) do { auto tmpY = (b); (b) = (a); (a) = tmpY; } while (false);
 ** else do { auto tmpY = (c); (c) = (a); (a) = tmpY; } while (false);
 ** ```
 **
 ** Note that if hadn't the `STATEMENT` macro in `SWAP` (only `{ … }`)
 ** the replacement would have been
 **
 ** ```
 ** if (something) { auto tmpY = (b); (b) = (a); (a) = tmpY; };
 ** else { auto tmpY = (c); (c) = (a); (a) = tmpY; };
 ** ```
 **
 ** which is a syntax error because the `else` looses its
 ** corresponding `if`.
 **
 ** This replacement work even if the inner part of the macro would
 ** contain other nested `{}`; the `do` is always put in place of the
 ** `STATEMENT` macro, the `while (false)` after the closing brace
 ** that corresponds to the first opening brace after the `STATEMENT`
 ** macro.
 **
 ** Evidently, this is only a not-so-interesting example to show how
 ** the feature works. If you want to see a more sophisticated use
 ** please refer to the `LAMBDA` macro or the ellipsis-lambda.h header
 ** file.
 **/

/**
 ** @file ellipsis-lambda.h
 **
 ** @brief Add a rudimentary lambda feature to gnu C23.
 **
 ** @warning This feature needs nested functions under the hood, so it
 ** may provoke allergic reactions for some and it doesn't work with
 ** clang.
 **/

/**
 ** @def LAMBDA(RET, ...)
 **
 ** @brief Define a lambda with return type `RET` and argument list `__VA_ARGS__`
 **
 ** Other than a function definition, a lambda is an expression that
 ** can be used everywhere where function pointer could be used.
 **
 ** The following lambda
 **
 ** ```
 ** LAMBDA(double, double x){
 **    return 2.0*x + 1;
 ** }
 ** ```
 **
 ** as if an anonymous function were defined somewhere and then the
 ** expression would have been replace with a pointer to that
 ** function. So a use of that expression in an assignment
 **
 ** ```
 ** y = LAMBDA(double, double x){
 **    return 2.0*x + 1;
 ** }(35);
 ** ```
 **
 ** has some function
 **
 ** ```
 ** double SOME_WEIRD_NAME(double x) {
 **    return 2.0*x + 1;
 ** }
 ** ```
 **
 ** defined in some place, and then replaces the assignment above as
 ** if it were written as
 **
 ** ```
 ** y = &SOME_WEIRD_NAME(35);
 ** ```
 **
 ** This is by itself as presented here is not too interesting.
 **
 ** One point where it becomes interesting is when encapsulated inside
 ** a macro that provides a type-generic feature:
 **
 ** ```
 ** #define MIN(X, Y)                                    \
 ** LAMBDA(typeof((X)+(Y)), typeof(X) _X, typeof(Y) _Y){ \
 **   return (_X < _Y) ? _X : _Y;                        \
 ** }
 ** ```
 **
 ** The other point that makes this form of lambda interesting is that
 ** the lambda has access to local variables of the enclosing function.
 **
 ** ```
 ** double sumup(size_t n, double A[n], double fact) {
 **    auto λ = LAMBDA(double, double x) {
 **        return fact*x;
 **    };
 **    double accu = 0.0;
 **    for (size_t i = 0; i < n; ++i) {
 **        accu += λ(A[i]);
 **    }
 **    return accu;
 ** }
 ** ```
 **
 ** Here `λ` has access to `fact` at each call. Such a feature is
 ** difficult to reproduce with a "normal" function, somehow we'd have
 ** to know how to provide the information to `λ`.
 **
 ** The model of access that is used by this implementation of
 ** ::LAMBDA is that an access to the original variable is made at
 ** each call. If for example we'd change `fact` between two calls,
 ** the second call would see the updated value. We even could modify
 ** `fact` from within `λ`, but that is generally considered to be bad
 ** style.
 **
 ** If we want to be sure that the lambda always uses the value of the
 ** variable as it is at the point where the lambda is defined, we can
 ** use a combination of the ::CAPTURE and ::RECAP macros to freeze
 ** the values of the used outer variables in place.
 **
 ** ```
 ** double sumup2(size_t n, double A[n], double fact) {
 **    CAPTURE(fact);
 **    auto κ = LAMBDA(double, double x) {
 **        RECAP(fact);
 **        return fact*x;
 **    };
 **    double accu = 0.0;
 **    for (size_t i = 0; i < n; ++i) {
 **        accu += κ(A[i]);
 **    }
 **    return accu;
 ** }
 ** ```
 **
 ** Here the invocation of ::CAPTURE stores the current value of
 ** `fact` in a safe and immutable place, and then ::RECAP accesses
 ** that stored value and makes it available under the name `fact`
 ** within `κ`, again. Note that the variable `fact` in `κ` has the
 ** same type as the function argument, but that it is in addition
 ** `const`-qualified and thus cannot be changed.
 **
 ** @see CAPTURE
 **/

/**
 ** @def CAPTURE(...)
 **
 ** @brief Freeze the values of a set of variables in some secret
 ** place.
 **
 ** The use of ::CAPTURE and ::RECAP usually comes in pairs
 **
 ** ```
 ** CAPTURE(argc, argv);
 ** auto λ = LAMBDA(double, double x) {
 **     RECAP(argc, argv);
 **     ... do something with argc or argv ...
 **     return argc*x;
 ** };
 ** ```
 **
 ** Here `λ` will use the frozen value of `argc` whenever it is called
 ** and possible modifications that have been inflicted to `argc` are
 ** ignored by these calls.
 **
 ** @warning Be careful when capturing array values. This mechanism
 ** simply generates a *pointer* to the original array and does *not*
 ** freeze the values of the array elements. Also the length of the
 ** array would not be accessible in the lambda.
 **
 ** @remark One capture can serve several ::RECAP invocations; all
 ** ::RECAP refer to the latest ::CAPTURE that is in their scope.
 **
 ** @remark The type of the variables after ::RECAP is
 ** `const`-qualified.
 **
 ** @remark ::RECAP may be used at any inner scope that needs to
 ** restore the frozen values, it is not limited to lambdas.
 **
 ** @remark A ::RECAP invocation always has to be in another, inner,
 ** scope, otherwise we would have a redeclaration of the variables in
 ** the list.
 **
 ** @hideinitializer
 **/

/**
 ** @def RECAP(...)
 **
 ** @brief Restore a set values frozen by ::CAPTURE for the current scope.
 **
 ** @see CAPTURE
 **/

/**
 ** @file ellipsis-blockstate.h
 **
 ** @brief Maintain a set variables indexed by a level.
 **
 ** Using the macros with only one argument results in creating a
 ** block-local variable with the name that is passed as argument. For
 ** example
 **
 ** ```
 ** __BLOCKSTATE_INC(MINE)__BLOCKSTATE_TST(MINE) {
 **       __BLOCKSTATE_TST(MINE) {
 **                 __BLOCKSTATE_DEC(MINE)
 **       }
 **       __BLOCKSTATE_TST(MINE) {
 **                 __BLOCKSTATE_DEC(MINE)
 **       }
 ** }
 ** __BLOCKSTATE_TST(MINE) __BLOCKSTATE_CLR(MINE)__BLOCKSTATE_TST(MINE)
 ** ```
 **
 ** has three variables in the `MINE` series, one that is active outside
 ** the braces and another at each brace level. So the expansion of the above would be
 **
 ** ```
 ** 1 {
 **   0 {
 **      -1
 **   }
 **   0 {
 **      -2
 **   }
 ** }
 ** 1 0
 ** ```
 **
 ** @remark There is one variable for each level.
 **
 ** @remark The macros all guarantee that the necessary support
 ** macros are always defined.
 **
 ** With a second argument, that argument is expected to represent
 ** another concept of "level" than nestedness of braces. In the
 ** example above if we'd use `()` parenthesis instead of `{}` braces
 ** by adding [__PARENTHESIS_DEPTH__](@ref __PARENTHESIS_DEPTH__) as
 ** second argument to the macros, the result would be similar.
 **/

/**
 ** @def __BLOCKSTATE_TST
 **
 ** @brief Test the brace-level-specific variable with name `NAME`.
 **
 ** If the variable is not yet defined, define it first and set it to zero.
 **
 ** @remark The default for the second argument is [__BRACE_LEVEL__](@ref __BRACE_LEVEL__).
 ** @return Expands to the current integer value of the variable.
 **
 ** @hideinitializer
 **/

/**
 ** @def __BLOCKSTATE_INC
 **
 ** @brief Increment brace-level-specific variable with name `NAME`.
 **
 ** If the variable is not yet defined, sets its value to `1`.
 **
 ** @remark The default for the second argument is [__BRACE_LEVEL__](@ref __BRACE_LEVEL__).
 ** @return Expands to the empty token.
 **
 ** @hideinitializer
 **/

/**
 ** @def __BLOCKSTATE_DEC
 **
 ** @brief Decrement brace-level-specific variable with name `NAME`.
 **
 ** If the variable is not yet defined, sets its value to `-1`.
 **
 ** @remark The default for the second argument is [__BRACE_LEVEL__](@ref __BRACE_LEVEL__).
 ** @return Expands to the empty token.
 **
 ** @hideinitializer
 **/

/**
 ** @def __BLOCKSTATE_CLR
 **
 ** @brief Set the brace-level-specific variable with name `NAME` to zero.
 **
 ** If the variable is not yet defined, define it first.
 **
 ** @remark The default for the second argument is [__BRACE_LEVEL__](@ref __BRACE_LEVEL__).
 ** @return Expands to the empty token.
 **
 ** @hideinitializer
 **/

/**
 ** @def __BLOCKSTATE_SET0
 **
 ** @brief Set the brace-level-specific variable with name `NAME` to `0`.
 **
 ** If the variable is not yet defined, define it first.
 **
 ** @remark The default for the second argument is [__BRACE_LEVEL__](@ref __BRACE_LEVEL__).
 ** @return Expands to the empty token.
 **
 ** @hideinitializer
 **/

/**
 ** @def __BLOCKSTATE_SET1
 **
 ** @brief Set the brace-level-specific variable with name `NAME` to `1`.
 **
 ** @see __BLOCKSTATE_SET0
 ** @hideinitializer
 **/

/**
 ** @def __BLOCKSTATE_SET2
 **
 ** @brief Set the brace-level-specific variable with name `NAME` to `2`.
 **
 ** @see __BLOCKSTATE_SET0
 ** @hideinitializer
 **/

/**
 ** @def __BLOCKSTATE_SET3
 **
 ** @brief Set the brace-level-specific variable with name `NAME` to `3`.
 **
 ** @see __BLOCKSTATE_SET0
 ** @hideinitializer
 **/
