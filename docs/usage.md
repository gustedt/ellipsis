# Usage {#usage}

Currently we have two command line interfaces

- `ellipsis`, the preprocessor binary
- ellipsis-gnuc.sh, a frontend script for `gcc`, `clang` and perhaps
  similar other compilers

The first should be typically used if you process your sources in two
steps, the second (currently C only) if you preprocess and then
compile in one go.

## The ellipsis binary {#binary}

This binary is a standalone preprocessor that can be used to
preprocess a file in one of the supported languages. It is standalone,
in the sense that it does not need any headers or configuration files,
everything it needs is built into the executable.

Usually you will use this to compile your (programming) text in two
phases, the result of the preprocessing stored in an intermediate file
or maybe piped into a subsequent phase. For example to tranform
markdown files that contain preprocess directives into `html` I
usually use a pipe such as the following

```{.sh}
ellipsis intro.md | pandoc - -o intro.html
```

Being standalone has the advantage of being usable without much
prerequesites but also has the disadvantage that there might be
platform specific informations that are missing. For C in particular,
a preprocessor needs a lot of information, for example

- include directories
- width of all basic types
- C23 attributes that are supported by the compiler

and many more. Such information cannot be made completely available by
a generic tool such as `ellipsis`.

@see ellipsis-gnuc.sh below for a script that assembles that
information

For the general case we provide a preprocessing mode that allows to
process ellipsis-specific constructs in a first step and then pass the
result into the usual compiler framework that does the remainder of
the preprocessing.

For C we have the reserved the preprocessor token `%%` reserved for
that purpose, for example

```{.C}
%%ifndef __HEADER_H__
%%define __HEADER_H__ __HEADER_H__

// ellipsis specific include
⎔ include <ellipsis-defer.h>

... use ellipsis features ...

    /*^ comment that is kept ^*/
    char* p = malloc(45);
    defer free(p);

%%endif
```

per default `ellipsis` will translate this to something like

```{.C}
#ifndef __HEADER_H__
#define __HEADER_H__ __HEADER_H__

// replaces the ellipsis specific include
#define DEFER_TO(TARGET, LOCATION) ... something complicated ...


... ellipsis features expanded ...

    /* comment that is kept */
    char* p = malloc(45);
    DEFER_TO(DEFER_END_ID_x_y, DEFER_ID_u_v): free(p);

    ... other code added by ellipsis ...
#endif
```

This mechanism is meant to help you use the specific extensions that
are provided by eĿlipsis, but to produce secondary source files that
can be compile by a normal C compiler. The latter is particularly
important if you want to distribute your sources to others that are
then able to compile your project in their specific setting.

To see an example of a project that is built like that, have a look at
the sources of ellipsis itself.

## The GnuC frontend

The script ellipsis-gnuc.sh provides a direct front end that can be
used as a replacement for your C compiler's preprocessor, as long as
that C compiler adheres to some extensions that are found in Gnu
compilers. It is tested with recent versions of `gcc` and `clang`. The
idea here is that you use this really as if you'd use a normal C
compiler (here as you'd do that typically on a POSIX system)

```{.shell}
ellipsis-gnuc.sh -O3 -march=whatever -Wall -c toto.c
```

which should produce an object file `toto.o` or

```{.shell}
ellipsis-gnuc.sh -O3 -march=whatever -Wall toto.c -o toto
```

(without the `-c`) which should produce the executable `toto`.

This tries to find a C compiler as `cc` somewhere in your executable
path.  If you want to use another compiler underneath you may provide
its name (and path) through the variable @ref ELLIPSIS_CC. For example
to use `clang`

```{.shell}
ELLIPSIS_CC=clang ellipsis-gnuc.sh -O3 -march=whatever -Wall -c toto.c
```

or, if you even want to force as specific version of that compiler:

```{.shell}
ELLIPSIS_CC=clang-19 ellipsis-gnuc.sh -O3 -march=whatever -Wall toto.c -o toto
```
