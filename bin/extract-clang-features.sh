# receives the name of file containing the dump of the
# LanguageExtensions.html file from the most recent clang
# documentation. Usually this should be
# https://clang.llvm.org/docs/LanguageExtensions.html

sed -n '
/__has_feature([a-z][^)]/{
        s!^.*__has_feature(!!
        s!).*!!
        p
}
/__has_extension([a-z][^)]/{
        s!^.*__has_extension(!!
        s!).*!!
        p
}' "$1" | sort -u
