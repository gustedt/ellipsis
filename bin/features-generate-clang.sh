# receives the name of file containing the list of extensions and
# features

for name in `cat $1` ; do
    echo "# if __has_extension(${name})
#  define __has_extension‿${name}	,
# endif"
    echo "# if __has_feature(${name})
#  define __has_feature‿${name}	,
# endif"
done
