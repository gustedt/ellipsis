sed '
/xdefine __HAS_C_ATTRIBUTE/!d
s!.*(\([^,]\{1,\}\), \([^,]\{1,\}\)).*!# if __has_c_attribute(\1::\2)\n#  define __has_c_attribute‿\1‿\2\t1\n#  define __has_c_attribute‿\1‿__\2__\t1\n#  define __has_c_attribute‿__\1__‿\2\t1\n#  define __has_c_attribute‿__\1__‿__\2__\t1\n# endif!
s|.*(\([^,]\{1,\}\),,\([^,]\{1,\}\)).*|# if __has_c_attribute(\1)\n#  define __has_c_attribute‿\1 \2\n#  define __has_c_attribute‿__\1__ \2\n# endif|
' $*
