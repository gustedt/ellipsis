# receives the name of file containing the dump of the
# AttributeReference.html file from the most recent clang
# documentation. Usually this should be
# https://clang.llvm.org/docs/AttributeReference.html

sed -n '
/clang::/{
        s!^.*clang::!clang::!
        s![ <>(),;.].*!!
        s!]!!g
        p
}' "$1" | sort -u
