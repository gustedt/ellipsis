#!/bin/sh

echo "digraph \"$1\" {"
strings -eS $* | sed '
        /ONCE_DEPEND_MARKER:/!d
        /#define/d
        /\*\*/d
        s!ONCE_DEPEND_MARKER:!\t!
        s![.a-zA-Z_][-+./a-zA-Z0-9_]*/!!g
        s![./a-zA-Z_][-+./a-zA-Z0-9_]*!"&"!g
        s!"‿"!∷!g
        s!"∷"!∷!g
'
echo "}"
