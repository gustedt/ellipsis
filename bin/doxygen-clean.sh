#!/bin/sh

sed "
/^[[:blank:]]*#\|\*\*/{
  s|¬|!|g
  s|…|...|g
  s!∨!||!g
  s!∧!\&\&!g
}
/^[[:blank:]]*#\|\*\*/!{
  s!SPECIAL_NAME(\([^() ]*\)[ ]*,[ ]*\([^() ]*\)[ ]*,[ ]*\([^() ]*\)[ ]*)!\1\2\3!g
  /^ONCE_DEFINE/s!ONCE_DEFINE(\([^()]*\))!\n\n/** @see ONCE_DEFINE */ void _Once_\1_init(void)!g
  /^ONCE_DEFINE/s!ONCE_DEFINE!\n\n/** @see ONCE_DEFINE */ void _Once_something_init(void)!g
  s!ONCE_DEFINE_STRONG(\([^()]*\))!/** @see ONCE_DEFINE_STRONG */ void _Once_\1_init(void)!g
  s!ONCE_DEPEND(\([^()]*\))!_Once_\1_init()!g
  s!ONCE_DEPEND_WEAK(\([^()]*\))!_Once_\1_init()!g
  s!ONCE_ATEXIT(\([^()]*\))!/** @see ONCE_ATEXIT */ void _Once_\1_atexit(void)!g
  s!ONCE_AT_QUICK_EXIT(\([^()]*\))!/** @see ONCE_AT_QUICK_EXIT */ void _Once_\1_at_quick_exit(void)!g
  s!\[\[LIP_RETURN_NONNULL!\n/** @warning returns non-null value that should not be discarded */ \[\[LIP_RETURN_NONNULL!g
  /^[[:blank:]]*\#[[:blank:]]*xdefine/s!xdefine!define!
  /^[[:blank:]]*⎔[[:blank:]]*environment/s!environment!define!
  s|^⎔⎔|//!|
  s!^⎔!\#!
  s!%:!\#!g
}
" $*
