echo '
#include <stdio.h>
int main(void) {
'
sed '
/xdefine __HAS_C_ATTRIBUTE/!d
s!.*(\([^,]\{1,\}\), \([^,]\{1,\}\)).*!#ifdef __has_c_attribute\n# if __has_c_attribute(\1::\2)\n\tputs(\"#xdefine __HAS_C_ATTRIBUTE(\1,\2)\");\n# endif\n#endif!
s|.*(\([^,]\{1,\}\),,\([^,]\{1,\}\)).*|#if defined(__has_c_attribute)\n# if __has_c_attribute(\1)\n\tputs(\"#ifndef __has_c_attribute‿\1\\n#xdefine __has_c_attribute‿\1 __EVALUATE__(\2)\\n#endif\\n#ifndef __has_c_attribute‿__\1__\\n#xdefine __has_c_attribute‿__\1__ __EVALUATE__(\2)\\n#endif\");\n# endif\n#endif|
' $*


echo '
}
'
