#!/bin/sh

## @file
## @brief Preprocessing Frontend for C
##
## In addition to the ellipsis preprocessor itself, this is used to
## determine predefined macros of a given compiler tool chain and to
## then preprocess source files with ellipsis using this knowledge.
## Thereby you should be able to use all [extensions that eĿlipsis
## provides](#extensions) out of the box. I'd personally particularly
## recommend
##
## - @ref defer, a new control structure to organize cleanup
## - [identifier composition](#composition), a way to express modularity of translation units
##
## This uses the following features to determine the basic
## preprocessor configuration of a C compiler, provided in the macro
## @ref ELLIPSIS_CC. In general you should be able to use this as a
## first level replacement for your C compiler. For example if you
## have the `make` utility
##
## ```{.sh}
## ELLIPSIS_CC=gcc CC="ellipsis-gnuc.sh" make all
## ```
## or
##
## ```{.sh}
## ELLIPSIS_CC=clang CC="ellipsis-gnuc.sh" make all
## ```
##
## if you want to use `clang` instead of `gcc`. Obviously, for the
## above to work the compiler and ellipsis-gnuc.sh should be in your
## search path for executables. If not, you'd have to adapt that to
## provide the full path.
##
## Properties that your C compiler should fulfill are as described for
## the POSIX `c17` executable, namely arguments
##
## - `-E` for preprocessing only
## - `-D` for defining macros
## - `-U` for undefining macros
## - `-I` for include paths
## - `-o` for a (single) output path
##
## and the suffixes
##
## - `.c` for C source files
## - `.i` for preprocessed C source files
##
## In addition it builds on the commonly supported options for
## compilers that try to be compatible with GNUC,
##
## - `-dM` to dump macro definitions
## - `-v` to dump configuration information.
##
## and also implements GNUC's option
##
## - `-P`  to omit line number information in the output
## - `-MM` to dump `make` dependency information to stdout
##
## Other options such as
##
## - `-c` to compile to one `.o` object file per translation unit
## - `-S` to compile to one `.x` assembly file per translation unit
##
## are just forwarded to the native compiler underneath. In
## particular, if no such option is present this usually should link
## all translation units into one executable at the end. If no `.c`
## files are given this just passes all command line arguments through
## to the "real" compiler. In most case this will then link all object
## files together.
##
## For `-v`, it is assumed that the default include path are listed
## between lines that read
##
## ```{.text}
## #include <...> search starts here:
## ...
## End of search list.
## ```
##
## If no command line option is given, a file with predefined
## information similar to `predefined-clang-14-x86_64-linux-gnu.h` is
## produced. The name of that file is composed from guesses about the
## compiler, its primary version, the processor architecture, the OS
## and the C library.
##
## Otherwise, the mechanism is as follows:
##
## - A header file ellipsis-predefined.h with that information is
##   produced in a temporary directory and the name is exported as
##   `ELLIPSIS_PREDEFINED` to the remaining steps.
## - Arguments that target the preprocessor are are collected to be
##   passed on to ellipsis.
## - Source file names are collected and replaced in the argument list
##   by file names with `.i` that are located in a temporary directory.
## - Ellipsis is called for each of the source files to produce the
##   `.i` file(s).
## - Unless the `-E` option had been given, the compiler is called
##   with the modified argument list.
## - If the `-E` option had been given, the result is either dumped to stdout,
##   or into a file specified with the `-o` option.
##
## Thereby the compiler should not see any arguments they don't know
## about, they operate in the same directory as before and they should
## not try to preprocess the .i files, again.
##
## There are some special rules that apply:
##
## - Predefined macro names that are not standard conforming, such as
##   e.g `linux`, are not transferred.
## - An isolated `-` option reads the source from stdin.
## - If `-` but not `-E` options haven been given, an output has to be
##   specified with `-o`
## - If there are multiple source files with `-E` no output file may
##   be specified.
## - This enables one-phase preprocessing of eĿlipsis, that is the
##   token `%%` at the beginning of a line is the same as a `#`.
##
## Some of preprocessor information was found at
##
## https://sourceforge.net/p/predef/wiki/Home/

## @name Tools
## @brief Configurable command line tools
##
## These can be used to adapt the script to your needs on systems that
## are not directly POSIX.  The only one with importance here should
## be @ref ELLIPSIS_CC which is used as the real C compiler after
## preprocessing.
##
##@{

## @brief Command used to dump files to stdout
CAT=${CAT:-cat}
## @brief Command used to copy files
CP=${CP:-cp}
## @brief Echos its arguments to stdout
ECHO=${ECHO:-echo}
## @brief Used to create a temporay directory that is destroyed on exit
MKTEMP=${MKTEMP:-mktemp}
## @brief Delete files, should accept `-rf` to forcably delete directories recursively
RM=${RM:-rm}
## @brief The `sed` <b>S</b>tream <b>ED</b>itor as it comes with POSIX since the epoch
SED=${SED:-sed}
## @brief The `sort` utility as it comes with POSIX since the epoch, should understand the `-u` option
SORT=${SORT:-sort}
## @brief The top level directory used for temporary files.
##
## For big compilation jobs it could be a good idea to have this in a
## file system that is not backed by a physical disk but only
## represented in memory.
TMP=${TMP:-/tmp}

ELLIPSIS_CC=${ELLIPSIS_CC:-cc}
ELLIPSIS_CC=`which ${ELLIPSIS_CC}`
ELLIPSIS_CC=`realpath ${ELLIPSIS_CC}`

## @brief Set to 1 to augment verbosity of the script
ELLIPSIS_VERBOSE=${ELLIPSIS_VERBOSE:-0}

##@}

## @name Tuning
## @brief Variables that are used to tune eĿlipsis
##
## You should generally not touch these.
##@{

export ELLIPSIS_ONE_PHASE=1
export ELLIPSIS_MARK_LINE="/*^*/%% __LINE__ __FILE__/*^*/"
export ELLIPSIS_MARK_LINE_START="/*^*/%% __LINE__ __FILE__/**/ 1/*^*/"
export ELLIPSIS_MARK_LINE_RETURN="/*^*/%% __LINE__ __FILE__/**/ 2/*^*/"
export ELLIPSIS_PRAGMA_FORWARD=1

##@}


## @cond PRIVATE

bindir="`dirname $0`"
script="`basename $0`"

if [ "${#ELLIPSIS}" -eq 0 ] ; then
    ELLIPSIS="${bindir}/ellipsis"
fi

com_plain () {
    "${ECHO}" "${0}: $*" 1>&2
}
level=1
com_bash () {
    "${ECHO}" "${0}:${FUNCNAME[${level}]}:${BASH_LINENO[$((${level}-1))]}: $*" 1>&2
}
com_nothing () {
    true
}

if [ "${ELLIPSIS_VERBOSE}" -gt 0 ] ; then
    if [ -z "${BASH_VERSION}" ] ; then
        alias complain=com_plain
    else
        shopt -s expand_aliases
        alias complain=com_bash
    fi
else
    alias complain=com_nothing
fi

assignInner () {
    eval "${listVar}='$*'"
}

append () {
    listVar="$1"
    shift
    eval "listCont=\${${listVar}}"
    assignInner ${listCont} "$*"
}

popInner () {
    local listVar="$2"
    eval "$1=$3"
    shift 3
    assignInner "$*"
}

pop () {
    eval "listCont=\${$2}"
    popInner $1 $2 ${listCont}
}

garbage () {
    append garbageBin $*
}

#### Program exit
# This function is called at the end of execution to clean up
# temporary files. It is installed on all exits that we might be able
# to capture.
cleanup () {
    # close stdout and stderr such that the filter can terminate
    exec 1>&-
    exec 2>&-
    if [ "${#ELLIPSIS_KEEP}" -eq 0 ] ; then
        ${RM} -rf ${garbageBin}
    fi
}

#### Signal handling
# This function is called in addition, if a signal has been caught. It
# prints the number of the signal and then runs `cleanup`.
killup () {
    sig="$?"
    complain "exiting on signal (${sig}), cleaning up"
    cleanup
    exit 1
}

trap cleanup EXIT
for sig in  HUP INT QUIT ILL ABRT BUS FPE SEGV PIPE TERM IO SYS ; do
    trap killup ${sig} || true
done

DUMPONLY="$#"

# we need some temporary
tmpd=$(${MKTEMP} -d "${TMP}/cmod-tmpd.XXXXXXXXXXXXXXXX")
if [ -z "${CMOD_KEEP}" ] ; then
    garbage "${tmpd}"
fi

LOG="${tmpd}/ellipsis-compilation-log.txt"

IN="${tmpd}/ellipsis-stdin.c"
export ELLIPSIS_PREDEFINED="${tmpd}/ellipsis_predefined.h"
MAC="${tmpd}/macros.h"
"${ECHO}" > "${IN}"

MM=
ONLY=0
STDIN=0

while [ "$#" -gt 0 ] ; do
    case "$1" in
        (*.c|*.h)
            append SOURCES "$1"
            name=`basename "$1" .c`
            append ISOURCES "${tmpd}/${name}.i"
            append ARGS "${tmpd}/${name}.i"
            shift
            ;;
        (-)
            append SOURCES "${IN}"
            append ISOURCES "${IN%.c}.i"
            append ARGS "${IN%.c}.i"
            STDIN=1
            shift
            ;;
        (-E)
            ONLY=1
            shift
            ;;
        (-MM)
            ONLY=1
            MM="${LOG}"
            shift
            ;;
        (-P)
            export ELLIPSIS_MARK_LINE=
            shift
            ;;
        (-CC|-C)
            complain "warning: $1 comand line option ignored"
            shift
            ;;
        (-o?*)
            OUTFILE="${1#-o}"
            append ARGS "$1"
            shift
            ;;
        (-o)
            append ARGS "$1"
            shift
            OUTFILE="$1"
            append ARGS "$1"
            shift
            ;;
        # options that may trigger the definition of a macro
        (-O*|-std=*|-ansi|-next-runtime|-Wstringop-overflow*|-f*|-fno-*|-m*)
            append ARGS "$1"
            append MARGS "$1"
            shift
            ;;
        # options with arguments that go to the preprocessor
        (-[DIU]?*)
            append ARGS "$1"
            append EARGS "$1"
            shift
            ;;
        (-[DIU])
            append ARGS "$1"
            append EARGS "$1"
            shift
            append ARGS "$1"
            append EARGS "$1"
            shift
            ;;
        (*)
            append ARGS "$1"
            shift
            ;;
    esac
done

attributes="${tmpd}/predefined_c_attribute_generate.h"

${SED} -n '
/\#ifdef __has_c_attribute/,/\#endif \/\*__has_c_attribute\*\//{
         # the image of the file in the executable will not start at a new line
         # remove all the garbage on that line
         /\#ifdef __has_c_attribute/{
                  i\#ifdef __has_c_attribute
                  d
         }
         p
}
/\#ifdef __is_identifier/,/\#endif \/\*__is_identifier\*\//{
         # the image of the file in the executable will not start at a new line
         # remove all the garbage on that line
         /\#ifdef __is_identifier/{
                  i\#ifdef __is_identifier
                  d
         }
         p
}
' ${ELLIPSIS} > "${attributes}"

# clang sometimes doesn't provide this value, and ellipsis doesn't
# know how to deal with `L'\0'` constants in preprocessor
# conditionals, yet. If so, we provide a macro that evaluates to the
# correct value, regardless of whether it is used in preprocessing or
# core language contexts.
cat <<HERE    >>"${attributes}"
#ifndef __WCHAR_MIN__
# if L'\0' - 1 > 0
typedef __typeof__(L'\0') __ellipsis_wchar_t;
#  define __WCHAR_MIN__	((__ellipsis_wchar_t)+0u)
# else
#  define __WCHAR_MIN__	(-__WCHAR_MAX__ - 1)
# endif
#endif
HERE

${ELLIPSIS_CC} ${MARGS} -E -dM "${attributes}"                 \
    | "${SED}" '/#define [a-zA-Z]/d ; s![\\]U0000203f!‿!g' > "${MAC}"

echo '#include_source <ellipsis-predefined.h>' > "${ELLIPSIS_PREDEFINED}"

${ELLIPSIS_CC} ${MARGS} -E -v ${IN} 2>&1                 \
    | "${SED}" -n '/#include <...> search starts here:/,/End of search list./ {
/#include <...> search starts here:/d
/End of search list./d
s![ ]*\(.*\)!#include_source <\1/>!
p
}' >>"${ELLIPSIS_PREDEFINED}"

"${CAT}" "${MAC}"                                                >>"${ELLIPSIS_PREDEFINED}"

# Work around the crazy __clang__ macro that cannot be used as a
# vendor prefix and add some feature test macros upfront in the exact
# form they appear in the clang stubs. We need this for the moment
# because clang and glibc race to define these macros.
"${CAT}" <<HERE                                                  >>"${ELLIPSIS_PREDEFINED}"
⎔ ifdef __clang__
⎔ xdefine __clang_replace__ __clang__
⎔ define __clang_vendor__ __WARNING__(the __clang__ macro should not be used as a attribute prefix)_Clang
⎔ define __clang_0 __clang_replace__
⎔ define __clang_1 __clang_vendor__
⎔ undef __clang__
⎔ define __clang__ __clang_(__ATTR_LEVEL__)
⎔ define __clang_(C) __CONCAT__(__clang_, C)
⎔ define BOOL_WIDTH   __BOOL_WIDTH__
⎔ define CHAR_WIDTH   CHAR_BIT
⎔ define SCHAR_WIDTH  CHAR_BIT
⎔ define UCHAR_WIDTH  CHAR_BIT
⎔ define USHRT_WIDTH  __SHRT_WIDTH__
⎔ define SHRT_WIDTH   __SHRT_WIDTH__
⎔ define UINT_WIDTH   __INT_WIDTH__
⎔ define INT_WIDTH    __INT_WIDTH__
⎔ define ULONG_WIDTH  __LONG_WIDTH__
⎔ define LONG_WIDTH   __LONG_WIDTH__
⎔ define ULLONG_WIDTH __LLONG_WIDTH__
⎔ define LLONG_WIDTH  __LLONG_WIDTH__
⎔ else
⎔ undef __is_identifier
⎔ undef __has_feature
⎔ undef __has_extension
⎔ endif
HERE


case "`${ELLIPSIS_CC} --version`" in
    (*clang*)
        complain "assuming clang"
        export __COMPN__=clang
        export __COMPV__=`"${SED}" -n '/__clang_major__/{ s/.* \([0-9]\{1,\}\)$/\1/ ; p }' "${MAC}"`
        ;;
    (*gcc*)
        complain "assuming gcc"
        export __COMPN__=gcc
        export __COMPV__=`"${SED}" -n '/__GNUC__/{ s/.* \([0-9]\{1,\}\)$/\1/ ; p }' "${MAC}"`
        ;;
    (*)
        complain "unknown compiler platform"
esac

ARCHS='aarch64\|arm\|generic\|i386\|loongarch64\|m68k\|microblaze\|mips\|mips64\|mipsn32\|or1k\|powerpc\|powerpc64\|riscv32\|riscv64\|s390x\|sh\|x32\|x86_64'

CPU=`"${SED}" -n "/[_]\{1,\}\(${ARCHS}\)[_]*/"'{ s/^[^ ]* *// ; p ; q }' "${MAC}"`

CPU="${CPU#_}"
CPU="${CPU#_}"
CPU="${CPU%% *}"
CPU="${CPU%_}"
CPU="${CPU%_}"

OS=`"${SED}" -n "/[_]\{2,\}\(linux\|GNU\|FreeBSD\|NetBSD\|OpenBSD\)[_]*\>/"'{ s/^[^ ]* *// ; p ; d }' "${MAC}"`

OS="${OS#_}"
OS="${OS#_}"
OS="${OS%% *}"
OS="${OS%_}"
OS="${OS%_}"

if [ "${OS}" = "GNU" ] ; then
    OS=hurd
fi

LIBC=`"${SED}" -n "/[_]\{2,\}\(gnu\|GLIBC\)[_]*/"'{ s/^[^ ]* *// ; p ; d }' "${MAC}"`

LIBC="${LIBC#_}"
LIBC="${LIBC#_}"
LIBC="${LIBC%% *}"
LIBC="${LIBC%_}"
LIBC="${LIBC%_}"

case "${LIBC}" in
    (gnu*)
        LIBC=gnu
        ;;
    (GLIBC*)
        LIBC=gnu
        ;;
    (*)
        LIBC=unknown
        ;;
esac

if [ "${DUMPONLY}" -eq 0 ] ; then
    TARGET="predefined-${__COMPN__}-${__COMPV__}-${CPU}-${OS}-${LIBC}.h"
    "${CP}" "${ELLIPSIS_PREDEFINED}" "${TARGET}"
    exit 0
fi

if [ "${#OUTFILE}" -gt 0 -a "${ONLY}" -gt 0 ] ; then
    case "${SOURCES}" in
        (*\ *)
            complain "outfile ${OUTFILE} specified with option -E with more than one source file: ${SOURCES}"
            exit 1
            ;;
    esac
fi

if [ "${#MM}" -gt 0 -a "${#OUTFILE}" -eq 0 ] ; then
    OUTFILE="/dev/null"
fi

complain "executing ${ELLIPSIS} with predefined includes and macros in ${ELLIPSIS_PREDEFINED}"

if [ "${STDIN}" -gt 0 ] ; then
    if [ "${ONLY}" -eq 0  -a "${#OUTFILE}" -eq 0 ] ; then
        complain "input from stdin but not output file specified, exiting"
        exit 1
    fi
    complain "${CAT} > ${IN}" || exit 1
    "${CAT}" > "${IN}"
fi

ERR=0
SRCS="${SOURCES}"
while [ "${#SRCS}" -gt 0 ] ; do
    pop SRC SRCS
    if [ ! -r "${SRC}" ] ; then
        complain "source ${SRC} is not readable, exiting"
        ERR=1
    fi
done
if [ "${ERR}" -gt 0 ] ; then
    exit "${ERR}"
fi

if [ "${ONLY}" -eq 0 ] ; then
    ISRCS="${ISOURCES}"
    while [ "${#SOURCES}" -gt 0 ] ; do
        pop SOURCE SOURCES
        pop ISRC ISRCS
        complain "${ELLIPSIS} ${EARGS} ${SOURCE} > ${ISRC}"
        "${ELLIPSIS}" ${EARGS} "${SOURCE}" > "${ISRC}" || exit 1
    done
    complain "${ELLIPSIS_CC} ${ARGS}" 1>&2
    "${ELLIPSIS_CC}" ${ARGS} 1>&2
else
    if [ "${#OUTFILE}" -eq 0 ] ; then
        while [ "${#SOURCES}" -gt 0 ] ; do
            pop SOURCE SOURCES
            complain "${ELLIPSIS} ${EARGS} ${SOURCE}"
            "${ELLIPSIS}" ${EARGS} "${SOURCE}" || exit 1
        done
    else
        if [ "${#MM}" -gt 0 ] ; then
            while [ "${#SOURCES}" -gt 0 ] ; do
                pop SOURCE SOURCES
                complain "${ELLIPSIS} -d ${EARGS} ${SOURCE} 2> ${MM} > ${OUTFILE}"
                "${ELLIPSIS}" ${EARGS} -d "${SOURCE}" 2> "${MM}" > "${OUTFILE}" || exit 1
                "${SED}" '/^dependency-.:/!d'  "${MM}"  \
                    | "${SORT}" -u                      \
                    | "${SED}" '
/dependency-1/{
        s!\([^:]*\)\.c\>\(.*\)!\1.o \1.s\2 \1.c!
}
s!^[^:]*:!!
$,$ ! { s!$!\t\\!g }'
            done
        else
            complain "${ELLIPSIS} ${EARGS} ${SOURCE} > ${OUTFILE}"
            "${ELLIPSIS}" ${EARGS} "${SOURCES}" > "${OUTFILE}" || exit 1
        fi
    fi
fi

## @endcond
