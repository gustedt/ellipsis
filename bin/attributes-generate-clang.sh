# receives the name of file containing the list of clang::something
# attributes

for e in `cat $1` ; do
    name="${e#clang::}"
    echo "# if __has_c_attribute(clang::${name})
#  define __has_c_attribute‿clang‿${name}	1
#  define __has_c_attribute‿__clang__‿${name}	1
#  define __has_c_attribute‿clang‿__${name}__	1
#  define __has_c_attribute‿__clang__‿__${name}__	1
#  define __has_c_attribute‿_Clang‿${name}	1
#  define __has_c_attribute‿_Clang‿__${name}__	1
# endif"
done
